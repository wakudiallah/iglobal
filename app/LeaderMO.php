<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class LeaderMO extends Model
{
    //
    
    protected $table = 'leader_mo';

    protected $fillable = ['leader_id','leader_name','user_id'];

    use SoftDeletes;

	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
