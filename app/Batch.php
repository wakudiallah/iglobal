<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Batch extends Model
{
    protected $table = 'batch';

    protected $guarded = ["id"]; 
    public $timestamps = true;


    //$pra = praapplication::where('uniq_no', $batch->uniq_id)->get();

    public function detail_pra() {
        return $this->hasMany('App\praapplication', 'uniq_id','uniq_no')->where('praapplication.stage','=','W17');    
    }

}
