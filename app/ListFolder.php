<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class ListFolder extends Model
{
    protected $table = 'list_folder';

    use SoftDeletes;

	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
