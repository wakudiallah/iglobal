<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class History extends Model
{
    
    protected $table = 'history';

    protected $fillable = ['name','cus_id', 'activity', 'remark_id', 'note', 'user_id' ];


	protected $guarded = ["id"]; 
	public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function remarks()
    {
        return $this->belongsTo('App\Remark', 'remark_id', 'id');
    }

    public function stage()
    {
        return $this->belongsTo('App\Stage', 'remark_id', 'id_stage' );
    }
}
