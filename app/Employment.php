<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employment extends Model
{
    protected $table = 'employment';

    protected $fillable = ['id','code_employment','name'];

    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates = ['deleted_at'];
    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
