<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Manager extends Model
{
    //`id`, `name`, `email`, `status`, `deleted_at`, `created_at`, `updated_at`

	protected $table = 'manager';

	protected $fillable = [
        'id', 'name', 'email', 'status'
    ];


    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates   = ['deleted_at'];
    public $timestamps = true;


    public function managerz() {
        return $this->belongsTo('App\User','id','manager'); //id =  table user == model_id = table model has roles
    }

}
