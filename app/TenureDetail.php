<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TenureDetail extends Model
{
   protected $table = 'tenure_detail';

   public function loan(){
   		return $this->belongsTo('App\LoanDetail','id_loan','id');
   }
}
