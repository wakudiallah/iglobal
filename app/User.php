<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;


class User extends Authenticatable
{
    use Notifiable, HasRoles;

    protected $fillable = [
        'name', 'email', 'password', 'status', 'status_head', 'leader', 'name_leader', 'profile', 'name_manager','manager' 
    ];


    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates   = ['deleted_at'];
    public $timestamps = true;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function messages()
    {
        return $this->hasMany(Messsage::class);
    }

    public function homeimage()
    {
        return $this->hasMany(Homeimage::class);
    }

    public function package()
    {
        return $this->hasMany(Package::class);
    }

    public function employment()
    {
        return $this->hasMany(Employment::class);
    }

    public function loanpkg()
    {
        return $this->hasMany(Loanpkg::class);
    }

    public function comm()
    {
        return $this->hasMany(Comm::class);
    }

    public function emp()
    {
        return $this->hasMany(Emp::class);
    }

    public function announc()
    {
        return $this->hasMany(Announcement::class);
    }

    public function remark()
    {
        return $this->hasMany(Remark::class);
    }

    public function history()
    {
        return $this->hasMany(History::class);
    }

    public function stage()
    {
        return $this->hasMany(Stage::class);
    }

    public function filemanager()
    {
        return $this->hasMany(FileManager::class);
    }

    public function model() {
        return $this->belongsTo('App\Model_has_role','id','model_id'); //id =  table user == model_id = table model has roles
    }

      public function MO() {
        return $this->belongsTo('App\praaplication','id','assessment'); //id =  table user == model_id = table model has roles
    }

    public function Managerss() {
        return $this->belongsTo('App\user','id','manager'); //id =  table user == model_id = table model has roles
    }

    public function managernya() {
        return $this->belongsTo('App\Manager','manager','id'); //id =  table user == model_id = table model has roles
    }

    public function nama_manager() {
        return $this->belongsTo('App\user','manager','id'); //id =  table user == model_id = table model has roles
    }

    public function nama_leader() {
        return $this->belongsTo('App\user','leader','id'); //id =  table user == model_id = table model has roles
    }
    

}
