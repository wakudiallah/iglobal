<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Announcement;
use App\Role;
use App\Permission;
use App\Authorizable;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;



class AnnounController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
            
        $announc = Announcement::latest()->get();;

        return view('adminpage.announcement.index', compact('announc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $roles = Role::pluck('name', 'id');

        return view('adminpage.announcement.new', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
            'title'   => 'required|max:200',
            'desc'    => 'required|max:2000',
            'attach'  => 'nullable|max:2025',
            'role_id' => 'nullable',
            'act'     => 'required'
        ]);
 
        
        if (empty($request->file('attach'))){
            $user = Auth::user()->id;

            $data          = new Announcement;
            
            $data->title   = $request->input('title');
            $data->desc    = $request->input('desc');
            $data->act     = $request->input('act');
            $data->attach  = "NULL";
            $data->user_id = $user;

            $data->save();
        }
        else{

            $data = $request->all();
     
            $image = $request->file('attach')->getClientOriginalName();
            $destination = base_path() . '/public/attach';
            $request->file('attach')->move($destination, $image);
     
            $data['attach'] = $image;

            $request->user()->announc()->create($data);
        }
        
        return redirect('announc')->with(['success' => 'Announcement data successfully Saved']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $announc = Announcement::where('id',$id)->first();  
        $roles = Role::all(); 

        return view('adminpage.announcement.edit', compact('announc','roles'));
    }


    public function view_announc($id,$view=FALSE)
    {            
        $announc =  Announcement::latest('id')->where('id','=',$id)->where('act', '=', 1)->first();
        $other = Announcement::latest('id')->where('id','<>',$id)->where('act', '=', 1)->paginate(5);

        return view('adminpage.announcement.show', compact('announc','other'));
    }

    public function data($id) //dak jadi yang ini
    {
        
        $announc =  Announcement::latest('id')->limit('1')->first(); 

        return view('adminpage.announcement.embed', compact('announc'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $user = Auth::user()->id;

        $data = Announcement::findOrFail($id);
        $data->title = $request->input('title');
        $data->desc = $request->input('desc');
        $data->act = $request->input('act');
        $data->user_id = $user;

        if (empty($request->file('attach'))){
            $data->attach = $data->attach;
        }
        else{
            $image = $request->file('attach')->getClientOriginalName();
            $destination = base_path() . '/public/attach';
            $request->file('attach')->move($destination, $image);
     
            $data['attach'] = $image;
        }
        $data->save();
        
        return redirect('announc')->with(['update' => 'Announcement data successfully updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $emp = Announcement::findOrFail($id)->delete();   
                        
        return redirect('announc')->with(['delete' => 'Announcement data successfully deleted']);

        
    }

    public function update1($id)
    {
        $st = Announcement::find($id);
        $st->act = '1';
        $st->save();
                        
        return redirect('announc')->with(['update' => 'Announcement data successfully updated']);
 
    }

    public function update2(Request $request , $id)
    {
        $st = Announcement::find($id);
        $st->act = '0';
        $st->save();

        return redirect('announc')->with(['update' => 'Announcement data successfully updated']);
    }

    
    
}
