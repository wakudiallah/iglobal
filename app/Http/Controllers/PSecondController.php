<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\praapplication;
use App\Authorizable;
use Illuminate\Support\Facades\Auth;
use App\Package;
use App\Loanpkg;
use App\Employment;
use App\Emp;
use Ramsey\Uuid\Uuid;
use App\DocCust;
use App\DocAssest;
use App\User;
use App\History;
use App\Sendmbsb;
use App\LoanDetail;
use App\LoanDetails;
use App\Tenures;
use App\TenureDetail;
use App\MeetCust;
use Mail;
use App\Model_has_role;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Carbon\Carbon;
use Disk;
use App\Loanammount;
use App\AddInfo;
use DateTime;
use Response;
use Session;
use PDF;
use App\Batch;
use Redirect;

class PSecondController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }

    
    public function loan_internal_cal_perone($id)
    {
        $user = Auth::user()->id;

        $internal_cal = praapplication::where('stage', 'W4' )->where('id_cus', $id )->orderBy('created_at', 'desc')->get(); 

        return view('processor2.internal_calculation', compact('internal_cal'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function call_103()
    {
        $pending = praapplication::where('stage', 'W10' )->orderBy('created_at', 'desc')->get(); 

        return view('processor2.call_103', compact('pending'));  
    }


    public function doc_check()
    {
        $pending = praapplication::where('stage', 'W9' )->orderBy('created_at', 'desc')->get(); 

        return view('processor2.doc_check', compact('pending'));  
    }


    public function doc_incomplete()
    {
        $docin = praapplication::where('stage', 'W7' )->orderBy('created_at', 'desc')->get(); 
        $user = Auth::user()->id;

        
        return view('processor2.doc_incomplete', compact('docin'));  
    }

    public function detail($id)
    {
        $docin = praapplication::where('stage', 'W7' )->orderBy('created_at', 'desc')->get(); 
        $user = Auth::user()->id;

        $step1        = praapplication::where('id_cus', $id)->first();

        $info         = AddInfo::where('cus_id', $id)->latest('updated_at')->limit('1')->first();

        $existing = MeetCust::where('cus_id', $id)->latest('created_at')->limit('1')->first();


        $approval = praapplication::where('id_cus', $id)->limit('1')->first();
        $history = History::where('cus_id', $id)->latest('id')->first();
        $loanammount = LoanAmmount::where('id_praapplication',$id)->first();
        $approval = praapplication::where('id_cus', $id)->get();
        
        return view('processor2.view_detail', compact('docin', 'step1', 'approval', 'history', 'info', 'existing', 'loanammount', 'approval'));  
    }


    public function updates(Request $request, $id)
    {
        $loanammount = $request->input('jml_pem');
        $praapplication = praapplication::where('id_cus', $id)->update(['jml_pem' => $loanammount]);

        $LoanAmmount = LoanAmmount::where('id_praapplication', $id)->update(['loanammount' => $loanammount]);

        
         return redirect('/recount/loan/'.$id)->with('message', 'Sila Pilih Tempoh Pembiayaan Anda');
    }

    
    public function submission_list(Request $request)
    {
        $user = Auth::user()->id;

        $item = array_map(null, $request->id, $request->ids, $request->sta);
 

            $hariini  = date('ymd');
       

            $bc = Batch::latest('created_at')->limit('1')->first();

            if(empty($bc->uniq_id)){
                $l = 'B000-181003';                
            }
            else{
                $l = $bc->uniq_id;
            }
            
            $letter = substr($l,0,1);
            $nu = substr($l,1,3);

            if($nu == 999){
                $letter++;
                $nu=1;
            }
            else{
                $nu++;
            }
            
            $cc=  $letter.str_pad($nu,3,'0',STR_PAD_LEFT).$hariini;
            $pra = praapplication::select('uniq_no')->get();
            
            $today  = date('Y-m-d H:i:s');
            $format = date('Ymd-Hi');
            $number = 'B'.$format;

            $validate = Batch::latest('created_at')->where('uniq_id',$cc)->count();

            if($validate==0){
                $batch          = new Batch;
                $batch->uniq_id = $cc;
                $batch->date    = $today;
                $batch->save();

            foreach($item as $val) {
                
                $pra = praapplication::Where('id',$val[0])->update([
                    
                    "uniq_no"   =>$cc,
                    "stage"  => 'W17'
                    
                ]);
            }
            }


            //Dia Download
        $current = date('Y-m-d H:i:s');
        $excel = Excel::create('Submission'.$current, function($excel) use($cc, $item) {

            $excel->setTitle('My awesome report 2018');

            // Chain the setters
            $excel->setCreator('Global')->setCompany('Global');

            $excel->sheet('Sheet 1', function($sheet) use($cc, $item) {

            $submission = praapplication::join('emp', 'emp.id', '=', 'praapplication.emp_code')
                ->select(
                  'praapplication.updated_at', 
                  'praapplication.name', 
                  'praapplication.ic',
                  'praapplication.email',
                  'emp.Emp_Desc')
                ->orderBy('praapplication.created_at', 'desc')->where('praapplication.stage', '=', 'W17')->where('uniq_no',$cc)
                ->get();

                $i = 1;
                    foreach($submission as $product) {
                     $data[] = array(
                        $i++,
                        $product->updated_at->toFormattedDateString(),
                        $product->name,
                        $product->ic,
                        $product->email,
                        $product->Emp_Desc,
                    );
                }

                $sheet->fromArray($data, null, 'A1', false, false);
                    $ir = $i-1;
                    $is = $i+1;
                  $range = "A1:F{$ir}";
                $sheet->setBorder($range, 'thin');
                $headings = array('No', 'Date','Customers Name','New IC', 'Email', 'Company');
                 $sheet->setBorder( "A{$is}:F{$is}" ,'none');
                //$sheet->setBorder('A2:A10');
                $sheet->prependRow(1, $headings);
                // add header
                $current = date('Y-m-d ');
                    $sheet->prependRow(1, ["Batch Header".$cc]);

                    //$sheet->prependRow(2, ["MASTER AGENT : GLOBAL I EXCEED MANAGEMENT SDN BHD"]);

                    $sheet->mergeCells("A1:F1");

                    $sheet->cell('A1', function($cell) {
                        // change header color
                      
                        $cell->setBackground('#ffffff')
                            ->setFontColor('#000000')
                            ->setFontWeight('bold')
                            ->setAlignment('center')
                            ->setValignment('center');
                          // ->setBorder('none', 'none', 'thin', 'thin');
                    });

                    $footerRow = count($submission) + 3;
                    $sheet->appendRow([
                        'Name : '  
                    ]);
                    $sheet->appendRow([
                        'Date / Time : '  
                    ]);
                    $sheet->mergeCells("F{$footerRow}:G{$footerRow}");
                    $sheet->cell("F{$footerRow}", function($cell) {
                        $cell->setAlignment('center')
                            ->setValignment('center')
                            ->setFontColor('#000000')
                            ->setFontSize(10);
                    });
                });
            
            })->download('xlsx');

    
        Session::flash('download.in.the.next.request', 'filetodownload.pdf');

        return Redirect::to('/batch-to-mbsb-process-2')->with('message', 'Submission list created successfully');
        
        
        //return redirect('/batch-to-mbsb-process-2')->with('message', 'Submission list created successfully');
    }


    public function get_submission_list()
    {
        $batch = Batch::get();
        //$pra = praapplication::where('uniq_id', $batch->uniq_no)->get();

        return view('processor2.get_submission_list', compact('batch', 'pra'));    
    }

    

    public function pending_doc_pro2()
    {
        $pending = praapplication::where('stage', 'W3' )->orderBy('created_at', 'desc')->get(); 

        return view('processor2.loan_calculation_menu', compact('pending'));    
    }

    public function loan_cal($id)
    {
        $package    = Package::all();
        $employment = Employment::all();
        $loanpkg    = Loanpkg::all();
        $emp        = Emp::all();
        $pra =      Praapplication::where('id_cus', $id)->first(); 

        
        return view('processor2.loan_calculation_p2', compact('package', 'employment', 'loanpkg', 'emp', 'pra'));
    }

    public function post_loan_cal_p2(Request $request, $id)
    {
        $user = Auth::user()->id;
        $id_pra = $request->input('id_praapplication');

        $routeback = $request->input('process5');
        
        $stagep6     = $request->input('remarkp6');

        $notep6      = $request->input('notep6');

        if($request->input('remarkp6') == 'W6' ){  //back mo

            $id_pra = $request->input('id_praapplication');

            praapplication::where('id_cus', $id)->update(array('routeto' => $routeback, 'stage' => $request->input('remarkp6'), 'process6' =>  $user, 'remark6' =>  $stagep6));
        
            $request                  = new History;

            $request->cus_id          = $id;  
            $request->activity        = "";
            $request->remark_id       = $stagep6;
            $request->user_id         = $user;
            $request->note            = $notep6;
            $request->save();

            return redirect('adminnew')->with(['update' => 'Data saved successfully']);
        }

        elseif($request->input('remarkp6') == 'W9'){ //next
            
            $id_pra = $request->input('id_praapplication');

            praapplication::where('id_cus', $id_pra)->update(array( 'stage' => $stagep6, 'process6' =>  $user, 'remark6' =>  $stagep6));
        
            $request                  = new History;

            $request->cus_id          = $id;  
            $request->activity        = "";
            $request->remark_id       = $stagep6;
            $request->user_id         = $user;
            $request->note            = "";
            $request->save();
        }

        return redirect('loan-eli/'.$id_pra.'/step2')->with(['update' => 'Data saved successfully']);

    }
    
    public function cek_loan(Request $request, $id)
    { 
        $idx = $request->input('id_cus');

        $package    = Package::all();
        $employment = Employment::all();
        $loanpkg    = Loanpkg::all();
        $emp        = Emp::all();
        $pra        = Praapplication::where('id_cus', $id)->first(); 

        $step1        = Praapplication::where('id_cus', $id)->first();
        $process6 = praapplication::where('stage', 'W4')->orWhere('stage', 'W9')->orWhere('stage', 'W10')->orWhere('stage', 'W8')->orderBy('created_at', 'desc')->get();


        $pra          = Praapplication::where('id_cus', $id)->first();

        $id_type      = $pra->employment_code;
        $loanpkg_code = $pra->loanpkg_code;
        $total_salary = $pra->gaji_asas + $pra->elaun ;
        $zbasicsalary = $pra->gaji_asas + $pra->elaun;
        $zdeduction   = $pra->pot_bul;


        
        $loan = LoanDetails::where('emp_id', $id_type)  
              ->where('loanpkg_id',$loanpkg_code)->limit('1')->get();   //mencari minimal salary nya & dsr nya (rasio utang terhadap pendapatan)

          // return $total_salary;
          $id_loan= $loan->first()->id;  


        $salary_dsr = ($zbasicsalary * ($loan->first()->dsr / 100)) - $zdeduction; 
        
        


        $icnumber = $pra->first()->icnumber;
        $tanggal  = substr($icnumber,4, 2);
        $bulan    = substr($icnumber,2, 2);
        $tahun    = substr($icnumber,0, 2); 

        if($tahun > 30) {
            $tahun2 = "19".$tahun;
        }
        else {
             $tahun2 = "20".$tahun;
        }
       
        $lahir          = $tahun2.'-'.$bulan.'-'.$tanggal; 
        $lahir          =  date('Y-m-d', strtotime($lahir. ' - 16 month'));
        $oDateNow       = new DateTime();
        $oDateBirth     = new DateTime($lahir);
        $oDateIntervall = $oDateNow->diff($oDateBirth);

        $umur = 61 - $oDateIntervall->y;

        $tenure = Tenures::where('id_loan',$id_loan)->where('years','<', $umur)->where('years','!=','1')->get(); 



        return view('processor2.step1a_new', compact('package', 'employment', 'loan', 'emp', 'pra', 'loan', 'tenure', 'zbasicsalary', 'zdeduction', 'total_salary','id_loan', 'step1', 'process6','loanpkg'));



    }

    public function post_tenus_p2(Request $request, $id)
    { 
        
        $idx = $request->input('id_cus');

        $package    = Package::all();
        $employment = Employment::all();
        $loanpkg    = Loanpkg::all();
        $emp        = Emp::all();
        $pra        = Praapplication::where('id_cus', $id)->first(); 

        $step1        = Praapplication::where('id_cus', $id)->first();
        $process6 = praapplication::where('stage', 'W4')->orWhere('stage', 'W9')->orWhere('stage', 'W10')->orWhere('stage', 'W8')->orderBy('created_at', 'desc')->get();


        $pra          = Praapplication::where('id_cus', $id)->first();

        $id_type      = $pra->employment_code;
        $loanpkg_code = $pra->loanpkg_code;
        $total_salary = $pra->gaji_asas + $pra->elaun ;
        $zbasicsalary = $pra->gaji_asas + $pra->elaun;
        $zdeduction   = $pra->pot_bul;


        
        $loan = LoanDetails::where('emp_id', $id_type)  
              ->where('loanpkg_id',$loanpkg_code)->limit('1')->get();   //mencari minimal salary nya & dsr nya (rasio utang terhadap pendapatan)

          // return $total_salary;
          $id_loan= $loan->first()->id;  


        $salary_dsr = ($zbasicsalary * ($loan->first()->dsr / 100)) - $zdeduction; 
        
        


        $icnumber = $pra->first()->icnumber;
        $tanggal  = substr($icnumber,4, 2);
        $bulan    = substr($icnumber,2, 2);
        $tahun    = substr($icnumber,0, 2); 

        if($tahun > 30) {
            $tahun2 = "19".$tahun;
        }
        else {
             $tahun2 = "20".$tahun;
        }
       
        $lahir          = $tahun2.'-'.$bulan.'-'.$tanggal; 
        $lahir          =  date('Y-m-d', strtotime($lahir. ' - 16 month'));
        $oDateNow       = new DateTime();
        $oDateBirth     = new DateTime($lahir);
        $oDateIntervall = $oDateNow->diff($oDateBirth);

        $umur = 61 - $oDateIntervall->y;

        $tenure = Tenures::where('id_loan',$id_loan)->where('years','<', $umur)->where('years','!=','1')->get(); 



        return view('processor2.step1a', compact('package', 'employment', 'loan', 'emp', 'pra', 'loan', 'tenure', 'zbasicsalary', 'zdeduction', 'total_salary','id_loan', 'step1', 'process6','loanpkg'));

        
    
    }

    
    public function step1($id)
    {
        $user = Auth::user()->id;

        $step1 = praapplication::where('id_cus',$id)->first();
         //$financial = Financial::where('user_id',$id)->first();
         $loanammount = LoanAmmount::where('id_praapplication',$id)->first();
         $pra =      Praapplication::where('id_cus', $id)->first(); 
         $package    = Package::all();
        $employment = Employment::all();
        $loanpkg    = Loanpkg::all();
        $emp        = Emp::all();
        $process6 = praapplication::where('stage', 'W4')->orWhere('stage', 'W9')->orWhere('stage', 'W10')->orWhere('stage', 'W8')->where('routeto', $user)->orderBy('created_at', 'desc')->get();

        return view('processor2.step1', compact('step1', 'financial', 'loanammount', 'emp', 'pra', 'package', 'employment', 'loanpkg', 'emp', 'process6'));
    }


    
    public function personal_financing(Request $request, $id)
    {
        
        $pra        = Praapplication::where('id_cus', $id)->first(); 
        $loanammount = LoanAmmount::where('id_praapplication',$id)->first();


        $pdf = PDF::loadView('adminpage.reporting.personal_financing_scheme', compact( 'loanammount', 'pra'));

        $pdf->setPaper('A4', 'landscape');
            
        return $pdf->stream('Personal Financing Scheme.pdf', array('Attachment'=>false));
    }

    public function store(Request $request)
    {
        //
    }

    public function all_check_loan($id)
    {
        $pending = praapplication::where('stage', 'W3' )->orderBy('created_at', 'desc')->get(); 

        return view('processor2.doc_check', compact('pending'));     
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function cek_tenus($id)
    {
        $pra  =   Praapplication::where('id_cus', $id)->first(); 
        $employment = Employment::all();
        $loanpkg    = Loanpkg::all();
        $emp        = Emp::all();

        return view('processor2.cektenus', compact('pra','employment','loanpkg','emp')); 
    }


    public function resulttenus(Request $request)
    {
        
        $employment = Employment::all();
        $loanpkg    = Loanpkg::all();
        $emp        = Emp::all();

        $id      = $request->input('id_cus');

        $icnumber      = $request->input('ic');
        $employment_code      = $request->input('Employment');
        $loanpkg_code      = $request->input('Package');
        $gaji_asas      = $request->input('gaji_asas');
        $elaun      = $request->input('elaun');
        $pot_bul      = $request->input('pot_bul');
        $jml_pem      = $request->input('jml_pem');

        
        $pra        = Praapplication::where('id_cus', $id)->first(); 
        $step1        = Praapplication::where('id_cus', $id)->first(); 

        $id_type      = $employment_code;
        $total_salary = $gaji_asas + $elaun ;
        $zbasicsalary = $gaji_asas + $elaun ;
        $zdeduction   = $pot_bul;

        $loan = LoanDetails::where('emp_id', $id_type)  
            ->where('loanpkg_id',$loanpkg_code)->limit('1')->get();   //mencari minimal salary nya & dsr nya (rasio utang terhadap pendapatan)

        // return $total_salary;
        $id_loan= $loan->first()->id;  
        //$id_loan= $loan->id;  

        $salary_dsr = ($zbasicsalary * ($loan->first()->dsr / 100)) - $zdeduction;

        $id_loan  = $loan->first()->id;  
        
        $icnumber = $pra->first()->icnumber;
        $tanggal  = substr($icnumber,4, 2);
        $bulan    = substr($icnumber,2, 2);
        $tahun    = substr($icnumber,0, 2); 

        if($tahun > 30) {
            $tahun2 = "19".$tahun;
        }
        else {
             $tahun2 = "20".$tahun;
        }
       
        $lahir          = $tahun2.'-'.$bulan.'-'.$tanggal; 
        $lahir          =  date('Y-m-d', strtotime($lahir. ' - 16 month'));
        $oDateNow       = new DateTime();
        $oDateBirth     = new DateTime($lahir);
        $oDateIntervall = $oDateNow->diff($oDateBirth);

        $umur = 61 - $oDateIntervall->y;

        $tenure = Tenures::where('id_loan',$id_loan)->where('years','<', $umur)->where('years','!=','1')->get();

        $today  = date('Y-m-d H:i:s');  //today 
        

        return view('processor2.step1a', compact('pra','employment','loanpkg','emp', 'step1', 'loan', 'zbasicsalary', 'zdeduction', 'total_salary', 'tenure','jml_pem', 'gaji_asas','elaun', 'pot_bul', 'jml_pem', 'loanpkg_code' )); 
    }

    



    public function cektenus_post(Request $request, $id)
    { 
        
        $id_cus    = $request->input('id_cus');

        //--------------- -----  Proceed ---------- */
        
        $basicsalary = $request->input('gaji_asas');
        $allowance   = $request->input('elaun');
        $deduction   = $request->input('pot_bul');
        $loanAmount  = $request->input('jml_pem');
        $package     = $request->input('Package');
        $employment_code     = $request->input('Employment');

        $gaji_tambah_tunjangan = $basicsalary + $allowance; 

        if($gaji_tambah_tunjangan <= 3500){  //3500 = had sementara


                   
            return redirect('tenos/custtenos/'.$id_cus)->with(['update' => 'Sorry you are not eligible to apply, your deduction or funding exceeds the limit']);
        }
        else{
            if(($loanAmount < 50000) || ($loanAmount > 250000)){
                return redirect('tenos/custtenos/'.$id_cus)->with(['update' => 'Loan Amount at least RM 50000 and less than RM 250000']);
            }
            else{
                  $pra          =      Praapplication::where('id_cus', $id_cus)->first(); 

        $id_type      = $employment_code;
        $total_salary = $basicsalary + $allowance ;
        $zbasicsalary = $basicsalary + $allowance;
        $zdeduction   = $deduction ;

        $loan = LoanDetails::where('emp_id', $id_type)  
        ->where('loanpkg_id',$package)->limit('1')->get();   //mencari minimal salary nya & dsr nya (rasio utang terhadap pendapatan)

        // return $total_salary;
        $id_loan= $loan->first()->id;  
        $salary_dsr = ($zbasicsalary * ($loan->first()->dsr / 100)) - $zdeduction;


        $icnumber = $pra->ic;
        $tanggal  = substr($icnumber,4, 2);
        $bulan    = substr($icnumber,2, 2);
        $tahun    = substr($icnumber,0, 2); 

        if($tahun > 30) {

            $tahun2 = "19".$tahun;
        }
        else {
             $tahun2 = "20".$tahun;
        }
        

        $lahir          = $tahun2.'-'.$bulan.'-'.$tanggal; 
        $lahir          =  date('Y-m-d', strtotime($lahir. ' - 16 month'));
        $oDateNow       = new DateTime();
        $oDateBirth     = new DateTime($lahir);
        $oDateIntervall = $oDateNow->diff($oDateBirth);

        $umur = 61 - $oDateIntervall->y;

        $durasix = 60 - $oDateIntervall->y;
        if( $durasix  > 10) { 
            $durasi = 10 ;
        } 
        else { 
            $durasi = $durasix ;
        }


        function pembulatan($uang) {
            $puluhan = substr($uang, -3);
                  if($puluhan<500) {
                    $akhir = $uang - $puluhan; 
                  } 
                  else {
                    $akhir = $uang - $puluhan;
                  }
                  return $akhir;
                }


        foreach($loan as $loan) {
            $salary_dsr = ($zbasicsalary * ($loan->dsr / 100)) - $zdeduction;
            $ndi        = ($zbasicsalary - $zdeduction) -  1300; 
            $max        =  $salary_dsr * 12 * 10 ;
                                           

            if(!empty($loan->max_byammount))  {
                  $ansuran = intval($salary_dsr)-1;
                    if($package == "1") {    //aku edit
                        $bunga = 3.8/100;
                    }
                    elseif($package == "2") { //aku edit
                        $bunga = 4.9/100;
                    }

                    else {
                        $bunga = 5.92/100;
                    }
                  $pinjaman = 0;

                  for ($i = 0; $i <= $loan->max_byammount; $i++) {
                      $bungapinjaman = $i  * $bunga * $durasi ;
                      $totalpinjaman = $i + $bungapinjaman ;
                      $durasitahun = $durasi * 12;
                      $ansuran2 = intval($totalpinjaman / ($durasi * 12))  ;
                      if ($ansuran2 < $ndi)
                      {
                          $pinjaman = $i;
                      }
                    
                  }   

                  if($pinjaman > 1) {                                              
                      $bulat = pembulatan($pinjaman);
                      $loanx =  number_format($bulat, 0 , ',' , ',' ) ; 
                      $loanz = $bulat;
                  }
                  else {
                      $loanx =  number_format($loan->max_byammount, 0 , ',' , ',' ) ; 
                      $loanz = $loan->max_byammount;
                  }

                }
                else { 
                    $bulat = pembulatan($loan->max_bysalary * $total_salary);
                    $loanx =  number_format($bulat, 0 , ',' , ',' ) ; 
                    $loanz = $bulat;
                    if ($loanz > 199000) {

                          $loanz  = 250000;
                          $loanx =  number_format($loanz, 0 , ',' , ',' ) ; 
                    }
                }
            }

        $tenure = Tenures::where('id_loan',$id_loan)->where('years','<', $umur)->where('years','!=','1')->get();  
         
         if( $pra->first()->loanamount <= $loanz ) {
            $ndi_limit=$loan->ndi_limit;
            foreach($tenure as $tenure) {
                $bunga2 =  $pra->first()->loanamount * $tenure->rate /100   ;
                $bunga = $bunga2 * $tenure->years;
                $total = $pra->first()->loanamount + $bunga ;
                $bulan = $tenure->years * 12 ;
                $installment =  $total / $bulan ;
                 $ndi_state = ($total_salary - $zdeduction) - $installment; 

                    $count_installment=0;
                 if($installment  <= $salary_dsr && $ndi_state>=$ndi_limit) {
                    $count_installment++;
                }

            }
         } else {
            $count_installment=0;
         }

        if($count_installment>0) {

            $emp    = $request->input('Employment');
            $today  = date('Y-m-d H:i:s');  //today

            if($emp == '1'){ //loanpkg_code 4 = mumtaz 
                praapplication::where('id_cus', $id_cus)->update(array( 'loanpkg_code' => '4','employment_code' => $request->input('Employment'), 'gaji_asas' => $request->input('gaji_asas'), 'elaun' => $request->input('elaun'), 'pot_bul' => $request->input('pot_bul'), 'jml_pem' => $request->input('jml_pem')));
            }
            elseif($emp == '2'){ //2 = afdhal
                praapplication::where('id_cus', $id_cus)->update(array( 'loanpkg_code' => '2','employment_code' => $request->input('Employment'), 'gaji_asas' => $request->input('gaji_asas'), 'elaun' => $request->input('elaun'), 'pot_bul' => $request->input('pot_bul'), 'jml_pem' => $request->input('jml_pem')));
            }elseif($emp == '3'){ //2 = afdhal
                praapplication::where('id_cus', $id_cus)->update(array( 'loanpkg_code' => '2', 'employment_code' => $request->input('Employment'), 'gaji_asas' => $request->input('gaji_asas'), 'elaun' => $request->input('elaun'), 'pot_bul' => $request->input('pot_bul'), 'jml_pem' => $request->input('jml_pem')));
            }elseif($emp == '4'){ //1 = private
                praapplication::where('id_cus', $id_cus)->update(array( 'loanpkg_code' => '1', 'employment_code' => $request->input('Employment'), 'gaji_asas' => $request->input('gaji_asas'), 'elaun' => $request->input('elaun'), 'pot_bul' => $request->input('pot_bul'), 'jml_pem' => $request->input('jml_pem')));
            }elseif($emp == '5'){ //2 = afdhal
                praapplication::where('id_cus', $id_cus)->update(array( 'loanpkg_code' => '2', 'employment_code' => $request->input('Employment'), 'gaji_asas' => $request->input('gaji_asas'), 'elaun' => $request->input('elaun'), 'pot_bul' => $request->input('pot_bul'), 'jml_pem' => $request->input('jml_pem')));
            }

        
            $document                          = new LoanAmmount;
            $document->id_praapplication       = $id_cus;
        
            $document->save();

          return redirect('resulttenus/'.$id_cus)->with(['update' => 'Congratulations, you are eligible to apply for up to RM '.$loanx]);
           


        } 

        else {
            $had2 = number_format($had, 0 , ',' , ',' ).'.00' ; 
            Session::flash('fullname', $fullname); 
            Session::flash('icnumber', $icnumber);
            Session::flash('phone', $phone); 
            Session::flash('basicsalary', $basicsalary); 
            Session::flash('allowance', $allowance);
            Session::flash('deduction', $deduction);
            Session::flash('loanAmount', $loanAmount);
            Session::flash('employer2', $employer2);
            Session::flash('employment', $employment);
            Session::flash('employment2', $employment2);
            Session::flash('package_name', $package_name);
            Session::flash('majikan', $majikan);

            Session::flash('hadpotongan', $basicsalary); 
            return redirect('/')->with(['update' => 'Sorry you are not eligible to apply, your deduction or funding exceeds the limit']);
        }       
            }
        }

         
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function batch_to_mbsb()
    {
        $mbsb = praapplication::where('stage', 'W8' )->orderBy('created_at', 'desc')->get(); 

        return view('processor2.batch_to_mbsb', compact('mbsb'));  

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
