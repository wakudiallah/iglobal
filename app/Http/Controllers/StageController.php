<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Authorizable;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Stage;

class StageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $stage = Stage::orderBy('id', 'desc')->get();

        return view('adminpage.setupmodule.stage.index', compact('stage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('adminpage.setupmodule.stage.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'id_stage' => 'required|max:4',
            'desc'      => 'required|max:200',
            'color'     => 'required|max:100'
        ]);

        $request->user()->stage()->create($request->all());
        
        return redirect()->route('stage.index')->with(['success' => 'Stage data successfully saved']);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $stage = Stage::where('id',$id)->first();  
        
        return view('adminpage.setupmodule.stage.edit', compact('stage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   
    public function update(Request $request, Stage $stage)
    {
        $this->validate($request, [
            'id_stage' => 'required|max:4',
            'desc'      => 'required|max:200',
            'color'     => 'required|max:100'
            
        ]);

        $me = $request->user();

        if( $me->hasRole('Admin') ) {
            $stage = Stage::findOrFail($stage->id);
        } else {
            $stage = $me->stage()->findOrFail($stage->id);
        }

        $stage->update($request->all());

        return redirect('stage')->with(['update' => 'Stage data successfully updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $stage = Stage::findOrFail($id)->delete();   
                        
        return redirect('stage')->with(['delete' => 'Stage data successfully deleted']);
    }
}
