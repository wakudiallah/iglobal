<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Manager;
use App\Permission;
use App\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    //use Authorizable;

    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        

        $result = User::latest()->where('email','<>', 'wakudiallah05@gmail.com')->orderBy('id', 'DESC')->paginate();
        
        /*$result = User::latest()
            ->with('users') //bring along details of the friend
            ->join('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')
            ->get(); // exclude extra details from friends table*/

            /*$result = User::join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
    ->select('users.id', 'contacts.phone', 'orders.price')
    ->where('model_has_roles.model_id', '<>', '')
    ->get();*/


        return view('adminpage.user.index', compact('result'));
    }

    public function status(Request $request , $id)
    {
        $st         = User::find($id);
        $st->status = $request->status;
        $st->save();

        return view('adminpage.user.index')->with(['update' => 'User data successfully updated']);
    }


    public function updatestatus(Request $request , $id)
    {
        $st         = User::find($id);
        $st->status = '1';
        $st->save();

        return redirect('users')->with(['update' => 'User data successfully updated']);
    }

    public function updatestatus1(Request $request , $id)
    {
        $st = User::find($id);
        $st->status = '0';
        $st->save();

        return redirect('users')->with(['update' => 'User data successfully updated']);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$roles = Role::pluck('name', 'id');
        $roles = Role::where('id', '!=', 3)->pluck('name', 'id');

        return view('adminpage.user.new', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'bail|required|min:2',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'roles' => 'required|min:1',
            'status' => 'required'
        ]);

        // hash password
        $request->merge(['password' => bcrypt($request->get('password'))]);

        // Create the user
        if ( $user = User::create($request->except('roles', 'permissions')) ) {

            $this->syncPermissions($request, $user);

            flash('User has been created.');

        } else {
            flash()->error('Unable to create user.');
        }

        return redirect()->route('users.index')->with(['success' => 'User data saved successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $user = User::find($id);
        $roles = Role::pluck('name', 'id');
        $permissions = Permission::all('name', 'id');

        return view('adminpage.user.edit', compact('user', 'roles', 'permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'bail|required|min:2',
            'email' => 'required|email|unique:users,email,' . $id,
            'roles' => 'required|min:1'
        ]);

        // Get the user
        $user = User::findOrFail($id);

        // Update user
        $user->fill($request->except('roles', 'permissions', 'password'));

        // check for password change
        if($request->get('password')) {
            $user->password = bcrypt($request->get('password'));
        }

        // Handle the user roles
        $this->syncPermissions($request, $user);

        $user->save();

        return redirect()->route('users.index')->with(['update' => 'User data successfully updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @internal param Request $request
     */
    public function destroy($id)
    {
        if ( Auth::user()->id == $id ) {
            flash()->warning('Deletion of currently logged in user is not allowed :(')->important();
            return redirect()->back();
        }

        if( User::findOrFail($id)->delete() ) {
            flash()->success('User has been deleted');
        } else {
            flash()->success('User not deleted');
        }

        return redirect('users')->with(['delete' => 'User data successfully deleted']);
    }

    /**
     * Sync roles and permissions
     *
     * @param Request $request
     * @param $user
     * @return string
     */
    private function syncPermissions(Request $request, $user)
    {
        // Get the submitted roles
        $roles = $request->get('roles', []);
        $permissions = $request->get('permissions', []);

        // Get the roles
        $roles = Role::find($roles);

        // check for current role changes
        if( ! $user->hasAllRoles( $roles ) ) {
            // reset all direct permissions for user
            $user->permissions()->sync([]);
        } else {
            // handle permissions
            $user->syncPermissions($permissions);
        }

        $user->syncRoles($roles);

        return $user;
    }
}
