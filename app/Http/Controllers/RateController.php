<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TenureDetail;
use App\Tenures;
use App\Package;
use App\Loanpkg;
use App\Employment;
use App\Emp;
use Auth;
use App\LoanDetail;
use App\LoanDetails;
class RateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $tenure = TenureDetail::get();
        $pkg    = Loanpkg::get();
         $emp        = Emp::all();
          $employment = Employment::all();
        return view('adminpage.setupmodule.rate.index',compact('tenure','pkg','employment', 'emp'));
    }

 public function view(Request $request)
    {
            
            $user = Auth::user()->id;

            $all_employer = Emp::all();
             $pkg    = Loanpkg::get();
          $employment = Employment::all();
           

            $pkgs  = $request->input('pkg');
            $emp  = $request->input('emp');

            $loan = LoanDetail::where('id_emp_type',$emp)
                ->where('id_loanpkg',$pkgs)->limit('1')->get();
            $id_loan= $loan->first()->id;  
            $tenure = TenureDetail::where('id_loan',$id_loan)->get(); 

              
    

           return view('adminpage.setupmodule.rate.index_view',compact('tenure','pkg','employment', 'emp'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function loan_detail(Request $request)
    {
      $tenure     = TenureDetail::get();
      $pkg        = Loanpkg::get();
      $emp        = Emp::all();
      $employment = Employment::all();
      $loan       = LoanDetails::all();
      $tenure     = Tenures::get();
          

        return view('adminpage.setupmodule.rate.loan',compact('tenure','pkg','employment', 'emp','loan','tenure'));
    }
     public function loan_detail_view(Request $request)
    {
        $user = Auth::user()->id;

        $all_employer   = Emp::all();
        $pkg            = Loanpkg::get();
        $employment     = Employment::all();
           
        $pkgs       = $request->input('pkg');
        $emp        = $request->input('emp');
        $dsr        = $request->input('dsr');
        $ndi_limit  = $request->input('ndi_limit');
        $max        = $request->input('max');

        $loan = LoanDetails::all();
              
        $validate           = LoanDetails::latest('created_at')->where('emp_id', $emp)->where('loanpkg_id', $pkgs)->count();
        
        if ($validate > 0) 
            {
             return redirect('loan_detail')->with('success', 'Package already exist');
            }
        else 
            {
                if($emp!=''){
                    $new                = new LoanDetails;
                    $new->emp_id        =  $emp;
                    $new->loanpkg_id    =  $pkgs;
                    $new->max_byammount =  $max;
                    $new->ndi_limit     =  $ndi_limit;
                    $new->dsr           =  $dsr;
                    $new->save();
                    return redirect('loan_detail')->with('success', 'Save successfully');

                    $loanpkg = Loanpkg::Where('id',$pkgs)->update([
                    "EmpType" => $emp
                    ]);
                }
            }
    }

    public function add_rate(Request $request)
    {
            
            $user = Auth::user()->id;

            $all_employer = Emp::all();
             $pkg    = Loanpkg::get();
          $employment = Employment::all();
           

            $pkgs  = $request->input('pkg');
            $emp  = $request->input('emp');

            $id_loan  = $request->input('id_loan');

            $years2  = $request->input('years2');
            $rate2  = $request->input('rate2');

            $years3  = $request->input('years3');
            $rate3  = $request->input('rate3');

            $years4  = $request->input('years4');
            $rate4  = $request->input('rate4');

            $years5  = $request->input('years5');
            $rate5  = $request->input('rate5');

            $years6  = $request->input('years6');
            $rate6  = $request->input('rate6');

            $years7  = $request->input('years7');
            $rate7  = $request->input('rate7');

            $years8  = $request->input('years8');
            $rate8  = $request->input('rate8');

            $years9  = $request->input('years9');
            $rate9  = $request->input('rate9');

            $years10  = $request->input('years10');
            $rate10  = $request->input('rate10');

            $id_tenure10  = $request->input('id_tenure10');
            $id_tenure2  = $request->input('id_tenure2');
            $id_tenure3  = $request->input('id_tenure3');
            $id_tenure4  = $request->input('id_tenure4');
            $id_tenure5  = $request->input('id_tenure5');
            $id_tenure6  = $request->input('id_tenure6');
            $id_tenure7  = $request->input('id_tenure7');
            $id_tenure8  = $request->input('id_tenure8');
            $id_tenure9  = $request->input('id_tenure9');

            $loan = LoanDetails::all();
            $ten = Tenures::where('id_loan',$id_loan)->count();

            if($ten==0){
                
             //for ($i=2; $i<$=10;$i++){
                if($years =2){
                    $tenure = new Tenures;
                    $tenure->years = $years2;
                    $tenure->rate = $rate2;
                    $tenure->id_loan = $id_loan;
                    $tenure->loan_details_id = $id_loan;
                    $tenure->save();
                }
                if($years =3){
                    $tenure = new Tenures;
                    $tenure->years = $years3;
                    $tenure->rate = $rate3;
                    $tenure->id_loan = $id_loan;
                    $tenure->loan_details_id = $id_loan;
                    $tenure->save();
                }
                if($years =4){
                    $tenure = new Tenures;
                    $tenure->years = $years4;
                    $tenure->rate = $rate4;
                    $tenure->id_loan = $id_loan;
                    $tenure->loan_details_id = $id_loan;
                    $tenure->save();
                }
                if($years =5){
                    $tenure = new Tenures;
                    $tenure->years = $years5;
                    $tenure->rate = $rate5;
                    $tenure->id_loan = $id_loan;
                    $tenure->loan_details_id = $id_loan;
                    $tenure->save();
                }
                if($years =6){
                    $tenure = new Tenures;
                    $tenure->years = $years6;
                    $tenure->rate = $rate6;
                    $tenure->id_loan = $id_loan;
                    $tenure->loan_details_id = $id_loan;
                    $tenure->save();
                }
                if($years =7){
                    $tenure = new Tenures;
                    $tenure->years = $years7;
                    $tenure->rate = $rate7;
                    $tenure->id_loan = $id_loan;
                    $tenure->loan_details_id = $id_loan;
                    $tenure->save();
                }
                if($years =8){
                    $tenure = new Tenures;
                    $tenure->years = $years8;
                    $tenure->rate = $rate8;
                    $tenure->id_loan = $id_loan;
                    $tenure->loan_details_id = $id_loan;
                    $tenure->save();
                }
                if($years =9){
                    $tenure = new Tenures;
                    $tenure->years = $years9;
                    $tenure->rate = $rate9;
                    $tenure->id_loan = $id_loan;
                    $tenure->loan_details_id = $id_loan;
                    $tenure->save();
                }
                if($years =10){
                    $tenure = new Tenures;
                    $tenure->years = $years10;
                    $tenure->rate = $rate10;
                    $tenure->id_loan = $id_loan;
                    $tenure->loan_details_id = $id_loan;
                    $tenure->save();
                }
            }
            else{
                if($years =2){
                     $tenure = Tenures::Where('id',$id_tenure2)->update([
                    "rate" => $rate2
                    ]);
                }

                if($years =3){
                     $tenure = Tenures::Where('id',$id_tenure3)->update([
                    "rate" => $rate3
                    ]);
                }

                if($years =4){
                     $tenure = Tenures::Where('id',$id_tenure4)->update([
                    "rate" => $rate4
                    ]);
                }

                if($years =5){
                     $tenure = Tenures::Where('id',$id_tenure5)->update([
                    "rate" => $rate5
                    ]);
                }

                if($years =6){
                     $tenure = Tenures::Where('id',$id_tenure6)->update([
                    "rate" => $rate6
                    ]);
                }

                if($years =7){
                     $tenure = Tenures::Where('id',$id_tenure7)->update([
                    "rate" => $rate7
                    ]);
                }

                if($years =10){
                     $tenure = Tenures::Where('id',$id_tenure10)->update([
                    "rate" => $rate10
                    ]);
                }

                if($years =8){
                     $tenure = Tenures::Where('id',$id_tenure8)->update([
                    "rate" => $rate8
                    ]);
                }

                if($years =9){
                     $tenure = Tenures::Where('id',$id_tenure9)->update([
                    "rate" => $rate9
                    ]);
                }

            }

               
            
            return redirect('loan_detail')->with('success', 'Success');
   
          
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
