<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Authorizable;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user()->id;
        $login    =  User::where('id',$user)->first();
        $pra    =  User::latest('id')->where('id','=',$user)->first();

        return view('adminpage.profile', compact('login','pra'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }


    public function store_profile(Request $request, $id)
    {
        $user = Auth::user()->id;
        //documents\user_doc\240593
        //$id      = $request->input('id');
        $id_cus = $request->input('id_cus');
        $destinationPath = 'admin/images/profile';
        $name      = $request->input('name');

        if($request->hasFile('fileToUpload')) {
            $file = $request->file('fileToUpload');
            $extension = $file->getClientOriginalExtension();
            $file_name = 'profile-'. $id. '.' . $extension;
            
            $file->move($destinationPath, $file_name );
        }
        
            $upload_file = $file_name;

            $st = User::find($id);  
            $st->profile = $upload_file;
            $st->save();

        
        return redirect('/profile1')->with(['update' => 'Data saved successfully']);

    }

    public function store_background(Request $request, $id)
    {
        $user = Auth::user()->id;
        //documents\user_doc\240593
        //$id      = $request->input('id');
        $id_cus = $request->input('id_cus');
        $destinationPath = 'admin/images/';
        $name      = $request->input('name');

        if($request->hasFile('fileToUpload1')) {
            $file = $request->file('fileToUpload1');
            $extension = $file->getClientOriginalExtension();
            $file_name = 'bc-'. $id. '.' . $extension;
            
            $file->move($destinationPath, $file_name );
        }
        
            $upload_file = $file_name;

            $st = User::find($id);  
            $st->bc = $upload_file;
            $st->save();

        
        return redirect('profile1')->with(['update' => 'Data saved successfully']);

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = Auth::user()->id;

        $this->validate($request, [
            'profile' => ['mimes:jpg,jpeg,JPEG,png,gif,bmp']
        ]);
 
 
        $data = $request->all();
 
        $image = $request->file('profile')->getClientOriginalName();
        $destination = base_path() . '/public/uploads';
        $request->file('profile')->move($destination, $image);
 
        $data['profile'] = $image;
 
        //Homeimage::create($data);

        $request->user()->homeimage()->create($data);

        //$request->user()->create($data);

        //return redirect('profile')->with(['success' => 'Home Image berjaya di tambah']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
