<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\praapplication;
use Illuminate\Support\Facades\Auth;
use App\Announcement;
use App\AnnouncementGroup;
use App\Emp;
use App\DocCust;
use DB;
use Mapper;
use DateTime;
use PDF;
use Hash;
use App\History;
use App\Remark;
use App\User;
use App\Role;
use App\Model_has_role;
use App\DocAssest;

class AdminNewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
      public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
          

         $users = Auth::user();
         $user = $users->id;
  
        
        //Admin  
        $tasklist_admin        = praapplication::wherein('stage', ['W0','W1','W2' ,'W3','W4','W5','W6','W7','W8','W9','W10','W11','W12','W13','W14'])->orderBy('created_at', 'desc')->get();
        $count_admin           = praapplication::count();

        //Processor3 Dashboard
        $tasklist_p3                   = praapplication::orderBy('created_at', 'desc')->wherein('stage', ['W17', 'W13', 'W11', 'W12', 'W16'])->get();
        $count_mbsb_processing_p3      = praapplication::where('stage', 'W8')->count();
        $count_mbsb_processing_app     = praapplication::where('stage', 'W11')->count();
        $count_mbsb_processing_reject  = praapplication::where('stage', 'W12')->count();
        $count_mbsb_processing_add_doc = praapplication::where('stage', 'W13')->count();



        //MO DASHBOARD
        $count_submission    = praapplication::where('process2', $user)->where('stage', 'W0')->count();
        $count_recommend_new_sub    = praapplication::where('process2', $user)->where('stage', 'W15')->count();
        $count_doc_incomplete    = praapplication::where('process2', $user)->where('stage', 'W7')->count();
        $count_doc_reject    = praapplication::where('process2', $user)->where('stage', 'W13')->count();
        $count_add_doc    = praapplication::where('process2', $user)->where('stage', 'W13')->count();
        $count_pending_doc   = praapplication::where('process2', $user)->where('stage', 'W3')->count();
        $countmo_calculation = praapplication::where('process2', $user)->where('stage', 'W2')->count();
        $count_disbursement = praapplication::where('process2', $user)->where('stage', 'W11')->count();
        $countmo_calculation_n_change = praapplication::where('process2', $user)->where('stage', 'W6')->count();
        //$tasklist_mo         = praapplication::where('process2', $user)->wherein('stage', ['W0','W1','W2','W3','W6','W7','W13', 'W14', 'W15', 'W11', 'W12', 'W17'])->orderBy('updated_at', 'Desc')->get();

        $tasklist_mo         = praapplication::where('process2', $user)->wherein('stage', ['W0','W1','W2','W3','W6','W7','W13', 'W14', 'W15', 'W11', 'W12', 'W17', 'W19'])->where( DB::raw('MONTH(created_at)'), '=', date('n') )->get();
        

        //Team Lead
        $tasklist_tl = praapplication::where('process2', $user)->wherein('stage', ['W0', 'W13', 'W11', 'W12', 'W16', 'W19'])->orderBy('created_at', 'desc')->get();
        $count_submission_tl    = praapplication::where('team_lead', $user)->count();
        $count_approved_tl    = praapplication::where('team_lead', $user)->where('stage', 'W11')->count();
        $count_rejected_tl    = praapplication::where('team_lead', $user)->where('stage', 'W12')->count(); 


        

        //manager dashboard
        $count_failed_mo = praapplication::where('manager', $user)->where('stage', 'W5')->orderBy('created_at', 'desc')->count();
        $count_rejected = praapplication::where('manager', $user)->where('stage', 'W12')->orderBy('created_at', 'desc')->count();
        $count_approved = praapplication::where('manager', $user)->where('stage', 'W11')->orderBy('created_at', 'desc')->count();

        $countsub_manager       = praapplication::where('manager', $user)->where('stage', '!=' , 'W5')->where('stage', '!=' , 'W12')->count();

        $tasklist_manager        = praapplication::where('manager', $user)->wherein('stage', ['W5', 'W12', 'W11'])->orderBy('created_at', 'desc')->get();
        
       
       //Admin 1
       $count_loan_online = praapplication::where('stage', 'ON1')->orderBy('created_at', 'desc')->count();

       $managernya          = Auth::user()->manager; 

        $count         = praapplication::count();
        $countsub      = praapplication::where('submission', 0)->count();
        $praaplication = praapplication::orderBy('created_at', 'desc')->get();
        $announc       = Announcement::latest('id')->where('act', '1')->limit('1')->first();
        $announcgroup  = AnnouncementGroup::latest('id')->where('status', '1')->where('group_id', $managernya)->limit('1')->first();

        $role = Model_has_role::all();

        $users          = Auth::user();        
        $roless         = $users->model->role->id;

        $praaplication = praapplication::orderBy('created_at', 'desc')->get();


        //Processor 1
        $countw0                = praapplication::where('stage', 'W0')->count();
        $countp1newapp          = praapplication::where('routeto', $user)->where('stage', 'W14')->count();
        $countp1sendmbsb        = praapplication::where('stage', 'W1')->count();
        $countp1upload_spekar   = praapplication::where('stage', 'W1')->whereNULL('spekar')->count();
        $count_pen_mo           = praapplication::where('stage', 'W1')->where('spekar', '!=','')->count();
        $processor1             = praapplication::orderBy('created_at', 'desc')->where('stage', '=', 'W0')->get();
        $mbsb                   = DocAssest::orderBy('created_at', 'desc')->get();
        $tasklist_p1            = praapplication::wherein('stage', ['W0', 'W1'])->orderBy('created_at', 'desc')->get();
         

        //processor2
        $tasklist_p2          = praapplication::wherein('stage', ['W3','W7','W8','W9','W10', 'W17'])->orderBy('created_at', 'desc')->get();
        $pending_doc_p2     = praapplication::where('stage', 'W3')->count();
        $mbsb_processing    = praapplication::where('stage', 'W4')->count();
        $pen_doc_check      = praapplication::where('stage', 'W9')->count();
        $pen_103            = praapplication::where('stage', 'W10')->count();
        $doc_incom_p2       = praapplication::where('stage', 'W7')->count();
        

        $spekar = praapplication::where('stage', 'W0' )->orderBy('created_at', 'desc')->get(); //Siap upload SPEKAR  //W2 Ready upload Spekar

        /* Best majikan */
       /* $bestmajikan = praapplication::whereRaw('emp_code = (select max("emp_code") from praapplication)')->get(); 
        $bestmajikan = praapplication::select(DB::raw('MAX(emp_code) AS emp_code'))->limit('5')->get(); */

        //$bestmajikan = praapplication::Count('emp_code')->groupBy('emp_code')->take(5)->get();

        
        //Berdasarkan bulan ini
        //$countw0 = praapplication::where('stage', 'W0')->where('created_at', '>=', \Carbon\Carbon::now()->startOfMonth())->count();
        
        $bestmajikan = praapplication::groupBy('emp_code')->select('emp_code', DB::raw('count(emp_code) as total'))->take(5)->get();

        $bestagentmonth = praapplication::groupBy('user_id')->select('user_id', DB::raw('count(assessment) as total'))->take(5)->where('created_at', '>=', \Carbon\Carbon::now()->startOfMonth())->get();

        $bestagentyears = praapplication::groupBy('user_id')->select('user_id', DB::raw('count(user_id) as total'))->take(5)->where(DB::raw('YEAR(created_at)'), '=', date('Y'))->get(); 

        $assessment = praapplication::orderBy('created_at', 'desc')->where('stage', '=', 'W0')->get();
        //$mbsb       = DocAssest::orderBy('created_at', 'desc')->get();


        return view('adminpage.dashboard', compact('praaplication', 'count_submission','announc','count','bestmajikan','bestagentmonth','bestagentyears', 'countsub', 'countuser', 'role','roless', 'meetcus', 'spekar', 'tasklist_p2', 'countmanager', 'manager', 'assessment' ,'mbsb', 'tasklist_p1', 'countw0', 'count_doc_reject', 'countp1sendmbsb', 'countp1upload_spekar', 'countmo_calculation','processor1','count_pen_mo','mbsb_processing','pen_doc_check','pen_103', 'count_pending_doc','user','userss', 'count_failed_mo', 'countsub_manager', 'count_mbsb_processing_p3', 'tasklist_p3', 'pending_doc_p2', 'tasklist_admin', 'count_admin', 'tasklist_mo', 'count_recommend_new_sub','remind_mo_cal', 'tasklist_manager', 'count_rejected', 'count_mbsb_processing_app', 'count_mbsb_processing_reject', 'count_mbsb_processing_add_doc', 'countmo_calculation_n_change', 'count_add_doc', 'count_doc_incomplete','count_disbursement', 'count_approved', 'doc_incom_p2', 'count_loan_online', 'count_rejected_tl', 'count_approved_tl', 'count_submission_tl', 'announcgroup'));
    }

    /**
     * view_location
     * Show the form for creating a new resource.
     
    

     *
     * @return \Illuminate\Http\Response ver
     */
    
    public function ver($id)
    {
        $user = Auth::user();        
        $roless = $user->model->role->id;
        
        $remark = Remark::orderBy('role_id', 'asc')->where('role_id', '=', $roless)->get();
        $pra    =  praapplication::latest('id')->where('id_cus','=',$id)->first();
        

        return view('adminpage.praapplication.ver.index', compact('pra','remark'));
    }


    //Veryfikasi By Role
    public function submit_verify($id)
    {
        $user = Auth::user();        
        $roless = $user->model->role->id;

        $remark = Remark::orderBy('role_id', 'asc')->where('role_id', '=', $roless)->get();
        $reg = praapplication::latest('created_at')->where('id_cus',$id)->limit('1')->first();

        return view('adminpage.application.verifyapplication', compact('reg','remark'));
    }


    
    public function view_location($id)
    {
        $location =  praapplication::latest('id')->where('id','=',$id)->first();

        return view('adminpage.praapplication.location.index', compact('location'));
    }

     
    public function view_doc($cus_id)
    {
        $pra =  praapplication::latest('created_at')->where('id_cus','=',$cus_id)->limit('1')->first();
        $dcmt = DB::table('doc_cust')->select(DB::raw(" max(id) as id"))->where('cus_id',$cus_id)->groupBy('type')->pluck('id');
        $files = DocCust::whereIn('id', $dcmt)->get();

        $document1 = DocCust::latest('created_at')->where('cus_id',  $cus_id )->where('type',  '1' )->first();

        $document2 = DocCust::latest('created_at')->where('cus_id',  $cus_id )->where('type',  '2' )->first();

        $document3 = DocCust::latest('created_at')->where('cus_id',  $cus_id )->where('type',  '3' )->first();

        $document4 = DocCust::latest('created_at')->where('cus_id',  $cus_id )->where('type',  '4' )->first();
        
        $url = url('')."/documents/user_doc/".$pra->ic."/"; 

        if((!empty($document1->doc_pdf)) OR (!empty($document2->doc_pdf)) OR (!empty($document3->doc_pdf)) OR (!empty($document4->doc_pdf)) ) {
                # create new zip opbject
                $zip = new \ZipArchive();
                
                # create a temp file & open it
                $tmp_file = tempnam('.','');
                $zip->open($tmp_file, \ZipArchive::CREATE);
                
                # loop through each file
                foreach($files as $file){
                        $url2 = $url.$file->doc_pdf;
                        $url2 = str_replace(' ', '%20', $url2);
                        
                    
                           if (!function_exists('curl_init')){ 
                                die('CURL is not installed!');
                            }
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $url2);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            $output = curl_exec($ch);
                            curl_close($ch);
                        $download_file = $output;
                                        
                        $type = substr($url2, -5, 5); 
                        #add it to the zip
                        $zip->addFromString(basename($url.$file->doc_pdf.'.'.$type),$download_file);
                }
                
                # close zip
                $zip->close();
        
        
                // Set Time Download
                date_default_timezone_set("Asia/Kuala_Lumpur");
                $time_name                       = date('Ymd');
                $time_download                   = date('Y-m-d H:i:s');
                # send the file to the browser as a download
                header('Content-disposition: attachment; filename=DOC-'.$time_name.'-'.$pra->ic.'.zip');
                header('Content-type: application/zip');
                readfile($tmp_file);
        }
    }

    public function submit_doc_cus($id)
    {
        $reg         = praapplication::latest('created_at')->where('id_cus',$id)->limit('1')->first();
         
        $ids = $reg->id_cus;
        $document1 = DocCust::latest('created_at')->where('cus_id',$id)->where('type','1')->first();

        return view('adminpage.application.index', compact('reg'));
    }
    
    public function uploads(Request $request,$id)
    {
        $cus_id      = $request->input('id_cus');
        $type        = $request->input('type');
        $verification = $request->input('verification');

        $reg         = praapplication::latest('created_at')->where('id_cus',$cus_id)->limit('1')->first();
         
        $ics = $reg->ic;
                  
        $ic_number2 =  str_replace('/', '', $ics);

                 
                  if ($request->hasFile('file'.$id)) {
                  $name = $request->input('document'.$id);
                 $file = $request->file('file'.$id);
                $tipe_file   = $_FILES['file'.$id]['type'];
                //if($tipe_file == "application/pdf") {


                 $filename = str_random(15).'-'.$name.'-'.$file->getClientOriginalName();
                 $destinationPath = 'documents/user_doc/'.$ic_number2.'/';
                 $file->move($destinationPath, $filename);
       

                $document               = new DocCust;
                $document->cus_id       = $cus_id;
                $document->doc_pdf      = $filename;
                $document->type         = $id;
                $document->verification = $verification;

                $document->save();
                 return response()->json(['file' => "$filename"]);

              // }       

                }  
    }


    
    

    
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view_history($id)
    {
        $praapplication = praapplication::where('id_cus', $id)->first();
        $history = History::where('cus_id', $id)->orderBy('updated_at', 'ASC')->get();

        return view('adminpage.praapplication.history.index', compact('history', 'praapplication'));
    }

    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
