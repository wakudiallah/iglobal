<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\praapplication;
use Illuminate\Support\Facades\Auth;
use App\Announcement;
use App\DocCust;
use DB;
use Mapper;
use DateTime;
use PDF;
use Hash;
use App\History;
use App\Remark;
use App\User;
use App\Role;
use App\Model_has_role;
use App\AddInfo;
use App\MeetCust;
use App\Package;
use App\Loanpkg;
use App\Employment;
use App\Emp;
use App\Homeimage;
use App\Tenure;
use App\Loan;
use App\LoanDetail;
use App\TenureDetail;
use App\Financial;
use App\LoanAmmount;
use App\LoanDetails;
use App\Tenures;
use Input;
use Session;


class LoanEliController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        
        //$reg = praapplication::latest('created_at')->where('id_cus','=', 'a4985c50-aaa5-43ea-967a-9558220d02fb')->limit('1')->first();
        $user = Auth::user()->id;

        $praaplication = praapplication::orderBy('created_at', 'desc')->get();

        $processor1 = praapplication::where('stage', '=', 'W1' )->orderBy('created_at', 'desc')->get();
        
        $spekar = praapplication::where('stage', '=', 'W1' )->where('routeto', $user)->orderBy('created_at', 'desc')->get(); //Siap upload SPEKAR  //W2 Ready upload Spekar
        
        $emp = Emp::first();
        
        $processor2 = praapplication::where('id', '=', 5 )->orderBy('created_at', 'desc')->get();

        $processor3 = praapplication::where('stage', '=', 'W1' )->orderBy('created_at', 'desc')->get();

        $document1 = DocCust::latest('created_at')->where('type',  '4' )->first(); 
        
        //
        $workgroup = Model_has_role::where('role_id', 3)->get();
        $workgroupprocessor2 = Model_has_role::where('role_id', 6)->get();

        //routeto
        $meetcus = praapplication::where('routeto', $user)->where('stage', '=', 'W2')->orWhere('stage', 'W3')->orWhere('stage', 'W6')->orderBy('created_at', 'desc')->get(); 

        
        $process6 = praapplication::where('stage', 'W4')->orWhere('stage', 'W9')->orWhere('stage', 'W10')->orWhere('stage', 'W8')->where('routeto', $user)->orderBy('created_at', 'desc')->get();
        $workgroupprocess6 = Model_has_role::where('role_id', 7)->get();


        //
        $process7 = praapplication::where('stage', 'W5')->orWhere('stage', 'W7')->orWhere('stage', 'W9')->where('routeto', $user)->orderBy('created_at', 'desc')->get();
        $workgroupprocess6 = Model_has_role::where('role_id', 7)->get();
        $documentcust = DocCust::latest('created_at');


        //processor2
        


        //process11 
        $process11 = praapplication::where('stage', 'W8')->orWhere('stage', 'W11')->orderBy('created_at', 'desc')->get();



        return view('adminpage.loaneligi.index', compact('praaplication','processor2', 'processor1', 'spekar', 'emp','reg', 'processor3','document1', 'workgroup', 'routeto', 'meetcus','workgroupprocessor2', 'process6', 'workgroupprocess6', 'process7', 'documentcust', 'process11'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function process11()
    {
         $process11 = praapplication::where('stage', 'W8')->orderBy('created_at', 'desc')->get();

         return view('processor3.loan_processor4', compact('process11'));
    }


    public function process6(Request $request, $id)
    {
        $user = Auth::user()->id;
        $id_pra = $request->input('id_praapplication');

        $routeback = $request->input('process5');
        
        $stagep6     = $request->input('remarkp6');

        $notep6      = $request->input('notep6');
        $loan_amount = $request->input('l4_jumlahpinjam');
        //$telpoffice = $request->input('telpoffice');
        //$iddatadoc1 = $request->input('iddatadoc1');
        //$iddatadoc2 = $request->input('iddatadoc2');
        //$iddatadoc3 = $request->input('iddatadoc3');
        $fn_monthly_basic = $request->input('fn_monthly_basic');
            $fn_govt_service = $request->input('fn_govt_service');
            $fn_financial_incentive = $request->input('fn_financial_incentive');
            $fn_fixed_allowance = $request->input('fn_fixed_allowance');
            $fn_housing_allowance = $request->input('fn_housing_allowance');
            $fn_cost_of = $request->input('fn_cost_of');
            $fn_special_allowance = $request->input('fn_special_allowance');
            $fn_gross_income = $request->input('fn_gross_income');
            //FN Non Fixed
            $fn_overtime_allowance = $request->input('fn_overtime_allowance');
            $fn_travel = $request->input('fn_travel');
            $fn_commission = $request->input('fn_commission');
            $fn_business_income = $request->input('fn_business_income');
            $fn_yearly_devidend = $request->input('fn_yearly_devidend');
            $fn_food_allowance = $request->input('fn_food_allowance');
            $fn_yearly_contractual = $request->input('fn_yearly_contractual');
            $fn_housing_building = $request->input('fn_housing_building');
            $fn_non_fixed = $request->input('fn_non_fixed');
            $fn_share_income = $request->input('fn_share_income');
            $fn_total_income = $request->input('fn_total_income');

            // FN Pay SLip
            $fn_knwsp = $request->input('fn_knwsp');
            $fn_income_tax = $request->input('fn_income_tax');
            $fn_cp38 = $request->input('fn_cp38');
            $fn_house_financing = $request->input('fn_house_financing');
            $fn_others = $request->input('fn_others');
            $fn_lembaga_tabung = $request->input('fn_lembaga_tabung');
            $fn_insurance = $request->input('fn_insurance');
            $fn_perkeso = $request->input('fn_perkeso');
            $fn_zakat = $request->input('fn_zakat');
            $fn_bpa = $request->input('fn_bpa');
            $fn_asb = $request->input('fn_asb');
            $fn_total_payslip = $request->input('fn_total_payslip');
            
            // FN Non Pay SLip
            $fn_location = $request->input('fn_location');
            $fn_cost_of2 = $request->input('fn_cost_of2');
            $fn_other_non = $request->input('fn_other_non');
            $fn_total_non = $request->input('fn_total_non');
            $fn_total_deductions = $request->input('fn_total_deductions');
            $fn_total_net = $request->input('fn_total_net');
            
            $bulan = $request->input('cal_bulan');
            $nmonth = date('m',strtotime($bulan));
            $nyear = date('Y',strtotime($bulan));
            $cal_bulan  = $nyear."-".$nmonth."-01";

             $fn_remark = $request->input('fn_remark');

               $pemrumah1 = $request->input('pemrumah1');
            $pemrumah2 = $request->input('pemrumah2');
            $pemrumah3 = $request->input('pemrumah3');
            $kadkredit1 = $request->input('kadkredit1');
            $kadkredit2 = $request->input('kadkredit2');
            $kadkredit3 = $request->input('kadkredit3');
            $pempribadi = $request->input('pempribadi');
            $pemperabot = $request->input('pemperabot');
            $pemkenderaan = $request->input('pemkenderaan');
            $pemlain = $request->input('pemlain');
            $pemlain_nyatakan = $request->input('pemlain_nyatakan');
            $jumlahpembiayaan = $request->input('jumlahpembiayaan');

             $lainlain2 = $request->input('lainlain2');
            $jumlah_pendapatan = $request->input('jumlah_pendapatan');
            // langkah 2
            $l2_jumlahbayaran  = $request->input('l2_jumlahbayaran');
            // Langkah 3
            $l3_bkr  = $request->input('l3_bkr');
            $l3_koperasi  = $request->input('l3_koperasi');
            $l3_bank  = $request->input('l3_bank');
            $l3_lainlain  = $request->input('l3_lainlain');
            $l3_jumlah  = $request->input('l3_jumlah');

             $l4_jumlahbayaran  = $request->input('l4_jumlahbayaran');
            $l4_jumlah_pendapatan  = $request->input('l4_jumlah_pendapatan');
            $l4_dsr  = $request->input('l4_dsr');
            $l4_jumlahpinjam  = $request->input('l4_jumlahpinjam');
            $l4_jumlahpinjam2  = $request->input('l4_jumlahpinjam2');
            $l4_tempoh  = $request->input('l4_tempoh');
            $l4_kadar  = $request->input('l4_kadar');
            $l4_tempohbulan  = $request->input('l4_tempohbulan');
            $l4_ansuran  = $request->input('l4_ansuran');
            $l4_bayaranbalik  = $request->input('l4_bayaranbalik');
            $l4_jumlah_pendapatan2  = $request->input('l4_jumlah_pendapatan2');
            $l4_dsr2  = $request->input('l4_dsr2');
            $note1  = $request->input('note1');
            $note2  = $request->input('note2');
            $note3  = $request->input('note3');
            $note4  = $request->input('note4');
            $note5  = $request->input('note5');
            $note6  = $request->input('note6');
            $note7  = $request->input('note7');
            $note8  = $request->input('note8');
            $note9  = $request->input('note9');
            $note10  = $request->input('note10');  

            $admin_fee             = $request->input('admin_fee');  
            $gst_idsb              = $request->input('gst_idsb');  
            $loan_amaun            = $request->input('loan_amaun');  
            $tenure                = $request->input('tenure');  
            $total_repayment       = $request->input('total_repayment');  
            $installment           = $request->input('installment');  
            $deduction_fee         = $request->input('deduction_fee');  
            $gst_fee               = $request->input('gst_fee');  
            $salary_deduction      = $request->input('salary_deduction');  
            $insurance             = $request->input('insurance_premium');     


        if($request->input('remarkp6') == 'W6' ){  //back mo

            $id_pra = $request->input('id_praapplication');

            praapplication::where('id_cus', $id_pra)->update(array('routeto' => $routeback, 'stage' => $request->input('remarkp6'), 'process6' =>  $user, 'remark6' =>  $stagep6));
        
            $request                  = new History;

            $request->cus_id          = $id;  
            $request->activity        = "";
            $request->remark_id       = $stagep6;
            $request->user_id         = $user;
            $request->note            = $notep6;
            $request->save();

            return redirect('adminnew')->with(['update' => 'Data saved successfully']);
        }

        elseif($request->input('remarkp6') == 'W9'){ //next
            $id_pra = $request->input('id_praapplication');

            praapplication::where('id_cus', $id_pra)->update(array( 'stage' => $stagep6, 'process6' =>  $user, 'remark6' =>  $stagep6));
        
            $request                  = new History;

            $request->cus_id          = $id;  
            $request->activity        = "";
            $request->remark_id       = $stagep6;
            $request->user_id         = $user;
            $request->note            = "";
            $request->save();

             
             // FN Fixed
         
             $financial = Financial::where('user_id', $id_pra)
          ->update([
           'fn_monthly_basic' => $fn_monthly_basic, 'fn_govt_service' => $fn_govt_service,'fn_financial_incentive' => $fn_financial_incentive,
           'fn_fixed_allowance' => $fn_fixed_allowance,'fn_housing_allowance' => $fn_housing_allowance,'fn_cost_of' => $fn_cost_of,
           'fn_special_allowance' => $fn_special_allowance,'fn_gross_income' => $fn_gross_income,'fn_overtime_allowance' => $fn_overtime_allowance,
           'fn_travel' => $fn_travel,'fn_commission' => $fn_commission,'fn_business_income' => $fn_business_income,'fn_business_income' => $fn_business_income,
           'fn_yearly_devidend' => $fn_yearly_devidend, 'fn_food_allowance' => $fn_food_allowance, 'fn_yearly_contractual' => $fn_yearly_contractual,
           'fn_housing_building' => $fn_housing_building,'fn_non_fixed' => $fn_non_fixed,'fn_share_income' => $fn_share_income, 'fn_total_income' => $fn_total_income, 
           'fn_knwsp' => $fn_knwsp,'fn_income_tax' => $fn_income_tax,'fn_cp38' => $fn_cp38,'fn_house_financing' => $fn_house_financing,'fn_others' => $fn_others,
           'fn_lembaga_tabung' => $fn_lembaga_tabung,'fn_insurance' => $fn_insurance,'fn_perkeso' => $fn_perkeso,'fn_zakat' => $fn_zakat,'fn_bpa' => $fn_bpa,
           'fn_asb' => $fn_asb,'fn_total_payslip' => $fn_total_payslip,'fn_location' => $fn_location,'fn_cost_of2' => $fn_cost_of2,'fn_other_non' => $fn_other_non,
           'fn_total_non' => $fn_total_non, 'fn_total_deductions' => $fn_total_deductions, 'fn_total_net' => $fn_total_net, 'cal_bulan' => $cal_bulan, 'remark' => $fn_remark ]);


             $financial2 = Financial::where('user_id', $id_pra)
          ->update(['pemrumah1' => $pemrumah1,  'pemrumah2' => $pemrumah2, 'pemrumah3' => $pemrumah3, 
           'kadkredit1' => $kadkredit1, 'kadkredit2' => $kadkredit2, 'kadkredit3' => $kadkredit3, 'pempribadi' => $pempribadi,
           'pemperabot' => $pemperabot, 'pemkenderaan' => $pemkenderaan,'pemlain' => $pemlain, 'jumlahpembiayaan' => $jumlahpembiayaan, 'pemlain_nyatakan' => $pemlain_nyatakan]);
            
            if($l3_jumlah>0) {
                $debth_update = Financial::where('user_id', $id_pra)
                ->update(['debt_consolidation' => 1]);
            } 
            
           $financial3 = Financial::where('user_id', $id_pra)
          ->update(['lainlain2' => $lainlain2,  'jumlah_pendapatan' => $jumlah_pendapatan, 'l2_jumlahbayaran' => $l2_jumlahbayaran,
            'l3_bkr' => $l3_bkr,  'l3_koperasi' => $l3_koperasi,
            'l3_bank' => $l3_bank,  'l3_lainlain' => $l3_lainlain,
            'l3_jumlah' => $l3_jumlah]);
            $financial4 = Financial::where('user_id', $id_pra)
          ->update(['l4_jumlahbayaran' => $l4_jumlahbayaran,  'l4_jumlah_pendapatan' => $l4_jumlah_pendapatan, 'l4_dsr' => $l4_dsr,
            'l4_jumlahpinjam' => $l4_jumlahpinjam,  'l4_jumlahpinjam2' => $l4_jumlahpinjam2,
            'l4_tempoh' => $l4_tempoh ,  'l4_kadar' => $l4_kadar, 'l4_tempohbulan' => $l4_tempohbulan, 
            'l4_ansuran' => $l4_ansuran, 'l4_bayaranbalik' => $l4_bayaranbalik, 'l4_jumlah_pendapatan2' => $l4_jumlah_pendapatan2,
            'l4_dsr2' => $l4_dsr2,  'note1' => $note1,  'note2' => $note2,  'note3' => $note3,
            'note4' => $note4,  'note5' => $note5,  'note6' => $note6,  'note7' => $note7,
            'note8' => $note8,  'note9' => $note9,  'note10' => $note10]);
            
           $loanammount = LoanAmmount::where('id_praapplication', $id_pra)
          ->update(['loanammount' => $loan_amount ]);

        }
        
        return redirect('loan-eli/'.$id_pra.'/step2')->with(['update' => 'Data saved successfully']);

    }

    public function process7(Request $request, $id)
    {
        $user         = Auth::user()->id;
        $stagep7       = $request->input('stagep7');
         $id_pra       = $request->input('id_praapplication');
        
        $routomobalik = $request->input('process5_7');

        $notep7 = $request->input('notep7');
        $iddatadoc1 = $request->input('iddatadoc1');
        $iddatadoc2 = $request->input('iddatadoc2');
        $iddatadoc3 = $request->input('iddatadoc3');

        $checkbox1 = $request->input('checkbox1');
        $checkbox2 = $request->input('checkbox2');
        $checkbox3 = $request->input('checkbox3');
        

        //Check complete atau tidak 
        
        if($stagep7 == 'W10'){  //Jika doc complete

            praapplication::where('id_cus', $id)->update(array('stage' => $stagep7, 'process7' =>  $user, 'doc' => '1', 'routeto' => $user));

            $doc1= DocCust::where('id', $iddatadoc1)->where('cus_id', $id)->where('type','1')->latest('created_at')->limit('1')->update(array('verification' => '1'));
            $doc2 = DocCust::where('id', $iddatadoc2)->where('cus_id', $id)->where('type','2')->latest('created_at')->limit('1')->update(array('verification' => '1'));
            $doc3 = DocCust::where('id', $iddatadoc3)->where('cus_id', $id)->where('type','4')->latest('created_at')->limit('1')->update(array('verification' => '1'));


            $request                  = new History;

            $request->cus_id          = $id;  
            $request->activity        = "5";
            $request->remark_id       = $stagep7;
            $request->user_id         = $user;
            $request->note            = $notep7;
            $request->save();
        

        }elseif($stagep7 == 'W7'){ //Doc tdk complete

            praapplication::where('id_cus', $id)->update(array('stage' => $stagep7, 'process7' =>  $user, 'doc' => '0', 'routeto' => $routomobalik));

            
            if ($checkbox1 == '1')
            {
                $doc1= DocCust::where('cus_id', $id)->where('type','1')->latest('created_at')->limit('1')->update(array('verification' => '1'));
            }else
            {
                $doc1= DocCust::where('cus_id', $id)->where('type','1')->latest('created_at')->limit('1')->update(array('verification' => '0'));
            }

            if ($checkbox2 == '1')
            {
            $doc2 = DocCust::where('cus_id', $id)->where('type','2')->latest('created_at')->limit('1')->update(array('verification' => '1'));
            }else{
               $doc2 = DocCust::where('cus_id', $id)->where('type','2')->latest('created_at')->limit('1')->update(array('verification' => '0')); 
            }

            if($checkbox3 == '1'){
                $doc3 = DocCust::where('cus_id', $id)->where('type','4')->latest('created_at')->limit('1')->update(array('verification' => '1'));
            }else{
                $doc3 = DocCust::where('cus_id', $id)->where('type','4')->latest('created_at')->limit('1')->update(array('verification' => '0'));
            }



            $request                  = new History;

            $request->cus_id          = $id;  
            $request->activity        = "5";
            $request->remark_id       = $stagep7;
            $request->user_id         = $user;
            $request->note            = $notep7;
            $request->save();

            return redirect('adminnew')->with(['update' => 'Data saved successfully']);
        }

        return redirect('loan-eli/'.$id_pra.'/step3')->with(['update' => 'Data saved successfully']);
        
    }

    public function process8(Request $request, $id)
    {
        $user         = Auth::user()->id;

        $stage        = $request->input('stage');
        $telp         = $request->input('telpoffice');
        $note         = $request->input('note');

        if($stage == 'W8'){
            praapplication::where('id_cus', $id)->update(array('routeto' => $user, 'stage' => $stage, 'process8' =>  $user));

            AddInfo::where('cus_id', $id)->latest('updated_at')->update(array('office_telp' => $telp, 'note' => $note));
            
            $request                  = new History;

            $request->cus_id          = $id;  
            $request->activity        = "8";
            $request->remark_id       = $stage;
            $request->user_id         = $user;
            $request->note            = $telp;
            $request->save();
        }else{
            
            praapplication::where('id_cus', $id)->update(array('routeto' => $user, 'stage' => $stage, 'process8' =>  $user));

            $request                  = new History;

            $request->cus_id          = $id;  
            $request->activity        = "8";
            $request->remark_id       = $stage;
            $request->user_id         = $user;
            $request->note            = $telp;
            $request->save();
        }


        return redirect('adminnew')->with(['update' => 'Data saved successfully']);
    }
    

    public function meetcus(Request $request, $id)
    {
            $user = Auth::user()->id;
            
            

            //save table
            
            $document                   = new MeetCust;
            $document->cus_id           = $id;
            $document->existing         = $request->input('group2');
            $document->date_disbustment = $request->input('disb');

            $document->save();

            
            
                /*$request                  = new History;

                $request->cus_id          = $id;  
                $request->activity        = "";
                $request->remark_id       = $stage;
                $request->user_id         = $user;
                $request->note            = "";

                $request->save();
            
            return redirect('loan-eli')->with(['update' => 'Data saved successfully']); */

            return redirect('/tenos/custtenos/'.$id);
            
    }

    public function disbursement($id)
    {
        $user = Auth::user()->id;

        praapplication::where('id_cus', $id)->update(array('process11' => $user, 'stage' => 'W11'  ));

        $request                  = new History;

        $request->cus_id          = $id;  
        $request->activity        = "11";
        $request->remark_id       = 'W11';
        $request->user_id         = $user;
        $request->note            = '';
        $request->save();

        return redirect('loan-eli')->with(['update' => 'Data saved successfully']);
    } 

    public function rejectdisbursement($id)
    {
        $user = Auth::user()->id;

        praapplication::where('id_cus', $id)->update(array('process11' => $user, 'stage' => 'W12'  ));

        $request                  = new History;

        $request->cus_id          = $id;  
        $request->activity        = "11";
        $request->remark_id       = 'W12';
        $request->user_id         = $user;
        $request->note            = '';
        $request->save();

        return redirect('loan-eli')->with(['update' => 'Data saved successfully']);
    }

    public function adddocprocess11(Request $request, $id)
    {
        $user = Auth::user()->id;
        
        $note     = $request->input('note');
        $process5 = $request->input('process5');

        praapplication::where('id_cus', $id)->update(array('process11' => $user, 'stage' => 'W13', 'routeto' => $process5  ));

        $request                  = new History;

        $request->cus_id          = $id;  
        $request->activity        = "11";
        $request->remark_id       = 'W13';
        $request->user_id         = $user;
        $request->note            = $note;
        $request->save();

        return redirect('loan-eli')->with(['update' => 'Data saved successfully']);
    }

    public function iclocreject(Request $request, $id)
    {
        $user = Auth::user()->id;

        $process2 = $request->input('process2');
        $note     = $request->input('note');

       praapplication::where('id_cus', $id)->update(array('stage' => 'W14', 'routeto' => $process2  )); 

       $request                  = new History;

        $request->cus_id          = $id;  
        $request->activity        = "1";
        $request->remark_id       = 'W14';
        $request->user_id         = $user;
        $request->note            = $note;
        $request->save();

        return redirect('assessment1')->with(['update' => 'Data saved successfully']);

    }

    public function upload_ic_loc_back(Request $request, $id)
    {
        $user = Auth::user()->id;

        $reg = praapplication::latest('id')->limit('1')->first();
        
        return view('adminpage.customer.kelayakanback', compact('reg'));
    }
      
    public function passreject_mo(Request $request, $id)
    {
        $user = Auth::user()->id;
        
        $stage           = $request->input('moremark');
        //$routeto         = $request->input('routetoq');

        praapplication::where('id_cus', $id)->update(array('process5' => $user, 'stage' => $stage  ));


        $request                  = new History;

        $request->cus_id          = $id;  
        $request->activity        = "5";
        $request->remark_id       = $stage;
        $request->user_id         = $user;
        $request->note            = '';
        $request->save();

        return redirect('loan-eli')->with(['update' => 'Data saved successfully']);

    }

    public function download_spekar(Request $request, $id)
    {
        $user = Auth::user()->id;

        $request                  = new History;

        $request->cus_id          = $id;  
        $request->activity        = "4";
        $request->remark_id       = "W91";
        $request->user_id         = $user;
        $request->note            = '';
        $request->save();

        $pra =      Praapplication::where('id_cus', $id)->first();

        //return redirect('loan-eli', compact('pra'));
        return response()->download('documents/user_doc/'.$pra->ic.'/'.$pra->spekar, $pra->spekar, [], 'inline');

    }




    public function kelayakantenos($id)
    {
        $package    = Package::all();
        $employment = Employment::all();
        $loanpkg    = Loanpkg::all();
        $emp        = Emp::all();
        $pra =      Praapplication::where('id_cus', $id)->first(); 

        return view('adminpage.customer.kelayakantenos', compact('package', 'employment', 'loanpkg', 'emp', 'pra'));
    }


    public function savekelayakantenos1(Request $request)
    {
        
        
        $user         = Auth::user()->id;
        
        $id           = $request->input('id_cus');
        $pra          = praapplication::where('id_cus', $id)->first();
        $gaji_asas    = $request->gaji_asas;
        $elaun        = $request->elaun;
        $pot_bul      = $request->pot_bul;
        
        //$id_type      = $pra->emp_type;  //disini sebenarnya emp_code 
        $id_type      = $request->job_status;
        $total_salary = $gaji_asas + $elaun;
        $zbasicsalary = $gaji_asas + $elaun;
        $zdeduction   = $pot_bul;


        $loan = LoanDetail::where('id_emp_type',$id_type)
                ->where('min_salary','<=',$total_salary)
                ->where('max_salary','>=',$total_salary)->limit('1')->get();

        $id_loan= $loan->first()->id;  
        //$id_loan= $loan->id;  
        
        $icnumber = $pra->ic;
        $tanggal  = substr($icnumber,4, 2);
        $bulan    = substr($icnumber,2, 2);
        $tahun    = substr($icnumber,0, 2); 

        if($tahun > 30) {

            $tahun2 = "19".$tahun;
        }
        else {
             $tahun2 = "20".$tahun;
        }
        
        $lahir          = $tahun2.'-'.$bulan.'-'.$tanggal; 
        $lahir          =  date('Y-m-d', strtotime($lahir. ' - 16 month'));
        $oDateNow       = new DateTime();
        $oDateBirth     = new DateTime($lahir);
        $oDateIntervall = $oDateNow->diff($oDateBirth);

        $umur = 61 - $oDateIntervall->y;

        $tenure = TenureDetail::where('id_loan',$id_loan)->where('years','<', $umur)->get();   



         $workgroupprocessor2 = Model_has_role::where('role_id', 6)->get();

         //$data->latitude        = $request->latitude;
            //$data->longitude       = $request->longitude;
            //$data->location        = $request->location;
 
         
        praapplication::where('id_cus', $id)->update(array('employment_code' => $request->input('employment_code'), 'job_status' => $request->input('job_status'), 'gaji_asas' => $request->input('gaji_asas'), 'elaun' => $request->input('elaun'), 'pot_bul' => $request->input('pot_bul'), 'jml_pem' => $request->input('jml_pem')));

        return view('adminpage.customer.tenos', compact('pra','loan','total_salary','tenure','id','zbasicsalary' ,'zdeduction','user', 'workgroupprocessor2','id_loan','umur'));
       
    }


 
     public function post_tenus_step1(Request $request)
    { 
        
        $id_cus    = $request->input('id_cus');


        //--------------- -----  Proceed ---------- */
        
        $basicsalary = $request->input('gaji_asas');
        $allowance   = $request->input('elaun');
        $deduction   = $request->input('pot_bul');
        $loanAmount  = $request->input('jml_pem');
        $package     = $request->input('Package');
        $employment_code     = $request->input('Employment');

        $gaji_tambah_tunjangan = $basicsalary + $allowance; 

        if($gaji_tambah_tunjangan <= 3500){  //3500 = had sementara

            Session::flash('gaji_asas', $basicsalary); 
            Session::flash('elaun', $allowance);
            Session::flash('pot_bul', $deduction); 
            Session::flash('jml_pem', $loanAmount); 
                   
            return redirect('tenos/custtenos/'.$id_cus)->with(['update' => 'Sorry you are not eligible to apply, your deduction or funding exceeds the limit']);
        }
        else{
            if(($loanAmount < 50000) || ($loanAmount > 250000)){

                Session::flash('gaji_asas', $basicsalary); 
                Session::flash('elaun', $allowance);
                Session::flash('pot_bul', $deduction); 
                Session::flash('jml_pem', $loanAmount); 

                return redirect('tenos/custtenos/'.$id_cus)->with(['update' => 'Loan Amount at least RM 50000 and less than RM 250000']);
            }
            else{
                  $pra          =      Praapplication::where('id_cus', $id_cus)->first(); 

        $id_type      = $employment_code;
        $total_salary = $basicsalary + $allowance ;
        $zbasicsalary = $basicsalary + $allowance;
        $zdeduction   = $deduction ;

        $loan = LoanDetails::where('emp_id', $id_type)  
        ->where('loanpkg_id',$package)->limit('1')->get();   //mencari minimal salary nya & dsr nya (rasio utang terhadap pendapatan)

        // return $total_salary;
        $id_loan= $loan->first()->id;  
        $salary_dsr = ($zbasicsalary * ($loan->first()->dsr / 100)) - $zdeduction;


        $icnumber = $pra->ic;
        $tanggal  = substr($icnumber,4, 2);
        $bulan    = substr($icnumber,2, 2);
        $tahun    = substr($icnumber,0, 2); 

        if($tahun > 30) {

            $tahun2 = "19".$tahun;
        }
        else {
             $tahun2 = "20".$tahun;
        }
        

        $lahir          = $tahun2.'-'.$bulan.'-'.$tanggal; 
        $lahir          =  date('Y-m-d', strtotime($lahir. ' - 16 month'));
        $oDateNow       = new DateTime();
        $oDateBirth     = new DateTime($lahir);
        $oDateIntervall = $oDateNow->diff($oDateBirth);

        $umur = 61 - $oDateIntervall->y;

        $durasix = 60 - $oDateIntervall->y;
        if( $durasix  > 10) { 
            $durasi = 10 ;
        } 
        else { 
            $durasi = $durasix ;
        }


        function pembulatan($uang) {
            $puluhan = substr($uang, -3);
                  if($puluhan<500) {
                    $akhir = $uang - $puluhan; 
                  } 
                  else {
                    $akhir = $uang - $puluhan;
                  }
                  return $akhir;
                }


        foreach($loan as $loan) {
            $salary_dsr = ($zbasicsalary * ($loan->dsr / 100)) - $zdeduction;
            $ndi        = ($zbasicsalary - $zdeduction) -  1300; 
            $max        =  $salary_dsr * 12 * 10 ;
                                           

            if(!empty($loan->max_byammount))  {
                  $ansuran = intval($salary_dsr)-1;
                    if($package == "1") {    //aku edit
                        $bunga = 3.8/100;
                    }
                    elseif($package == "2") { //aku edit
                        $bunga = 4.9/100;
                    }

                    else {
                        $bunga = 5.92/100;
                    }
                  $pinjaman = 0;

                  for ($i = 0; $i <= $loan->max_byammount; $i++) {
                      $bungapinjaman = $i  * $bunga * $durasi ;
                      $totalpinjaman = $i + $bungapinjaman ;
                      $durasitahun = $durasi * 12;
                      $ansuran2 = intval($totalpinjaman / ($durasi * 12))  ;
                      if ($ansuran2 < $ndi)
                      {
                          $pinjaman = $i;
                      }
                    
                  }   

                  if($pinjaman > 1) {                                              
                      $bulat = pembulatan($pinjaman);
                      $loanx =  number_format($bulat, 0 , ',' , ',' ) ; 
                      $loanz = $bulat;
                  }
                  else {
                      $loanx =  number_format($loan->max_byammount, 0 , ',' , ',' ) ; 
                      $loanz = $loan->max_byammount;
                  }

                }
                else { 
                    $bulat = pembulatan($loan->max_bysalary * $total_salary);
                    $loanx =  number_format($bulat, 0 , ',' , ',' ) ; 
                    $loanz = $bulat;
                    if ($loanz > 199000) {

                          $loanz  = 250000;
                          $loanx =  number_format($loanz, 0 , ',' , ',' ) ; 
                    }
                }
            }

        $tenure = Tenures::where('id_loan',$id_loan)->where('years','<', $umur)->where('years','!=','1')->get();  
         
         if( $pra->first()->loanamount <= $loanz ) {
            $ndi_limit=$loan->ndi_limit;
            foreach($tenure as $tenure) {
                $bunga2 =  $pra->first()->loanamount * $tenure->rate /100   ;
                $bunga = $bunga2 * $tenure->years;
                $total = $pra->first()->loanamount + $bunga ;
                $bulan = $tenure->years * 12 ;
                $installment =  $total / $bulan ;
                 $ndi_state = ($total_salary - $zdeduction) - $installment; 

                    $count_installment=0;
                 if($installment  <= $salary_dsr && $ndi_state>=$ndi_limit) {
                    $count_installment++;
                }

            }
         } else {
            $count_installment=0;
         }

        if($count_installment>0) {

            $emp    = $request->input('Employment');
            $today  = date('Y-m-d H:i:s');  //today

          
                praapplication::where('id_cus', $id_cus)->update(array( 'loanpkg_code' => $package,'employment_code' => $request->input('Employment'), 'gaji_asas' => $request->input('gaji_asas'), 'elaun' => $request->input('elaun'), 'pot_bul' => $request->input('pot_bul'), 'jml_pem' => $request->input('jml_pem')));
    

        
            $document                          = new LoanAmmount;
            $document->id_praapplication       = $id_cus;
        
            $document->save();

            Session::flash('gaji_asas', $basicsalary); 
            Session::flash('elaun', $allowance);
            Session::flash('pot_bul', $deduction); 
            Session::flash('jml_pem', $loanAmount);

          return redirect('resulttenus/'.$id_cus)->with(['update' => 'Congratulations, you are eligible to apply for up to RM '.$loanx]);
          //return view('adminpage.customer.tenos', compact('pra','loan','total_salary','tenure','id','zbasicsalary' ,'zdeduction','user', 'workgroupprocessor2'));

           //return redirect('loan-eli')->with(['update' => 'Data saved successfully']);  


        } 

        else {
            $had2 = number_format($had, 0 , ',' , ',' ).'.00' ; 
            
            Session::flash('gaji_asas', $basicsalary); 
            Session::flash('elaun', $allowance);
            Session::flash('pot_bul', $deduction); 
            Session::flash('jml_pem', $loanAmount);

            Session::flash('hadpotongan', $basicsalary); 
            return redirect('/')->with(['update' => 'Sorry you are not eligible to apply, your deduction or funding exceeds the limit']);
        }       
            }
        }

         
    
    }


    public function resulttenus($id)
    {
        $package    = Package::all();
        $employment = Employment::all();
        $loanpkg    = Loanpkg::all();
        $emp        = Emp::all();
        $pra        = Praapplication::where('id_cus', $id)->first(); 

        $id_type      = $pra->employment_code;
        $total_salary = $pra->gaji_asas + $pra->elaun ;
        $zbasicsalary = $pra->gaji_asas + $pra->elaun ;
        $zdeduction   = $pra->pot_bul;

        /*$loan = LoanDetail::where('id_emp_type',$id_type)
                ->where('min_salary','<=',$total_salary)
                ->where('max_salary','>=',$total_salary)->limit('1')->get();*/

        $loan = LoanDetails::where('emp_id', $id_type)  
        ->where('loanpkg_id',$pra->loanpkg_code)->limit('1')->get();   //mencari minimal salary nya & dsr nya (rasio utang terhadap pendapatan)



        $id_loan  = $loan->first()->id;  
        
        $icnumber = $pra->first()->icnumber;
        $tanggal  = substr($icnumber,4, 2);
        $bulan    = substr($icnumber,2, 2);
        $tahun    = substr($icnumber,0, 2); 

        if($tahun > 30) {
            $tahun2 = "19".$tahun;
        }
        else {
             $tahun2 = "20".$tahun;
        }
       
        $lahir          = $tahun2.'-'.$bulan.'-'.$tanggal; 
        $lahir          =  date('Y-m-d', strtotime($lahir. ' - 16 month'));
        $oDateNow       = new DateTime();
        $oDateBirth     = new DateTime($lahir);
        $oDateIntervall = $oDateNow->diff($oDateBirth);

        $umur = 61 - $oDateIntervall->y;

        
        $tenure = Tenures::where('id_loan',$id_loan)->where('years','<', $umur)->where('years','!=','1')->get(); 

        $today  = date('Y-m-d H:i:s');  //today 
        praapplication::where('id_cus', $id)->update(array( 'date_pending_doc' => $today));

        return view('adminpage.customer.tenos', compact('package', 'employment', 'loan', 'emp', 'pra', 'loan', 'tenure', 'zbasicsalary', 'zdeduction', 'total_salary','id_loan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $user = Auth::user()->id;
        //documents\user_doc\240593
        $ic      = $request->input('ic');
        $id_cus = $request->input('id_cus');
        $destinationPath = 'documents/user_doc/'.$ic.'/';
        $name      = $request->input('name');

        if($request->hasFile('fileToUpload')) {
            $file = $request->file('fileToUpload');
            $extension = $file->getClientOriginalExtension();
            $file_name = 'Spekar-'. $name. '.' . $extension;
            
            $file->move($destinationPath, $file_name );
        }
        
            $upload_file = $file_name;

            $st = praapplication::find($id);  //gak bisa karena linknya ic
            $st->spekar = $upload_file;
            $st->process4 = $user;
            $st->save();

            $document               = new DocCust;
            $document->cus_id       = $id_cus;
            $document->doc_pdf      = $upload_file;
            $document->type         = '4';
            $document->user_id      = $user;
        
            $document->save();
        
        return redirect('uploadspekar_p1')->with(['update' => 'Data saved successfully']);

    }

    public function save_tenure(Request $request)
    {
        $user = Auth::user()->id;
        
        
        $id         = $request->input('cus_id');
        $id_tenure  = $request->input('tenure');
        $maxloan    = $request->input('maxloanz');
        $loanammount= $request->input('LoanAmount2');
        
        $remark     = $request->input('remark');
        $note       = $request->input('notep6');
        $installment= $request->input('installment_value');

        $latitude = $request->latitude;
        $longitude = $request->longitude;
        $location = $request->location;

        $date       = date('Y-m-d H:i:s');
        
        if($remark == 'W3') //Pass (pending doc)
        {
            praapplication::where('id_cus', $id)->update(array('process5' => $user, 'stage' => $remark, 'date_send_doc' => $date, 'latitude' => $latitude, 'longitude' => $longitude, 'location' => $location ));

            LoanAmmount::where('id_praapplication', $id)->update(array('id_tenure' => $id_tenure, 'maxloan' => $maxloan, 'loanammount' =>  $loanammount, 'installment' =>  $installment));

            $request                  = new History;

            $request->cus_id          = $id;  
            $request->activity        = "4";
            $request->remark_id       = $remark;
            $request->user_id         = $user;
            $request->note            = '';
            $request->save();
        }
        elseif($remark == 'W5') // Reject to MO
        {
            praapplication::where('id_cus', $id)->update(array('process5' => $user, 'stage' => $remark ));

            $request                  = new History;

            $request->cus_id          = $id;  
            $request->activity        = "4";
            $request->remark_id       = $remark;
            $request->user_id         = $user;
            $request->note            = $note;
            $request->save();
        }

        
        //praapplication::where('id_cus', $id)->update(array('process5' => $user, 'stage' => 'W3' ));
        
        return redirect('adminnew')->with(['update' => 'Data saved successfully']);

    }
    

    public function pdfbatchheader($id)
    {
        
            $datax1 = DB::table('doc_cust')->join('praapplication', 'doc_cust.cus_id', '=', 'praapplication.id_cus')->where('cus_id', $id)->where('doc_cust.type','1')->where('verification', '1')->select('verification', 'doc_pdf')->latest('doc_cust.created_at')->limit('1')->first();

            $datax2 = DB::table('doc_cust')->join('praapplication', 'doc_cust.cus_id', '=', 'praapplication.id_cus')->where('cus_id', $id)->where('doc_cust.type','2')->where('verification', '1')->select('verification', 'doc_pdf')->latest('doc_cust.created_at')->limit('1')->first();

            $datax3 = DB::table('doc_cust')->join('praapplication', 'doc_cust.cus_id', '=', 'praapplication.id_cus')->where('cus_id', $id)->where('doc_cust.type','4')->where('verification', '1')->select('verification', 'doc_pdf')->latest('doc_cust.created_at')->limit('1')->first();


            $pra = praapplication::where('id_cus', $id)->first();

            $pdf = PDF::loadView('adminpage.reporting.pdf', compact('datax1', 'datax2', 'datax3', 'pra'));
            
            
            return $pdf->stream('batchheader.pdf', array('Attachment'=>false));
            
    }

    
    /**
     * Display the specified resource.
     *
     
$pra =  praapplication::latest('created_at')->where('id_cus','=',$cus_id)->limit('1')->first();
        $dcmt = DB::table('doc_cust')->select(DB::raw(" max(id) as id"))->where('cus_id',$cus_id)->groupBy('type')->pluck('id');
        $files = DocCust::whereIn('id', $dcmt)->get();

        $document1 = DocCust::latest('created_at')->where('cus_id',  $cus_id )->where('type',  '1' )->first();

     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($cus_id)
    {
        $pra =  praapplication::latest('created_at')->where('id_cus','=',$cus_id)->limit('1')->first();
        $dcmt = DB::table('doc_cust')->select(DB::raw(" max(id) as id"))->where('cus_id',$cus_id)->groupBy('type')->pluck('id');

        $files = DocCust::whereIn('id', $dcmt)->get();

        $document1 = DocCust::latest('created_at')->where('cus_id',  $cus_id )->where('type',  '4' )->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */ 
    public function routeto(Request $request, $id)
    {
            $user = Auth::user()->id;

            praapplication::where('id_cus', $id)->update(array('routeto' => $request->input('routetoq'), 'submission' => $user, 'stage' => 'W2', 'process4' => $user));

            //update history
            $request                  = new History;

            $request->cus_id          = $id;  
            $request->activity        = "";
            $request->remark_id       = 'W2';
            $request->user_id         = $user;
            $request->note            = "";

            $request->save();

            return redirect('loan-eli')->with(['update' => 'Data saved successfully']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function spekar_routeto(Request $request)
    {
        $user = Auth::user()->id;

        //$password = Hash::make($id_no); 
        //$password   = $this->generateStrongPassword('12',false,'ld');

        $process2 = $request->process2;
        $route    = $request->input('process2');
        $item     = array_map(null, $request->id, $request->process2);
        
            foreach($item as $val) {

                $pra = praapplication::Where('id',$val[0])->update([
                    "stage"       => 'W2',
                    "submission" => $val[1],
                    "routeto"     => $val[1],
                    "process5"    => $user 
                ]);
            }

        $itemx     = array_map(null, $request->id, $request->ci);
            foreach($itemx as $his) {

                $request                  = new History;

                $request->cus_id          = $his[1];  
                $request->activity        = '5';
                $request->remark_id       = 'W2';
                $request->user_id         = $user;
                $request->save();
            }

            return redirect('adminnew')->with(['update' => 'Data saved successfully']);
    }

     public function verify_step1($user_id)
    {
        
        //$reg = praapplication::latest('created_at')->where('id_cus','=', 'a4985c50-aaa5-43ea-967a-9558220d02fb')->limit('1')->first();
        $employment = Employment::all();

        $package    = Package::all();
        $employment = Employment::all();
        $loanpkg    = Loanpkg::all();
        $emp        = Emp::all();
        $pra =      Praapplication::where('id_cus', $user_id)->first(); 

        $user = Auth::user()->id;

        $praaplication = praapplication::orderBy('created_at', 'desc')->get();

        $processor1 = praapplication::where('stage', '=', 'W1' )->orderBy('created_at', 'desc')->get();
        
        $spekar = praapplication::where('stage', '=', 'W1' )->where('routeto', $user)->orderBy('created_at', 'desc')->get(); //Siap upload SPEKAR  //W2 Ready upload Spekar
        
        $emp = Emp::first();
        
        $processor2 = praapplication::where('id', '=', 5 )->orderBy('created_at', 'desc')->get();

        $processor3 = praapplication::where('stage', '=', 'W1' )->orderBy('created_at', 'desc')->get();

        $document1 = DocCust::latest('created_at')->where('type',  '4' )->first(); 
        
        //
        $workgroup = Model_has_role::where('role_id', 3)->get();
        $workgroupprocessor2 = Model_has_role::where('role_id', 6)->get();

        //routeto
        $meetcus = praapplication::where('routeto', $user)->where('stage', '=', 'W2')->orWhere('stage', 'W3')->orWhere('stage', 'W6')->orderBy('created_at', 'desc')->get(); 

        
        $process6 = praapplication::where('stage', 'W4')->orWhere('stage', 'W9')->orWhere('stage', 'W10')->orWhere('stage', 'W8')->where('routeto', $user)->orderBy('created_at', 'desc')->get();
        $workgroupprocess6 = Model_has_role::where('role_id', 7)->get();

         $step1 = praapplication::where('id_cus',$user_id)->first();
         $financial = Financial::where('user_id',$user_id)->first();
         $loanammount = LoanAmmount::where('id_praapplication',$user_id)->first();
        //
        $process7 = praapplication::where('stage', 'W5')->orWhere('stage', 'W7')->orWhere('stage', 'W9')->where('routeto', $user)->orderBy('created_at', 'desc')->get();
        $workgroupprocess6 = Model_has_role::where('role_id', 7)->get();
        $documentcust = DocCust::latest('created_at');


        //process11 
        $process11 = praapplication::where('stage', 'W8')->orWhere('stage', 'W11')->orderBy('created_at', 'desc')->get();


        return view('adminpage.loaneligi.index1', compact('praaplication','processor2', 'processor1', 'spekar', 'emp','reg', 'processor3','document1', 'workgroup', 'routeto', 'meetcus','workgroupprocessor2', 'process6', 'workgroupprocess6', 'process7', 'documentcust', 'process11','step1','financial','loanammount', 'employment', 'loanpkg', 'package'));
    }

     public function verify_step2($user_id)
    {
        
        //$reg = praapplication::latest('created_at')->where('id_cus','=', 'a4985c50-aaa5-43ea-967a-9558220d02fb')->limit('1')->first();
        $user = Auth::user()->id;

        $praaplication = praapplication::orderBy('created_at', 'desc')->get();

        $processor1 = praapplication::where('stage', '=', 'W1' )->orderBy('created_at', 'desc')->get();
        
        $spekar = praapplication::where('stage', '=', 'W1' )->where('routeto', $user)->orderBy('created_at', 'desc')->get(); //Siap upload SPEKAR  //W2 Ready upload Spekar
        
        $emp = Emp::first();
        
        $processor2 = praapplication::where('id', '=', 5 )->orderBy('created_at', 'desc')->get();

        $processor3 = praapplication::where('stage', '=', 'W1' )->orderBy('created_at', 'desc')->get();

        $document1 = DocCust::latest('created_at')->where('type',  '4' )->first(); 
        
        //
        $workgroup = Model_has_role::where('role_id', 3)->get();
        $workgroupprocessor2 = Model_has_role::where('role_id', 6)->get();

        //routeto
        $meetcus = praapplication::where('routeto', $user)->where('stage', '=', 'W2')->orWhere('stage', 'W3')->orWhere('stage', 'W6')->orderBy('created_at', 'desc')->get(); 

        
        $process6 = praapplication::where('stage', 'W4')->orWhere('stage', 'W9')->orWhere('stage', 'W10')->orWhere('stage', 'W8')->where('routeto', $user)->orderBy('created_at', 'desc')->get();
        $workgroupprocess6 = Model_has_role::where('role_id', 7)->get();

         $step1 = praapplication::where('id_cus',$user_id)->first();
         $financial = Financial::where('user_id',$user_id)->first();
         $loanammount = LoanAmmount::where('id_praapplication',$user_id)->first();
        //
        $process7 = praapplication::where('stage', 'W5')->orWhere('stage', 'W7')->orWhere('stage', 'W9')->where('routeto', $user)->orderBy('created_at', 'desc')->get();
        $workgroupprocess6 = Model_has_role::where('role_id', 7)->get();
        $documentcust = DocCust::latest('created_at');


        //process11 
        $process11 = praapplication::where('stage', 'W8')->orWhere('stage', 'W11')->orderBy('created_at', 'desc')->get();


        return view('adminpage.loaneligi.index2', compact('praaplication','processor2', 'processor1', 'spekar', 'emp','reg', 'processor3','document1', 'workgroup', 'routeto', 'meetcus','workgroupprocessor2', 'process6', 'workgroupprocess6', 'process7', 'documentcust', 'process11','step1','financial','loanammount'));
    }

    public function verify_step3($user_id)
    {
        
        //$reg = praapplication::latest('created_at')->where('id_cus','=', 'a4985c50-aaa5-43ea-967a-9558220d02fb')->limit('1')->first();
        $user = Auth::user()->id;

        $praaplication = praapplication::orderBy('created_at', 'desc')->get();

        $processor1 = praapplication::where('stage', '=', 'W1' )->orderBy('created_at', 'desc')->get();
        
        $spekar = praapplication::where('stage', '=', 'W1' )->where('routeto', $user)->orderBy('created_at', 'desc')->get(); //Siap upload SPEKAR  //W2 Ready upload Spekar
        
        $emp = Emp::first();
        
        $processor2 = praapplication::where('id', '=', 5 )->orderBy('created_at', 'desc')->get();

        $processor3 = praapplication::where('stage', '=', 'W1' )->orderBy('created_at', 'desc')->get();

        $document1 = DocCust::latest('created_at')->where('type',  '4' )->first(); 
        
        //
        $workgroup = Model_has_role::where('role_id', 3)->get();
        $workgroupprocessor2 = Model_has_role::where('role_id', 6)->get();

        //routeto
        $meetcus = praapplication::where('routeto', $user)->where('stage', '=', 'W2')->orWhere('stage', 'W3')->orWhere('stage', 'W6')->orderBy('created_at', 'desc')->get(); 

        
        $process6 = praapplication::where('stage', 'W4')->orWhere('stage', 'W9')->orWhere('stage', 'W10')->orWhere('stage', 'W8')->where('routeto', $user)->orderBy('created_at', 'desc')->get();
        $workgroupprocess6 = Model_has_role::where('role_id', 7)->get();

         $step1 = praapplication::where('id_cus',$user_id)->first();
         $financial = Financial::where('user_id',$user_id)->first();
         $loanammount = LoanAmmount::where('id_praapplication',$user_id)->first();
        //
        $process7 = praapplication::where('stage', 'W5')->orWhere('stage', 'W7')->orWhere('stage', 'W9')->where('routeto', $user)->orderBy('created_at', 'desc')->get();
        $workgroupprocess6 = Model_has_role::where('role_id', 7)->get();
        $documentcust = DocCust::latest('created_at');


        //process11 
        $process11 = praapplication::where('stage', 'W8')->orWhere('stage', 'W11')->orderBy('created_at', 'desc')->get();


        return view('adminpage.loaneligi.index3', compact('praaplication','processor2', 'processor1', 'spekar', 'emp','reg', 'processor3','document1', 'workgroup', 'routeto', 'meetcus','workgroupprocessor2', 'process6', 'workgroupprocess6', 'process7', 'documentcust', 'process11','step1','financial','loanammount'));
    }


}
