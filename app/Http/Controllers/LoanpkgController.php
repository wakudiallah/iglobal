<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Loanpkg;
use App\Authorizable;
use Illuminate\Support\Facades\Auth;

class LoanpkgController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $package = Loanpkg::orderBy('LnPkg_Code', 'ASC')->get();
        
        return view('adminpage.setupmodule.packageloan.index', compact('package'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('adminpage.setupmodule.packageloan.new');
    }

    public function act($id)
    {
        $st         = Loanpkg::find($id);
        $st->Act = '1';
        $st->save();

        return redirect('loanpkg')->with(['update' => 'Loan Package successfully updated']);
    }

    public function act1($id)
    {
        $st         = Loanpkg::find($id);
        $st->Act = '0';
        $st->save();

        return redirect('loanpkg')->with(['update' => 'Loan Package successfully updated']);
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     
        $this->validate($request, [
            //'LnPkg_Code'  => 'required',
            'Ln_Desc'     => 'required',
            /*'Min_Ten'     => 'required',
            'Max_Ten'     => 'required',
            'Mln_amt'     => 'required|numeric|between:0.00,99.99',
            'Max_amt'     => 'required|numeric|between:0.00,99.99',
            'Max_amt'     => 'required',
            'Min_Service' => 'required',
            'Min_Age'     => 'required',
            'Max_Age'     => 'required',
            'Ln_Int'      => 'required|numeric|between:0.00,99.99'*/
        ]);

        $request->user()->loanpkg()->create($request->all());
        
        return redirect()->route('loanpkg.index')->with(['success' => 'Loan package data successfully saved']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $package = Loanpkg::where('id',$id)->first();   
                        
        return view('adminpage.setupmodule.packageloan.edit', compact('package'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Loanpkg $loanpkg)
    {
        $this->validate($request, [
            //'LnPkg_Code'  => 'required',
            'Ln_Desc'     => 'required',
            /*'Min_Ten'     => 'required',
            'Max_Ten'     => 'required',
            'Mln_amt'     => 'required|numeric|between:0.00,99.99',
            'Max_amt'     => 'required|numeric|between:0.00,99.99',
            'Max_amt'     => 'required',
            'Min_Service' => 'required',
            'Min_Age'     => 'required',
            'Max_Age'     => 'required',
            'Ln_Int'      => 'required|numeric|between:0.00,99.99'*/
        ]);

        $me = $request->user();

        if( $me->hasRole('Admin') ) {
            $loanpkg = Loanpkg::findOrFail($loanpkg->id);
        } else {
            $loanpkg = $me->loanpkg()->findOrFail($loanpkg->id);
        }

        $loanpkg->update($request->all());

        return redirect()->route('loanpkg.index')->with(['update' => 'Loan package data successfully updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $package = Loanpkg::where('id',$id)->delete();   
                        
        return redirect('loanpkg')->with(['delete' => 'Loan package data successfully deleted']);
    }
}
