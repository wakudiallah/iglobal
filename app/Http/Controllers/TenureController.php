<?php

namespace App\Http\Controllers;

use App\Tenure;
use Illuminate\Http\Request;

class TenureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tenure  $tenure
     * @return \Illuminate\Http\Response
     */
    public function show(Tenure $tenure)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tenure  $tenure
     * @return \Illuminate\Http\Response
     */
    public function edit(Tenure $tenure)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tenure  $tenure
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tenure $tenure)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tenure  $tenure
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tenure $tenure)
    {
        //
    }
}
