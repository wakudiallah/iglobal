<?php

namespace App\Http\Controllers;

use App\Employment;
use Illuminate\Http\Request;
use App\Authorizable;
use Illuminate\Support\Facades\Auth;

class EmploymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        
        $employment = Employment::orderBy('created_at', 'desc')->get();
        return view('adminpage.setupmodule.jenispekerjaan.index', compact('employment'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('adminpage.setupmodule.jenispekerjaan.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'code_employment' => 'required|unique:employment|max:4'
        ]);

        $request->user()->employment()->create($request->all());

        flash('Jenis Pekerjaan berjaya ditambah');

        return redirect()->route('employment.index')->with(['success' => 'Job Type data successfully saved']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employment  $employment
     * @return \Illuminate\Http\Response
     */
    public function show(Employment $employment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employment  $employment
     * @return \Illuminate\Http\Response
     */
    public function edit(Employment $employment)
    {
        $employment = Employment::findOrFail($employment->id);
        
        return view('adminpage.setupmodule.jenispekerjaan.edit', compact('employment'));


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employment  $employment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employment $employment)
    {
        $this->validate($request, [
            'name' => 'required',
            'code_employment' => 'required|max:4'
            
        ]);

        $me = $request->user();

        if( $me->hasRole('Admin') ) {
            $employment = Employment::findOrFail($employment->id);
        } else {
            $employment = $me->employment()->findOrFail($employment->id);
        }

        $employment->update($request->all());

        return redirect()->route('employment.index')->with(['update' => 'Job type data successfully updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employment  $employment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employment $employment)
    {
        $me = Auth::user();

        if( $me->hasRole('Admin') ) {
            $post = Employment::findOrFail($employment->id);
        } else {
            $employment = $me->employment()->findOrFail($employment->id);
        }

        $employment->delete();

        return redirect()->route('employment.index')->with(['delete' => 'Job type data successfully deleted']);
    }
}
