<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\praapplication;
use Illuminate\Support\Facades\Auth;
use App\Announcement;
use App\DocCust;
use DB;
use Mapper;
use DateTime;
use PDF;
use Hash;
use App\History;
use App\Remark;
use App\User;
use App\Role;
use App\Model_has_role;
use App\AddInfo;
use App\MeetCust;
use App\Package;
use App\Loanpkg;
use App\Employment;
use App\Emp;
use App\Homeimage;
use App\Tenure;
use App\Loan;
use App\LoanAmmount;
use Input;

use Artisaninweb\SoapWrapper\SoapWrapper;
use App\Soap\Request\GetConversionAmount;
use App\Soap\Response\GetConversionAmountResponse;
//use SoapClient;

class MoaqsController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function loginRequest(){
       
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     *

$xml = simplexml_load_string($source);
$xml->registerXPathNamespace('test', 'http://test.test.com/');
$elements = $xml->xpath('//soap:Envelope/soap:Body/test:GetInfoFromSendingResponse/test:GetInfoFromSendingResult');
$result = json_decode($elements[0], true);
print_r($result);
     *
     * 
     */
    public function index()
    {


$source = <<<SOURCE
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <GetDS4 xmlns="http://tempuri.org/WAPSWS/WAPSWS">
      <CommandString>select top 10 * from applpendingremark</CommandString>
    </GetDS4>
  </soap:Body>
</soap:Envelope>
SOURCE;


$xml = simplexml_load_string($source);
$xml->registerXPathNamespace('tempuri', 'http://tempuri.org/WAPSWS/WAPSWS');
$elements = $xml->xpath('//soap:Envelope/soap:Body/tempuri:GetDS4/tempuri:GetDS4');
$result = json_decode($elements, true);
print_r($result);


        return view('adminpage.moaqs.indexmoaqs', compact('result'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('adminpage.moaqs.moaqsbrowser');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
