<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\praapplication;
use App\Authorizable;
use Illuminate\Support\Facades\Auth;
use App\Package;
use App\Loanpkg;
use App\Employment;
use App\Emp;
use Ramsey\Uuid\Uuid;
use App\DocCust;
use App\DocAssest;
use App\User;
use App\History;
use App\Sendmbsb;
use App\LoanDetail;
use App\TenureDetail;
use Mail;
use App\Model_has_role;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Carbon\Carbon;
use Disk;
use App\Loanammount;
use DateTime;
use Response;
use Session;

class ReportManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user()->id;

        $reportmanag = praapplication::where('stage', 'W5' )->where('manager', $user)->orderBy('created_at', 'desc')->get();
        
        return view('manager.reject', compact('reportmanag', 'history'));
    }


    public function report_approved()
    {
        $user = Auth::user()->id;

        $reportmanag = praapplication::where('stage', 'W11' )->where('manager', $user)->orderBy('created_at', 'desc')->get();
        
        return view('manager.report_approved', compact('reportmanag', 'history'));
    }


    public function report_rejected()
    {
        $user = Auth::user()->id;

        $reportmanag = praapplication::where('stage', 'W12' )->where('manager', $user)->orderBy('created_at', 'desc')->get();
        
        return view('manager.report_rejected', compact('reportmanag', 'history'));
    }

    
    public function make_new_cus($id)
    {
        
        $make_new_cus = praapplication::where('id_cus', $id)->where('stage', 'W5' )->orderBy('created_at', 'desc')->get();
        
        return view('adminpage.reporting.reportmanager.index', compact('make_new_cus'));
    }

    public function rejected_perone($id)
    {
        
        $user = Auth::user()->id;

        $internal_cal = praapplication::where('stage', 'W5' )->where('id_cus', $id )->orderBy('created_at', 'desc')->get(); 
        $history = History::where('remark_id', 'W5' )->where('cus_id', $id)->latest('id')->first();
        
        return view('manager.reject_perone', compact('internal_cal', 'history'));
    }



    public function recommend_perone(Request $request, $id)
    {
        
        $user = Auth::user()->id;

        $recommend = $request->input('recommend');

        $today  = date('Y-m-d H:i:s');

        praapplication::where('id_cus', $id)->update(array('date_recommend' => $today, 'stage' => 'W15'));

        $request                  = new History;

        $request->cus_id          = $id;  
        $request->activity        = "";
        $request->remark_id       = 'W15';
        $request->user_id         = $user;
        $request->note            = $recommend;
        $request->save();
        
        return redirect('adminnew')->with(['update' => 'Data saved successfully']);
    }
 

    public function report_employer()
    {
        $all_employer = Emp::all();
        
        return view('manager.report_employer', compact('all_employer'));
    }


    public function report_employer_view(Request $request)
    {
            
            $user = Auth::user()->id;

            $all_employer = Emp::all();

            $tanggal1  = $request->input('tanggal1');
            $tanggal2  = $request->input('tanggal2');

            //$empname = Employment::latest('created_at')->where('id',$jenis_pekerjaan)->first();

            $status  = $request->input('status');
            $emp  = $request->input('emp');


            $viewdate1 = date("d-m-Y", strtotime($tanggal1));
            $viewdate2 = date("d-m-Y", strtotime($tanggal2));
            $date1 =  date('Y-m-d 00:00:01', strtotime($tanggal1)); 
            $date2 =  date('Y-m-d 23:59:59', strtotime($tanggal2)); 
            
               if($emp == 'W'){
                 $employer = Emp::all();
            }
            else{
                $employer = Emp::where('id',$emp)->get();
            }
            
            if($emp == 'W'){

              
            $pra = DB::table('emp')
            ->leftJoin('praapplication', 'emp.id', '=', 'praapplication.emp_code')
          ->select('emp.Emp_Desc','praapplication.stage',DB::raw("COUNT(praapplication.emp_code) AS total_s"))
          ->where('praapplication.manager', $user)->where('praapplication.updated_at','>=',$date1)->where('praapplication.updated_at','<=',$date2)->wherein('stage', ['W1','W8'])
            ->groupBy('emp.Emp_Desc','praapplication.stage')
            ->get();

             $all = DB::table('emp')
            ->leftJoin('praapplication', 'emp.id', '=', 'praapplication.emp_code')
          ->select('emp.Emp_Desc','praapplication.stage',DB::raw("COUNT(praapplication.emp_code) AS total_s"))
          ->where('emp.Emp_Desc','!=', 'praapplication.emp_code')
            ->groupBy('emp.Emp_Desc','praapplication.stage')
            ->get();

                $as= DB::table('emp')
              ->leftJoin('praapplication', 'emp.id', '=', 'praapplication.emp_code')
            ->select( 'emp.Emp_Desc', DB::raw("(CASE WHEN praapplication.updated_at >= '$date1' AND praapplication.updated_at <= '$date2' AND praapplication.stage = 'W1' OR praapplication.stage = 'W8' AND praapplication.manager= $user THEN 1 ELSE 0 END) AS total"))
             ->groupBy('emp.Emp_Desc','praapplication.stage','praapplication.manager','praapplication.updated_at')
            ->get();

             $a= DB::table('emp')
              ->leftJoin('praapplication', 'emp.id', '=', 'praapplication.emp_code')
            ->select( 'emp.Emp_Desc', DB::raw("sum(CASE WHEN (praapplication.updated_at >= '$date1' AND praapplication.updated_at <= '$date2') AND (praapplication.stage = 'W1' OR praapplication.stage = 'W8') AND (praapplication.manager = $user) THEN 1 ELSE 0 END) AS total"),DB::raw("sum(CASE WHEN (praapplication.updated_at >= '$date1' AND praapplication.updated_at <= '$date2') AND (praapplication.stage = 'W1') AND (praapplication.manager= $user) THEN 1 ELSE 0 END) AS assessment"), DB::raw("sum(CASE WHEN (praapplication.updated_at >= '$date1' AND praapplication.updated_at <= '$date2') AND (praapplication.stage = 'W8') AND (praapplication.manager= $user) THEN 1 ELSE 0 END) AS submission"))
             ->groupBy('emp.Emp_Desc')
            ->get();

               $assessment = praapplication::latest('updated_at')->where('manager', $user)->where('updated_at','>=',$date1)->where('updated_at','<=',$date2)->where('stage', 'W1')->where('emp_code',$emp)->count();
                $submission = praapplication::latest('updated_at')->where('manager', $user)->where('updated_at','>=',$date1)->where('updated_at','<=',$date2)->where('stage', 'W8')->count();
                }
            else{

                $pra = DB::table('emp')
                ->leftJoin('praapplication', 'emp.id', '=', 'praapplication.emp_code')
                ->select( DB::raw('emp.Emp_Desc,   count(praapplication.emp_code) AS total_s'))
                ->groupBy(
                    'emp.Emp_Desc')
                ->get();

                $assessment = praapplication::latest('updated_at')->where('manager', $user)->where('updated_at','>=',$date1)->where('updated_at','<=',$date2)->where('stage', 'W1')->where('emp_code',$emp)->count();
                $submission = praapplication::latest('updated_at')->where('manager', $user)->where('updated_at','>=',$date1)->where('updated_at','<=',$date2)->where('stage', 'W8')->where('emp_code',$emp)->count();
                $total = $assessment + $submission;
            }
            

           return view('manager.report_employer_view', compact('pra','user','tanggal1','tanggal2','date1','date2','viewdate1','viewdate2','sales','jenis_pekerjaan','employment','empname','status','s_string', 'emp', 'all_employer','employer','assessment','submission','total','all','a'));   
    }



    public function report_mo()
    {
        $user = Auth::user()->id;

        $all_employer = Emp::all();
        $user         = User::where('manager', $user)->get();

        //$user = User::latest()->where('email','<>', 'wakudiallah05@gmail.com')->orderBy('id', 'DESC')->paginate();
        
        return view('manager.report_mo', compact('user'));
    }

    
    public function report_mo_view(Request $request)
    {
        $users = Auth::user()->id;

        $all_employer = Emp::all();
        $user         = User::where('manager', $users)->get();
        
        //$user = User::latest()->where('email','<>', 'wakudiallah05@gmail.com')->orderBy('id', 'DESC')->paginate();
        $tanggal1  = $request->input('tanggal1');
        $tanggal2  = $request->input('tanggal2');

            //$empname = Employment::latest('created_at')->where('id',$jenis_pekerjaan)->first();

        $status  = $request->input('status');
        $emp  = $request->input('emp');


        $viewdate1 = date("d-m-Y", strtotime($tanggal1));
        $viewdate2 = date("d-m-Y", strtotime($tanggal2));
        $date1 =  date('Y-m-d 00:00:01', strtotime($tanggal1)); 
        $date2 =  date('Y-m-d 23:59:59', strtotime($tanggal2)); 

        if($status == 'W11'){
            $pra = praapplication::latest('updated_at')->where('updated_at','>=',$date1)->where('updated_at','<=',$date2)->where('stage', 'W11')->get();
        }
        else{
            $pra = praapplication::latest('updated_at')->where('updated_at','>=',$date1)->where('updated_at','<=',$date2)->where('stage', 'W12')->get();
        }

        return view('manager.report_mo_view', compact('user','all_employer','emp','tanggal1','tanggal2','date1','date2','viewdate1','viewdate2', 'pra'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function report_processor4()
    {
        $report = praapplication::where('stage', 'W12' )->orWhere('stage', 'W11')->orderBy('created_at', 'desc')->get();
        
        return view('adminpage.reporting.disbursement.index', compact('report'));
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
