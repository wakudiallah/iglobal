<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\praapplication;
use App\Authorizable;
use Illuminate\Support\Facades\Auth;
use App\Package;
use App\Loanpkg;
use App\Employment;
use App\Emp;
use Ramsey\Uuid\Uuid;
use App\DocCust;
use App\DocAssest;
use App\User;
use App\History;
use App\Sendmbsb;
use Mail;
use App\Model_has_role;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Carbon\Carbon;
use App\Loanammount;
use DateTime;
use Response;
use Session;
use Exception;
use PhpOffice\PhpWord\PhpWord;
use PDF;
use App\MeetCust;
use App\LoanDetail;
use App\TenureDetail;
use App\Additionaldoc;


class PMOController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user       = User::where('status', 1)->get();

        $workgroup  = Model_has_role::where('role_id', 3)->get();


        $employment = Employment::all();
        $emp        = Emp::all();
        $loanpkg    = Loanpkg::all();
        $assessment = praapplication::orderBy('created_at', 'desc')->get();

        return view('adminpage.customer.add_by_mo', compact('assessment', 'employment', 'emp', 'loanpkg', 'workgroup'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function form_upload($id)
    {
        $reg = praapplication::latest('id')->limit('1')->first();
        $ids = $reg->id_cus;
        $document1 = DocCust::latest('created_at')->where('cus_id',$id)->where('type','1')->first();
        $document2 = DocCust::latest('created_at')->where('cus_id',$id)->where('type','2')->first();
        
        return view('mo.first_upload_ic_loc', compact('reg', 'document1', 'document2'));
    }
    
    public function post_uploadic(Request $request, $id)
    {
        $user = Auth::user()->id;
        //documents\user_doc\240593
        //$id      = $request->input('id');
        $ic = $request->input('ic');
        $destinationPath = 'documents/user_doc/'.$ic;
        $file = $request->input('fileIc');

        if($request->hasFile('fileIc')) {
            $file = $request->file('fileIc');
            $extension = $file->getClientOriginalExtension();
            $file_name =  $ic. '-IC.' . $extension;
            
            $file->move($destinationPath, $file_name );
        }
        
            $upload_file = $file_name;

            $document               = new DocCust;
            $document->cus_id       = $id;
            $document->doc_pdf      = $upload_file;
            $document->type         = '1';
            $document->user_id      = $user;

            $document->save();

        
        return redirect('/upload/doc/loc/ic/'.$id)->with(['update' => 'Copy of IC saved successfully']);
    }

    public function post_uploadloc(Request $request, $id)
    {
        $user = Auth::user()->id;
        //documents\user_doc\240593
        //$id      = $request->input('id');
        $ic = $request->input('ic');
        $destinationPath = 'documents/user_doc/'.$ic;
        $file = $request->input('fileLoc');

        if($request->hasFile('fileLoc')) {
            $file = $request->file('fileLoc');
            $extension = $file->getClientOriginalExtension();
            $file_name =  $ic. '-LOC.' . $extension;
            
            $file->move($destinationPath, $file_name );
        }
        
            $upload_file = $file_name;

            $document               = new DocCust;
            $document->cus_id       = $id;
            $document->doc_pdf      = $upload_file;
            $document->type         = '2';
            $document->user_id      = $user;

            $document->save();

        
        return redirect('/upload/doc/loc/ic/'.$id)->with(['update' => 'Copy of assessment saved successfully']);
    }
    
    

    public function meetcus_post_1(Request $request, $id)
    {
            $user = Auth::user()->id;
            
            $tanggal1  = $request->input('disb');
            $basic_salary  = $request->input('basic_salary');
            $allowance  = $request->input('allowance');
            $total_deduction  = $request->input('total_deduction');
            $loan_amount  = $request->input('loan_amount');

            
            $date     =  date('Y-m-d 00:00:01', strtotime($tanggal1));




            MeetCust::where('cus_id', $id)->update(array('existing' => $request->input('group2'), 'date_disbustment' => $date));

            praapplication::where('id_cus', $id)->update(array('gaji_asas' => $basic_salary));

            return redirect('/excel-file/'.$id);            

            //return redirect('/tenos/custtenos/'.$id);  //ini tenus lama
            
    }

    public function excel_file(Request $request, $id)
    {
        $user = Auth::user()->id;

        return view('mo.excel_file');
    }

    


     public function resulttenus($id)
    {
        $package    = Package::all();
        $employment = Employment::all();
        $loanpkg    = Loanpkg::all();
        $emp        = Emp::all();
        $pra        = Praapplication::where('id_cus', $id)->first(); 

        $id_type      = $pra->employment_code;
        $total_salary = $pra->gaji_asas + $pra->elaun ;
        $zbasicsalary = $pra->gaji_asas + $pra->elaun ;
        $zdeduction   = $pra->pot_bul;

        $loan = LoanDetail::where('id_emp_type',$id_type)
                ->where('min_salary','<=',$total_salary)
                ->where('max_salary','>=',$total_salary)->limit('1')->get();

        $id_loan  = $loan->first()->id;  
        
        $icnumber = $pra->first()->icnumber;
        $tanggal  = substr($icnumber,4, 2);
        $bulan    = substr($icnumber,2, 2);
        $tahun    = substr($icnumber,0, 2); 

        if($tahun > 30) {
            $tahun2 = "19".$tahun;
        }
        else {
             $tahun2 = "20".$tahun;
        }
       
        $lahir          = $tahun2.'-'.$bulan.'-'.$tanggal; 
        $lahir          =  date('Y-m-d', strtotime($lahir. ' - 16 month'));
        $oDateNow       = new DateTime();
        $oDateBirth     = new DateTime($lahir);
        $oDateIntervall = $oDateNow->diff($oDateBirth);

        $umur = 61 - $oDateIntervall->y;

        $tenure = TenureDetail::where('id_loan',$id_loan)->where('years','<', $umur)->where('years','!=','1')->get();

        $today  = date('Y-m-d H:i:s');  //today 
        praapplication::where('id_cus', $id)->update(array( 'date_pending_doc' => $today));

        return view('adminpage.customer.tenos', compact('package', 'employment', 'loan', 'emp', 'pra', 'loan', 'tenure', 'zbasicsalary', 'zdeduction', 'total_salary','id_loan'));
    }



    public function mo_cal_check(Request $request)
    {
        $user = Auth::user()->id;

        $meetcus = praapplication::where('process2', $user)->where('stage', 'W2' )->orderBy('created_at', 'desc')->get(); 

        $workgroupprocessor2 = Model_has_role::where('role_id', 6)->get();

        return view('mo.loan_checks', compact('meetcus', 'workgroupprocessor2'));
        
    }

    
    public function mo_cal_check_perone($id)
    {
        $user = Auth::user()->id;

        $meetcus = praapplication::where('process2', $user)->where('stage', 'W2')->where('id_cus', $id)->orderBy('created_at', 'desc')->get(); 

        $workgroupprocessor2 = Model_has_role::where('role_id', 6)->get();

        return view('mo.loan_checks_perone', compact('meetcus', 'workgroupprocessor2'));
    }

    public function cal_change($id)
    {
        $user = Auth::user()->id;

        $meetcus = praapplication::where('process2', $user)->Where('stage', 'W6')->where('id_cus', $id)->orderBy('created_at', 'desc')->first(); 

        $package    = Package::all();
        $employment = Employment::all();
        $loanpkg    = Loanpkg::all();
        $emp        = Emp::all();

        $workgroupprocessor2 = Model_has_role::where('role_id', 6)->get();

        return view('mo.loan_cal_n_change', compact('meetcus', 'workgroupprocessor2', 'emp', 'loanpkg', 'employment','package' ));
    }

    public function nextcalchange()
    {
        $user = Auth::user()->id;
        
        $change = praapplication::where('process2', $user)->where('stage', 'W6')->orderBy('created_at', 'desc')->get();


        return view('mo.change_calculation', compact('change'));
    }

    public function doc_incomplete($id)
    {
        $user = Auth::user()->id;

        $docin = praapplication::where('process2', $user)->wherein('stage',['W7', 'W13'])->where('id_cus', $id)->orderBy('created_at', 'desc')->get(); 

        return view('mo.docincomplete', compact('docin'));            
    }

    public function doc_incomplete_menu()
    {
        $user = Auth::user()->id;

        $docin = praapplication::where('process2', $user)->where('stage','W7')->orderBy('created_at', 'desc')->get(); 

        return view('mo.docincomplete', compact('docin'));            
    }
    
    
    public function doc_additional_perone($id)
    {
        $user = Auth::user()->id;

        $docin = praapplication::where('process2', $user)->Where('stage', 'W13')->where('id_cus', $id)->orderBy('created_at', 'desc')->get(); 

        return view('mo.docincomplete', compact('docin'));  
    }


    public function doc_additional()
    {
        $user = Auth::user()->id;

        $docin = praapplication::where('process2', $user)->Where('stage', 'W13')->orderBy('created_at', 'desc')->get(); 

        return view('mo.doc_additional', compact('docin'));  

        
    }

    public function upload_additional(Request $request, $id)
    {
        $user = Auth::user()->id;

        $ic      = $request->input('ic');
        $name      = $request->input('name');
        $id_cus      = $request->input('id_cus');

        $destinationPath = 'additional/'.$ic.'/';
        $name      = $request->input('name');

        if($request->hasFile('fileToUpload')) {
            $file = $request->file('fileToUpload');
            $extension = $file->getClientOriginalExtension();
            $file_name = 'Additional-Doc-'. $name. '.' . $extension;
            
            $file->move($destinationPath, $file_name );
        }
        
            $upload_file = $file_name;

            $document               = new Additionaldoc;
            $document->cus_id       = $id_cus;
            $document->document      = $upload_file;
            $document->type         = '99';
            $document->user_id      = $user;
        
            $document->save();
        
        return redirect('additional_doc')->with(['update' => 'Data saved successfully']);
        
    }

    




    public function pending_doc()
    {
        $user = Auth::user()->id;

        $meetcus = praapplication::where('process2', $user)->Where('stage', 'W3')->orderBy('created_at', 'desc')->get();
        $workgroupprocessor2 = Model_has_role::where('role_id', 6)->get(); 

        foreach ($meetcus as $loanammount) {
            $loanammount = LoanAmmount::where('id_praapplication', $loanammount->id_cus)->latest('id')->limit('1')->first(); 
        }


        return view('mo.pending_doc', compact('meetcus', 'workgroupprocessor2', 'loanammount'));  
    }


    public function pendingdoc_perone($id)  
    {
        $user = Auth::user()->id;

        $meetcus = praapplication::where('process2', $user)->Where('stage', 'W3')->Where('id_cus', $id)->orderBy('created_at', 'desc')->get();
        $loanammount = LoanAmmount::where('id_praapplication', $id)->latest('id')->limit('1')->first(); 

        $workgroupprocessor2 = Model_has_role::where('role_id', 6)->get(); 

        return view('mo.pending_doc_perone', compact('meetcus', 'workgroupprocessor2', 'loanammount'));  
    }

    public function doc_iclocreject($id)  
    {
        $user = Auth::user()->id;

        $meetcus = praapplication::where('process2', $user)->Where('stage', 'W14')->Where('id_cus', $id)->orderBy('created_at', 'desc')->get();

        $reg = praapplication::latest('id')->Where('id_cus', $id)->limit('1')->first();

        return view('mo.ic_loc_not_complete', compact('meetcus', 'workgroupprocessor2', 'reg'));

    }

    public function update_locic($id)  
    {
        $user = Auth::user()->id;

        praapplication::where('id_cus', $id)->update(array('stage' => 'W0' ));

        $request                  = new History;

        $request->cus_id          = $id;  
        $request->activity        = "1";
        $request->remark_id       = 'W0';
        $request->user_id         = $user;
        $request->note            = '';
        $request->save();

        return redirect('adminnew')->with(['update' => 'Data updated successfully']);
    }
    
    public function passreject_mo(Request $request,$id)
    {
        $user = Auth::user()->id;

        $stage           = $request->input('moremark');
        $manager           = $request->input('mng');
        $note            = $request->input('note');

        praapplication::where('id_cus', $id)->update(array('process5' => $user, 'stage' => $stage, 'reject_mo' =>  $manager ));


        $request                  = new History;

        $request->cus_id          = $id;  
        $request->activity        = "5";
        $request->remark_id       = $stage;
        $request->user_id         = $user;
        $request->note            = $note;
        $request->save();

        return redirect('pendingdoc')->with(['update' => 'Data saved successfully']);
    }


    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
