<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\LeaderMO;
use App\Manager;
use App\Permission;
use App\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Model_has_role;
class MOController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $user = DB::table('users')
                    ->join('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')
                    ->join('roles', 'roles.id', '=', 'model_has_roles.role_id')
                    ->where('model_has_roles.role_id',  5)
                    ->select('users.id AS id', 'users.name AS name', 'users.email AS email', 'users.status AS status', 'users.status_head AS status_head', 'users.leader AS leader', 'users.manager AS manager','users.name_manager AS name_manager')
                    ->paginate();

         $name_manager = User::get();

         $result = User::get();          
                    
        return view('adminpage.user.mo.index', compact('result','name_manager'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::where('id', '3')->pluck('name', 'id');
        $team_leader = DB::table('users')
                    ->join('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')
                    ->join('roles', 'roles.id', '=', 'model_has_roles.role_id')
                    ->where('model_has_roles.role_id',  12)
                    ->select('users.id AS id', 'users.name AS name', 'users.email AS email', 'users.status AS status', 'users.status_head AS status_head', 'users.leader AS leader', 'users.manager AS manager','users.name_manager AS name_manager')
                    ->paginate();

        $manager = DB::table('users')
                    ->join('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')
                    ->join('roles', 'roles.id', '=', 'model_has_roles.role_id')
                    ->where('model_has_roles.role_id',  5)
                    ->select('users.id AS id', 'users.name AS name', 'users.email AS email', 'users.status AS status', 'users.status_head AS status_head', 'users.leader AS leader', 'users.manager AS manager','users.name_manager AS name_manager')
                    ->paginate();


        return view('adminpage.user.mo.new', compact('roles', 'team_leader', 'manager'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'bail|required|min:2',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'roles' => 'required|min:1',
            'leader' => 'nullable|min:1',
            'status_head' => 'nullable',
            'manager' => 'nullable',
            'leader' => 'nullable',
        ]);

        $request->merge(['password' => bcrypt($request->get('password'))]);

        // Create the user
        if ( $user = User::create($request->except('roles', 'permissions')) ) {

            $this->syncPermissions($request, $user);

            flash('User has been created.');

        } else {
            flash()->error('Unable to create user.');
        }


        //cek apakah input head 1 atau 0  belum bisa guna
        /*if($request->input('head') == 1){
            $name = $request->input('name');

            $doc               = new LeaderMO;
            $doc->leader_name  = $name;
            
            $doc->save();

        }*/

        return redirect()->route('mo.index')->with(['success' => 'User MO successfully saved']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response  php 
     */
    
    public function add_leader($id)
    {
        $leader = User::get();
        $detail = User::where('id', '=', $id)->first();

        return view('adminpage.user.mo.add_leader', compact('leader','detail'));

    }

    
    public function post_leader(Request $request)
    {
        $id = $request->input('id');

        $st = User::find($id);
        $st->leader = $request->input('leader');
        $st->name_leader = $request->input('name_mo');
        $st->save();

        return redirect('mo')->with(['update' => 'User MO successfully updated']);
    }

    public function updatestatus1($id)
    {
        $st = User::find($id);
        $st->status = '1';
        $st->save();

        return redirect('mo')->with(['update' => 'User MO successfully updated']);
    }

    public function updatestatus2($id)
    {
        $st = User::find($id);
        $st->status = '0';
        $st->save();

        return redirect('mo')->with(['update' => 'User MO successfully updated']);
    }

    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $mo = User::where('id',$id)->first();
        //$roles = Role::where('id','=', '3')->pluck('name', 'id');
        $roles = Role::where('id', '!=', 3)->pluck('name', 'id');
        //$team_leader = User::get();
        
        //$manager_sekarang =  $request->input('id_cus');
        $team_leader = DB::table('users')
                    ->join('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')
                    ->join('roles', 'roles.id', '=', 'model_has_roles.role_id')
                    ->where('model_has_roles.role_id',  12)
                    ->select('users.id AS id', 'users.name AS name', 'users.email AS email', 'users.status AS status', 'users.status_head AS status_head', 'users.leader AS leader', 'users.manager AS manager','users.name_manager AS name_manager')
                    ->paginate();

        $manager = DB::table('users')
                    ->join('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')
                    ->join('roles', 'roles.id', '=', 'model_has_roles.role_id')
                    ->where('model_has_roles.role_id',  5)
                    ->select('users.id AS id', 'users.name AS name', 'users.email AS email', 'users.status AS status', 'users.status_head AS status_head', 'users.leader AS leader', 'users.manager AS manager','users.name_manager AS name_manager')
                    ->paginate();

        //$manager = User::all()->pluck('name', 'id');
        //$user_all = User::get();
        //$user->model->role->id;
        //$user         = $user_all->model->role->id;

        return view('adminpage.user.mo.edit', compact('roles', 'mo', 'team_leader', 'manager', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'bail|required|min:2',
            'email' => 'required|email|unique:users,email,' . $id,
            'password' => 'required|min:6',
            'roles' => 'required|min:1',
            'leader' => 'nullable|min:1',
            'status_head' => 'nullable',
            'manager' => 'nullable',
            'leader' => 'nullable',
        ]);

        // Get the user
        $user = User::findOrFail($id);

        // Update user
        $user->fill($request->except('roles', 'permissions', 'password'));

        // check for password change
        if($request->get('password')) {
            $user->password = bcrypt($request->get('password'));
        }

        // Handle the user roles
        $this->syncPermissions($request, $user);

        $user->save();

        return redirect()->route('mo.index')->with(['update' => 'User data successfully updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function syncPermissions(Request $request, $user)
    {
        // Get the submitted roles
        $roles = $request->get('roles', []);
        $permissions = $request->get('permissions', []);

        // Get the roles
        $roles = Role::find($roles);

        // check for current role changes
        if( ! $user->hasAllRoles( $roles ) ) {
            // reset all direct permissions for user
            $user->permissions()->sync([]);
        } else {
            // handle permissions
            $user->syncPermissions($permissions);
        }

        $user->syncRoles($roles);

        return $user;
    }

    public function add_manager($id)
    {
        $manager = User::get();
        $detail = User::where('id', '=', $id)->first();

        $users          = User::get();        
        //$roless         = $users->model->role->id;

        $role  = Model_has_role::where('role_id', 5)->get();
        
        $mng = Manager::get(); 



        return view('adminpage.user.mo.add_manager', compact('manager','detail', 'mng'));
    }

    
    public function post_manager(Request $request)
    {
        $id = $request->input('id');
        $mg =  $request->input('manager');

        $st = User::find($id);
        $name = User::where('id',$mg)->limit(1)->first();

        $st->manager      = $request->input('manager');
        $st->name_manager = $name->name;
        $st->save();

        return redirect('mo')->with(['update' => 'User Manager successfully updated']);
    }
}
