<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Authorizable;
use Illuminate\Support\Facades\Auth;
use App\Emp;
class EmpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $emp = Emp::orderBy('created_at', 'desc')->get();

        return view('adminpage.setupmodule.emp.index', compact('emp'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('adminpage.setupmodule.emp.new');
    }

    public function act($id)
    {
        $st      = Emp::find($id);
        $st->act = '1';
        $st->save();

        return redirect('emp')->with(['update' => 'Employer data successfully updated']);
    }

    public function act1($id)
    {
        $st      = Emp::find($id);
        $st->act = '0';
        $st->save();

        return redirect('emp')->with(['update' => 'Employer data successfully updated']);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'Emp_Code' => 'required|unique:emp|max:4',
            'Emp_Desc' => 'required|max:91', 
            'Act'      => 'nullable'
        ]);

        $request->user()->emp()->create($request->all());

        return redirect()->route('emp.index')->with(['success' => 'Employer data successfully saved']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function emp_show(Request $request, $id)
    {
        /*$data = Emp::find($id);
        $data->Act  = $request->status;
        $data->save();*/

       
         $ids = Emp::findOrFail($id);
         $act = $request->input('status');
        $emp= Emp::where('id',$id)->update(["Act" => $act]);

        return redirect()->route('emp.index')->with(['success' => 'Employer data successfully updated']);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $emp = Emp::where('id',$id)->first();   

        return view('adminpage.setupmodule.emp.edit', compact('emp'));
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Emp $emp)
    {
        $this->validate($request, [
            'Emp_Code' => 'required|max:4',
            'Emp_Desc' => 'required|max:91', 
            'Act'      => 'nullable'
        ]);

        $me = $request->user();

        if( $me->hasRole('Admin') ) {
            $emp = Emp::findOrFail($emp->id);
        } else {
            $emp = $me->emp()->findOrFail($emp->id);
        }

        $emp->update($request->all());

        flash()->success('Majikan telah berjaya dikemaskini.');

        return redirect()->route('emp.index')->with(['success' => 'Employer data successfully updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $emp = Emp::where('id',$id)->delete();   
                        
        return redirect('emp')->with(['delete' => 'Employer data successfully deleted']);
    }
}
