<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Announcement;
use App\Role;
use App\AnnouncementGroup;
use App\Model_has_role;
use App\User;
use App\Authorizable;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;


class AnnouncementGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $announc = AnnouncementGroup::latest()->get();;

        return view('adminpage.announcement_group.index', compact('announc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('name', 'id');

        $manager  = Model_has_role::where('role_id', 5)->get();
        

        return view('adminpage.announcement_group.new', compact('roles', 'manager'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'   => 'required|max:2000',
            'description'    => 'required|max:2000',
            'attach'  => 'nullable|max:2025',
            'group_id' => 'required',
            'status'     => 'required'
        ]);
 
            $user = Auth::user()->id;

        if (empty($request->file('attach'))){
            

            $data          = new AnnouncementGroup;
            
            $data->title   = $request->input('title');
            $data->description    = $request->input('description');
            $data->status     = $request->input('status');
            $data->group_id  = $request->input('group_id');
            $data->attach  = "NULL";
            $data->user_id = $user;

            $data->save();
        }
        else{

            $fileName = $request->file('attach')->getClientOriginalName();
            $destinationPath =base_path().'/public/file_announcement/';
            $proses = $request->file('attach')->move($destinationPath, $fileName);

            $data          = new AnnouncementGroup;
            
            $data->title   = $request->input('title');
            $data->description    = $request->input('description');
            $data->status  = $request->input('status');
            $data->attach  = $fileName;
            $data->group_id  = $request->input('group_id');
            $data->user_id = $user;
            $data->save();
        }
        
        return redirect('announcementgroups')->with(['success' => 'Announcement Groups data successfully Saved']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function update1($id)
    {
        $st = AnnouncementGroup::find($id);
        $st->status = '1';
        $st->save();
                        
        return redirect('announcementgroups')->with(['update' => 'Announcement Groups data successfully updated']);
 
    }

    public function update0($id)
    {
        $st = AnnouncementGroup::find($id);
        $st->status = '0';
        $st->save();

        return redirect('announcementgroups')->with(['update' => 'Announcement Groups data successfully updated']);
    }


    public function show($id, $view=FALSE)
    {
                    
        $announc =  AnnouncementGroup::latest('id')->where('id','=',$id)->where('status', 1)->first();
        $other = AnnouncementGroup::latest('id')->where('id','<>',$id)->where('status',  1)->paginate(5);

        return view('adminpage.announcement_group.show', compact('announc','other'));
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $announc = AnnouncementGroup::where('id',$id)->first();  
        $manager  = Model_has_role::where('role_id', 5)->get();

        return view('adminpage.announcement_group.edit', compact('announc','manager'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'   => 'required|max:2000',
            'description'    => 'required|max:2000',
            'attach'  => 'nullable|max:2025',
            'group_id' => 'required',
            'status'     => 'required'
        ]);

        $user = Auth::user()->id;

        $data = AnnouncementGroup::findOrFail($id);
        $data->title = $request->input('title');
        $data->description = $request->input('description');
        $data->group_id = $request->input('group_id');
        $data->status = $request->input('status');
        $data->user_id = $user;

        if (empty($request->file('attach'))){
            $data->attach = $data->attach;
        }
        else{
            $fileName = $request->file('attach')->getClientOriginalName();
            $destinationPath =base_path().'/public/file_announcement/';
            $proses = $request->file('attach')->move($destinationPath, $fileName);

            $data->attach  = $fileName;    
        }
        $data->save();
        
        
        return redirect('announcementgroups')->with(['success' => 'Announcement Groups data successfully Saved']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $emp = AnnouncementGroup::findOrFail($id)->delete();   
                        
        return redirect('announcementgroups')->with(['delete' => 'Announcement Groups data successfully deleted']);
    }
}
