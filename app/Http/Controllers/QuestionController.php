<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Announcement;
use App\Question;
use App\Role;
use App\Permission;
use App\Authorizable;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $question = Question::latest()->get();

        return view('adminpage.setupmodule.question.index', compact('question'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('adminpage.setupmodule.question.new', compact('question'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'question'    => 'required|max:2000',
            'status'     => 'required'
        ]);
        
            $user           = Auth::user()->id;
            
            $data           = new Question;
            
            $data->question = $request->input('question');
            $data->status   = $request->input('status');
            $data->user_id  = $user;
            
            $data->save();
        
        
        return redirect('question')->with(['success' => 'Question data successfully Saved']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $question = Question::where('id', $id)->limit(1)->first();

        return view('adminpage.setupmodule.question.edit', compact('question'));
    }

    public function update1($id)
    {
        $st = Question::find($id);
        $st->status = '1';
        $st->save();
                        
        return redirect('question')->with(['update' => 'Question data successfully updated']);
    }

    public function update0($id)
    {
        $st = Question::find($id);
        $st->status = '0';
        $st->save();
                        
        return redirect('question')->with(['update' => 'Question data successfully updated']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'question'    => 'required|max:2000',
            'status'     => 'required'
        ]);
        
            $user           = Auth::user()->id;
            
            $data = Question::findOrFail($id);
            
            $data->question = $request->input('question');
            $data->status   = $request->input('status');
            $data->user_id  = $user;
            
            $data->save();


        
        return redirect('question')->with(['success' => 'Question data successfully Saved']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $question = Question::findOrFail($id)->delete();   
                        
        return redirect('question')->with(['delete' => 'Question data successfully deleted']);
    }
}
