<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\praapplication;
use Illuminate\Support\Facades\Auth;
use App\Announcement;
use App\DocCust;
use DB;
use Mapper;
use DateTime;
use PDF;
use Hash;
use App\History;
use App\Remark;
use App\User;
use App\Role;
use App\Model_has_role;
use App\AddInfo;
use App\MeetCust;
use App\Package;
use App\Loanpkg;
use App\Employment;
use App\Emp;
use App\Homeimage;
use App\Tenure;
use App\Loan;
use App\LoanAmmount;
class LoanEliController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        //$reg = praapplication::latest('created_at')->where('id_cus','=', 'a4985c50-aaa5-43ea-967a-9558220d02fb')->limit('1')->first();
        $user = Auth::user()->id;

        $praaplication = praapplication::orderBy('created_at', 'desc')->get();

        $processor1 = praapplication::where('stage', '=', 'W1' )->orderBy('created_at', 'desc')->get();
        
        $spekar = praapplication::where('stage', '=', 'W1' )->where('routeto', $user)->orderBy('created_at', 'desc')->get(); //Siap upload SPEKAR  //W2 Ready upload Spekar
        
        $emp = Emp::first();
        
        $processor2 = praapplication::where('id', '=', 5 )->orderBy('created_at', 'desc')->get();

        $processor3 = praapplication::where('stage', '=', 'W1' )->orderBy('created_at', 'desc')->get();

        $document1 = DocCust::latest('created_at')->where('type',  '4' )->first(); 
        
        //
        $workgroup = Model_has_role::where('role_id', 3)->get();
        $workgroupprocessor2 = Model_has_role::where('role_id', 6)->get();

        //routeto
        $meetcus = praapplication::where('routeto', $user)->where('stage', '=', 'W2')->orWhere('stage', 'W3')->orWhere('stage', 'W6')->orWhere('stage', 'W8')->orderBy('created_at', 'desc')->get();

        $process6 = praapplication::where('stage', 'W4')->where('routeto', $user)->orderBy('created_at', 'desc')->get();
        $workgroupprocess6 = Model_has_role::where('role_id', 7)->get();

        //
        $process7 = praapplication::where('stage', 'W5')->orWhere('stage', 'W7')->orWhere('stage', 'W9')->where('routeto', $user)->orderBy('created_at', 'desc')->get();
        $workgroupprocess6 = Model_has_role::where('role_id', 7)->get();
        $documentcust = DocCust::latest('created_at');



        return view('adminpage.loaneligi.index', compact('praaplication','processor2', 'processor1', 'spekar', 'emp','reg', 'processor3','document1', 'workgroup', 'routeto', 'meetcus','workgroupprocessor2', 'process6', 'workgroupprocess6', 'process7', 'documentcust'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function process6(Request $request, $id)
    {
        $user = Auth::user();

        $to = $request->input('routetoq');
        $stage = $request->input('pro2remark');
        
        
        praapplication::where('id_cus', $id)->update(array('routeto' => $to, 'stage' => $stage, 'process6' =>  $user->id));

        //update history jika W3 Internal Calculation Check dan W4 MO Reject
        
            $request                  = new History;

            $request->cus_id          = $id;  
            $request->activity        = "";
            $request->remark_id       = $stage;
            $request->user_id         = $user->id;
            $request->note            = "";
            $request->save();
        

        return redirect('loan-eli')->with(['update' => 'Data saved successfully']);
    }

    public function process7(Request $request, $id)
    {
        $user         = Auth::user()->id;
        $stage        = $request->input('pro3remark');
        
        $routomobalik = $request->input('routomobalik');

        $note = $request->input('note');
        

        //Check complete atau tidak 
        
        if($stage == 'W7'){  //Jika doc complete

            praapplication::where('id_cus', $id)->update(array('stage' => $stage, 'process7' =>  $user, 'doc' => '1', 'routeto' => $user));

            $doc1= DocCust::where('cus_id', $id)->where('type','1')->latest('created_at')->limit('1')->update(array('verification' => '1'));
            $doc2 = DocCust::where('cus_id', $id)->where('type','2')->latest('created_at')->limit('1')->update(array('verification' => '1'));
            $doc3 = DocCust::where('cus_id', $id)->where('type','4')->latest('created_at')->limit('1')->update(array('verification' => '1'));
        

        }elseif($stage == 'W8'){ //Doc tdk complete

            praapplication::where('id_cus', $id)->update(array('stage' => $stage, 'process7' =>  $user, 'doc' => '0', 'routeto' => $routomobalik));

            $doc1= DocCust::where('cus_id', $id)->where('type','1')->latest('created_at')->limit('1')->update(array('verification' => '0'));
            $doc2 = DocCust::where('cus_id', $id)->where('type','2')->latest('created_at')->limit('1')->update(array('verification' => '0'));
            $doc3 = DocCust::where('cus_id', $id)->where('type','4')->latest('created_at')->limit('1')->update(array('verification' => '0'));

        }

            $request                  = new History;

            $request->cus_id          = $id;  
            $request->activity        = "5";
            $request->remark_id       = $stage;
            $request->user_id         = $user;
            $request->note            = $note;
            $request->save();


        return redirect('loan-eli')->with(['update' => 'Data saved successfully']);
        
    }

    public function process8(Request $request, $id)
    {
        $user         = Auth::user()->id;

        $stage        = $request->input('pro3remark103');
        $telp        = $request->input('103checking');

        praapplication::where('id_cus', $id)->update(array('routeto' => $user, 'stage' => $stage, 'process8' =>  $user));

        $request                  = new AddInfo;
        $request->cus_id          = $id;
        $request->office_telp     = $telp;
        $request->office_telp2    = $telp;
        $request->user_id         = $user;
        $request->save();

        
        $request                  = new History;

        $request->cus_id          = $id;  
        $request->activity        = "8";
        $request->remark_id       = $stage;
        $request->user_id         = $user;
        $request->note            = '';
        $request->save();


        return redirect('loan-eli')->with(['update' => 'Data saved successfully']);
    }
    

    public function meetcus(Request $request, $id)
    {
            $user = Auth::user()->id;
            
            

            //save table
            
            $document                   = new MeetCust;
            $document->cus_id           = $id;
            $document->existing         = $request->input('group2');
            $document->date_disbustment = $request->input('disb');

            $document->save();

            //update history jika W3 Internal Calculation Check dan W4 MO   
            //
            //CEK LAGI KARENA PAGE BERUBAH ADANYA ---- TENOS ----
            
                /*$request                  = new History;

                $request->cus_id          = $id;  
                $request->activity        = "";
                $request->remark_id       = $stage;
                $request->user_id         = $user;
                $request->note            = "";

                $request->save();
            
            return redirect('loan-eli')->with(['update' => 'Data saved successfully']); */

            return redirect('/tenos/custtenos/'.$id);

            
    }

    
    public function passreject_mo(Request $request, $id)
    {
        $user = Auth::user()->id;
        
        $stage           = $request->input('moremark');
        $routeto         = $request->input('routetoq');

        praapplication::where('id_cus', $id)->update(array('process5' => $user, 'stage' => $stage, 'routeto' => $routeto  ));


        $request                  = new History;

        $request->cus_id          = $id;  
        $request->activity        = "5";
        $request->remark_id       = $stage;
        $request->user_id         = $user;
        $request->note            = '';
        $request->save();

        return redirect('loan-eli')->with(['update' => 'Data saved successfully']);

    }

    public function download_spekar(Request $request, $id)
    {
        $user = Auth::user()->id;

        $request                  = new History;

        $request->cus_id          = $id;  
        $request->activity        = "4";
        $request->remark_id       = "W91";
        $request->user_id         = $user;
        $request->note            = '';
        $request->save();

        $pra =      Praapplication::where('id_cus', $id)->first(); 


        //return redirect('loan-eli', compact('pra'));
        return response()->download('documents/user_doc/'.$pra->ic.'/'.$pra->spekar, $pra->spekar, [], 'inline');

    }


    public function kelayakantenos($id)
    {
        $package    = Package::all();
        $employment = Employment::all();
        $loanpkg    = Loanpkg::all();
        $emp        = Emp::all();
        $pra =      Praapplication::where('id_cus', $id)->first(); 

        return view('adminpage.customer.kelayakantenos', compact('package', 'employment', 'loanpkg', 'emp', 'pra'));
    }


    public function savekelayakantenos(Request $request)
    {
        
        $user         = Auth::user()->id;
        
        $id           = $request->input('id_cus');
        $pra          = praapplication::where('id_cus', $id)->first();
        $gaji_asas    = $request->gaji_asas;
        $elaun        = $request->elaun;
        $pot_bul      = $request->pot_bul;
        
        //$id_type      = $pra->emp_type;  //disini sebenarnya emp_code 
        $id_type      = $request->job_status;
        $total_salary = $gaji_asas + $elaun;
        $zbasicsalary = $gaji_asas + $elaun;
        $zdeduction   = $pot_bul;


        $loan = Loan::where('id_type',$id_type)
                ->where('min_salary','<=',$total_salary)
                ->where('max_salary','>=',$total_salary)->limit('1')->get();

        $id_loan= $loan->first()->id;  
        //$id_loan= $loan->id;  
        
        $icnumber = $pra->ic;
        $tanggal  = substr($icnumber,4, 2);
        $bulan    = substr($icnumber,2, 2);
        $tahun    = substr($icnumber,0, 2); 

        if($tahun > 30) {

            $tahun2 = "19".$tahun;
        }
        else {
             $tahun2 = "20".$tahun;
        }
        
        $lahir          = $tahun2.'-'.$bulan.'-'.$tanggal; 
        $lahir          =  date('Y-m-d', strtotime($lahir. ' - 16 month'));
        $oDateNow       = new DateTime();
        $oDateBirth     = new DateTime($lahir);
        $oDateIntervall = $oDateNow->diff($oDateBirth);

        $umur = 61 - $oDateIntervall->y;

        $tenure = Tenure::where('id_loan',$id_loan)->where('years','<', $umur)->get();   


         $workgroupprocessor2 = Model_has_role::where('role_id', 6)->get();
 
         
    praapplication::where('id_cus', $id)->update(array('employment_code' => $request->input('employment_code'), 'job_status' => $request->input('job_status'), 'gaji_asas' => $request->input('gaji_asas'), 'elaun' => $request->input('elaun'), 'pot_bul' => $request->input('pot_bul'), 'jml_pem' => $request->input('jml_pem')));

        return view('adminpage.customer.tenos', compact('pra','loan','total_salary','tenure','id','zbasicsalary' ,'zdeduction','user', 'workgroupprocessor2'));
       
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $user = Auth::user()->id;
        //documents\user_doc\240593
        $ic      = $request->input('ic');
        $id_cus = $request->input('id_cus');
        $destinationPath = 'documents/user_doc/'.$ic.'/';
        $name      = $request->input('name');

        if($request->hasFile('fileToUpload')) {
            $file = $request->file('fileToUpload');
            $extension = $file->getClientOriginalExtension();
            $file_name = 'Spekar-'. $name. '.' . $extension;
            
            $file->move($destinationPath, $file_name );
        }
        
            $upload_file = $file_name;

            $st = praapplication::find($id);  //gak bisa karena linknya ic
            $st->spekar = $upload_file;
            $st->process4 = $user;
            $st->save();

            $document               = new DocCust;
            $document->cus_id       = $id_cus;
            $document->doc_pdf      = $upload_file;
            $document->type         = '4';
            $document->user_id      = $user;
        
            $document->save();
        
        return redirect('loan-eli')->with(['update' => 'Data saved successfully']);

    }

     public function save_tenure(Request $request)
    {
        $user = Auth::user()->id;
        //documents\user_doc\240593
        
        $id         = $request->input('id');
        $id_tenure  = $request->input('tenure');
        $maxloan    = $request->input('maxloan');
        $loanammount= $request->input('loanammount');

       
        LoanAmmount::where('id_praapplication', $id)->update(array('id_tenure' => $id_tenure, 'maxloan' => $maxloan, 'loanammount' =>  $loanammount));

        praapplication::where('id_cus', $id)->update(array('process5' => $user, 'stage' => 'W3' ));
        
        return redirect('loan-eli')->with(['update' => 'Data saved successfully']);

    }

    /**
     * Display the specified resource.
     *
     
$pra =  praapplication::latest('created_at')->where('id_cus','=',$cus_id)->limit('1')->first();
        $dcmt = DB::table('doc_cust')->select(DB::raw(" max(id) as id"))->where('cus_id',$cus_id)->groupBy('type')->pluck('id');
        $files = DocCust::whereIn('id', $dcmt)->get();

        $document1 = DocCust::latest('created_at')->where('cus_id',  $cus_id )->where('type',  '1' )->first();

     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($cus_id)
    {
        $pra =  praapplication::latest('created_at')->where('id_cus','=',$cus_id)->limit('1')->first();
        $dcmt = DB::table('doc_cust')->select(DB::raw(" max(id) as id"))->where('cus_id',$cus_id)->groupBy('type')->pluck('id');

        $files = DocCust::whereIn('id', $dcmt)->get();

        $document1 = DocCust::latest('created_at')->where('cus_id',  $cus_id )->where('type',  '4' )->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */ 
    public function routeto(Request $request, $id)
    {
            $user = Auth::user()->id;

            praapplication::where('id_cus', $id)->update(array('routeto' => $request->input('routetoq'), 'submission' => $user, 'stage' => 'W2', 'process4' => $user));

            //update history
            $request                  = new History;

            $request->cus_id          = $id;  
            $request->activity        = "";
            $request->remark_id       = 'W2';
            $request->user_id         = $user;
            $request->note            = "";

            $request->save();

            return redirect('loan-eli')->with(['update' => 'Data saved successfully']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function spekar_routeto(Request $request)
    {
        $user = Auth::user()->id;

        //$password = Hash::make($id_no); 
        //$password   = $this->generateStrongPassword('12',false,'ld');

        $process2 = $request->process2;
        $route    = $request->input('process2');
        $item     = array_map(null, $request->id, $request->process2);
        
            foreach($item as $val) {

                $pra = praapplication::Where('id',$val[0])->update([
                    "stage"       => 'W2',
                    "routeto"     => $val[1],
                    "process5"    => $user 
                ]);
            }

        $itemx     = array_map(null, $request->id, $request->ci);
            foreach($itemx as $his) {

                $request                  = new History;

                $request->cus_id          = $his[1];  
                $request->activity        = '5';
                $request->remark_id       = 'W2';
                $request->user_id         = $user;
                $request->save();
            }

            return redirect('loan-eli')->with(['update' => 'Data saved successfully']);
    }


}
