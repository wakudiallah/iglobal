<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\praapplication;
use App\Authorizable;
use Illuminate\Support\Facades\Auth;
use App\Package;
use App\Loanpkg;
use App\Employment;
use App\Emp;
use Ramsey\Uuid\Uuid;
use App\DocCust;
use App\DocAssest;
use App\User;
use App\History;
use App\AddInfo;
use App\Sendmbsb;
use Mail;
use App\Model_has_role;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Carbon\Carbon;
use App\Loanammount;
use App\Financial;
use App\MeetCust;
use DateTime;
use Response;
use Session;
use Exception;
use PhpOffice\PhpWord\PhpWord;
use PDF;
use App\Post;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Auth\Authenticatable;


class CustomerMoController extends Controller
{
    
    //use Authenticatable;
        //Authorizable;
    //use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user       = User::where('status', 1)->get();

        $workgroup  = Model_has_role::where('role_id', 3)->get();


        $employment = Employment::all();
        $emp        = Emp::all();
        $loanpkg    = Loanpkg::all();
        $assessment = praapplication::orderBy('created_at', 'desc')->get();

        return view('adminpage.customer.add_by_mo', compact('assessment', 'employment', 'emp', 'loanpkg', 'workgroup'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $icnumber = $request->input('ic');
        $name     = $request->input('name');
        $notelp   = $request->input('notelp');
        $email    = $request->input('email');
        $emp_code = $request->input('emp_code');
        $clerical = $request->input('clerical');



        $tanggal = substr($icnumber,4, 2);
        $bulan   = substr($icnumber,2, 2);
        $tahun   = substr($icnumber,0, 2); 

        if($tahun > 30) {
            $tahun2 = "19".$tahun;
        }
        else {
             $tahun2 = "20".$tahun;
        }
       
        $lahir          = $tahun2.'-'.$bulan.'-'.$tanggal;
        $lahir          =  date('Y-m-d', strtotime($lahir. ' - 16 month')); 
        
        $oDateNow       = new DateTime();
        $oDateBirth     = new DateTime($lahir);
        $oDateIntervall = $oDateNow->diff($oDateBirth);

        $umur =  $oDateIntervall->y;

        if($umur >= 60) {

            Session::flash('name', $name); 
            Session::flash('ic', $icnumber);
            Session::flash('notelp', $notelp); 
            Session::flash('email', $email); 
            Session::flash('emp_code', $emp_code);

            Session::flash('message', 'Sorry your age exceeds the limit!'); 
            Session::flash('alert-class', 'alert-warning'); 

            return redirect('/cus_mo/create')->with(['update' => 'Sorry, Age exceeds the limit']);

        }else{

            $user = Auth::user()->id;
            $manager = Auth::user()->manager;
            $team_lead = Auth::user()->leader;
            

            $id                    = Uuid::uuid4()->tostring();
            $data                  = new praapplication;
            $data->id_cus          = $id;
            $data->name            = $request->name;
            $data->ic              = $request->ic;
            $data->old_ic          = $request->old_ic; 
            $data->notelp          = $request->notelp;
            $data->email           = $request->email;
            $data->emp_code        = $request->emp_code;
            $data->process2        = $user;
            $data->stage           = 'W0';  //W0 = new app
            $data->send            = '0';
            $data->assessment      = $user; //siapa yg isi assessment
            $data->manager         = $manager;
            $data->team_lead       = $team_lead;
            $data->user_id         = $user;
            $data->latitude        = $request->latitude;
            $data->longitude       = $request->longitude;
            $data->location        = $request->location;
            $data->biro_type       = $request->biro;

            if($emp_code == 2){  //Maybank 
               $data->clerical_type   = $request->clerical;
            }
            elseif($emp_code == 1){ //Others
                $data->emp_others = $request->others_employer;
            }
            else{
                $data->clerical_type   = '0';
            }
            
            $data->save();

            
            
            $request                    = new MeetCust;
            $request->cus_id = $id;
            $request->save();

            $request                    = new Loanammount;
            $request->id_praapplication = $id;
            $request->save();

            $request                    = new Financial; //belum dulu
            $request->user_id = $id;
            $request->save();

            $request         = new AddInfo;
            $request->cus_id = $id;
            $request->save();

            
            $request                  = new History;

            $request->cus_id          = $id;
            $request->name            = $request->name;
            $request->activity        = "1";
            $request->remark_id       = "W0";
            $request->user_id         = $user;
            $request->note            = "";

            $request->save();

            return redirect('/upload/doc/loc/ic/'.$id);
            //return redirect('/assessment/kira/'.$id);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
