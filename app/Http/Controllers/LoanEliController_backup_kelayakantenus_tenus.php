public function post_tenus_step1(Request $request)
    { 
        
        $id_cus    = $request->input('id_cus');


        //--------------- -----  Proceed ---------- */
        
        $basicsalary = $request->input('gaji_asas');
        $allowance   = $request->input('elaun');
        $deduction   = $request->input('pot_bul');
        $loanAmount  = $request->input('jml_pem');
        $package     = $request->input('Package');
        $employment_code     = $request->input('Employment');

        $gaji_tambah_tunjangan = $basicsalary + $allowance; 

        if($gaji_tambah_tunjangan <= 3500){  //3500 = had sementara


                   
            return redirect('tenos/custtenos/'.$id_cus)->with(['update' => 'Sorry you are not eligible to apply, your deduction or funding exceeds the limit']);
        }
        else{
            if(($loanAmount < 50000) || ($loanAmount > 250000)){
                return redirect('tenos/custtenos/'.$id_cus)->with(['update' => 'Loan Amount at least RM 50000 and less than RM 250000']);
            }
            else{
                  $pra          =      Praapplication::where('id_cus', $id_cus)->first(); 

        $id_type      = $employment_code;
        $total_salary = $basicsalary + $allowance ;
        $zbasicsalary = $basicsalary + $allowance;
        $zdeduction   = $deduction ;

        $loan = LoanDetails::where('emp_id', $id_type)  
        ->where('loanpkg_id',$package)->limit('1')->get();   //mencari minimal salary nya & dsr nya (rasio utang terhadap pendapatan)

        // return $total_salary;
        $id_loan= $loan->first()->id;  
        $salary_dsr = ($zbasicsalary * ($loan->first()->dsr / 100)) - $zdeduction;


        $icnumber = $pra->ic;
        $tanggal  = substr($icnumber,4, 2);
        $bulan    = substr($icnumber,2, 2);
        $tahun    = substr($icnumber,0, 2); 

        if($tahun > 30) {

            $tahun2 = "19".$tahun;
        }
        else {
             $tahun2 = "20".$tahun;
        }
        

        $lahir          = $tahun2.'-'.$bulan.'-'.$tanggal; 
        $lahir          =  date('Y-m-d', strtotime($lahir. ' - 16 month'));
        $oDateNow       = new DateTime();
        $oDateBirth     = new DateTime($lahir);
        $oDateIntervall = $oDateNow->diff($oDateBirth);

        $umur = 61 - $oDateIntervall->y;

        $durasix = 60 - $oDateIntervall->y;
        if( $durasix  > 10) { 
            $durasi = 10 ;
        } 
        else { 
            $durasi = $durasix ;
        }


        function pembulatan($uang) {
            $puluhan = substr($uang, -3);
                  if($puluhan<500) {
                    $akhir = $uang - $puluhan; 
                  } 
                  else {
                    $akhir = $uang - $puluhan;
                  }
                  return $akhir;
                }


        foreach($loan as $loan) {
            $salary_dsr = ($zbasicsalary * ($loan->dsr / 100)) - $zdeduction;
            $ndi        = ($zbasicsalary - $zdeduction) -  1300; 
            $max        =  $salary_dsr * 12 * 10 ;
                                           

            if(!empty($loan->max_byammount))  {
                  $ansuran = intval($salary_dsr)-1;
                    if($package == "1") {    //aku edit
                        $bunga = 3.8/100;
                    }
                    elseif($package == "2") { //aku edit
                        $bunga = 4.9/100;
                    }

                    else {
                        $bunga = 5.92/100;
                    }
                  $pinjaman = 0;

                  for ($i = 0; $i <= $loan->max_byammount; $i++) {
                      $bungapinjaman = $i  * $bunga * $durasi ;
                      $totalpinjaman = $i + $bungapinjaman ;
                      $durasitahun = $durasi * 12;
                      $ansuran2 = intval($totalpinjaman / ($durasi * 12))  ;
                      if ($ansuran2 < $ndi)
                      {
                          $pinjaman = $i;
                      }
                    
                  }   

                  if($pinjaman > 1) {                                              
                      $bulat = pembulatan($pinjaman);
                      $loanx =  number_format($bulat, 0 , ',' , ',' ) ; 
                      $loanz = $bulat;
                  }
                  else {
                      $loanx =  number_format($loan->max_byammount, 0 , ',' , ',' ) ; 
                      $loanz = $loan->max_byammount;
                  }

                }
                else { 
                    $bulat = pembulatan($loan->max_bysalary * $total_salary);
                    $loanx =  number_format($bulat, 0 , ',' , ',' ) ; 
                    $loanz = $bulat;
                    if ($loanz > 199000) {

                          $loanz  = 250000;
                          $loanx =  number_format($loanz, 0 , ',' , ',' ) ; 
                    }
                }
            }

        $tenure = Tenures::where('id_loan',$id_loan)->where('years','<', $umur)->where('years','!=','1')->get();  
         
         if( $pra->first()->loanamount <= $loanz ) {
            $ndi_limit=$loan->ndi_limit;
            foreach($tenure as $tenure) {
                $bunga2 =  $pra->first()->loanamount * $tenure->rate /100   ;
                $bunga = $bunga2 * $tenure->years;
                $total = $pra->first()->loanamount + $bunga ;
                $bulan = $tenure->years * 12 ;
                $installment =  $total / $bulan ;
                 $ndi_state = ($total_salary - $zdeduction) - $installment; 

                    $count_installment=0;
                 if($installment  <= $salary_dsr && $ndi_state>=$ndi_limit) {
                    $count_installment++;
                }

            }
         } else {
            $count_installment=0;
         }

        if($count_installment>0) {

            $emp    = $request->input('Employment');
            $today  = date('Y-m-d H:i:s');  //today

            if($emp == '1'){ //loanpkg_code 4 = mumtaz 
                praapplication::where('id_cus', $id_cus)->update(array( 'loanpkg_code' => '4','employment_code' => $request->input('Employment'), 'gaji_asas' => $request->input('gaji_asas'), 'elaun' => $request->input('elaun'), 'pot_bul' => $request->input('pot_bul'), 'jml_pem' => $request->input('jml_pem')));
            }
            elseif($emp == '2'){ //2 = afdhal
                praapplication::where('id_cus', $id_cus)->update(array( 'loanpkg_code' => '2','employment_code' => $request->input('Employment'), 'gaji_asas' => $request->input('gaji_asas'), 'elaun' => $request->input('elaun'), 'pot_bul' => $request->input('pot_bul'), 'jml_pem' => $request->input('jml_pem')));
            }elseif($emp == '3'){ //2 = afdhal
                praapplication::where('id_cus', $id_cus)->update(array( 'loanpkg_code' => '2', 'employment_code' => $request->input('Employment'), 'gaji_asas' => $request->input('gaji_asas'), 'elaun' => $request->input('elaun'), 'pot_bul' => $request->input('pot_bul'), 'jml_pem' => $request->input('jml_pem')));
            }elseif($emp == '4'){ //1 = private
                praapplication::where('id_cus', $id_cus)->update(array( 'loanpkg_code' => '1', 'employment_code' => $request->input('Employment'), 'gaji_asas' => $request->input('gaji_asas'), 'elaun' => $request->input('elaun'), 'pot_bul' => $request->input('pot_bul'), 'jml_pem' => $request->input('jml_pem')));
            }elseif($emp == '5'){ //2 = afdhal
                praapplication::where('id_cus', $id_cus)->update(array( 'loanpkg_code' => '2', 'employment_code' => $request->input('Employment'), 'gaji_asas' => $request->input('gaji_asas'), 'elaun' => $request->input('elaun'), 'pot_bul' => $request->input('pot_bul'), 'jml_pem' => $request->input('jml_pem')));
            }

        
            $document                          = new LoanAmmount;
            $document->id_praapplication       = $id_cus;
        
            $document->save();

          return redirect('resulttenus/'.$id_cus)->with(['update' => 'Sorry Tahniah, Anda layak memohon sehingga RM '.$loanx]);
          //return view('adminpage.customer.tenos', compact('pra','loan','total_salary','tenure','id','zbasicsalary' ,'zdeduction','user', 'workgroupprocessor2'));

           //return redirect('loan-eli')->with(['update' => 'Data saved successfully']);  


        } 

        else {
            $had2 = number_format($had, 0 , ',' , ',' ).'.00' ; 
            Session::flash('fullname', $fullname); 
            Session::flash('icnumber', $icnumber);
            Session::flash('phone', $phone); 
            Session::flash('basicsalary', $basicsalary); 
            Session::flash('allowance', $allowance);
            Session::flash('deduction', $deduction);
            Session::flash('loanAmount', $loanAmount);
            Session::flash('employer2', $employer2);
            Session::flash('employment', $employment);
            Session::flash('employment2', $employment2);
            Session::flash('package_name', $package_name);
            Session::flash('majikan', $majikan);

            Session::flash('hadpotongan', $basicsalary); 
            return redirect('/')->with(['update' => 'Sorry you are not eligible to apply, your deduction or funding exceeds the limit']);
        }       
            }
        }

         
    
    }




public function resulttenus($id)
    {
        $package    = Package::all();
        $employment = Employment::all();
        $loanpkg    = Loanpkg::all();
        $emp        = Emp::all();
        $pra        = Praapplication::where('id_cus', $id)->first(); 

        $id_type      = $pra->employment_code;
        $total_salary = $pra->gaji_asas + $pra->elaun ;
        $zbasicsalary = $pra->gaji_asas + $pra->elaun ;
        $zdeduction   = $pra->pot_bul;

        $loan = LoanDetail::where('id_emp_type',$id_type)
                ->where('min_salary','<=',$total_salary)
                ->where('max_salary','>=',$total_salary)->limit('1')->get();

        $id_loan  = $loan->first()->id;  
        
        $icnumber = $pra->first()->icnumber;
        $tanggal  = substr($icnumber,4, 2);
        $bulan    = substr($icnumber,2, 2);
        $tahun    = substr($icnumber,0, 2); 

        if($tahun > 30) {
            $tahun2 = "19".$tahun;
        }
        else {
             $tahun2 = "20".$tahun;
        }
       
        $lahir          = $tahun2.'-'.$bulan.'-'.$tanggal; 
        $lahir          =  date('Y-m-d', strtotime($lahir. ' - 16 month'));
        $oDateNow       = new DateTime();
        $oDateBirth     = new DateTime($lahir);
        $oDateIntervall = $oDateNow->diff($oDateBirth);

        $umur = 61 - $oDateIntervall->y;

        $tenure = TenureDetail::where('id_loan',$id_loan)->where('years','<', $umur)->where('years','!=','1')->get();

        $today  = date('Y-m-d H:i:s');  //today 
        praapplication::where('id_cus', $id)->update(array( 'date_pending_doc' => $today));

        return view('adminpage.customer.tenos', compact('package', 'employment', 'loan', 'emp', 'pra', 'loan', 'tenure', 'zbasicsalary', 'zdeduction', 'total_salary','id_loan'));
    }