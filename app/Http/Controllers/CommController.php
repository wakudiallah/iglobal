<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Loanpkg;
use App\Comm;
use App\Authorizable;
use Illuminate\Support\Facades\Auth;

class CommController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() 
    {
        
        $comm = Comm::latest()->with('user')->paginate();

        return view('adminpage.setupmodule.comm.index', compact('comm'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $loan = Loanpkg::all();

        return view('adminpage.setupmodule.comm.new', compact('loan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *///LnPkg_Code','Dt_Start','Dt_Ent','Comm_MO','Comm_TeamLEad','Comm_Manager','Act'
    public function store(Request $request)
    {
        $this->validate($request, [
            'LnPkg_Code'    => 'required|max:4',
            'Dt_Start'      => 'required',
            'Dt_Ent'        => 'required',
            'Comm_MO'       => 'required',
            'Comm_TeamLEad' => 'required',
            'Comm_Manager'  => 'required'
            
        ]);

        $request->user()->comm()->create($request->all());


        flash('Komisyen telah berjaya ditambah');
        
        return redirect()->route('comm.index')->with(['success' => 'Commission data successfully saved']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $comm = Comm::where('id',$id)->first(); 
        $loan = Loanpkg::all();  
                        
        return view('adminpage.setupmodule.comm.edit', compact('comm','loan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comm $comm)
    {
        $this->validate($request, [
            'LnPkg_Code'    => 'required|max:4',
            'Dt_Start'      => 'required',
            'Dt_Ent'        => 'required',
            'Comm_MO'       => 'required',
            'Comm_TeamLEad' => 'required',
            'Comm_Manager'  => 'required'
            
        ]);

        $me = $request->user();

        if( $me->hasRole('Admin') ) {
            $comm = Comm::findOrFail($comm->id);
        } else {
            $comm = $me->comm()->findOrFail($comm->id);
        }

        $comm->update($request->all());

        flash()->success('Komisyen has been updated.');

        return redirect()->route('comm.index')->with(['update' => 'Commission data successfully updated']);
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Comm::where('id',$id)->delete();   
                        
        return redirect('comm')->with(['delete' => 'Commission data successfully deleted']);
    }
}
