<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Announcement;
use App\Question;
use App\Role;
use App\Permission;
use App\Authorizable;
use App\Filecalculation;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class FileCalculationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $file = Filecalculation::latest()->get();

        return view('adminpage.setupmodule.file_calculation.index', compact('file'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('adminpage.setupmodule.file_calculation.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            
            'title'  => 'required|max:2000',
            'file'   => 'required',
            'status' => 'nullable'
        ]);
 

        $user = Auth::user()->id;
      
        $file = $request->input('file');

        $fileName = $request->file('file')->getClientOriginalName();
        $destinationPath =base_path().'/public/filecalculation';
        $proses = $request->file('file')->move($destinationPath, $fileName);

        $data          = new Filecalculation;
        
        $data->title   = $request->input('title');
        $data->file    = $fileName;
        $data->status  = $request->input('status');
        $data->user_id = $user;
        $data->save();
        
        return redirect('file-calculation')->with(['success' => 'File Calculation data successfully Saved']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */



    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $file = Filecalculation::where('id', $id)->first();


        return view('adminpage.setupmodule.file_calculation.edit', compact('file'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            
            'title'  => 'required|max:2000',
            'file'   => 'nullable',
            'status' => 'nullable'
        ]);
 


        $user = Auth::user()->id;

        $data = Filecalculation::findOrFail($id);
        $data->title = $request->input('title');
        $data->status = $request->input('status');
        $data->user_id = $user;

        if (empty($request->file('file'))){
            $data->file = $data->file;
        }
        else{
            $image = $request->file('file')->getClientOriginalName();
            $destination = base_path() . '/public/filecalculation';
            $request->file('file')->move($destination, $image);
     
            $data['file'] = $image;
        }
        $data->save();

        
        return redirect('file-calculation')->with(['update' => 'File Calculation data successfully Saved']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
    {
        $emp = Filecalculation::findOrFail($id)->delete();   
                        
        return redirect('file-calculation')->with(['delete' => 'File Calculation data successfully deleted']);

        
    }

    public function update1($id)
    {
        $st = Filecalculation::find($id);
        $st->status = '1';
        $st->save();
                        
        return redirect('file-calculation')->with(['update' => 'File Calculation data successfully updated']);
 
    }

    public function update0(Request $request , $id)
    {
        $st = Filecalculation::find($id);
        $st->status = '0';
        $st->save();

        return redirect('file-calculation')->with(['update' => 'File Calculation data successfully updated']);
    }
}
