<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Package;
use App\Authorizable;
use Illuminate\Support\Facades\Auth;

class PackageController extends Controller
{
    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    

    public function index()
    {
        $package = Package::orderBy('created_at','desc')->get();
        return view('adminpage.setupmodule.package.index', compact('package'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('adminpage.setupmodule.package.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $request->user()->package()->create($request->all());

        flash('Package berjaya ditambah');

        return redirect()->route('package.index')->with(['success' => 'Package data successfully saved']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $package = Package::where('id',$id)->first();   
                        
        return view('adminpage.setupmodule.package.edit', compact('package'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $package = Package::find($id);
        $package->name = $request->name;
        $package->save();

        return redirect()->route('package.index')->with(['update' => 'Package data successfully updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $package = Package::where('id',$id)->delete();   
                        
        return redirect('package')->with(['delete' => 'Package data successfully deleted']);;
    }
}
