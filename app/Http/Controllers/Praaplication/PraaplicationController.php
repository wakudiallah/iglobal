<?php

namespace App\Http\Controllers\Praaplication;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Ramsey\Uuid\Uuid;
use App\praapplication;
use App\DocCust;
use App\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\History;
use App\Remark;
use App\Role;
use App\Model_has_role;
use App\AddInfo;
use App\MeetCust;
use App\Package;
use App\Loanpkg;
use App\Employment;
use App\Emp;
use App\Homeimage;
use App\Tenure;
use App\Loan;
use App\LoanDetail;
use App\TenureDetail;
use App\LoanDetails;
use App\Tenures;
use App\Financial;
use App\LoanAmmount;
use DB;
use Mapper;
use DateTime;
use PDF;
use Hash;
use Session;


class PraaplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('praaplication.kelayakan');
    }
   

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function store(Request $request)
    { 
        
         /*------------ Dia Simpan ------------------- */

        //$praapplication = new PraApplication;

        $data                  = new praapplication;

        
        $icnumber    = $request->input('ic');
        $fullname    = $request->input('name');
        $phone       = $request->input('notelp');
        $employment  = $request->input('Employment');
        $employment2 = $request->input('Employment2');
        $employer    = $request->input('employer');
        $employment2 = $request->input('Employment');
        $basicsalary = $request->input('gaji_asas');
        $employer2   = $request->input('employer');
        $allowance   = $request->input('elaun');
        $deduction   = $request->input('pot_bul');        
        $majikan    = $request->input('employer');
        $loanAmount = $request->input('jml_pem');
        $loanpkg_code = $request->input('loanpkg_code');
        $package_name = $request->input('loanpkg_code');
        
        

        $batas      = $allowance  +  $basicsalary;


     
        $tanggal = substr($icnumber,4, 2);
        $bulan   = substr($icnumber,2, 2);
        $tahun   = substr($icnumber,0, 2); 

        if($tahun > 30) {
            $tahun2 = "19".$tahun;
        }
        else {
             $tahun2 = "20".$tahun;
        }
       
        $lahir          = $tahun2.'-'.$bulan.'-'.$tanggal;
        $lahir          =  date('Y-m-d', strtotime($lahir. ' - 16 month')); 
        
        $oDateNow       = new DateTime();
        $oDateBirth     = new DateTime($lahir);
        $oDateIntervall = $oDateNow->diff($oDateBirth);

        $umur =  $oDateIntervall->y;

        if($umur >= 60) {  //1. cek umur 

            Session::flash('fullname', $fullname); 
            Session::flash('icnumber', $icnumber);
            Session::flash('phone', $phone); 
            Session::flash('basicsalary', $basicsalary); 
            Session::flash('allowance', $allowance);
            Session::flash('deduction', $deduction);
            Session::flash('loanAmount', $loanAmount);
            Session::flash('employer2', $employer2);
            Session::flash('employment', $employment);
            Session::flash('employment2', $employment2);
            Session::flash('majikan', $majikan);
            Session::flash('icnumber_error', $icnumber); 


            \Session::flash('flash_message','Sorry your age exceeds the limit');
            return redirect('/')->with('message', "Maaf umur anda melebihi had");

        }
        else {
            $had =  3500;    //2. cek ketentuan minimum gaji + tunjangan dio (berdasar job type)

            // Batas Gaji  ==   Jenis Pekerjaan
            
            /*if($employment == 1  )  { //kerajaan permanent
                $had =  3000;  
            }
            else if($employment == 2  )  { //kerajaan kontrak
                $had =  3000;  
            }
            else if ($employment == 3 )  { //swasta with sallary deduction
               $had =  3500;  
            } 
             else if ($employment == 4 )  { //swasta without sallary deduction
               $had =  3500;  
            } 
             else if ($employment == 5 )  { //premium 15K
               $had =  15001;  
            } */

            
            if($batas >= $had ) {  // batas = gaji + tunjangan

                if(($loanAmount < 50000) || ($loanAmount > 300000)){

                    Session::flash('fullname', $fullname); 
                    Session::flash('icnumber', $icnumber);
                    Session::flash('phone', $phone); 
                    Session::flash('basicsalary', $basicsalary); 
                    Session::flash('allowance', $allowance);
                    Session::flash('deduction', $deduction);
                    Session::flash('loanAmount', $loanAmount);
                    Session::flash('employer2', $employer2);
                    //Session::flash('employment', $employment);
                    //Session::flash('employment2', $employment2);
                    //Session::flash('package_name', $package_name);
                    Session::flash('majikan', $majikan);

                    Session::flash('hadpotongan', $basicsalary); 
                    \Session::flash('flash_message','Loan Amount at least RM 50000 and less than RM 250000');
                    
                    return redirect('/')->with('message', "Maaf umur anda melebihi had");
                
                }
                else
                {



                    $totalsalary           = $basicsalary + $allowance ;
                    
                    $id                    = Uuid::uuid4()->tostring(); 
                    $data->id_cus          = $id;
                    $data->name            = $request->name;
                    $data->ic              = $request->ic;
                    $data->notelp          = $request->notelp;
                    $data->employment_code = $employment;
                    $data->emp_code        = $request->emp_code;
                    $data->elaun           = $request->elaun;
                    $data->pot_bul         = $request->pot_bul;
                    $data->loanpkg_code    = $request->loanpkg_code;
                    $data->gaji_asas       = $request->gaji_asas;
                    $data->jml_pem         = $request->jml_pem;
                    $data->latitude        = $request->latitude;
                    $data->longitude       = $request->longitude;
                    $data->location        = $request->location;
                    //$data->status          = '1';
                    $data->save();
                    
                     $request                    = new Loanammount;
                    $request->id_praapplication = $id;
                     $request->save();

                    $tanggal = substr($icnumber,4, 2);
                    $bulan   = substr($icnumber,2, 2);
                    $tahun   = substr($icnumber,0, 2); 

                    if($tahun > 30) {

                        $tahun2 = "19".$tahun;
                    }
                    else {
                         $tahun2 = "20".$tahun;

                    }
           
                    $lahir = $tahun2.'-'.$bulan.'-'.$tanggal;                                                     
                                                                        
                  
                    /*------------------ End  Simpan ------------------- */
                    
                    //--------------- -----  Proceed

                    $pra          = Praapplication::where('id_cus', $id)->first(); 

                    $id_type      = $pra->employment_code;    //employment_code = job type 
                    $total_salary = $basicsalary + $allowance ;
                    $zbasicsalary = $basicsalary + $allowance;
                    $zdeduction   = $deduction ;

                    $loan = LoanDetails::where('emp_id', $id_type)  
                        ->where('loanpkg_id',$loanpkg_code)->limit('1')->get();   //mencari minimal salary nya & dsr nya (rasio utang terhadap pendapatan)

                    // return $total_salary;
                    $id_loan= $loan->first()->id;  
                    //$id_loan= $loan->id;  

                    $salary_dsr = ($zbasicsalary * ($loan->first()->dsr / 100)) - $zdeduction;

                    $icnumber = $pra->ic;
                    $tanggal  = substr($icnumber,4, 2);
                    $bulan    = substr($icnumber,2, 2);
                    $tahun    = substr($icnumber,0, 2); 

                    if($tahun > 30) {

                        $tahun2 = "19".$tahun;
                    }
                    else {
                         $tahun2 = "20".$tahun;
                    }
                    
           
                    $lahir          = $tahun2.'-'.$bulan.'-'.$tanggal; 
                    $lahir          =  date('Y-m-d', strtotime($lahir. ' - 16 month'));
                    $oDateNow       = new DateTime();
                    $oDateBirth     = new DateTime($lahir);
                    $oDateIntervall = $oDateNow->diff($oDateBirth);

                    $umur = 61 - $oDateIntervall->y;

                    $durasix = 60 - $oDateIntervall->y;
                    if( $durasix  > 10) { 
                        $durasi = 10 ;
                    } 
                    else { 
                        $durasi = $durasix ;
                    }


                     function pembulatan($uang) {
                              $puluhan = substr($uang, -3);
                              if($puluhan<500) {
                                $akhir = $uang - $puluhan; 
                              } 
                              else {
                                $akhir = $uang - $puluhan;
                              }
                              return $akhir;
                            }


                    foreach($loan as $loan) {
                        $salary_dsr = ($zbasicsalary * ($loan->dsr / 100)) - $zdeduction;
                         
                        $ndi        = ($zbasicsalary - $zdeduction) -  1300;
                         
                        $max        =  $salary_dsr * 12 * 10 ;
                                                       

                        if(!empty($loan->max_byammount))  {
                              $ansuran = intval($salary_dsr)-1;
                                if($pra->first()->loanpkg_code=="1") {
                                    $bunga = 3.8/100;
                                }
                                elseif($pra->first()->loanpkg_code=="2") {
                                    $bunga = 4.9/100;
                                }

                                else {
                                    $bunga = 5.92/100;
                                }
                              $pinjaman = 0;

                              for ($i = 0; $i <= $loan->max_byammount; $i++) {
                                  $bungapinjaman = $i  * $bunga * $durasi ;
                                  $totalpinjaman = $i + $bungapinjaman ;
                                  $durasitahun = $durasi * 12;
                                  $ansuran2 = intval($totalpinjaman / ($durasi * 12))  ;
                                  if ($ansuran2 < $ndi)
                                  {
                                      $pinjaman = $i;
                                  }
                                
                              }   

                              if($pinjaman > 1) {                                              
                                  $bulat = pembulatan($pinjaman);
                                  $loanx =  number_format($bulat, 0 , ',' , ',' ) ; 
                                  $loanz = $bulat;
                              }
                              else {
                                  $loanx =  number_format($loan->max_byammount, 0 , ',' , ',' ) ; 
                                  $loanz = $loan->max_byammount;

                              }

                            }
                            else { 

                              
                                $bulat = pembulatan($loan->max_bysalary * $total_salary);
                                $loanx =  number_format($bulat, 0 , ',' , ',' ) ; 
                                $loanz = $bulat;
                                if ($loanz > 199000) {

                                      $loanz  = 250000;
                                      $loanx =  number_format($loanz, 0 , ',' , ',' ) ; 
                                }
                            }
                        }

                    $tenure = Tenures::where('id_loan',$id_loan)->where('years','<', $umur)->where('years','!=','1')->get();
                     
                     if( $pra->first()->loanamount <= $loanz ) {
                        $ndi_limit=$loan->ndi_limit;
                        foreach($tenure as $tenure) {
                            $bunga2 =  $pra->first()->loanamount * $tenure->rate /100   ;
                            $bunga = $bunga2 * $tenure->years;
                            $total = $pra->first()->loanamount + $bunga ;
                            $bulan = $tenure->years * 12 ;
                            $installment =  $total / $bulan ;
                             $ndi_state = ($total_salary - $zdeduction) - $installment; 

                                $count_installment=0;
                             if($installment  <= $salary_dsr && $ndi_state>=$ndi_limit) {
                                $count_installment++;
                            }

                        }
                     } else {
                        $count_installment=0;
                     }

                    if($count_installment>0) {

                        \Session::flash('flash_message','Congratulations, you are eligible to apply for up to RM'.$loanx);
                      return redirect('praapplication/'.$id)->with('message', 'Tahniah, Anda layak memohon sehingga RM '.$loanx);
                     // return $installment;
                        // return $loanx;
                  
                    
                } 
                else {
                    $had2 = number_format($had, 0 , ',' , ',' ).'.00' ; 
                    Session::flash('fullname', $fullname); 
                    Session::flash('icnumber', $icnumber);
                    Session::flash('phone', $phone); 
                    Session::flash('basicsalary', $basicsalary); 
                    Session::flash('allowance', $allowance);
                    Session::flash('deduction', $deduction);
                    Session::flash('loanAmount', $loanAmount);
                    Session::flash('employer2', $employer2);
                    Session::flash('employment', $employment);
                    Session::flash('employment2', $employment2);
                    Session::flash('package_name', $package_name);
                    Session::flash('majikan', $majikan);

                    Session::flash('hadpotongan', $basicsalary); 

                    \Session::flash('flash_message','Sorry you are not eligible to apply, your deduction or funding exceeds the limit!');
                    return redirect('/')->withErrors('Sorry you are not eligible to apply, your deduction or funding exceeds the limit!');
                }
            }
 
            }

            else {  // cek bila gaji + tunjangan tidak lebi dari had 

                $had2 = number_format($had, 0 , ',' , ',' ).'.00' ; 
                Session::flash('fullname', $fullname); 
                Session::flash('icnumber', $icnumber);
                Session::flash('phone', $phone); 
                Session::flash('basicsalary', $basicsalary); 
                Session::flash('allowance', $allowance);
                Session::flash('deduction', $deduction);
                Session::flash('loanAmount', $loanAmount);
                Session::flash('employer2', $employer2);
                Session::flash('employment', $employment);
                Session::flash('employment2', $employment2);
                Session::flash('package_name', $package_name);
                Session::flash('majikan', $majikan);
                
                
                \Session::flash('flash_message','Minimum income requirement RM'. $had2 .' per month');
                return redirect('/')->with('message', "Syarat pendapatan minima RM   sebulan ");
            }
        }
 
    }

  
    public function kira($id)
    {
        
        $image      = Homeimage::latest('created_at')->where('status', '1')->first();

        $pra          = Praapplication::where('id_cus', $id)->first(); 

        $id_type      = $pra->employment_code;
        $loanpkg_code      = $pra->loanpkg_code;
        $total_salary = $pra->gaji_asas + $pra->elaun ;
        $zbasicsalary = $pra->gaji_asas + $pra->elaun;
        $zdeduction   = $pra->pot_bul;

      
          $loan = LoanDetails::where('emp_id', $id_type)  
              ->where('loanpkg_id',$loanpkg_code)->limit('1')->get();   //mencari minimal salary nya & dsr nya (rasio utang terhadap pendapatan)

          // return $total_salary;
          $id_loan= $loan->first()->id;  


        $salary_dsr = ($zbasicsalary * ($loan->first()->dsr / 100)) - $zdeduction;


        $icnumber = $pra->ic;
        $tanggal  = substr($icnumber,4, 2);
        $bulan    = substr($icnumber,2, 2);
        $tahun    = substr($icnumber,0, 2); 

        if($tahun > 30) {
            $tahun2 = "19".$tahun;
        }
        else {
             $tahun2 = "20".$tahun;
        }
       
        $lahir          = $tahun2.'-'.$bulan.'-'.$tanggal; 
        $lahir          =  date('Y-m-d', strtotime($lahir. ' - 16 month'));
        $oDateNow       = new DateTime();
        $oDateBirth     = new DateTime($lahir);
        $oDateIntervall = $oDateNow->diff($oDateBirth);

        $umur = 61 - $oDateIntervall->y;

        $tenure = Tenures::where('id_loan',$id_loan)->where('years','<', $umur)->where('years','!=','1')->get();   

         
        return view('utama.kira_utama', compact('pra','loan','total_salary','tenure','id','zbasicsalary' ,'zdeduction', 'image','id_loan' ));            
    }


    public function just_calculate(Request $request)
    {
        $id_cus      = $request->input('id_cus');
        $email       = $request->input('email');
        $loanammount = $request->input('loanamount');
        $maxloan     = $request->input('maxloanx');
        $tenure      = $request->input('tenure');

        praapplication::where('id_cus', $id_cus)->update(array('email' => $email, 'stage' => 'ON1'));

        LoanAmmount::where('id_praapplication', $id_cus)->latest('id')->update(array('loanammount' => $loanammount, 'maxloan' => $maxloan, 'id_tenure' => $tenure));

        \Session::flash('flash_message','We will process your application');

        return redirect('/')->with('welcome_msg');
    }
    

    public function kelayakan(Request $request)
    {
        $id = $request->id_cus;  
        // Get the value from the form
        $input['email'] = Input::get('email');

        // Must not already exist in the `email` column of `users` table
        $rules = array('email' => 'unique:users,email');

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            \Session::flash('flash_message','Maaf, email Anda sudah digunakan');

            return redirect('/praaplication/kira/'.$id);
        }
        
        else {
            $id = $request->id_cus;
            
            $data           = new User;
            
            $data->name     = $request->name;
            $data->email    = $request->email;
            $data->password = $request->password;
            $data->save();
            
            return redirect('/praaplication/upload-file/'.$id);
        }
    }


    public function finish()
    {
        $reg = praapplication::latest('id')->limit('1')->first();

        return view('praaplication.terimakasih', compact('reg'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
