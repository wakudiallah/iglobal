<?php

namespace App\Http\Controllers;

use App\FileManager;
use Illuminate\Http\Request;
use App\praapplication;
use Illuminate\Support\Facades\Auth;
use App\Announcement;
use App\Emp;
use App\DocCust;
use DB;
use Mapper;
use DateTime;
use PDF;
use Hash;
use App\History;
use App\Remark;
use App\User;
use App\Role;
use App\Model_has_role;
use App\DocAssest;
use App\LoanAmmount;
use App\ListFolder;
use File;

class ListFolderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $file = ListFolder::get();
                        
        return view('adminpage.file_manager.folder.index', compact('file'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('adminpage.file_manager.folder.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
         $user = Auth::user()->id;

        $this->validate($request, [
            'folder'   => 'required|max:200',
            'user_id'  => 'nullable'
        ]);
        $folder = $request->input('folder');

        $validate = ListFolder::where('folder',$folder)->count();

        if($validate==0)
        {
             $data          = new ListFolder;
            $data->folder   = $request->input('folder');
            $data->user_id    = $user;
            $data->save();
            
            $path = public_path().'/file_manager/'.$folder;
            File::makeDirectory($path, $mode = 0777, true, true);
            return redirect('list-folder')->with(['success' => 'List Folder data successfully Saved']);
        }
        else{
             return redirect('list-folder')->with(['success' => 'folder exist']);
        }

           
        
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $file = ListFolder::where('id',$id)->first();  

        return view('adminpage.file_manager.folder.edit', compact('file'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user()->id;

        $this->validate($request, [
            'folder'   => 'required|max:200',
            'user_id'  => 'nullable'
        ]);
 

            $data = ListFolder::findOrFail($id);
            
            $data->folder   = $request->input('folder');
            $data->user_id    = $user;
            $data->save();


            $folder = $request->input('folder');

            $old_path = public_path().'/file_manager/'.$old;
            $new_path = public_path().'/file_manager/'.$folder;
            $move = File::move($old_path, $new_path);
        
        
        return redirect('list-folder')->with(['update' => 'List Folder data successfully updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $file = ListFolder::findOrFail($id)->delete();   
                        
        return redirect('list-folder')->with(['delete' => 'List Folder data successfully deleted']);
    }
}
