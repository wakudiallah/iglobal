<?php

namespace App\Http\Controllers;

use App\Authorizable;
use App\Messsage;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UserStoreRequest;

class MesssageController extends Controller
{
    use Authorizable;
    
    private $user;

    public function  __construct( User $user )
    {
        $this->user = $user;
    }

    public function index()
    {
        $result = Messsage::latest()->with('user')->paginate();
        return view('message.index', compact('result'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('message.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|min:10',
            'body' => 'required|min:20'
        ]);

    
        /*$request->user()->messages()->create($request->all()); */

        flash('Message has been added');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Messsage  $messsage
     * @return \Illuminate\Http\Response
     */
    public function show(Messsage $messsage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Messsage  $messsage
     * @return \Illuminate\Http\Response
     */
    public function edit(Messsage $messsage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Messsage  $messsage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Messsage $messsage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Messsage  $messsage
     * @return \Illuminate\Http\Response
     */
    public function destroy(Messsage $messsage)
    {
        //
    }
}
