<?php

namespace App\Http\Controllers\Utama;

use App\Authorizable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Homeimage;

class HomeImageController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $image = Homeimage::orderBy('created_at','desc')->get();

        return view('adminpage.attribute.index', compact("image"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('adminpage.attribute.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
            'image' => ['mimes:jpg,jpeg,JPEG,png,gif,bmp'],
            'status' => 'required'
        ]);
 
        $data = $request->all();
 
        $image = $request->file('image')->getClientOriginalName();
        $destination = base_path() . '/public/uploads';
        $request->file('image')->move($destination, $image);
 
        $data['image'] = $image;
 
        //Homeimage::create($data);

        $request->user()->homeimage()->create($data);

        return redirect()->route('homeimage.index')->with(['success' => 'Home Image data successfully saved']);
        
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //show on utama
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $package = Homeimage::find($id);
        $package->status = $request->status;
        $package->save();

        return redirect()->route('homeimage.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function update1($id)
    {
        
        $st = Homeimage::find($id);
        $st->status = '1';
        $st->save();

        return redirect()->route('homeimage.index')->with(['update' => 'Home Image data successfully updated']);
    }

    public function update2($id)
    {
        
        $st = Homeimage::find($id);
        $st->status = '0';
        $st->save();

        return redirect()->route('homeimage.index')->with(['update' => 'Home Image data successfully updated']);
    }

}
