<?php

namespace App\Http\Controllers\Utama;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ContactInfo;
use Mapper;
use App\Package;
use App\Loanpkg;
use App\Employment;
use App\Emp;
use App\Homeimage;

class UtamaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //$images      = Homeimage::where('status', '1')->latest('created_at')->limit('1')->first();
        //$images      = Homeimage::where('status', '1')->whereNull('deleted_at')->latest('created_at')->limit('1')->first();

        $package    = Package::all();
        $employment = Employment::all();
        $loanpkg    = Loanpkg::all();
        $emp        = Emp::all();
        $image      = Homeimage::latest('created_at')->where('status', '1')->first();


        return view('utama.main', compact('package', 'employment', 'emp', 'loanpkg', 'image'));
        //return view('utama.guna.index', compact('images'));
        //return view('utama.guna.index', compact('images'));  //cif punya
    }

    public function hidden()
    {
        $package    = Package::all();
        $employment = Employment::all();
        $loanpkg    = Loanpkg::all();
        $emp        = Emp::all();
        $image      = Homeimage::latest('created_at')->where('status', '1')->first();

        return view('utama.main', compact('package', 'employment', 'emp', 'loanpkg', 'image'));

        //$images      = Homeimage::where('status', '1')->latest('created_at')->limit('1')->first();
        //$images      = Homeimage::where('status', '1')->whereNull('deleted_at')->latest('created_at')->limit('1')->first();
        
        //return view('utama.guna.index', compact('images'));
        //return view('utama.guna.index', compact('images'));  //cif punya
    }

    
    public function about()
    {
        $image      = Homeimage::latest('created_at')->where('status', '1')->first();

        return view('utama.about', compact('image'));
    }

    public function contact()
    {
        $image      = Homeimage::latest('created_at')->where('status', '1')->first();

        Mapper::map(3.211451, 101.673942);
        return view('utama.contact', compact('image'));
    }

    public function service()
    {
        $image      = Homeimage::latest('created_at')->where('status', '1')->first();

        return view('utama.service', compact('image'));
    }

    public function faq()
    {
        $image      = Homeimage::latest('created_at')->where('status', '1')->first();

        return view('utama.faq', compact('image'));
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
