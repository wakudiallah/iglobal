<?php

namespace App\Http\Controllers\Utama;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Message;
use Mail;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mes = Message::orderBy('created_at','desc')->get();

        return view('adminpage.message.index', compact("mes"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data          = new Message;
        $data->first   = $request->first;
        $data->last    = $request->last;
        $data->email   = $request->email;
        $data->message = $request->message;
        $data->save();

        \Session::flash('flash_message','Message send successfully');
        
        return redirect('contact');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $message = Message::where('id',$id)->first();

        return view('adminpage.message.reply', compact('message'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
        $no = $request->input('id');

        $st = Message::find($no);
        $st->reply = $request->input('reply');
        $st->status = '1';
        $st->save();


        $pesan = Message::where('id', '=', $no)->first();

        $emailfrom  = 'optikserasi.ind@gmail.com';
            $namefrom   = 'Global Test';
            $name =$pesan->last;
            $email = $pesan->email;

            Mail::send('adminpage.message.template_reply', compact('email','name', 'pesan'), function ($message) use ($email, $name, $emailfrom, $namefrom) 
                {
                  $message->from($emailfrom, $namefrom);
                  $message->subject('Global');
                  $message->to($email, $name);
                });
        

        return redirect('message')->with(['update' => 'Message data successfully updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
   
}
