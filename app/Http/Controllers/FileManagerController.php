<?php

namespace App\Http\Controllers;

use App\FileManager;
use Illuminate\Http\Request;
use App\praapplication;
use Illuminate\Support\Facades\Auth;
use App\Announcement;
use App\Emp;
use App\DocCust;
use DB;
use Mapper;
use DateTime;
use PDF;
use Hash;
use App\History;
use App\Remark;
use App\User;
use App\Role;
use App\Model_has_role;
use App\DocAssest;
use App\LoanAmmount;
use App\ListFolder;
use File;
use PHPExcel;
class FileManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $file = FileManager::get();

        return view('adminpage.file_manager.index', compact('file'));
        
    }
     public function detail_folder()
    {
        $folder = ListFolder::get();

        return view('adminpage.file_manager.detail', compact('folder'));
        
    }

    public function detail_file($id, $view=FALSE)
    {
        $file = FileManager::where('folder','=',$id)->orderby('id','ASC')->get();

        return view('adminpage.file_manager.detail_file', compact('file'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $file = ListFolder::get();
        return view('adminpage.file_manager.new', compact('file'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'   => 'required|max:200',
            'desc'    => 'required|max:2000',
            'folder'  => 'required',
            'attach'  => 'required|max:20000',
            'status'  => 'required'
        ]);
 
        
       
            $user = Auth::user()->id;
    
      
            $file = $request->input('folder');
            $list = ListFolder::where('id',$file)->limit('1')->first();
            $folder = $list->folder;

            $fileName = $request->file('attach')->getClientOriginalName();
            $destinationPath =base_path().'/public/file_manager/'.$folder.'/';
            $proses = $request->file('attach')->move($destinationPath, $fileName);

            $data          = new FileManager;
            
            $data->title   = $request->input('title');
            $data->desc    = $request->input('desc');
            $data->status  = $request->input('status');
            $data->attach  = $fileName;
            $data->user_id = $user;
            $data->folder  = $request->input('folder');
            $data->save();

        return redirect('file-manager')->with(['success' => 'File manager data successfully Saved']);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FileManager  $fileManager
     * @return \Illuminate\Http\Response
     */
    public function show(FileManager $fileManager)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FileManager  $fileManager
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $file = FileManager::where('id',$id)->first();  

        return view('adminpage.file_manager.edit', compact('file'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FileManager  $fileManager
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user()->id;

        $data = FileManager::findOrFail($id);
        $data->title = $request->input('title');
        $data->desc = $request->input('desc');
        $data->status = $request->input('status');
        $data->user_id = $user;

        if (empty($request->file('attach'))){
            $data->attach = $data->attach;
        }
        else{
            $image = $request->file('attach')->getClientOriginalName();
            $destination = base_path() . '/public/filemanager';
            $request->file('attach')->move($destination, $image);
     
            $data['attach'] = $image;
        }
        $data->save();
        
        return redirect('file-manager')->with(['update' => 'File Manager data successfully updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FileManager  $fileManager
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $file = FileManager::findOrFail($id)->delete();   
                        
        return redirect('file-manager')->with(['delete' => 'File Manager data successfully deleted']);
    }


    public function get_folder_name()
    {
        $file = ListFolder::get();
                        
        return view('adminpage.file_manager.folder.index', compact('file'));
    }


    public function update1($id)
    {
        $st         = FileManager::find($id);
        $st->status = '1';
        $st->save();
                        
        return redirect('file-manager')->with(['update' => 'File Manager data successfully updated']);
    }


    public function update0($id)
    {
        $st         = FileManager::find($id);
        $st->status = '0';
        $st->save();
                        
        return redirect('file-manager')->with(['update' => 'File Manager data successfully updated']);
    }


    
}
