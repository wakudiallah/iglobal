<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\praapplication;
use App\Authorizable;
use Illuminate\Support\Facades\Auth;
use App\Package;
use App\Loanpkg;
use App\Employment;
use App\Emp;
use Ramsey\Uuid\Uuid;
use App\DocCust;
use App\DocAssest;
use App\User;
use App\History;
use App\Sendmbsb;
use Mail;
use App\Model_has_role;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Carbon\Carbon;
use Disk;
use App\Loanammount;
use DateTime;
use Response;
use Session;


class AssessmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //status == 1 (New app)
        $assessment = praapplication::orderBy('created_at', 'desc')->where('stage', '=', 'W0')->get();
        $mbsb       = DocAssest::orderBy('created_at', 'desc')->get();
        
        return view('processor1.sendmbsb', compact('assessment','mbsb'));     
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user       = User::where('status', 1)->get();

        $workgroup  = Model_has_role::where('role_id', 3)->get();


        $employment = Employment::all();
        $emp        = Emp::all();
        $loanpkg    = Loanpkg::all();
        $assessment = praapplication::orderBy('created_at', 'desc')->get();

        return view('adminpage.customer.index', compact('assessment', 'employment', 'emp', 'loanpkg', 'workgroup'));
    }

    public function send_mbsb()
    {

        return view('adminpage.reporting.assessment.send_mbsb');
    }

    public function stage_update($id)
    {
        $user = Auth::user()->id;
        
        /*$st = praapplication::find('id_cus', $id);
        $st->stage = 'W1';
        $st->save();*/

        DB::table('praapplication')
        ->where('id_cus', $id)  // find your user by their email
        ->limit(1)  // optional - to ensure only one record is updated.
        ->update(array('stage' => 'W1', 'process3' => $user));



        $request                  = new History;

        $request->cus_id          = $id;  //masih salah harusnya id_cus == masih nungguu file update bnyk
        $request->activity        = "2";
        $request->remark_id       = "W1";
        $request->user_id         = $user;
        $request->note            = "";

        $request->save();

        return redirect('assessment1')->with(['update' => 'Assessment data successfully updated']);
        
    }
    
    public function post_send_mbsb(Request $request)
    {

        $user = Auth::user()->id;
        //'file', 'message', 'status', 'user_id'

        
        //$id_cus = $request->input('id_cus');

       /* $fileinput = $request->input('attach');
        $destinationPath = 'mbsb/doc/';
        //$name      = $request->input('name');
        $date = date('Y-m-d H:i:s');

        if($request->hasFile('attach')) {
            $file = $fileinput;
            $extension = $file->getClientOriginalExtension();
            $file_name = 'Assessment -'. $date. '.' . $extension;
            
            $file->move($destinationPath, $file_name );
        }
        
            $upload_file = $file_name;*/

        

        if($request->hasFile('attach')) {
            $file = $request->file('attach');
            $date = date('Y-m-d');
            $extension = $file->getClientOriginalExtension();
            $file_name = 'Assessment -'. $date. '.' . $extension;
            $destinationPath = 'mbsb/doc';
            $file->move($destinationPath, $file_name );
        }
        
            $upload_file = $file_name;

           /* $st = praapplication::find($id);  //gak bisa karena linknya ic
            $st->spekar = $upload_file;
            $st->process4 = $user;
            $st->save();*/




        $st          = new DocAssest;
    
        $st->message = $request->input('message');
        $st->file    = $upload_file;
        $st->status  = '0';
        $st->user_id = $user;
        $st->save();


            /*$emailfrom  = 'optikserasi.ind@gmail.com';
            $namefrom   = 'Global Test';
            $name = 'MBSB';
            $email = 'wakudiallah05@gmail.com';

            Mail::send('adminpage.message.template_reply', compact('email','name', 'pesan'), function ($message) use ($email, $name, $emailfrom, $namefrom) 
            {
              $message->from($emailfrom, $namefrom);
              $message->subject('Global');
              $message->to($email, $name);
            });*/
        

        return redirect('assessment')->with(['update' => 'Assessment data successfully updated']);

    }

    public function send_excel(Request $request)
        {

            $user = Auth::user();
        
            $item = array_map(null, $request->ids, $request->sta);

            //$ids = $request->id;

            /*DB::table('tablename')->whereIn('id', $ids)
            ->update(['NIS' => 0]);*/

            foreach($ids as $val) {

                 $pra = praapplication::Where('id',$val[0])->update([
                   "Stage"       => 'W1',
                    ]);
            }
       

    return view('adminpage.reporting.assessment.send_mbsb')->with(['update' => 'Assessment data successfully downloaded']);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $icnumber = $request->input('ic');
        $name     = $request->input('name');
        $ic       = $request->input('ic');
        $notelp   = $request->input('notelp');
        $email    = $request->input('email');
        $emp_code = $request->input('emp_code');


        $tanggal = substr($icnumber,4, 2);
        $bulan   = substr($icnumber,2, 2);
        $tahun   = substr($icnumber,0, 2); 

        if($tahun > 30) {
            $tahun2 = "19".$tahun;
        }
        else {
             $tahun2 = "20".$tahun;
        }
       
        $lahir          = $tahun2.'-'.$bulan.'-'.$tanggal;
        $lahir          =  date('Y-m-d', strtotime($lahir. ' - 16 month')); 
        
        $oDateNow       = new DateTime();
        $oDateBirth     = new DateTime($lahir);
        $oDateIntervall = $oDateNow->diff($oDateBirth);

        $umur =  $oDateIntervall->y;

        if($umur >= 60) {

            Session::flash('name', $name); 
            Session::flash('ic', $ic);
            Session::flash('notelp', $notelp); 
            Session::flash('email', $email); 
            Session::flash('emp_code', $emp_code);

            Session::flash('message', 'Sorry your age exceeds the limit!'); 
            Session::flash('alert-class', 'alert-warning'); 

            return redirect('/assess/create')->with(['update' => 'Sorry your age exceeds the limit']);

        }else{

            $user   = Auth::user()->id;
            $id     = Uuid::uuid4()->tostring();
            $custby = $request->input('custby');

            $data                  = new praapplication;

             
            $data->process2        = $request->custby;
            $data->id_cus          = $id;
            $data->name            = $request->name;
            $data->ic              = $request->ic;
            $data->old_ic          = $request->old_ic; 
            $data->notelp          = $request->notelp;
            $data->email           = $request->email;
            //$data->employment_code = $request->employment_code;
            $data->emp_code        = $request->emp_code;
            //$data->elaun           = $request->elaun;
            //$data->pot_bul         = $request->pot_bul;
            //$data->loanpkg_code    = $request->loanpkg_code; pindah ketika di step 5
            //$data->gaji_asas       = $request->gaji_asas;
            //$data->jml_pem         = $request->jml_pem;
            //$data->latitude        = $request->latitude;
            //$data->longitude       = $request->longitude;
            //$data->location        = $request->location;
            //$data->process2        = $request->input('user_mo');name="user_mo"
            $data->clerical_type   = $request->clerical;
            $data->stage           = 'W0';  //W0 = new app
            $data->send            = '0';
            $data->assessment      = $request->custby; //siapa yg isi assessment
            $data->user_id         = $user;
            
            
            $data->save();

            
            $request                  = new History;

            $request->cus_id          = $id;
            $request->name            = $request->name;
            $request->activity        = "1";
            $request->remark_id       = "W0";
            $request->user_id         = $user;
            $request->note            = "";

            $request->save();


            $request                    = new Loanammount;
            $request->id_praapplication = $id;
            $request->save();

     
            return redirect('/assessment/kira/'.$id);
        }
    }

    public function ver_mo(Request $request)
    {
        
        $id = $request->id_cus;
        
        //siapa login
        $user = Auth::user();

        //update praaplication
        //$datapra = praapplication::find($id);
        //$datapra->status = $request->status;
        //$datapra->update();
        
        praapplication::where('id_cus', $id)->update(array('status' => $request->status));

        //$update = praapplication::where('id_cus', $id)->update(['status', $request->status]);


        //tambah history
        $data                  = new History;

        $data->name            = $request->namepra;
        $data->cus_id          = $id;
        $data->activity        = $request->status;
        $data->remark_id       = $request->status;
        $data->user_id         = $user->id;
        $data->note            = $request->note;

        $data->save();
        
        return redirect('/adminnew'); 
    }


    public function kira($id)
    {
        
        $reg = praapplication::latest('id')->limit('1')->first();
        $ids = $reg->id_cus;
        $document1 = DocCust::latest('created_at')->where('cus_id',$id)->where('type','1')->first();
        $document2 = DocCust::latest('created_at')->where('cus_id',$id)->where('type','2')->first();
        
        return view('adminpage.customer.kelayakan', compact('reg', 'document1', 'document2'));
    }


    public function uploads(Request $request,$id)
    {
        $user = Auth::user();
        $cus_id      = $request->input('id_cus');
        $type        = $request->input('type');
        $verification = $request->input('verification');

        $reg         = praapplication::latest('created_at')->where('id_cus',$cus_id)->limit('1')->first();
         
        $ics = $reg->ic;
                  
        $ic_number2 =  str_replace('/', '', $ics);

                 
                if ($request->hasFile('file'.$id)) {
                $name = $request->input('document'.$id);
                $file = $request->file('file'.$id);
                $tipe_file   = $_FILES['file'.$id]['type'];
                //if($tipe_file == "application/pdf") {
                $ic      = $request->input('ic_user');

            
                 $filename = $ic.'-'.$name.'.'.$file->getClientOriginalExtension();
                 $destinationPath = 'documents/user_doc/'.$ic_number2.'/';
                 $file->move($destinationPath, $filename);
       

                $document               = new DocCust;
                $document->cus_id       = $cus_id;
                $document->doc_pdf      = $filename;
                $document->type         = $id;
                $document->user_id      = $user->id;
                $document->verification = $verification;

                $document->save();
                 return response()->json(['file' => "$filename"]);

              // }       

                }  

            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

   

    public function save_table(Request $request) 
    {
       $user = Auth::user();


        $item = array_map(null, $request->id, $request->id_cus, $request->ic, $request->ktp);
        $hash            = str_random(20); 
            
            foreach($item as $val) {
                
                $pra = praapplication::Where('id_cus',$val[0])->update([
                    "stage"       => 'W1',
                    "send"        => '1',
                    "routeto"     => $user->id,
                    "process3"    => $user->id,
                    "hash"         => $hash
                ]);
            }

            
            //update history
        $itemm2 = array_map(null, $request->id, $request->ci);

            foreach($itemm2 as $his) {

                $request                  = new History;

                $request->cus_id          = $his[1];  
                $request->activity        = '4';
                $request->remark_id       = 'W1';
                $request->user_id         = $user->id;
                $request->save();
            }


        //Dia Download
        $current = date('Y-m-d H:i:s');
        
        $excel = Excel::create('Assessment'.$current, function($excel) use($hash, $item) {

            $excel->setTitle('My awesome report 2018');

            // Chain the setters
            $excel->setCreator('Global File')->setCompany('Global');

            $excel->sheet('Sheet 1', function($sheet) use($hash, $item) {

            $assessments = praapplication::join('emp', 'emp.id', '=', 'praapplication.emp_code')
                ->select(
                  'praapplication.created_at', 
                  'praapplication.name', 
                  'praapplication.ic',
                  'praapplication.old_ic',
                  'emp.Emp_Desc')
                ->orderBy('praapplication.created_at', 'desc')->where('praapplication.stage', '=', 'W1')->where('hash',$hash)
                ->get();

                $i = 1;
                    foreach($assessments as $product) {
                     $data[] = array(
                        $i++,
                        $product->created_at->toFormattedDateString(),
                        $product->name,
                        $product->ic,
                        $product->old_ic,
                        $product->Emp_Desc,
                    );
                }
                $sheet->fromArray($data, null, 'A1', false, false);
                    $ir = $i-1;
                    $is = $i+1;
                  $range = "A1:F{$ir}";
                $sheet->setBorder($range, 'thin');
                $headings = array('No', 'By Hand','Customers Name','New IC', 'Old IC (If Any)', 'Company');
                 $sheet->setBorder( "A{$is}:F{$is}" ,'none');
                //$sheet->setBorder('A2:A10');
                $sheet->prependRow(1, $headings);
                // add header
                $current = date('Y-m-d ');
                    $sheet->prependRow(1, ["LIST OF ASSESSMENT FORM SUBMIT ON ".$current]);

                    //$sheet->prependRow(2, ["MASTER AGENT : GLOBAL I EXCEED MANAGEMENT SDN BHD"]);

                    $sheet->mergeCells("A1:F1");

                    $sheet->cell('A1', function($cell) {
                        // change header color
                      
                        $cell->setBackground('#ffffff')
                            ->setFontColor('#000000')
                            ->setFontWeight('bold')
                            ->setAlignment('center')
                            ->setValignment('center');
                          // ->setBorder('none', 'none', 'thin', 'thin');
                    });

                    $footerRow = count($assessments) + 3;
                    $sheet->appendRow([
                        'Name : '  
                    ]);
                    $sheet->appendRow([
                        'Date / Time : '  
                    ]);
                    $sheet->mergeCells("F{$footerRow}:G{$footerRow}");
                    $sheet->cell("F{$footerRow}", function($cell) {
                        $cell->setAlignment('center')
                            ->setValignment('center')
                            ->setFontColor('#000000')
                            ->setFontSize(10);
                    });
                
                });
      

            /*--------- Attachment Doc ----------- */
            $doc = DocCust::join('praapplication', 'doc_cust.cus_id', '=', 'praapplication.id_cus')
                ->select('doc_cust.doc_pdf as doc_pdf', 'praapplication.ic as icx')
                ->latest('doc_cust.created_at')->where('praapplication.hash',$hash)->where('doc_cust.type','1')->get();

            $doc2 = DocCust::join('praapplication', 'doc_cust.cus_id', '=', 'praapplication.id_cus')
                ->select('doc_cust.doc_pdf as doc_pdf', 'praapplication.ic as icx')
                ->latest('doc_cust.created_at')->where('praapplication.hash',$hash)->where('doc_cust.type','2')->get();
            /*--------- End Attachment Doc ----------- */


            Mail::send('adminpage.message.assessment_send', compact('doc','hash'),  function($message) use($excel,  $item, $doc, $doc2)
            {
                $message->from('optikserasi.ind@gmail.com', 'Global');
                $message->to('wakudiallah05@gmail.com')->subject('Assessment');
                $message->attach($excel->store("xls",false,true)['full']);
                
                foreach($doc as $doc){

                    $message->attach('documents/user_doc/'.$doc->icx.'/'.$doc->doc_pdf);
                }

                  foreach($doc2 as $doc2){

                    $message->attach('documents/user_doc/'.$doc2->icx.'/'.$doc2->doc_pdf);
                }
                   
                    
            });

             return redirect('/adminnew')->with(['update' => 'Assessment data successfully downloaded']);
              })->download('xlsx');
    }



     public function save_table_Benar(Request $request) 
    {
       $user = Auth::user();


        $item = array_map(null, $request->id, $request->id_cus, $request->ic, $request->ktp);
        $hash            = str_random(20); 
            
            foreach($item as $val) {
                
                $pra = praapplication::Where('id_cus',$val[0])->update([
                    "stage"       => 'W1',
                    "send"        => '1',
                    "routeto"     => $user->id,
                    "process3"    => $user->id,
                    "hash"         => $hash
                ]);
            }

            
            //update history
        $itemm2 = array_map(null, $request->id, $request->ci);

            foreach($itemm2 as $his) {

                $request                  = new History;

                $request->cus_id          = $his[1];  
                $request->activity        = '4';
                $request->remark_id       = 'W1';
                $request->user_id         = $user->id;
                $request->save();
            }


        //Dia Download
        $current = date('Y-m-d H:i:s');
        
        $excel = Excel::create('Assessment'.$current, function($excel) use($hash, $item) {

            $excel->setTitle('My awesome report 2016');

            // Chain the setters
            $excel->setCreator('Global File')->setCompany('Global');

            $excel->sheet('Sheet 1', function($sheet) use($hash, $item) {

            $assessments = praapplication::join('emp', 'emp.id', '=', 'praapplication.emp_code')
                ->select(
                  'praapplication.created_at', 
                  'praapplication.name', 
                  'praapplication.ic',
                  'praapplication.old_ic',
                  'emp.Emp_Desc')
                ->orderBy('praapplication.created_at', 'desc')->where('praapplication.stage', '=', 'W1')->where('hash',$hash)
                ->get();

                $i = 1;
                    foreach($assessments as $product) {
                     $data[] = array(
                        $i++,
                        $product->created_at->toFormattedDateString(),
                        $product->name,
                        $product->ic,
                        $product->old_ic,
                        $product->Emp_Desc,
                    );
                }
                $sheet->fromArray($data, null, 'A1', false, false);
                    $ir = $i-1;
                    $is = $i+1;
                  $range = "A1:F{$ir}";
                $sheet->setBorder($range, 'thin');
                $headings = array('No', 'By Hand','Customers Name','New IC', 'Old IC (If Any)', 'Company');
                 $sheet->setBorder( "A{$is}:F{$is}" ,'none');
                //$sheet->setBorder('A2:A10');
                $sheet->prependRow(1, $headings);
                // add header
                $current = date('Y-m-d ');
                    $sheet->prependRow(1, ["LIST OF ASSESSMENT FORM SUBMIT ON ".$current]);

                    //$sheet->prependRow(2, ["MASTER AGENT : GLOBAL I EXCEED MANAGEMENT SDN BHD"]);

                    $sheet->mergeCells("A1:F1");

                    $sheet->cell('A1', function($cell) {
                        // change header color
                      
                        $cell->setBackground('#ffffff')
                            ->setFontColor('#000000')
                            ->setFontWeight('bold')
                            ->setAlignment('center')
                            ->setValignment('center');
                          // ->setBorder('none', 'none', 'thin', 'thin');
                    });

                    $footerRow = count($assessments) + 3;
                    $sheet->appendRow([
                        'Name : '  
                    ]);
                    $sheet->appendRow([
                        'Date / Time : '  
                    ]);
                    $sheet->mergeCells("F{$footerRow}:G{$footerRow}");
                    $sheet->cell("F{$footerRow}", function($cell) {
                        $cell->setAlignment('center')
                            ->setValignment('center')
                            ->setFontColor('#000000')
                            ->setFontSize(10);
                    });
                
                });
      

            /*--------- Attachment Doc ----------- */
            $doc = DocCust::join('praapplication', 'doc_cust.cus_id', '=', 'praapplication.id_cus')
                ->select('doc_cust.doc_pdf as doc_pdf', 'praapplication.ic as icx')
                ->latest('doc_cust.created_at')->where('praapplication.hash',$hash)->where('doc_cust.type','1')->get();

            $doc2 = DocCust::join('praapplication', 'doc_cust.cus_id', '=', 'praapplication.id_cus')
                ->select('doc_cust.doc_pdf as doc_pdf', 'praapplication.ic as icx')
                ->latest('doc_cust.created_at')->where('praapplication.hash',$hash)->where('doc_cust.type','2')->get();
            /*--------- End Attachment Doc ----------- */


            Mail::send('adminpage.message.assessment_send', compact('doc','hash'),  function($message) use($excel,  $item, $doc, $doc2)
            {
                $message->from('optikserasi.ind@gmail.com', 'Global');
                $message->to('wakudiallah05@gmail.com')->subject('Assessment');
                $message->attach($excel->store("xls",false,true)['full']);
                
                foreach($doc as $doc){

                    $message->attach('documents/user_doc/'.$doc->icx.'/'.$doc->doc_pdf);
                }

                  foreach($doc2 as $doc2){

                    $message->attach('documents/user_doc/'.$doc2->icx.'/'.$doc2->doc_pdf);
                }
                   
                    
            });

             return redirect('/assessment1')->with(['update' => 'Assessment data successfully downloaded']);
              })->download('xlsx');
    }



}
