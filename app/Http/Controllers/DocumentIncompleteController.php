<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\praapplication;
use Illuminate\Support\Facades\Auth;
use App\Announcement;
use App\DocCust;
use DB;
use Mapper;
use DateTime;
use PDF;
use Hash;
use App\History;
use App\Remark;
use App\User;
use App\Role;
use App\Model_has_role;
use App\AddInfo;
use App\MeetCust;
use App\Package;
use App\Loanpkg;
use App\Employment;
use App\Emp;
use App\Homeimage;
use App\Tenure;
use App\Loan;
use App\LoanAmmount;
use Input;

class DocumentIncompleteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        
        $user = Auth::user()->id;

        $docin = praapplication::where('routeto', $user)->wherein('stage', ['W7','W13'])->orderBy('created_at', 'desc')->get();

        //$docin = praapplication::where('routeto', $user)->where('stage',  'W14')->orderBy('created_at', 'desc')->get();
        //$docin = praapplication::where('process2', $user)->wherein('stage', ['W0','W1','W2','W3','W6','W14','w4'])->orderBy('created_at', 'desc')->get();

        return view('mo.docincomplete', compact('docin'));
    }

    public function nextcalchange()
    {
        
        
        $change = praapplication::where('stage', 'W6')->orderBy('created_at', 'desc')->get();


        return view('mo.change', compact('change'));
    }

    public function reject_ic_loc()
    {
        //$change = DB::table('praapplication')
          //  ->join('history', 'praapplication.id_cus', '=', 'history.cus_id')
           // ->where('praapplication.stage', 'W6')
           // ->get();

        $reject = praapplication::where('stage', 'W14')->orderBy('created_at', 'desc')->get();


        return view('mo.rejecticloc', compact('reject'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
