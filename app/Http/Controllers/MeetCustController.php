<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\praapplication;
use Illuminate\Support\Facades\Auth;
use App\Announcement;
use App\Emp;
use App\DocCust;
use DB;
use Mapper;
use DateTime;
use PDF;
use Hash;
use App\History;
use App\Remark;
use App\User;
use App\Role;
use App\Model_has_role;
use App\AddInfo;
use App\MeetCust;


class MeetCustController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MeetCust  $meetCust
     * @return \Illuminate\Http\Response
     */
    public function show(MeetCust $meetCust)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MeetCust  $meetCust
     * @return \Illuminate\Http\Response
     */
    public function edit(MeetCust $meetCust)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MeetCust  $meetCust
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MeetCust $meetCust)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MeetCust  $meetCust
     * @return \Illuminate\Http\Response
     */
    public function destroy(MeetCust $meetCust)
    {
        //
    }
}
