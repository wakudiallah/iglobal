<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\praapplication;
use Illuminate\Support\Facades\Auth;
use App\Announcement;
use App\DocCust;
use DB;
use Mapper;
use DateTime;
use PDF;
use Hash;
use App\History;
use App\Remark;
use App\User;
use App\Role;
use App\Model_has_role;
use App\AddInfo;
use App\MeetCust;
use App\Package;
use App\Loanpkg;
use App\Employment;
use App\Emp;
use App\Homeimage;
use App\Tenure;
use App\Loan;
use App\LoanAmmount;
use App\Batchassessment;
use Input;
use Ramsey\Uuid\Uuid;
use Session;

class POneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user       = User::where('status', 1)->get();

        $workgroup  = Model_has_role::where('role_id', 3)->get();


        $employment = Employment::all();
        $emp        = Emp::all();
        $loanpkg    = Loanpkg::all();
        $assessment = praapplication::orderBy('created_at', 'desc')->get();

        return view('adminpage.customer.index', compact('assessment', 'employment', 'emp', 'loanpkg', 'workgroup'));
    }

    public function uploadspekar_p1()
    {
        $user = Auth::user()->id;
        
        $spekar = praapplication::where('stage', 'W1' )->orderBy('created_at', 'desc')->get(); //Siap upload 

        return view('processor1.upload_spekar_new', compact('spekar'));
        
    }

    public function route_to_mo()
    {
        $user = Auth::user()->id;
        
        $spekar = praapplication::where('stage', 'W1' )->where('spekar', '!=', 'NULL')->orderBy('created_at', 'desc')->get(); //Siap upload SPEKAR  //W2 Ready upload Spekar

        return view('processor1.pending_route_to_mo', compact('spekar'));
        
    }

    
    public function iclocreject(Request $request, $id)
    {
        
        $user = Auth::user()->id;

        $process2 = $request->input('process2');
        $note     = $request->input('note');

       praapplication::where('id_cus', $id)->update(array('stage' => 'W14', 'routeto' => $process2  )); 

       $request                  = new History;

        $request->cus_id          = $id;  
        $request->activity        = "1";
        $request->remark_id       = 'W14';
        $request->user_id         = $user;
        $request->note            = $note;
        $request->save();

        return redirect('assessment1')->with(['update' => 'Data rejected successfully']);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $icnumber = $request->input('ic');
        $name     = $request->input('name');
        $ic       = $request->input('ic');
        $notelp   = $request->input('notelp');
        $email    = $request->input('email');
        $emp_code = $request->input('emp_code');


        $tanggal = substr($icnumber,4, 2);
        $bulan   = substr($icnumber,2, 2);
        $tahun   = substr($icnumber,0, 2); 

        if($tahun > 30) {
            $tahun2 = "19".$tahun;
        }
        else {
             $tahun2 = "20".$tahun;
        }
       
        $lahir          = $tahun2.'-'.$bulan.'-'.$tanggal;
        $lahir          =  date('Y-m-d', strtotime($lahir. ' - 16 month')); 
        
        $oDateNow       = new DateTime();
        $oDateBirth     = new DateTime($lahir);
        $oDateIntervall = $oDateNow->diff($oDateBirth);

        $umur =  $oDateIntervall->y;

        if($umur >= 60) {

            Session::flash('name', $name); 
            Session::flash('ic', $ic);
            Session::flash('notelp', $notelp); 
            Session::flash('email', $email); 
            Session::flash('emp_code', $emp_code);

            Session::flash('message', 'Sorry your age exceeds the limit!'); 
            Session::flash('alert-class', 'alert-warning'); 

            return redirect('/assess/create')->with(['update' => 'Sorry, age exceeds the limit']);

        }else{

            $user   = Auth::user()->id;
            $id     = Uuid::uuid4()->tostring();
            $custby = $request->input('custby');

            $by_mo     = User::where('id', $custby)->first();
            $manager   = $by_mo->manager;
            $team_lead = $by_mo->leader;


            $data                  = new praapplication;

             
            $data->process2        = $request->custby;
            $data->id_cus          = $id;
            $data->name            = $request->name;
            $data->ic              = $request->ic;
            $data->old_ic          = $request->old_ic; 
            $data->notelp          = $request->notelp;
            $data->email           = $request->email;
            $data->emp_code        = $request->emp_code;
            //$data->latitude        = $request->latitude;
            //$data->longitude       = $request->longitude;
            //$data->location        = $request->location;
            $data->stage           = 'W0';  //W0 = new app
            $data->send            = '0';
            $data->assessment      = $request->custby; //siapa yg isi assessment
            $data->user_id         = $user;
            $data->manager         = $manager;
            $data->team_lead       = $team_lead;
            $data->biro_type       = $request->biro;
            
            if($emp_code == 2){  //Maybank 
               $data->clerical_type   = $request->clerical;
            }
            elseif($emp_code == 1){ //Others
                $data->emp_others = $request->others_employer;
            }
            else{
                $data->clerical_type   = '0';
            }
            
            $data->save();

            
            $request                  = new History;

            $request->cus_id          = $id;
            $request->name            = $request->name;
            $request->activity        = "1";
            $request->remark_id       = "W0";
            $request->user_id         = $user;
            $request->note            = "";

            $request->save();


            $request                    = new Loanammount;
            $request->id_praapplication = $id;
            $request->save();

            $request         = new MeetCust;
            $request->cus_id = $id;
            $request->save();

            $request         = new AddInfo;
            $request->cus_id = $id;
            $request->save();





     
            return redirect('/cus/add/'.$id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function get_assessment_list()
    {
        $batch = Batchassessment::get();

        return view('processor1.list_assessment', compact('batch'));
    }


    
    public function post_assessment_reject(Request $request)
    {
        $user = Auth::user()->id;

        $item = array_map(null, $request->id_cus, $request->ids, $request->sta);

        //praapplication::where('id_cus', $id)->update(array('process11' => $user, 'stage' => 'W12'  ));

        foreach($item as $val) {
                
                $pra = praapplication::Where('id_cus',$val[0])->update([
                    
                    "stage"  => 'W18'
                    
                ]);

                $request                  = new History;

                $request->cus_id          = $val[0];  
                $request->activity        = "11";
                $request->remark_id       = 'W18';
                $request->user_id         = $user;
                $request->note            = '';
                $request->save();
            }


        return redirect('get-assessment-list')->with(['update' => 'Data saved successfully']);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
