<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Authorizable;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Remark;
use App\Role;

class RemarkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        
        $remark = Remark::orderBy('role_id', 'asc')->get();

        return view('adminpage.setupmodule.remark.index', compact('remark'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('name', 'id');

        return view('adminpage.setupmodule.remark.new', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
            'id_remark' => 'required|max:4',
            'desc'      => 'required|max:60',
            'color'     => 'required|max:60',
            'role_id'   => 'required'
        ]);

        $request->user()->remark()->create($request->all());

        return redirect()->route('remark.index')->with(['success' => 'Remark berjaya ditambah']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Role::pluck('name', 'id');
        $remark = Remark::where('id',$id)->first();  
        
        return view('adminpage.setupmodule.remark.edit', compact('remark','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Remark $remark)
    {
        $this->validate($request, [
            'desc'      => 'required|max:60',
            'color'     => 'required|max:60'
            
        ]);

        $me = $request->user();

        if( $me->hasRole('Admin') ) {
            $remark = Remark::findOrFail($remark->id);
        } else {
            $remark = $me->remark()->findOrFail($remark->id);
        }

        $remark->update($request->all());

        return redirect('remark')->with(['update' => 'Remark berjaya di kemaskini']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $emp = Remark::findOrFail($id)->delete();   
                        
        return redirect('remark')->with(['delete' => 'Remark berjaya di padam']);
    }
}
