<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\praapplication;
use App\Authorizable;
use Illuminate\Support\Facades\Auth;
use App\Package;
use App\Loanpkg;
use App\Employment;
use App\Emp;
use Ramsey\Uuid\Uuid;
use App\DocCust;
use App\DocAssest;
use App\User;
use App\History;
use App\Sendmbsb;
use Mail;
use App\Model_has_role;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Carbon\Carbon;
use Disk;
use App\Loanammount;
use DateTime;
use Response;
use Session;


class PTeamLeaderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function submission()
    {
        $user = Auth::user()->id;  

        $submission = praapplication::where('stage', 'W0')->where('team_lead', $user)->orWhere('process2', $user)->orderBy('created_at', 'desc')->get();


        return view('teamlead.submission', compact('submission'));
    }

    public function rejected()
    {
        $user = Auth::user()->id;  

        $rejected = praapplication::where('stage', 'W12')->where('team_lead', $user)->orderBy('created_at', 'desc')->get();

        
        return view('teamlead.rejected', compact('rejected'));
    }

    public function approved()
    {
        $user = Auth::user()->id;  

        $approved = praapplication::where('stage', 'W11')->where('team_lead', $user)->orderBy('created_at', 'desc')->get();

        
        return view('teamlead.approved', compact('approved'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
