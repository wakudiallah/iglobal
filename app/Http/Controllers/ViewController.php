<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\praapplication;
use Illuminate\Support\Facades\Auth;
use App\Announcement;
use App\DocCust;
use DB;
use Mapper;
use DateTime;
use PDF;
use Hash;
use App\History;
use App\Remark;
use App\User;
use App\Role;
use App\Model_has_role;
use App\AddInfo;
use App\MeetCust;
use App\Package;
use App\Loanpkg;
use App\Employment;
use App\Emp;
use App\Homeimage;
use App\Tenure;
use App\Loandisburse;
use App\Loan;
use App\LoanDetail;
use App\TenureDetail;
use App\Financial;
use App\LoanAmmount;
use Input;

class ViewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function first_step($id)
    {
        $docin = praapplication::where('stage', 'W0' )->orderBy('created_at', 'desc')->get(); 
        $user = Auth::user()->id;
        $step1        = Praapplication::where('id_cus', $id)->first();

        $approval = praapplication::where('id_cus', $id)->get();
        $history = History::where('cus_id', $id)->latest('id')->first();
        
        return view('ashare.step1', compact('docin', 'step1', 'approval', 'history'));
    }


    public function second_step($id)
    {
        $docin    = praapplication::where('stage', 'W3' )->orderBy('created_at', 'desc')->get(); 
        $user     = Auth::user()->id;
        $step1    = Praapplication::where('id_cus', $id)->first();
        
        $approval = praapplication::where('id_cus', $id)->get();
        $history  = History::where('cus_id', $id)->latest('id')->first();
        $loanammount = LoanAmmount::where('id_praapplication',$id)->first();
        
        return view('ashare.step2', compact('docin', 'step1', 'approval', 'history', 'loanammount'));  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function fourth_step($id)
    {
        $docin    = praapplication::where('stage', 'W3' )->orderBy('created_at', 'desc')->get(); 
        $user     = Auth::user()->id;
        $step1    = Praapplication::where('id_cus', $id)->first();
        
        $approval = praapplication::where('id_cus', $id)->get();
        $history  = History::where('cus_id', $id)->latest('id')->first();
        $loanammount = LoanAmmount::where('id_praapplication',$id)->first();
        $info         = AddInfo::where('cus_id', $id)->latest('updated_at')->limit('1')->first();
        $loanammount = LoanAmmount::where('id_praapplication',$id)->first();

        $loandisburse = Loandisburse::where('cus_id',$id)->latest('created_at')->limit('1')->first();
        
        
        return view('ashare.step4', compact('docin', 'step1', 'approval', 'history', 'loanammount', 'info', 'loanammount', 'loandisburse'));  
    }

    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
