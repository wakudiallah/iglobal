<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\praapplication;
use Illuminate\Support\Facades\Auth;
use App\Announcement;
use App\Emp;
use App\DocCust;
use DB;
use Mapper;
use DateTime;
use PDF;
use Hash;
use App\History;
use App\Remark;
use App\User;
use App\Role;
use App\Model_has_role;
use App\DocAssest;
use App\LoanAmmount;
use App\Loandisburse;
use File;
use PHPExcel;
use Excel;
use Session;

class PThreeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function update_approval()
    {
        $user = Auth::user()->id;

        $reportmanag = praapplication::where('stage', 'W5' )->where('manager', $user)->orderBy('created_at', 'desc')->get();
        $approval = praapplication::where('stage', 'W8')->orderBy('created_at', 'desc')->get();
        

        return view('processor3.loan_approval', compact('reportmanag', 'approval'));
    }


    public function update_approval_perone($id)
    {
        $user = Auth::user()->id;

       $approval = praapplication::wherein('stage', ['W8', 'W16', 'W17'])->where('id_cus', $id)->get();
        $history = History::where('remark_id', 'W8')->where('cus_id', $id)->latest('id')->first();

        return view('processor3.loan_approval_perone', compact('history', 'approval'));
    }

    public function approve_fix()
    {
        $user = Auth::user()->id;
        
        $approval = praapplication::where('stage', 'W11')->get();

        return view('processor3.approved', compact( 'approval'));
    }

    public function reject_fix()
    {
        $user = Auth::user()->id;
        
        $approval = praapplication::where('stage', 'W12')->get();

        return view('processor3.rejected', compact( 'approval'));
    }

    public function add_doc()
    {
        $user = Auth::user()->id;
        
        $approval = praapplication::where('stage', 'W13')->get();

        return view('processor3.add_doc', compact( 'approval'));
    }

    /*public function additional_confirm()
    {
        $user = Auth::user()->id;
        
        $approval = praapplication::where('stage', 'W13')->get();

        return view('processor3.add_doc', compact( 'approval'));
    }*/


    public function approval_approve(Request $request, $id)
    {
        $user = Auth::user()->id;
        $loan_approve = $request->input('loan_approve');


        praapplication::where('id_cus', $id)->update(array('process11' => $user, 'stage' => 'W11', 'loan_approve' => $loan_approve  ));

        LoanAmmount::where('id_praapplication', $id)->update(array('loan_approve' => $loan_approve ));

        
        $request                  = new History;

        $request->cus_id          = $id;  
        $request->activity        = "11";
        $request->remark_id       = 'W11';
        $request->user_id         = $user;
        $request->note            = '';
        $request->save();

        return redirect('adminnew')->with(['update' => 'Data saved successfully']);
    }

    public function upload_get()
    {

        return view('processor3.form_upload');
    }

  
    public function upload_amount(Request $request)
    {
        
        //validate the xls file
        $this->validate($request, array(
            'file'      => 'required'
        ));
 
        if($request->hasFile('file')){
            $extension = File::extension($request->file->getClientOriginalName());
            
            if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {
 
                $path = $request->file->getRealPath();
                $data = Excel::load($path, function($reader) {
                })->get();
                if(!empty($data) && $data->count()){
 
                    foreach ($data as $value) {
                        $insert['data'] = [
                        'no'  => $value->no,
                        'nama'  => $value->nama,
                        'ic_no' => $value->ic_no,
                        'disburse_date' => $value->disburse_date,
                        'product_type_description' => $value->product_type_description,
                        'amount_release' => $value->amount_release,
                        'amount' => $value->amount,
                        'date_disburse' => $value->date_disburse,
                        'net_disbursement' => $value->net_disbursement,
                        ];
                    
 
                    if(!empty($insert))
                      {
                            $user = Auth::user()->id;

                            //ambil data praap
                            $pra = praapplication::where('ic', $value['ic_no'])->get();

                            praapplication::where('ic', $value['ic_no'])->update(array('stage' => 'W11', 'loan_approve' => $value['amount_release'] ));

                            //Loandisburse::where()
                            
                           

                            foreach ($pra as $data){
                                
                                $request                  = new History;

                                $request->cus_id          = $data->id_cus;  
                                $request->activity        = "11";
                                $request->remark_id       = 'W11';
                                $request->user_id         = $user;
                                $request->note            = '';
                                $request->save();


                                $request                  = new Loandisburse;

                                $request->cus_id           = $data->id_cus;  
                                $request->ic               = $data->ic;
                                $request->disburse_date    = $value['disburse_date'];
                                $request->amount_release   = $value['amount_release'];
                                $request->amount           = $value['amount'];
                                $request->date_disburse    = $value['date_disburse'];
                                $request->net_disbursement = $value['net_disbursement'];
                                    
                                $request->save();

                                

                            }



                        /*foreach ($insert as $value)
                          { 
                            $pra = praapplication::where('stage', 'W8')->get();
                            if($insert[1] == 920101324324){
                              praapplication::where('ic', $insert[1])->where('stage', 'W8')->update(array('loan_approve' => $insert[4] ));
                            }
                          }*/
                            
                    }
                }
                 return redirect('/upload/amount')->with(['update' => 'Data saved successfully   rows' ]);
                }
 
 
            }else {
                
                return redirect('/upload/amount')->with(['update' => 'File is a '.$extension.' file.!! Please upload a valid xls/csv file..!!' ]);
            }
        }
    
    }


    public function approval_reject($id)
    {
        $user = Auth::user()->id;

        praapplication::where('id_cus', $id)->update(array('process11' => $user, 'stage' => 'W12'  ));

        $request                  = new History;

        $request->cus_id          = $id;  
        $request->activity        = "11";
        $request->remark_id       = 'W12';
        $request->user_id         = $user;
        $request->note            = '';
        $request->save();

        return redirect('adminnew')->with(['update' => 'Data saved successfully']);
    }

    
    public function confirm_add_received($id)
    {
        $user = Auth::user()->id;

        praapplication::where('id_cus', $id)->update(array( 'stage' => 'W16'));

        $request                  = new History;

        $request->cus_id          = $id;  
        $request->activity        = "11";
        $request->remark_id       = 'W16';
        $request->user_id         = $user;
        $request->note            = '';
        $request->save();

        return redirect('adminnew')->with(['update' => 'Data saved successfully']);

    }

    
    public function adddocprocess11(Request $request, $id)
    {
        $user = Auth::user()->id;
        
        $note     = $request->input('note');
        $process5 = $request->input('process5');

        praapplication::where('id_cus', $id)->update(array('process11' => $user, 'stage' => 'W13', 'routeto' => $process5  ));

        $request                  = new History;

        $request->cus_id          = $id;  
        $request->activity        = "11";
        $request->remark_id       = 'W13';
        $request->user_id         = $user;
        $request->note            = $note;
        $request->save();

        return redirect('adminnew')->with(['update' => 'Data saved successfully']);
    }


    public function additional_doc(Request $request, $id)
    {
        $user = Auth::user()->id;
        
        $note     = $request->input('additional_doc');

        praapplication::where('id_cus', $id)->update(array('process11' => $user, 'stage' => 'W13'  ));

        $request                  = new History;

        $request->cus_id          = $id;  
        $request->activity        = "11";
        $request->remark_id       = 'W13';
        $request->user_id         = $user;
        $request->note            = $note;
        $request->save();

        return redirect('adminnew')->with(['update' => 'Data saved successfully']);
    }


    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function report_monthly()
    {
        
        
        return view('processor3.report_monthly');
    }


    public function report_monthly_view(Request $request)
    {

            $tanggal1  = $request->input('tanggal1');
            $tanggal2  = $request->input('tanggal2');

            //$empname = Employment::latest('created_at')->where('id',$jenis_pekerjaan)->first();

            $status  = $request->input('status');

            if ($status=='W11') {
                $s_string = "Approved"; 
                $like ="Change Status : Approved";

            }

            elseif ($status=='W12') {
                $s_string = "Rejected";
                $like = "Rejected";

            }

            elseif ($status=='W') {

                $s_string = "All";

            }


            $viewdate1 = date("d-m-Y", strtotime($tanggal1));
            $viewdate2 = date("d-m-Y", strtotime($tanggal2));
            $date1 =  date('Y-m-d 00:00:01', strtotime($tanggal1)); 
            $date2 =  date('Y-m-d 23:59:59', strtotime($tanggal2)); 
            

            if ($status=='W11') {
                $pra = praapplication::latest('updated_at')->where('updated_at','>=',$date1)->where('updated_at','<=',$date2)->where('stage','W11')->get(); 
                $sum_loan_approved = praapplication::latest('updated_at')->where('updated_at','>=',$date1)->where('updated_at','<=',$date2)->where('stage','W11')->sum('loan_approve');

                $sum_loan_amount = praapplication::latest('updated_at')->where('updated_at','>=',$date1)->where('updated_at','<=',$date2)->where('stage','W11')->sum('jml_pem');
                
               
                /*$sum_loan_amount = praapplication::join('loanammount','loanammount.id_praapplication','=','praapplication.id_cus')
                  ->select(
                   DB::raw('SUM(loanammount) as loanammount')
                  )
                  ->where('praapplication.updated_at','>=',$date1)->where('praapplication.updated_at','<=',$date2)->where('stage','W11')
                  ->groupBy('loanammount.loanammount')
                  ->orderBy("loanammount.created_at")
                  ->sum();*/
            }

            elseif($status=='W12') {

                $pra = praapplication::latest('updated_at')->where('updated_at','>=',$date1)->where('updated_at','<=',$date2)->where('stage','W12')->get(); 
                $sum_loan_approved = praapplication::latest('updated_at')->where('updated_at','>=',$date1)->where('updated_at','<=',$date2)->where('stage','W12')->sum('loan_approve'); 
                $sum_loan_amount = praapplication::latest('updated_at')->where('updated_at','>=',$date1)->where('updated_at','<=',$date2)->where('stage','W12')->sum('jml_pem');  
            } 

            elseif($status=='W') {
                $pra = praapplication::latest('updated_at')->where('updated_at','>=',$date1)->where('updated_at','<=',$date2)->whereIn('stage',['W11','W12'])->get(); 

                $sum_loan_approved = praapplication::latest('updated_at')->where('updated_at','>=',$date1)->where('updated_at','<=',$date2)->whereIn('stage',['W11','W12'])->sum('loan_approve');

                $sum_loan_amount = praapplication::latest('updated_at')->where('updated_at','>=',$date1)->where('updated_at','<=',$date2)->whereIn('stage',['W11','W12'])->sum('jml_pem');

            }


           return view('processor3.report_monthly_view', compact('pra','user','tanggal1','tanggal2','date1','date2','viewdate1','viewdate2','sales','jenis_pekerjaan','employment','empname','status','s_string', 'status', 'sum_loan_approved', 'sum_loan_amount'));   

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
