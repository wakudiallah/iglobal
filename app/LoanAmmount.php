<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loanammount extends Model
{
   	protected $table = 'loanammount';
   
    protected $fillable = ['package','loanammount','maxloan','id_tenure','method','id_praapplication'];

    
    public function Tenure() {
        return $this->belongsTo('App\Tenures','id_tenure','id'); //id =  table user == model_id = table model has roles
    }
}
