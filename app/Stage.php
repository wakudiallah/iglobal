<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Stage extends Model
{
    //`id`, `id_stage`, `desc`, `color`, `user_id`, `deleted_at`, `created_at`, `updated_at`
    
    protected $table = 'stage';

    protected $fillable = ['id_stage','desc', 'color' ];

    use SoftDeletes;

	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
