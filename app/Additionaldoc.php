<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Additionaldoc extends Model
{
    //`id`, `cus_id`, `office_telp`, `user_id`, `created_at`, `updated_at`,  empcode
    
    protected $table 	= 'additional_doc';


    use SoftDeletes;

	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;
}
