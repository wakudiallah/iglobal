<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class praapplication extends Model
{
    
    protected $table = 'praapplication';

    protected $fillable = ['name','id_cus','ic','old_ic','notelp','employment_code','emp_code','elaun', 'pot_bul', 'loanpkg_code', 'gaji_asas', 'jml_pem', 'act', 'status', 'routeto', 'stage', 'user_id', 'emp_type', 'process6', 'process2', 'uniq_date', 'uniq_no' ];

    use SoftDeletes;

	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;

    
    public function additionaldoc()
    {
        return $this->belongsTo('App\Additionaldoc', 'id_cus', 'cus_id')->latest();
    }

    public function spekar()
    {
        return $this->belongsTo('App\DocCust', 'id_cus', 'cus_id');
    }

    public function stages()
    {
        return $this->belongsTo('App\Stage', 'stage', 'id_stage');
    }
	
    public function majikan()
    {
        return $this->belongsTo('App\Emp', 'emp_code', 'id');
    }

    
    public function remarks()
    {
        return $this->belongsTo('App\Remark', 'status', 'id');
    }



    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function p2()
    {
        return $this->belongsTo('App\User', 'process2', 'id');
    }

    public function print()
    {
        return $this->belongsTo('App\User', 'process5', 'id');
    }

    public function loanpkg()
    {
        return $this->belongsTo('App\Loanpkg', 'loanpkg_code', 'id');
    }    

    public function doc()
    {
        return $this->belongsTo('App\DocCust', 'id_cus', 'cus_id');
    }

    public function doc1()
    {
        return $this->belongsTo('App\DocCust', 'id_cus', 'cus_id');
    }

     public function remark_6()
    {
        return $this->belongsTo('App\History', 'id_cus', 'cus_id');
    }

    public function historydate()
    {
        return $this->belongsTo('App\History', 'id_cus', 'cus_id');
    }

    public function historynote()
    {
        return $this->belongsTo('App\History', 'id_cus', 'cus_id')->where('history.remark_id','=','W13'); 
    }

    public function historydate2() {
        return $this->belongsTo('App\History', 'id_cus','cus_id')->Where('remark_id','=', 'stage');
    }

    public function MO()
    {
        return $this->belongsTo('App\User', 'assessment', 'id');
    }


    //MO Task List
    public function date_w2() {
        return $this->hasMany('App\History', 'cus_id','id_cus')->where('history.remark_id','=','W2');    
    }

    public function date_w3() {
        return $this->hasMany('App\History', 'cus_id','id_cus')->where('history.remark_id','=','W3');    
    }

    public function date_w6() {
        return $this->hasMany('App\History', 'cus_id','id_cus')->where('history.remark_id','=','W6');    
    }

    public function date_w7() {
        return $this->hasMany('App\History', 'cus_id','id_cus')->where('history.remark_id','=','W7');    
    }

    public function date_w13() {
        return $this->hasMany('App\History', 'cus_id','id_cus')->where('history.remark_id','=','W13');    
    }

    public function date_w14() {
        return $this->hasMany('App\History', 'cus_id','id_cus')->where('history.remark_id','=','W14');    
    }

    public function date_w15() {
        return $this->hasMany('App\History', 'cus_id','id_cus')->where('history.remark_id','=','W15');    
    }

    //End MO Task List
    
    //Task P1
    public function date_w0() {
        return $this->hasMany('App\History', 'cus_id','id_cus')->where('history.remark_id','=','W0');    
    }

    public function date_w1() {
        return $this->hasMany('App\History', 'cus_id','id_cus')->where('history.remark_id','=','W1');    
    }
    //End task P1
    
    //Task P2
    
    public function date_w8() {
        return $this->hasMany('App\History', 'cus_id','id_cus')->where('history.remark_id','=','W8');    
    }

    public function date_w9() {
        return $this->hasMany('App\History', 'cus_id','id_cus')->where('history.remark_id','=','W9');    
    }
    public function date_w10() {
        return $this->hasMany('App\History', 'cus_id','id_cus')->where('history.remark_id','=','W10');    
    }
    //End Task P2

    //Task P3
    
        //W8 & W13
   
    //End Task P3
    
    //Task Manager
    public function date_w5() {
        return $this->hasMany('App\History', 'cus_id','id_cus')->where('history.remark_id','=','W5');    
    }

    public function date_w12() {
        return $this->hasMany('App\History', 'cus_id','id_cus')->where('history.remark_id','=','W12');    
    }

    public function date_w11() {
        return $this->hasMany('App\History', 'cus_id','id_cus')->where('history.remark_id','=','W11');    
    }

    public function date_w17() {
        return $this->hasMany('App\History', 'cus_id','id_cus')->where('history.remark_id','=','W17');    
    }

    //End Task Manager
    //
    public function activity() {
        return $this->hasMany('App\History', 'cus_id','id_cus');    
    }



    //Meetcus
    public function meetcus()
    {
        return $this->belongsTo('App\MeetCust', 'id_cus', 'cus_id');
    }

    //
    public function addinfo()
    {
        return $this->belongsTo('App\AddInfo', 'cus_id', 'id_cus' );
    }

    public function loanapprove()
    {
        return $this->belongsTo('App\Loandisburse','id_cus', 'cus_id'  );
    }

}
