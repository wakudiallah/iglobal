<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class FileManager extends Model
{
    protected $table = 'file_managers';


    protected $fillable = ['title', 'desc', 'attach', 'status'];

    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates   = ['deleted_at'];
    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

     public function folders()
    {
        return $this->belongsTo('App\ListFolder', 'folder', 'id');
    }

}
