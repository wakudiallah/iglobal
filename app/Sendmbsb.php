<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sendmbsb extends Model
{
    //`id`, attach`, `cus_id`
    
    protected $table = 'sendmbsb';

    protected $fillable = ['subject','message', 'cus_id', 'attach', 'cus_id' ];


	use SoftDeletes;

	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;

}
