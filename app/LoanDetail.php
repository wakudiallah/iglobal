<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanDetail extends Model
{
   protected $table = 'loan_detail';

   public function type(){
   		return $this->belongsTo('App\Employment','id_emp_type','id');
   }

   public function package(){
   		return $this->belongsTo('App\Loanpkg','id_loanpkg','id');
   }
}
