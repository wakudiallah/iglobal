<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Financial extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'financial';

    /**
     * The attributes that are mutated to date
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
     
    
    
}
