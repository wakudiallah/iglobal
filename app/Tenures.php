<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\LoanDetails;
class Tenures extends Model
{
   protected $table = 'tenures';

   public function loan(){
   		return $this->belongsTo('App\LoanDetail','id_loan','id');
   }
public function store()
    {
   return $this->belongsTo(LoanDetails::class, 'id_loan', 'id');
}
}
