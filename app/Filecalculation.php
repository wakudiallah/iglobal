<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Filecalculation extends Model
{
    //`id`, `id_remark`, `desc`, `color`, `deleted_at`, `created_at`, `updated_at`

	protected $table = 'file_calculation';


    use SoftDeletes;

	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;


}