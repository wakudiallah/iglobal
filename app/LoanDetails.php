<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Tenures;
class LoanDetails extends Model
{
   protected $table = 'loan_details';

   public function types(){
   		return $this->belongsTo('App\Employment','emp_id','id');
   }

   public function packages(){
   		return $this->belongsTo('App\Loanpkg','loanpkg_id','id');
   }

    public function ten(){
   		return $this->hasMany(Tenures::class);
   }

}
