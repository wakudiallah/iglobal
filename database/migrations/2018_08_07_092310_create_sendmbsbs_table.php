<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSendmbsbsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        if(!Schema::hasTable('sendmbsb'))
        {
            Schema::create('sendmbsb', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject', 1000)->nullable();
            $table->string('message', 2000)->nullable();
            $table->string('attach', 2000)->nullable();
            $table->integer('cus_id')->nullable();
            $table->SoftDeletes();
            $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sendmbsb');
    }
}
