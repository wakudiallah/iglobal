<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeoLogErrorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('geolog_errors'))
        {
            Schema::create('geolog_errors', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('error_status')->unsigned();
                $table->string('page_title',60);
                $table->string('header_content',80);
                $table->text('content');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geolog_errors');
    }
}
