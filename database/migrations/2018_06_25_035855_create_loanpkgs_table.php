<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoanpkgsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         if(!Schema::hasTable('loanpkg'))
        {
            Schema::create('loanpkg', function (Blueprint $table) {
                $table->increments('id');
                $table->String('LnPkg_Code',4);
                $table->String('Ln_Desc',60);
                $table->Integer('Min_Ten');
                $table->Integer('Max_Ten');
                $table->decimal('Mln_amt', 13, 2);
                $table->decimal('Max_amt', 13, 2);
                $table->Integer('Min_Service');
                $table->Integer('Min_Age');
                $table->Integer('Max_Age');
                $table->decimal('Ln_Int', 13, 2);
                $table->String('Act');
                $table->unsignedInteger('user_id');

                $table->foreign('user_id')
                    ->references('id')->on('users')
                    ->onDelete('cascade');
                    
                $table->SoftDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loanpkgs');
    }
}
