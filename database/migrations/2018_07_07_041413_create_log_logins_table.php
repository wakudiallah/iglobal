<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogLoginsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('loglogins'))
        {
            Schema::create('loglogins', function (Blueprint $table) {
                $table->increments('id');
                $table->String('name',255);
                $table->String('email',255);
                $table->double('lat',10,6);
                $table->double('lng',10,6);
                $table->text('location');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loglogins');
    }
}
