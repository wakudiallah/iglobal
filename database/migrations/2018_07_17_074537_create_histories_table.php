<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('history'))
        {
            Schema::create('history', function (Blueprint $table) {
                $table->increments('id');
                $table->String('name', 255)->nullable();
                $table->String('cus_id',40)->nullable();
                $table->String('activity',255)->nullable();
                $table->String('remark_id',4)->nullable();
                
                $table->Integer('user_id')->unsigned()->nullable();

                
                $table->String('note',255)->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history');
    }
}
