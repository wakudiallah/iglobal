<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('emp'))
        {
            Schema::create('emp', function (Blueprint $table) {
                $table->increments('id');
                $table->String('Emp_Code',4);
                $table->String('Emp_Desc')->nullable();
                $table->date('Reg_Dt')->nullable();
                $table->boolean('Act')->defult(1);
                $table->unsignedInteger('user_id');

                $table->foreign('user_id')
                    ->references('id')->on('users')
                    ->onDelete('cascade');
                $table->SoftDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emps');
    }
}
