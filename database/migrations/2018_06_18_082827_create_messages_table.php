<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->increments('id');
            $table->String('first',20)->nullable();
            $table->String('last',20)->nullable();
            $table->String('email')->nullable();
            $table->String('message',80)->nullable();
            $table->String('reply',3000)->nullable();
            $table->boolean('status')->default('0')->nullable();
            $table->SoftDeletes();          
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
