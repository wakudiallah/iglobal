<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('add_info'))
        {
            Schema::create('add_info', function (Blueprint $table) {
                $table->increments('id');
                $table->String('cus_id',100)->nullable();
                $table->String('office_telp',100)->nullable();
                $table->String('office_telp2',100)->nullable();
                $table->String('empcode', 100)->nullable();
                $table->integer('user_id')->unsigned()->nullable();
                $table->SoftDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('add_infos');
    }
}
