<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaderMOsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('leader_mo'))
        {
            Schema::create('leader_mo', function (Blueprint $table) {
                $table->increments('id');
                $table->Integer('leader_id')->unsigned()->nullable();
                $table->String('leader_name',100)->nullable();
                $table->Integer('user_id')->unsigned()->nullable();
                $table->SoftDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leader_mo');
    }
}
