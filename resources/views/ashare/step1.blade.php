@extends('vadmin.tampilan')

@section('content')



    <section class="content">
        <div class="container-fluid">

            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">pageview</i> View Form  </a></li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->


            @include('shared.notif')

            <!-- Basic Example | Horizontal Layout -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>View Form  </h2>
                        </div>
                        <div class="body">
                            <div id="wizard_horizontal" style="">
                                <h2>Customer Information</h2>
                                <section> <!-- +++++++++++++++++++++Section Spekar ++++++++++++++++ -->
                                    <div class="row">   
                                        <div class="col-md-6"> <!-- kolom 1 -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Name : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                   <input  class="form-control" type="text" name="" value="{{$step1->name}} " readonly> 
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>IC :  </b>    
                                                </div>
                                                <div class="col-md-6">
                                                   <input  class="form-control" type="text" name="" value="{{$step1->ic}} " readonly> 
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Email :  </b>    
                                                </div>
                                                <div class="col-md-6">
                                                   <input  class="form-control" type="text" name="" value="{{$step1->email}} " readonly>  
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>By :  </b>    
                                                </div>
                                                <div class="col-md-6">
                                                   <p class="merah"><b>{{$step1->p2->name}}</b></p>  
                                                </div>
                                            </div>
                                        </div> <!-- End of kolom 1 -->
                                        
                                        <!--  =================kolom 2============== -->
                                        <div class="col-md-6">
                                            

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Phone : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                    <input  class="form-control" type="text" name="" value="{{$step1->notelp}} " readonly>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Employer : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                    <input  class="form-control" type="text" name="" value="{{$step1->majikan->Emp_Desc}} " readonly>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                </div>
                                                @if($step1->emp_code == 1)
                                                    @if($step1->clerical_type == 1)
                                                        <div class="col-md-6" >
                                                            <input name="group1" type="radio" id="radio_1{{$step1->id}}" value="1" checked disabled/>
                                                            <label for="radio_1{{$step1->id}}">Clerical</label>
                                                            <input name="group1" type="radio" id="radio_2{{$step1->id}}" value="0" disabled/>
                                                            <label for="radio_2{{$step1->id}}">Non Clerical</label>
                                                        </div>
                                                    @else
                                                        <div class="col-md-6" >
                                                            <input name="group1" type="radio" id="radio_1{{$step1->id}}" value="1" disabled/>
                                                            <label for="radio_1{{$step1->id}}">Clerical</label>
                                                            <input name="group1" type="radio" id="radio_2{{$step1->id}}" value="0" checked disabled/>
                                                            <label for="radio_2{{$step1->id}}">Non Clerical</label>
                                                        </div>
                                                    @endif
                                                @endif
                                            </div> 

                                            
                                                                                  

                                        </div> <!-- End of kolom 2 -->

                                    </div>
                                </section> <!-- +++++++++++++++++++++ End Section Spekar ++++++++++++++++ -->

                                <h2>Document</h2>
                                <section> <!-- +++++++++++++++++++++ Section Tenure ++++++++++++++++ -->
                                    @foreach($approval as $datas)
                                    <div class="row">
                                        <?php
                                           $datax1 = DB::table('doc_cust')->join('praapplication', 'doc_cust.cus_id', '=', 'praapplication.id_cus')->where('cus_id', $datas->id_cus)->where('doc_cust.type','1')->latest('doc_cust.created_at')->select('doc_cust.id as iddoc', 'doc_cust.doc_pdf as doc_pdf')->limit('1')->first();

                                           $datax2 = DB::table('doc_cust')->join('praapplication', 'doc_cust.cus_id', '=', 'praapplication.id_cus')->where('cus_id', $datas->id_cus)->where('doc_cust.type','2')->latest('doc_cust.created_at')->select('doc_cust.id as iddoc', 'doc_cust.doc_pdf as doc_pdf')->limit('1')->first();

                                          

                                        ?>

                                        <div class="col-md-2"></div>
                                        <div class="col-md-8">
                                            <table class="table table-responsive">
                                                <thead class="bg-red">
                                                    <tr>
                                                        <th width="5%">No</th>
                                                        <th width="30%">Document</th>
                                                        <th width="5%">File</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <?php $y = 1; ?>

                                                    <tr>  
                                                        <td>{{$y++}}</td>
                                                        <td>IC</td>
                                                        <td>
                                                            @if(empty($datax1->doc_pdf))
                                                            <a href="#" class="btn bg-red"><i class="material-icons disabled">do_not_disturb_alt</i> </a>
                                                            @else
                                                            <a href="{{asset('/documents/user_doc/'.$datas->ic.'/'.$datax1->doc_pdf)}}" target='_blank' class="btn bg-light-blue"><i class="material-icons">library_books</i></a>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>  
                                                        <td>{{$y++}}</td>
                                                        <td>Consent Letter</td>
                                                        <td>@if(empty($datax2->doc_pdf))
                                                            <a href="#" class="btn bg-red"><i class="material-icons disabled">do_not_disturb_alt</i> </a>
                                                            @else
                                                            <a href="{{asset('/documents/user_doc/'.$datas->ic.'/'.$datax2->doc_pdf)}}" target='_blank' class="btn bg-light-blue"><i class="material-icons">library_books</i></a>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    
                                                </tbody>

                                            </table>

                                            <a href={{url('/adminnew')}} role="menuitem" class="btn btn-sm bg-green">Back to Dashboard</a>
                                        </div>
                                        <div class="col-md-2"></div>

                                    </div>
                                    @endforeach
                                </section> <!-- +++++++++++++++++++++ End Section Tenure ++++++++++++++++ -->
                             

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Example | Horizontal Layout -->
            



        </div>
    </section>



@endsection

@push('js')

    <!-- Jquery Core Js -->
    <script src="{{asset('admin/plugins/jquery/jquery.min.js') }}"></script>

    <!-- Select Plugin Js -->
    <script src="{{asset('admin/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{asset('admin/plugins/jquery-slimscroll/jquery.slimscroll.js') }}"></script>

    <!-- Jquery Validation Plugin Css -->
    <script src="{{asset('admin/plugins/jquery-validation/jquery.validate.js') }}"></script>

    <!-- JQuery Steps Plugin Js -->
    <script src="{{asset('admin/plugins/jquery-steps/jquery.steps.js') }}"></script>

    <!-- Sweet Alert Plugin Js -->
    <script src="{{asset('admin/plugins/sweetalert/sweetalert.min.js') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{asset('admin/plugins/node-waves/waves.js') }}"></script>

    <!-- Custom Js -->
    <script src="{{ asset('admin/js/admin.js') }}"></script>
    <script src="{{ asset('admin/js/pages/forms/form-wizard.js') }}"></script>

    <!-- Demo Js -->
    <script src="{{ asset('admin/js/demo.js') }}"></script>

    <script type="text/javascript">
        $('#finish').click(function() {
          window.location='http://www.example.com';
        });
    </script>

    
@endpush