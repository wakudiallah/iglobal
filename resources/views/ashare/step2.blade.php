@extends('vadmin.tampilan')

@section('content')



	<section class="content">
        <div class="container-fluid">

            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">pageview</i> View Loan Calculation </a></li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->


            @include('shared.notif')

            <!-- Basic Example | Horizontal Layout -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>View Loan Calculation</h2>
                        </div>
                        <div class="body">
                            <div id="wizard_horizontal" style="">
                                <h2>Customer Information</h2>
                                <section> <!-- +++++++++++++++++++++Section Spekar ++++++++++++++++ -->
                                    <div class="row">   
                                        <div class="col-md-6"> <!-- kolom 1 -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Name : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                   <input  class="form-control" type="text" name="" value="{{$step1->name}} " readonly> 
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>IC :  </b>    
                                                </div>
                                                <div class="col-md-6">
                                                   <input  class="form-control" type="text" name="" value="{{$step1->ic}} " readonly> 
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Email :  </b>    
                                                </div>
                                                <div class="col-md-6">
                                                   <input  class="form-control" type="text" name="" value="{{$step1->email}} " readonly>  
                                                </div>
                                            </div>
                                        </div> <!-- End of kolom 1 -->
                                        
                                        <!--  =================kolom 2============== -->
                                        <div class="col-md-6">
                                            
                                            <?php $lm = DB::table('loanammount')->where('id_praapplication', $step1->id_cus)->latest('created_at')->limit('1')->first(); ?>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Phone : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                    <input  class="form-control" type="text" name="" value="{{$step1->notelp}} " readonly>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Employer : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                    <input  class="form-control" type="text" name="" value="{{$step1->majikan->Emp_Desc}} " readonly>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                </div>
                                                @if($step1->emp_code == 1)
                                                    @if($step1->clerical_type == 1)
                                                        <div class="col-md-6" >
                                                            <input name="group1" type="radio" id="radio_1{{$step1->id}}" value="1" checked disabled/>
                                                            <label for="radio_1{{$step1->id}}">Clerical</label>
                                                            <input name="group1" type="radio" id="radio_2{{$step1->id}}" value="0" disabled/>
                                                            <label for="radio_2{{$step1->id}}">Non Clerical</label>
                                                        </div>
                                                    @else
                                                        <div class="col-md-6" >
                                                            <input name="group1" type="radio" id="radio_1{{$step1->id}}" value="1" disabled/>
                                                            <label for="radio_1{{$step1->id}}">Clerical</label>
                                                            <input name="group1" type="radio" id="radio_2{{$step1->id}}" value="0" checked disabled/>
                                                            <label for="radio_2{{$step1->id}}">Non Clerical</label>
                                                        </div>
                                                    @endif
                                                @endif
                                            </div> 

                                            <?php $existing = DB::table('meet_cust')->where('cus_id', $step1->id_cus)->latest('created_at')->limit('1')->first(); ?>

                                            
                                            @if($existing->existing == 1)
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Existing Cust : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                    <input name="group2" type="radio" id="radio_11{{$step1->id}}" onclick="javascript:yesnoCheck2{{$step1->id}}();" value="1" disabled checked />
                                                    <label for="radio_11{{$step1->id}}" >Yes</label>

                                                    <input name="group2" type="radio" id="radio_21{{$step1->id}}" onclick="javascript:yesnoCheck2{{$step1->id}}();" value="0" disabled />
                                                    <label for="radio_21{{$step1->id}}">No</label>  
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6" >
                                                    <b>Date Disbursement : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                   <input type="text" id="datepicker{{$step1->id}}" name="disb" class="form-control" value="{{$existing->date_disbustment}}" readonly> 
                                                </div>
                                            </div>
                                            @else
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Existing Cust : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                    <input name="group2" type="radio" id="radio_11{{$step1->id}}" onclick="javascript:yesnoCheck2{{$step1->id}}();" value="1" disabled />
                                                    <label for="radio_11{{$step1->id}}" >Yes</label>

                                                    <input name="group2" type="radio" id="radio_21{{$step1->id}}" onclick="javascript:yesnoCheck2{{$step1->id}}();" value="0" disabled checked />
                                                    <label for="radio_21{{$step1->id}}">No</label>  
                                                </div>
                                            </div>

                                            @endif
                                                                                  

                                        </div> <!-- End of kolom 2 -->

                                    </div>
                                </section> <!-- +++++++++++++++++++++ End Section Spekar ++++++++++++++++ -->

                                <h2>Document</h2>
                                <section> <!-- +++++++++++++++++++++ Section Tenure ++++++++++++++++ -->
                                    @foreach($approval as $datas)
                                    <div class="row">
                                        <?php
                                           $datax1 = DB::table('doc_cust')->join('praapplication', 'doc_cust.cus_id', '=', 'praapplication.id_cus')->where('cus_id', $datas->id_cus)->where('doc_cust.type','1')->latest('doc_cust.created_at')->select('doc_cust.id as iddoc', 'doc_cust.doc_pdf as doc_pdf')->limit('1')->first();

                                           $datax2 = DB::table('doc_cust')->join('praapplication', 'doc_cust.cus_id', '=', 'praapplication.id_cus')->where('cus_id', $datas->id_cus)->where('doc_cust.type','2')->latest('doc_cust.created_at')->select('doc_cust.id as iddoc', 'doc_cust.doc_pdf as doc_pdf')->limit('1')->first();

                                           $datax3 = DB::table('doc_cust')->join('praapplication', 'doc_cust.cus_id', '=', 'praapplication.id_cus')->where('cus_id', $datas->id_cus)->where('doc_cust.type','4')->latest('doc_cust.created_at')->select('doc_cust.id as iddoc', 'doc_cust.doc_pdf as doc_pdf', 'doc_cust.verification as verification')->limit('1')->first();

                                        ?>

                                        <div class="col-md-2"></div>
                                        <div class="col-md-8">
                                            <table class="table table-responsive">
                                                <thead class="bg-red">
                                                    <tr>
                                                        <th width="5%">No</th>
                                                        <th width="30%">Document</th>
                                                        <th width="5%">File</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <?php $y = 1; ?>

                                                    <tr>  
                                                        <td>{{$y++}}</td>
                                                        <td>IC</td>
                                                        <td>
                                                            @if(empty($datax1->doc_pdf))
                                                            <a href="#" class="btn bg-red"><i class="material-icons disabled">do_not_disturb_alt</i> </a>
                                                            @else
                                                            <a href="{{asset('/documents/user_doc/'.$datas->ic.'/'.$datax1->doc_pdf)}}" target='_blank' class="btn bg-light-blue"><i class="material-icons">library_books</i></a>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>  
                                                        <td>{{$y++}}</td>
                                                        <td>Consent Letter</td>
                                                        <td>@if(empty($datax2->doc_pdf))
                                                            <a href="#" class="btn bg-red"><i class="material-icons disabled">do_not_disturb_alt</i> </a>
                                                            @else
                                                            <a href="{{asset('/documents/user_doc/'.$datas->ic.'/'.$datax2->doc_pdf)}}" target='_blank' class="btn bg-light-blue"><i class="material-icons">library_books</i></a>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>  
                                                        <td>{{$y++}}</td>
                                                        <td>Spekar</td>
                                                        <td>@if(empty($datax3->doc_pdf))
                                                            <a href="#" class="btn bg-red"><i class="material-icons disabled">do_not_disturb_alt</i> </a>
                                                            @else
                                                            <a href="{{asset('/documents/user_doc/'.$datas->ic.'/'.$datax3->doc_pdf)}}" target='_blank' class="btn bg-light-blue"><i class="material-icons">library_books</i></a>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    
                                                </tbody>

                                            </table>
                                        </div>
                                        <div class="col-md-2"></div>

                                    </div>
                                    @endforeach
                                </section> <!-- +++++++++++++++++++++ End Section Tenure ++++++++++++++++ -->


                                <h2>Loan</h2>
                                <section> <!-- +++++++++++++++++++++  Section Loan Calculation ++++++++++++++++ -->
                                    <!-- kolom 2 -->
                                    @foreach($approval as $datas)
                                    <div class="row">
                                        <div class="col-md-6">

                                            <?php $lm = DB::table('loanammount')->where('id_praapplication', $datas->id_cus)->latest('created_at')->limit('1')->first(); ?>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Package : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                   <input  class="form-control" type="text" name="" value="{{$step1->loanpkg->Ln_Desc}} " readonly> 
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Basic Salary (RM) : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                   <input  class="form-control" type="text" name="" value="{{$datas->gaji_asas}} " readonly> 
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Allowance (RM) : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                    <input  class="form-control" type="text" name="" value="{{$datas->elaun}}" readonly>
                                                </div>
                                            </div>

                                            <?php 
                                               $bunga2 = $loanammount->loanammount * $loanammount->Tenure->rate /100   ;
                                               $bunga = $bunga2 * $loanammount->Tenure->years;
                                               $total = $loanammount->loanammount + $bunga ;
                                               $bulan = $loanammount->Tenure->years * 12 ;
                                               $installment =  $total / $bulan;
                                            ?>
                                               
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Installement : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                   <input name="installment"  class="form-control" type="text" value="{{floor($installment) }} " readonly> 
                                                </div>
                                            </div>
                                            
                                        </div>

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Total Deduction (RM) : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                    <input  class="form-control" type="text" name="" value="{{$datas->pot_bul}}" readonly>
                                                </div>
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Loan Amount (RM) : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                   <input  class="form-control" type="text" name="" value="{{$lm->loanammount}}" readonly>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Max Financing Amount (RM) : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                   <input  class="form-control" type="text" name="" value="{{$lm->maxloan}}" readonly>
                                                </div>
                                            </div>

                                            

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Years : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                   <input  class="form-control" type="text" name="" value="{{$loanammount->Tenure->years}}" readonly>
                                                </div>
                                            </div>

                                        </div> <!-- End of kolom 2 -->
                                        
                                        

                                    </div>

                                    <!-- 
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-8">
                                            <table class="table table-hover" style="margin-top: 30px !important">
                                                <thead>
                                                    <tr>
                                                        <th width="20" valign="middle"> <b>  Duration   </b> </th>
                                                        <th width="40"> <b> Financing Amount  </b> </th>
                                                        <th width="20"><b>  Monthly installment </b> </th>
                                                        <th width="20"> <b>  Profit Rate </b> </th>
                                                        <th width="10" align="center"> <b>  Select </b> </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                     
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        
                                                        <td> 
                                                        </td>
                                                      
                                                        <td></td>
                                                        <td>  
                                                            <input name="tenure" type="radio" id="radio_1" value=""  required />
                                                            <label for="radio_1"></label>
                                                        </td>
                                                    </tr>
                                                                                
                                                                 
                                                </tbody>
                                                
                                            </table>
                                        </div>
                                        <div class="col-md-2"></div>
                                        
                                        
                                    </div> -->
                                    <div class="row">
                                        <a href={{url('/adminnew')}} role="menuitem" class="btn btn-sm bg-green">Back to Dashboard</a>
                                    </div>
                                    @endforeach
                                </section> <!-- +++++++++++++++++++++ End Section Loan Calculation ++++++++++++++++ -->


                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Example | Horizontal Layout -->
		    



        </div>
    </section>


    





@endsection

@push('js')

    <!-- Jquery Core Js -->
    <script src="{{asset('admin/plugins/jquery/jquery.min.js') }}"></script>

    <!-- Select Plugin Js -->
    <script src="{{asset('admin/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{asset('admin/plugins/jquery-slimscroll/jquery.slimscroll.js') }}"></script>

    <!-- Jquery Validation Plugin Css -->
    <script src="{{asset('admin/plugins/jquery-validation/jquery.validate.js') }}"></script>

    <!-- JQuery Steps Plugin Js -->
    <script src="{{asset('admin/plugins/jquery-steps/jquery.steps.js') }}"></script>

    <!-- Sweet Alert Plugin Js -->
    <script src="{{asset('admin/plugins/sweetalert/sweetalert.min.js') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{asset('admin/plugins/node-waves/waves.js') }}"></script>

    <!-- Custom Js -->
    <script src="{{ asset('admin/js/admin.js') }}"></script>
    <script src="{{ asset('admin/js/pages/forms/form-wizard.js') }}"></script>

    <!-- Demo Js -->
    <script src="{{ asset('admin/js/demo.js') }}"></script>

    
@endpush