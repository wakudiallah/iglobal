@extends('vadmin.tampilan')

@section('content')

	<section class="content">
        <div class="container-fluid">

            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">playlist_add_check</i> Report Approved </a></li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

            <div class="row clearfix">
			    <!-- Task Info -->
			        <div class="card">
			            <div class="header bg-red">
			                <h2>Report Approved </h2>
			            </div>
			            <div class="body">
			                <div class="table-responsive">
			                    <table class="table table-hover dashboard-task-infos" id="example">
			                        <thead>
			                            <tr>
			                                <th width="10%">#</th>
			                                <th width="30%">Name</th>
			                                <th width="20%">IC</th>
			                                <th width="20%">Date Submit</th>
			                                <th>Detail</th>
			                            </tr>
			                        </thead>
			                        <tbody>
			                            <?php $i = 1; ?>

			                            @foreach($reportmanag as $data)
			                            <tr>
			                                <td>{{$i++}}</td>
			                                <td>{{$data->name}}</td>
			                                <td>{{$data->ic}}</td>
			                                <td>{{$data->created_at->toFormattedDateString() }}</td>
			                                <td>
			                                	<!-- <button class="btn btn-success" aria-controls="collapse-{{$data->id}}" data-target="#collapseDetailOne{{$data->id}}" data-toggle="collapse" style="cursor:pointer;">Detail</button> -->

                                                <a href="{{ url('view_form/'.$data->id_cus.'/fourth_step') }}" class="btn btn-sm bg-green"><i class="material-icons">pageview</i> View Loan </a>
			                                </td>
			                            </tr>
			                            @endforeach
			                            
			                        </tbody>
			                    </table>
			                </div>
			            </div>
			        </div>
			    
			    <!-- #END# Task Info -->
			</div>

		    


            <!-- //////////////////  Modal ////////////////// -->
            @foreach($reportmanag as $datas)

            
            <!-- Large Size -->
            <div class="modal fade" id="largeModal{{$datas->id}}" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-red">
                            <h4 class="modal-title" id="largeModalLabel">Detail</h4>
                        </div>
                        <div class="modal-body">
                                        
                            <!-- start step -->
                           <section class="design-process-section" id="process-tab">
                                <div class="row">
                                  <div class="col-xs-12"> 
                                    
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                      <div role="tabpanel" class="tab-pane active" id="discover{{$datas->id}}">
                                        <div class="design-process-content">
                                           
                                            
                                            <input type="hidden" name="process5" value="{{$datas->process5}}">
                                            <div class="row">
                                                
                                                <div class="col-md-6"> <!-- kolom 1 -->
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <b>Name  </b>    
                                                        </div>
                                                        <div class="col-md-6">
                                                           <input  class="form-control" type="text" name="" value="{{$datas->name}} " readonly> 
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <b>IC  </b>    
                                                        </div>
                                                        <div class="col-md-6">
                                                           <input  class="form-control" type="text" name="" value="{{$datas->ic}} " readonly> 
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <b>No Telf : </b>    
                                                        </div>
                                                        <div class="col-md-6">
                                                            <input  class="form-control" type="text" name="" value="{{$datas->notelp}} " readonly>
                                                           
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <b>Email : </b>    
                                                        </div>
                                                        <div class="col-md-6">
                                                            <input  class="form-control" type="text" name="" value="{{$datas->email}} " readonly>
                                                           
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <b>Employer : </b>    
                                                        </div>
                                                        <div class="col-md-6">
                                                            <input  class="form-control" type="text" name="" value="{{$datas->majikan->Emp_Desc}} " readonly>
                                                           
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <b>By : </b>    
                                                        </div>
                                                        <div class="col-md-6">
                                                           <p style="color: red">{{$datas->user->name}} </p>  
                                                        </div>
                                                    </div>
                                                    
                                                </div> <!-- End of kolom 1 -->
                                                
                                                <!-- kolom 2 -->
                                                <div class="col-md-6">
                                                    
                                                    <?php $lm = DB::table('loanammount')->where('id_praapplication', $datas->id_cus)->latest('created_at')->limit('1')->first(); ?>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <b>Basic Salary (RM) : </b>    
                                                        </div>
                                                        <div class="col-md-6">
                                                           <input  class="form-control" type="text" name="" value="{{$datas->gaji_asas}} " readonly> 
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <b>Allowance (RM) : </b>    
                                                        </div>
                                                        <div class="col-md-6">
                                                            <input  class="form-control" type="text" name="" value="{{$datas->elaun}}" readonly>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <b>Total Deduction (RM) : </b>    
                                                        </div>
                                                        <div class="col-md-6">
                                                            <input  class="form-control" type="text" name="" value="{{$datas->pot_bul}}" readonly>
                                                        </div>
                                                    </div>
                                                    
                                                    <!--<div class="row">
                                                        <div class="col-md-6">
                                                            <b>Loan Amount (RM) : </b>    
                                                        </div>
                                                        <div class="col-md-6">
                                                           <input  class="form-control" type="text" name="" value="{{$lm->loanammount}}" readonly>
                                                        </div>
                                                    </div>-->
                                                </div> <!-- End of kolom 2 -->
                                            </div>

                                            <div class="row">
                                                <?php
                                                   $datax1 = DB::table('doc_cust')->join('praapplication', 'doc_cust.cus_id', '=', 'praapplication.id_cus')->where('cus_id', $datas->id_cus)->where('doc_cust.type','1')->latest('doc_cust.created_at')->select('doc_cust.id as iddoc', 'doc_cust.doc_pdf as doc_pdf')->limit('1')->first();

                                                   $datax2 = DB::table('doc_cust')->join('praapplication', 'doc_cust.cus_id', '=', 'praapplication.id_cus')->where('cus_id', $datas->id_cus)->where('doc_cust.type','2')->latest('doc_cust.created_at')->select('doc_cust.id as iddoc', 'doc_cust.doc_pdf as doc_pdf')->limit('1')->first();

                                                   $datax3 = DB::table('doc_cust')->join('praapplication', 'doc_cust.cus_id', '=', 'praapplication.id_cus')->where('cus_id', $datas->id_cus)->where('doc_cust.type','4')->latest('doc_cust.created_at')->select('doc_cust.id as iddoc', 'doc_cust.doc_pdf as doc_pdf', 'doc_cust.verification as verification')->limit('1')->first();

                                                ?>

                                                <div class="col-md-2"></div>
                                                <div class="col-md-8">
                                                    <table class="table table-responsive">
                                                        <thead class="bg-red">
                                                            <tr>
                                                                <th width="5%">No</th>
                                                                <th width="30%">Document</th>
                                                                <th width="5%">File</th>
                                                            </tr>
                                                        </thead>

                                                        <tbody>
                                                            <?php $y = 1; ?>

                                                            <tr>  
                                                                <td>{{$y++}}</td>
                                                                <td>IC</td>
                                                                <td>
                                                                    @if(empty($datax1->doc_pdf))
                                                                    <a href="#" class="btn bg-red"><i class="material-icons disabled">do_not_disturb_alt</i> </a>
                                                                    @else
                                                                    <a href="{{asset('/documents/user_doc/'.$datas->ic.'/'.$datax1->doc_pdf)}}" target='_blank' class="btn bg-light-blue"><i class="material-icons">library_books</i></a>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                            <tr>  
                                                                <td>{{$y++}}</td>
                                                                <td>Consent Letter</td>
                                                                <td>@if(empty($datax2->doc_pdf))
                                                                    <a href="#" class="btn bg-red"><i class="material-icons disabled">do_not_disturb_alt</i> </a>
                                                                    @else
                                                                    <a href="{{asset('/documents/user_doc/'.$datas->ic.'/'.$datax2->doc_pdf)}}" target='_blank' class="btn bg-light-blue"><i class="material-icons">library_books</i></a>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                            <tr>  
                                                                <td>{{$y++}}</td>
                                                                <td>Spekar</td>
                                                                <td>@if(empty($datax3->doc_pdf))
                                                                    <a href="#" class="btn bg-red"><i class="material-icons disabled">do_not_disturb_alt</i> </a>
                                                                    @else
                                                                    <a href="{{asset('/documents/user_doc/'.$datas->ic.'/'.$datax3->doc_pdf)}}" target='_blank' class="btn bg-light-blue"><i class="material-icons">library_books</i></a>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                            
                                                        </tbody>

                                                    </table>
                                                </div>
                                                <div class="col-md-2"></div>

                                            </div>

                                         </div>
                                      </div><!-- end loan calculation -->


                                    </div>
                                  </div>
                                </div>
                            </section><!-- end of step -->


                        </div>
                        <div class="modal-footer">
                            
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

            <!-- ////////////////// End  Modal ////////////////// -->



        </div>
    </section>

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {
		    $('#example').DataTable( {
		        dom: 'Bfrtip',
		        buttons: [
		            {
		                extend: 'excelHtml5',
		                title: 'LIST OF ASSESSMENT',
		                title : 'MASTER AGENT: GLOBAL I EXCEED MANAGEMENT SDN BHD'
		            },
		            {
		                extend: 'pdfHtml5',
		                title: 'LIST OF ASSESSMENT',
		                title : 'MASTER AGENT: GLOBAL I EXCEED MANAGEMENT SDN BHD'
		            }
		        ]
		    } );
		} );
	</script>


	@foreach($reportmanag as $datas)
	<!-- ================= Data Target hidden ======= -->
        <script src="https://code.jquery.com/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
         

        <script>
            $("[data-collapse-group]").on('show.bs.collapse', function () {
                  var $this = $(this);
                  var thisCollapseAttr = $this.attr('data-collapse-group');
                  $("[data-collapse-group='" + thisCollapseAttr + "']").not($this).collapse('hide');
                });
        </script>

    <!-- ================= End Data Target hidden ======= -->
    @endforeach

@endsection