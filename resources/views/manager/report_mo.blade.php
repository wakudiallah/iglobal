@extends('vadmin.tampilan')

@section('content')

    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

	<section class="content">
        <div class="container-fluid">
            <div class="row clearfix"> <!-- Breadcrumber -->

                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">assignment</i> Report MO</a></li>
                    </ol>
                </div>

            </div> <!-- End of breadcrumber -->



			<div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="row clearfix">

                    <div class="card">
                        <div class="header bg-red">
                            <h2>Report MO</h2>
                        </div>

                        <div class="body">

                            <div class="table-responsive">
                            

                                {!! Form::open(['url' => 'report-manager-for-mo','class' => 'smart-form client-form', 'id' =>'smart-form-register3', 'method' => 'post' ]) !!}

                                <div class="body">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <h2 class="card-inside-title">MO</h2>
                                            
                                            <select class="form-control show-tick js-example-basic-single js-states" data-live-search="true" name="emp" id="one" required>
                                                <option value="W">All </option>
                                                @foreach($user as $data) 
                                                    @if($data->model->role->id == '3')
                                                        <option value="{{$data->id}}">{{$data->name}} </option>
                                                    @endif
                                                @endforeach
                                            </select>

                                        </div>

                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <h2 class="card-inside-title">Status</h2>
                                            {{Form::select('status', array('W' => 'All', 'W11' => 'Approved', 'W12' => 'Rejected'), null, ['class' => 'form-control'])}}
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <h2 class="card-inside-title">From Date</h2>

                                                <input type="text" class="form-control" placeholder="From Date" id="datepicker"  name="tanggal1" required/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <h2 class="card-inside-title">To Date</h2>
                                                <input type="text" class="form-control" placeholder="From Date" id="datepicker2"  name="tanggal2" required/>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-sm-12">

                                        <div class="form-group">
                                            <button type="submit" name="submit" class="btn btn-lg bg-green">
                                                <i class="material-icons">visibility</i> &nbsp; Generate &nbsp;
                                            </button>
                                            <input type="hidden" name="_token" value="{{csrf_token()}}">

                                            {!! Form::close() !!} 

                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>
            <div class="col-md-2"></div>
        </div>

    </section>



    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>


    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css" rel="stylesheet" type="text/css" />

	

	<script>
	   var table = $('#example').DataTable({
            lengthChange: false,
            buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
        });
	</script>



    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script>
    $(function() {
       $( "#datepicker" ).datepicker();
     });
    $(function() {
       $( "#datepicker2" ).datepicker();
     });
    </script>



@endsection


@push('js')



 <!-- Bootstrap Material Datetime Picker Plugin Js -->

    <script src="{{asset('admin/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script> 



@endpush