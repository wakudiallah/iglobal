@extends('vadmin.tampilan')

@section('content')


<!-- CSS Step =-->
<link href="{{ asset('admin/css/tabsteptype.css') }}" rel="stylesheet" />


    <section class="content">
        <div class="container-fluid">    

            <div class="row clearfix">
                <!-- Task Info -->
                    <div class="row clearfix"> <!-- Breadcrumber -->
                        <div class="col-md-6">
                            <ol class="breadcrumb breadcrumb-col-pink">
                                <li><a href="javascript:void(0);"><i class="material-icons">create</i>Recommend New Submission</a></li>
                            </ol>
                        </div>
                    </div> <!-- End of breadcrumber -->

                    @include('shared.notif')
                <!-- #END# Task Info -->
            </div>


            @foreach($internal_cal as $datas)
                <div class="row clearfix">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Recommend New Submission</h2>
                        </div>
                        <div class="body">
                            
                            <!-- start step -->
                           <section class="design-process-section" id="process-tab">
                                <div class="row">
                                  <div class="col-xs-12"> 
                                    
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                      <div role="tabpanel" class="tab-pane active" id="discover{{$datas->id}}">
                                        <div class="design-process-content">
                                           
                                            
                                            <input type="hidden" name="process5" value="{{$datas->process5}}">
                                            <div class="row">
                                                
                                                <div class="col-md-6"> <!-- kolom 1 -->
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <b>Name  </b>    
                                                        </div>
                                                        <div class="col-md-6">
                                                           <input  class="form-control" type="text" name="" value="{{$datas->name}} " readonly> 
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <b>IC  </b>    
                                                        </div>
                                                        <div class="col-md-6">
                                                           <input  class="form-control" type="text" name="" value="{{$datas->ic}} " readonly> 
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <b>No Telf : </b>    
                                                        </div>
                                                        <div class="col-md-6">
                                                            <input  class="form-control" type="text" name="" value="{{$datas->notelp}} " readonly>
                                                           
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <b>Email : </b>    
                                                        </div>
                                                        <div class="col-md-6">
                                                            <input  class="form-control" type="text" name="" value="{{$datas->email}} " readonly>
                                                           
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <b>Employer : </b>    
                                                        </div>
                                                        <div class="col-md-6">
                                                            <input  class="form-control" type="text" name="" value="{{$datas->majikan->Emp_Desc}} " readonly>
                                                           
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <b>By : </b>    
                                                        </div>
                                                        <div class="col-md-6">
                                                           <b style="color: red">{{$datas->user->name}} </b>  
                                                        </div>
                                                    </div>
                                                </div> <!-- End of kolom 1 -->
                                                <!-- kolom 2 -->
                                                <div class="col-md-6">
                                                    
                                                    <?php $lm = DB::table('loanammount')->where('id_praapplication', $datas->id_cus)->latest('created_at')->limit('1')->first(); ?>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <b>Basic Salary (RM) : </b>    
                                                        </div>
                                                        <div class="col-md-6">
                                                           <input  class="form-control" type="text" name="" value="{{$datas->gaji_asas}} " readonly> 
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <b>Allowance (RM) : </b>    
                                                        </div>
                                                        <div class="col-md-6">
                                                            <input  class="form-control" type="text" name="" value="{{$datas->elaun}}" readonly>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <b>Total Deduction (RM) : </b>    
                                                        </div>
                                                        <div class="col-md-6">
                                                            <input  class="form-control" type="text" name="" value="{{$datas->pot_bul}}" readonly>
                                                        </div>
                                                    </div>
                                                    
                                                    
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <b>Loan Amount (RM) : </b>    
                                                        </div>
                                                        <div class="col-md-6">
                                                           <input  class="form-control" type="text" name="" value="{{$datas->jml_pem}}" readonly>
                                                        </div>
                                                    </div>
                                                </div> <!-- End of kolom 2 -->
                                            </div>

                                            <div class="row">
                                                <?php
                                                   $datax1 = DB::table('doc_cust')->join('praapplication', 'doc_cust.cus_id', '=', 'praapplication.id_cus')->where('cus_id', $datas->id_cus)->where('doc_cust.type','1')->latest('doc_cust.created_at')->select('doc_cust.id as iddoc', 'doc_cust.doc_pdf as doc_pdf')->limit('1')->first();

                                                   $datax2 = DB::table('doc_cust')->join('praapplication', 'doc_cust.cus_id', '=', 'praapplication.id_cus')->where('cus_id', $datas->id_cus)->where('doc_cust.type','2')->latest('doc_cust.created_at')->select('doc_cust.id as iddoc', 'doc_cust.doc_pdf as doc_pdf')->limit('1')->first();
                                                   $datax3 = DB::table('doc_cust')->join('praapplication', 'doc_cust.cus_id', '=', 'praapplication.id_cus')->where('cus_id', $datas->id_cus)->where('doc_cust.type','4')->latest('doc_cust.created_at')->select('doc_cust.id as iddoc', 'doc_cust.doc_pdf as doc_pdf', 'doc_cust.verification as verification')->limit('1')->first();
                                                ?>

                                                <div class="col-md-2"></div>
                                                <div class="col-md-8">
                                                    <table class="table table-responsive">
                                                        <thead class="bg-red">
                                                            <tr>
                                                                <th width="5%">No</th>
                                                                <th width="30%">Document</th>
                                                                <th width="5%">File</th>
                                                            </tr>
                                                        </thead>

                                                        <tbody>
                                                            <?php $y = 1; ?>

                                                            <tr>  
                                                                <td>{{$y++}}</td>
                                                                <td>IC</td>
                                                                <td>
                                                                    @if(empty($datax1->doc_pdf))
                                                                    <a href="#" class="btn bg-red"><i class="material-icons disabled">do_not_disturb_alt</i> </a>
                                                                    @else
                                                                    <a href="{{asset('/documents/user_doc/'.$datas->ic.'/'.$datax1->doc_pdf)}}" target='_blank' class="btn bg-light-blue"><i class="material-icons">library_books</i></a>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                            <tr>  
                                                                <td>{{$y++}}</td>
                                                                <td>Consent Letter</td>
                                                                <td>@if(empty($datax2->doc_pdf))
                                                                    <a href="#" class="btn bg-red"><i class="material-icons disabled">do_not_disturb_alt</i> </a>
                                                                    @else
                                                                    <a href="{{asset('/documents/user_doc/'.$datas->ic.'/'.$datax2->doc_pdf)}}" target='_blank' class="btn bg-light-blue"><i class="material-icons">library_books</i></a>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                            <tr>  
                                                                <td>{{$y++}}</td>
                                                                <td>Spekar</td>
                                                                <td>@if(empty($datax3->doc_pdf))
                                                                    <a href="#" class="btn bg-red"><i class="material-icons disabled">do_not_disturb_alt</i> </a>
                                                                    @else
                                                                    <a href="{{asset('/documents/user_doc/'.$datas->ic.'/'.$datax3->doc_pdf)}}" target='_blank' class="btn bg-light-blue"><i class="material-icons">library_books</i></a>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                            
                                                           

                                                        </tbody>

                                                    </table>
                                                </div>
                                                <div class="col-md-2"></div>

                                            </div>


                                            {!! Form::open(array('url'=>'recommend/newsubmit/perone/'.$datas->id_cus, 'method'=>'post', 'files'=>'true')) !!}

                                            {{ csrf_field() }}

                                            

                                            <div class="row">
                                                <div class="col-md-2"></div>
                                                <div class="col-md-8">
                                                    <div class="resources2" >
                                                        <b>Recommendation Note :</b>
                                                        <textarea id="txtArea" name="recommend" class="form-control" rows="5" cols="3"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-2"></div>

                                            </div>

                                            <div class="row">
                                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-md-offset-8">
                                                    <button type="submit" class="btn bg-green btn-block btn-lg waves-effect">
                                                        Save
                                                    </button>
                                                    {{ csrf_field() }}
                                                </div>
                                                <div class="col-md-2"></div>
                                            </div>

                                            {{ Form::close() }}

                                            </div>
                                         </div>
                                      </div><!-- end loan calculation -->


                                    </div>
                                  </div>
                                </div>
                            </section><!-- end of step -->

                        </div>

                    </div>
                </div>

            @endforeach
        </div>
    </section> 

@endsection

