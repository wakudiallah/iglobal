	@if($data->stage == 'W0')
    	<span class="label bg-cyan">New App</span>
    @elseif(($data->stage == 'W1') AND (is_null($data->spekar)))
    	<span class="label bg-teal">Pending Spekar MBSB</span>
    @elseif(($data->stage == 'W1') AND ($data->spekar != ''))
    	<span class="label bg-deep-orange">Pending Route MO</span>
    @endif