@extends('vadmin.tampilan')

@section('content')



	<section class="content">
        <div class="container-fluid">

            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">work</i> Loan Eligibility & Scoring </a></li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->


            @include('shared.notif')

            <!-- Basic Example | Horizontal Layout -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Loan Eligibility & Scoring </h2>
                        </div>
                        <div class="body">
                            <div id="wizard_horizontal" style="">
                                <h2>Existing Customer</h2>
                                <section> <!-- +++++++++++++++++++++Section Spekar ++++++++++++++++ -->
                                    aadada
                                </section> <!-- +++++++++++++++++++++ End Section Spekar ++++++++++++++++ -->

                                <h2>Loan Calculation</h2>
                                <section> <!-- +++++++++++++++++++++  Section Loan Calculation ++++++++++++++++ -->
                                    <p>
                                        Donec mi sapien, hendrerit nec egestas a, rutrum vitae dolor. Nullam venenatis diam ac
                                        ligula elementum pellentesque. In lobortis sollicitudin felis non eleifend. Morbi
                                        tristique tellus est, sed tempor elit. Morbi varius, nulla quis condimentum dictum,
                                        nisi elit condimentum magna, nec venenatis urna quam in nisi. Integer hendrerit sapien
                                        a diam adipiscing consectetur. In euismod augue ullamcorper leo dignissim quis elementum
                                        arcu porta. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum leo
                                        velit, blandit ac tempor nec, ultrices id diam. Donec metus lacus, rhoncus sagittis
                                        iaculis nec, malesuada a diam. Donec non pulvinar urna. Aliquam id velit lacus.
                                    </p>
                                </section> <!-- +++++++++++++++++++++ End Section Loan Calculation ++++++++++++++++ -->

                                <h2>Tenure</h2>
                                <section> <!-- +++++++++++++++++++++ Section Tenure ++++++++++++++++ -->
                                    <p>
                                        Morbi ornare tellus at elit ultrices id dignissim lorem elementum. Sed eget nisl at justo
                                        condimentum dapibus. Fusce eros justo, pellentesque non euismod ac, rutrum sed quam.
                                        Ut non mi tortor. Vestibulum eleifend varius ullamcorper. Aliquam erat volutpat.
                                        Donec diam massa, porta vel dictum sit amet, iaculis ac massa. Sed elementum dui
                                        commodo lectus sollicitudin in auctor mauris venenatis.
                                    </p>
                                </section> <!-- +++++++++++++++++++++ End Section Tenure ++++++++++++++++ -->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Example | Horizontal Layout -->
		    



        </div>
    </section>


    

        <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

        <script>
            $(function() {
               $( "#datepicker" ).datepicker();
             });
            
        </script>




@endsection

@push('js')

    <!-- Jquery Core Js -->
    <script src="{{asset('admin/plugins/jquery/jquery.min.js') }}"></script>

    <!-- Select Plugin Js -->
    <script src="{{asset('admin/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{asset('admin/plugins/jquery-slimscroll/jquery.slimscroll.js') }}"></script>

    <!-- Jquery Validation Plugin Css -->
    <script src="{{asset('admin/plugins/jquery-validation/jquery.validate.js') }}"></script>

    <!-- JQuery Steps Plugin Js -->
    <script src="{{asset('admin/plugins/jquery-steps/jquery.steps.js') }}"></script>

    <!-- Sweet Alert Plugin Js -->
    <script src="{{asset('admin/plugins/sweetalert/sweetalert.min.js') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{asset('admin/plugins/node-waves/waves.js') }}"></script>

    <!-- Custom Js -->
    <script src="{{ asset('admin/js/admin.js') }}"></script>
    <script src="{{ asset('admin/js/pages/forms/form-wizard.js') }}"></script>

    <!-- Demo Js -->
    <script src="{{ asset('admin/js/demo.js') }}"></script>

    
@endpush