@extends('vadmin.tampilan')       


@section('content')
    
    <section class="content">
        <div class="container-fluid">

            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">work</i> Loan Eligibility & Scoring</a></li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

            <!-- Notification -->
            <div class="row clearfix">
                @if ($message = Session::get('success')) 
                <div class="alert bg-green alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @elseif($message = Session::get('delete'))
                <div class="alert bg-pink alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @elseif($message = Session::get('update'))
                <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @endif
            </div>
            <!-- End of Notif -->
    

            <div class="row clearfix">
                <!-- Task Info -->
         
                <div class="card">
                    <div class="header bg-red">
                        <h2>Loan Eligibility & Scoring</h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-hover dashboard-task-infos" id="example">
                                <thead>
                                    <tr>
                                        <th width="5%">#</th>
                                        <th width="30%">Name</th>
                                        <th width="20%">IC</th>
                                        <th>Spekar</th>
                                        <th>Status</th> 
                                        <th>Detail</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>

                                    @foreach($meetcus as $data)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$data->name}}</td>
                                        <td>{{$data->ic}}</td>
                                        <td>
                                            @if((empty($data->spekar))) 
                                                <a href="#" class="btn bg-red"><i class="material-icons disabled">do_not_disturb_alt</i> </a>
                                            @else

                                                {!! Form::open(array('url'=>'download/spekar/'.$data->id_cus, 'method'=>'post', 'files'=>'true', 'target'=>'_blank')) !!}

                                                {{ csrf_field() }}

                                                <button type="submit" name="submit" class="btn bg-light-blue" target='_blank'><i class="material-icons">library_books</i></button>
                                                <!-- <a href="{{asset('/documents/user_doc/'.$data->ic.'/'.$data->spekar)}}" class="btn bg-light-blue" target='_blank'> <i class="material-icons">library_books</i></a>-->

                                                 {{ Form::close() }}  

                                            @endif
                                        </td>
                                        
                                        <td>
                                            @include('shared.stage_new')
                                        </td>
                                        <td>
                                             <button class="btn btn-success" aria-controls="collapse-{{$data->id}}" data-target="#collapseDetailOne{{$data->id}}" data-toggle="collapse" style="cursor:pointer;">Detail</button>
                                        </td>
                                    </tr>

                                    @endforeach
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            
            <!-- #END# Task Info -->
            </div>

            <!-- ////////////////  Modal Route ///////////////-->
            @foreach($meetcus as $dataz)
                <div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
                        <div class="modal-dialog " role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="defaultModalLabel">Remark</h4>
                                </div>
                                <div class="modal-body">
                                   {!! Form::open(array('url'=>'passreject/mo/'.$dataz->id_cus, 'method'=>'post', 'files'=>'true')) !!}

                                    {{ csrf_field() }} 

                                    <div class="row ">
                                        <h5><b>Remark :</b></h5>
                                        <select name="moremark" class="form-control" id="one{{$dataz->id}}">
                                            <option value="" selected disabled hidden>Choose Remark</option>
                                            <option value="W4">Pass</option>
                                            <option value="W5">Reject</option>
                                        </select>
                                    </div>

                                    <!-- <div class="row resources" style="display: none; margin-top: 20px !important" id="two">
                                        <h5><b>Route to :</b></h5>
                                        <select name="routetoq" class="form-control">
                                            <option value="" selected disabled hidden>Choose Processor2</option>
                                            @foreach($workgroupprocessor2 as $work)
                                            <option value="{{$work->model_id}}">{{$work->user->name}}</option>
                                            @endforeach
                                        </select>
                                    </div> -->

                                    

                                </div>
                                <div class="modal-footer">
                                    
                                    <input type="submit" value="Submit" class="btn btn btn-success">
                                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                </div>

                                {{ Form::close() }}   
                            </div>
                        </div>
                </div> 
            @endforeach
            <!-- ////////////////  Modal Route ///////////////-->


            <!-- ///////////////////  Data Target  //////////////// -->
            @foreach($meetcus as $datas)
            <div id="collapseDetailOne{{$datas->id}}" class="collapse" aria-expanded="false" data-collapse-group="collapse-group" aria-labelledby="headingFive" data-parent="#accordionExample">
                <div class="row clearfix">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Detail {{$datas->name}}</h2>
                        </div>
                        <div class="body">
                            {!! Form::open(array('url'=>'save/meetcus/'.$datas->id_cus, 'method'=>'post', 'files'=>'true')) !!}

                            {{ csrf_field() }}
                            <span id="latitude"></span>
                            <span id="longitude"></span>
                            <span id="location"></span>

                            <div class="row">
                                <!-- kolom 1 -->
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>Name : </b>    
                                        </div>
                                        <div class="col-md-6"> 
                                           <input type="text" name="" value="{{$datas->name}}  " class="form-control" readonly> 
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>IC : </b>    
                                        </div>
                                        <div class="col-md-6">
                                           <input type="text" name="" value="{{$datas->ic}}  " class="form-control" readonly>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>Phone Number : </b>    
                                        </div>
                                        <div class="col-md-6"> 
                                           <input type="text" name="" value="{{$datas->notelp}} " class="form-control" readonly>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>Email : </b>    
                                        </div>
                                        <div class="col-md-6">
                                           <input type="text" name="" value="{{$datas->email}} " class="form-control" readonly>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>By : </b>    
                                        </div>
                                        <div class="col-md-6">
                                           <b><p style="color: red">{{$datas->user->name}} </p>  </b>
                                        </div>
                                    </div>

                                    <!-- 
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="probootstrap-date-arrival">Remark</label>
                                        </div>
                                        <div class="col-md-6">
                                           
                                            
                                                <input  class="hidden" type="text" name="name" value="{{$datas->name}} " >
                                                <input  class="hidden" type="text" name="ic" value="{{$datas->ic}} " >
                                                <input  class="hidden" type="text" name="id_cus" value="{{$datas->id_cus}} " >

                                                
                                                <select name="moremark" class="form-control" id="one{{$datas->id}}">
                                                    <option value="" selected disabled hidden>Choose Remark</option>
                                                    <option value="W3">Pass</option>
                                                    <option value="W4">Reject</option>
                                                </select>
                                        </div>                                         
                                    </div>
                                -->
                                    

                                   
                                </div> <!-- End of kolom 1 -->
                                

                                <!-- kolom 2 -->
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>Spekar : </b>    
                                        </div>
                                        <div class="col-md-6">
                                            @if((empty($datas->spekar))) 
                                                <a href="#" class="btn bg-red"><i class="material-icons disabled">do_not_disturb_alt</i> </a>
                                            @else

                                                {!! Form::open(array('url'=>'download/spekar/'.$datas->id_cus, 'method'=>'post', 'files'=>'true', 'target'=>'_blank')) !!}

                                                {{ csrf_field() }}

                                                <button type="submit" name="submit" class="btn bg-light-blue" target='_blank'><i class="material-icons">library_books</i></button>
                                                <!-- <a href="{{asset('/documents/user_doc/'.$data->ic.'/'.$data->spekar)}}" class="btn bg-light-blue" target='_blank'> <i class="material-icons">library_books</i></a>-->

                                                 {{ Form::close() }}  

                                            @endif
                                        </div>
                                    </div>
                                    
                                    
                                    @if($datas->meetcus->existing == '0')
                                        <div class="row">
                                            <div class="col-md-6">
                                                <b>Existing Cust : </b>    
                                            </div>
                                            <div class="col-md-6">
                                                <input name="group2" type="radio" id="radio_11{{$datas->id}}" onclick="javascript:yesnoCheck2{{$datas->id}}();" value="1" disabled/>
                                                <label for="radio_11{{$datas->id}}">Yes</label>
                                                <input name="group2" type="radio" id="radio_21{{$datas->id}}" onclick="javascript:yesnoCheck2{{$datas->id}}();" value="0" checked  disabled/>
                                                <label for="radio_21{{$datas->id}}">No</label>  
                                            </div>
                                        </div>

                                     @else   

                                        <div class="row">
                                            <div class="col-md-6">
                                                <b>Existing Cust : </b>    
                                            </div>
                                            <div class="col-md-6">
                                                <input name="group2" type="radio" id="radio_11{{$datas->id}}" onclick="javascript:yesnoCheck2{{$datas->id}}();" value="1" checked  odisabled/>
                                                <label for="radio_11{{$datas->id}}">Yes</label>

                                                <input name="group2" type="radio" id="radio_21{{$datas->id}}" onclick="javascript:yesnoCheck2{{$datas->id}}();" value="0" disabled/>
                                                <label for="radio_21{{$datas->id}}" disabled>No</label>  
                                            </div>
                                        </div>

                                        <div class="row" >
                                            <div class="col-md-6" >
                                                <b>Date Disbursement : </b>    
                                            </div>
                                            <div class="col-md-6">
                                               
                                               <input type="date" id="datepicker{{$datas->id}}" name="disb" class="form-control" value="{{$datas->meetcus->date_disbustment}}" readonly> 

                                            </div>
                                        </div>
                                    @endif


                                    <!-- <div class="row resources" style="display: none" id="two">
                                        <div class="col-md-6"><b>Route to :</b></div>
                                        <div class="col-md-6">
                                            <select name="routetoq" class="form-control">
                                                <option value="" selected disabled hidden>Choose Processor2</option>
                                                @foreach($workgroupprocessor2 as $work)
                                                <option value="{{$work->model_id}}">{{$work->user->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div> -->

                                    <div class="row">
                                        <div class="form-group">
                                        <div class="col-xs-6 col-sm-3 col-md-4 col-lg-4 col-md-offset-8">
                                            <button type="submit" class="btn bg-green btn-block btn-lg waves-effect">
                                                Save
                                            </button>
                                            {{ csrf_field() }}
                                        </div>
                                        </div>

                                        
                                    </div>
                                </div> <!-- End of kolom 2 -->
                            </div>
                            {{ Form::close() }}   
                            
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            <!-- ///////////////////  End Data Target  //////////////// -->
        </div>
    </section>

    <!-- ================= Data Target hidden ======= -->
    <script src="https://code.jquery.com/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
     

    <script type="text/javascript">
        $("[data-collapse-group]").on('show.bs.collapse', function () {
              var $this = $(this);
              var thisCollapseAttr = $this.attr('data-collapse-group');
              $("[data-collapse-group='" + thisCollapseAttr + "']").not($this).collapse('hide');
            });
    </script>

    <!-- ================= End Data Target hidden ======= -->

    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>

    <script>
        $(document).ready(function() {
        $('#example').DataTable();
        });
    </script>

@endsection


@push('js')


    @foreach($meetcus as $datas)
        <script type="text/javascript">
        var Privileges = jQuery('#one{{$datas->id}}');
        var select = this.value;
        Privileges.change(function () {
            if ($(this).val() == 'W4') {
                $('.resources').show();
            }
            else $('.resources').hide();
        });


        function yesnoCheck2{{$datas->id}}() {
            if (document.getElementById('radio_11{{$datas->id}}').checked) {
                document.getElementById('ifYes1{{$datas->id}}').style.display = 'block';
            }
            else document.getElementById('ifYes1{{$datas->id}}').style.display = 'none';
        }
        </script>

    @endforeach



    @foreach($meetcus as $datas)

        <script type="text/javascript">

           
        $( function() {
            $( "#datepicker{{$datas->id}}" ).datepicker();
            
          } );
        </script>
    @endforeach

<!-- Untuk Location -->
<input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
<script>
var apiGeolocationSuccess = function(position) {
  showLocation(position);
};

var tryAPIGeolocation = function() {
  jQuery.post( "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyDCa1LUe1vOczX1hO_iGYgyo8p_jYuGOPU", function(success) {
    apiGeolocationSuccess({coords: {latitude: success.location.lat, longitude: success.location.lng}});
  })
  .fail(function(err) {
    
  });
};

var browserGeolocationSuccess = function(position) {
  showLocation(position);
};

var browserGeolocationFail = function(error) {
  switch (error.code) {
    case error.TIMEOUT:
      alert("Browser geolocation error !\n\nTimeout.");
      break;
    case error.PERMISSION_DENIED:
      if(error.message.indexOf("Only secure origins are allowed") == 0) {
        tryAPIGeolocation();
      }
      break;
    case error.POSITION_UNAVAILABLE:
      alert("Browser geolocation error !\n\nPosition unavailable.");
      break;
  }
};

var tryGeolocation = function() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(
      browserGeolocationSuccess,
      browserGeolocationFail,
      {maximumAge: 50000, timeout: 20000, enableHighAccuracy: true});
  }
};

tryGeolocation();

function showLocation(position) {
  var latitude = position.coords.latitude;
  var longitude = position.coords.longitude;
  var _token = $('#_token').val();
  $.ajax({
    type:'POST',
    url: "{{url('/')}}/getLocation",
    data: { latitude: latitude, _token : _token, longitude : longitude },
    success:function(data){
            if(data){
              $("#latitude").html("<input type='hidden' name='latitude' id='latitude' value='"+data.latitude+"'>");
              $("#longitude").html("<input type='hidden' name='longitude' id='longitude' value='"+data.longitude+"'>");
               $("#location").html("<input type='hidden' name='location' id='location' value='"+data.location+"'>");
                 

            }else{
                $("#location").html('Not Available');
            }
    }
  });
}
</script>

  
<!-- End Location -->

    @endpush