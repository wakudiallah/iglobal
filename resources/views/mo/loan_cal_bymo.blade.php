@extends('vadmin.tampilan')

@section('content')



	<section class="content">
        <div class="container-fluid">

            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">work</i> Loan Eligibility & Scoring </a></li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->


            @include('shared.notif')

            <!-- Basic Example | Horizontal Layout -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Loan Eligibility & Scoring </h2>
                        </div>
                        <div class="body">
                            <div id="wizard_horizontal" style="">
                                <h2>Existing Customer</h2>
                                <section> <!-- +++++++++++++++++++++Section Spekar ++++++++++++++++ -->
                                    @foreach($meetcus as $datas)           

                                        <div class="row">
                                            <!-- kolom 1 -->
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <b>Name : </b>    
                                                    </div>
                                                    <div class="col-md-6">
                                                       <input type="text" name="" value="{{$datas->name}}" class="form-control" readonly>   
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <b>IC : </b>    
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="text" name="" value="{{$datas->ic}}" class="form-control" readonly>
                                                        
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <b>Phone Number : </b>    
                                                    </div>
                                                    <div class="col-md-6">
                                                        
                                                       <input type="text" name="" value="{{$datas->notelp}} " class="form-control" readonly>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <b>Email : </b>    
                                                    </div>
                                                    <div class="col-md-6">
                                                        
                                                       <input type="text" name="" value="{{$datas->email}} " class="form-control" readonly>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <b>By : </b>    
                                                    </div>
                                                    <div class="col-md-6">
                                                       <b><p style="color: red">{{$datas->user->name}} </p></b>  
                                                    </div>
                                                </div>

                                              
                                               
                                            </div> <!-- End of kolom 1 -->
                                            <!-- kolom 2 -->
                                            <div class="col-md-6">
                                                <!-- 
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <b>Employer : {{$datas->majikan->Emp_Desc}}</b>    
                                                    </div>
                                                    @if($datas->emp_code == 1)
                                                    <div class="col-md-6">
                                                        
                                                        <input name="group1" type="radio" id="radio_1{{$datas->id}}" value="1" checked />
                                                        <label for="radio_1{{$datas->id}}">Crarical</label>
                                                        <input name="group1" type="radio" id="radio_2{{$datas->id}}" value="0" />
                                                        <label for="radio_2{{$datas->id}}">Non Crarical</label>
                                                    </div>
                                                    @endif
                                                </div> -->
                                                
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <b>Spekar : </b>    
                                                    </div>
                                                    <div class="col-md-6">
                                                        @if((empty($data->spekar))) 
                                                            <a href="#" class="btn bg-red"><i class="material-icons disabled">do_not_disturb_alt</i> </a>
                                                        @else

                                                            {!! Form::open(array('url'=>'download/spekar/'.$data->id_cus, 'method'=>'post', 'files'=>'true', 'target'=>'_blank')) !!}

                                                            {{ csrf_field() }}

                                                            <button type="submit" name="submit" class="btn bg-light-blue" target='_blank'><i class="material-icons">library_books</i></button>
                                                            <!-- <a href="{{asset('/documents/user_doc/'.$data->ic.'/'.$data->spekar)}}" class="btn bg-light-blue" target='_blank'> <i class="material-icons">library_books</i></a>-->

                                                             {{ Form::close() }}  

                                                        @endif
                                                    </div>
                                                </div>
                                                
                                            {!! Form::open(array('url'=>'save/meetcus/'.$datas->id_cus, 'method'=>'post', 'files'=>'true')) !!}

                                            {{ csrf_field() }}
                                            <span id="latitude"></span>
                                            <span id="longitude"></span>
                                            <span id="location"></span>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <b>Existing Cust : </b>    
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input name="group2" type="radio" id="radio_11{{$datas->id}}" onclick="javascript:yesnoCheck2{{$datas->id}}();" value="1" required />
                                                        <label for="radio_11{{$datas->id}}">Yes</label>

                                                        <input name="group2" type="radio" id="radio_21{{$datas->id}}" onclick="javascript:yesnoCheck2{{$datas->id}}();" value="0" required/>
                                                        <label for="radio_21{{$datas->id}}">No</label>  
                                                    </div>
                                                </div>

                                                <div class="row" style="display:none" id="ifYes1{{$datas->id}}">
                                                    <div class="col-md-6" >
                                                        <b>Date Disbursement : </b>    
                                                    </div>
                                                    <div class="col-md-6">
                                                       
                                                       <input type="text" id="datepicker" name="disb" class="form-control"> 

                                                    </div>
                                                   
                                                </div>

                                                <!-- <div class="row resources" style="display: none" id="two">
                                                    <div class="col-md-6"><b>Route to :</b></div>
                                                    <div class="col-md-6">
                                                        <select name="routetoq" class="form-control">
                                                            <option value="" selected disabled hidden>Choose Processor2</option>
                                                            @foreach($workgroupprocessor2 as $work)
                                                            <option value="{{$work->model_id}}">{{$work->user->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div> -->

                                               
                                            </div> <!-- End of kolom 2 -->
                                        </div>
                                        {{ Form::close() }}   
                                                
                                    @endforeach
                                </section> <!-- +++++++++++++++++++++ End Section Spekar ++++++++++++++++ -->

                                <h2>Loan Calculation</h2>
                                <section> <!-- +++++++++++++++++++++  Section Loan Calculation ++++++++++++++++ -->
                                    <p>
                                        Donec mi sapien, hendrerit nec egestas a, rutrum vitae dolor. Nullam venenatis diam ac
                                        ligula elementum pellentesque. In lobortis sollicitudin felis non eleifend. Morbi
                                        tristique tellus est, sed tempor elit. Morbi varius, nulla quis condimentum dictum,
                                        nisi elit condimentum magna, nec venenatis urna quam in nisi. Integer hendrerit sapien
                                        a diam adipiscing consectetur. In euismod augue ullamcorper leo dignissim quis elementum
                                        arcu porta. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum leo
                                        velit, blandit ac tempor nec, ultrices id diam. Donec metus lacus, rhoncus sagittis
                                        iaculis nec, malesuada a diam. Donec non pulvinar urna. Aliquam id velit lacus.
                                    </p>
                                </section> <!-- +++++++++++++++++++++ End Section Loan Calculation ++++++++++++++++ -->

                                <h2>Tenure</h2>
                                <section> <!-- +++++++++++++++++++++ Section Tenure ++++++++++++++++ -->
                                    <p>
                                        Morbi ornare tellus at elit ultrices id dignissim lorem elementum. Sed eget nisl at justo
                                        condimentum dapibus. Fusce eros justo, pellentesque non euismod ac, rutrum sed quam.
                                        Ut non mi tortor. Vestibulum eleifend varius ullamcorper. Aliquam erat volutpat.
                                        Donec diam massa, porta vel dictum sit amet, iaculis ac massa. Sed elementum dui
                                        commodo lectus sollicitudin in auctor mauris venenatis.
                                    </p>
                                </section> <!-- +++++++++++++++++++++ End Section Tenure ++++++++++++++++ -->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Example | Horizontal Layout -->
		    



        </div>
    </section>


    @foreach($meetcus as $datas)
        <script type="text/javascript">
        var Privileges = jQuery('#one{{$datas->id}}');
        var select = this.value;
        Privileges.change(function () {
            if ($(this).val() == 'W4') {
                $('.resources').show();
            }
            else $('.resources').hide();
        });


        function yesnoCheck2{{$datas->id}}() {
            if (document.getElementById('radio_11{{$datas->id}}').checked) {
                document.getElementById('ifYes1{{$datas->id}}').style.display = 'block';
            }
            else document.getElementById('ifYes1{{$datas->id}}').style.display = 'none';
        }
        </script>

        <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

        <script>
            $(function() {
               $( "#datepicker" ).datepicker();
             });
            
        </script>

    @endforeach



@endsection

@push('js')

    <!-- Jquery Core Js -->
    <script src="{{asset('admin/plugins/jquery/jquery.min.js') }}"></script>

    <!-- Select Plugin Js -->
    <script src="{{asset('admin/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{asset('admin/plugins/jquery-slimscroll/jquery.slimscroll.js') }}"></script>

    <!-- Jquery Validation Plugin Css -->
    <script src="{{asset('admin/plugins/jquery-validation/jquery.validate.js') }}"></script>

    <!-- JQuery Steps Plugin Js -->
    <script src="{{asset('admin/plugins/jquery-steps/jquery.steps.js') }}"></script>

    <!-- Sweet Alert Plugin Js -->
    <script src="{{asset('admin/plugins/sweetalert/sweetalert.min.js') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{asset('admin/plugins/node-waves/waves.js') }}"></script>

    <!-- Custom Js -->
    <script src="{{ asset('admin/js/admin.js') }}"></script>
    <script src="{{ asset('admin/js/pages/forms/form-wizard.js') }}"></script>

    <!-- Demo Js -->
    <script src="{{ asset('admin/js/demo.js') }}"></script>

    
@endpush