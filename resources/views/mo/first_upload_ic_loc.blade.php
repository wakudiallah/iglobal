@extends('vadmin.tampilan')       

@section('content')
	
	<section class="content">
        <div class="container-fluid">

            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">person</i> Customer</a></li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

            @include('shared.notif')
	
			<div class="row clearfix">
		        <div class="col-md-12">
		            <div class="card">
		                <div class="header bg-red">
		                    <h2>Add Doc New Customer </h2>
		                </div>
		                
		                <div class="body">
		                	<div class="row">
		                		<div class="col-md-2"></div>

		                			<div class="col-md-6"> <!-- isi -->
		                                
		                                <h4>{{$reg->name}}  ( {{$reg->ic}} )</h4>
		                                <h3 style="color: red !important; margin-bottom: 30px !important ">*Documents in PDF/JPG/JPEG/PNG</h3>

		                                <div class="col-sm-6 col-md-6">

		                                    <div class="">
		                                        
		                                        @if(empty($document1->doc_pdf))
		                                        <a href="#" class="btn bg-red"><i class="material-icons disabled">do_not_disturb_alt</i> </a>
		                                        @else
		                                        <a href="{{asset('/documents/user_doc/'.$reg->ic.'/'.$document1->doc_pdf)}}" class="btn bg-light-blue btn-lg" target='_blank'><i class="material-icons" >library_books</i></a>
		                                        @endif

		                                        <div class="caption">
		                                            <h4 style="color: red !important">Copy of IC </h4>
		                                            
		                                            <p>
		                                            	<form id="uploadic" action="{{url('save/uploadic/'.$reg->id_cus)}}" method="post" enctype="multipart/form-data">

		         										{{ csrf_field() }}

			                                                <input type="file" name="fileIc" id="changePicture1" class="hidden-input test1 form-control" onchange='this.form.submit()''>
			                                                <input type="hidden" name="ic" value="{{$reg->ic}}">

		                                            	</form>
		                                            </p>
		                                        </div>
		                                    </div>
		                                </div>

		                                <div class="col-sm-6 col-md-6">
		                                    <div class="">
		                                        @if(empty($document2->doc_pdf))
		                                        <a href="#" class="btn bg-red"><i class="material-icons disabled">do_not_disturb_alt</i> </a>
		                                        @else
		                                        <a href="{{asset('/documents/user_doc/'.$reg->ic.'/'.$document2->doc_pdf)}}" class="btn bg-light-blue btn-lg" target='_blank'><i class="material-icons" >library_books</i></a>
		                                        @endif

		                                        <div class="caption">
		                                            <h4 style="color: red !important">Copy of Assessment</h4>
		                                            
		                                            <p>
		                                            	<form id="uploadloc" action="{{url('save/uploadloc/'.$reg->id_cus)}}" method="post" enctype="multipart/form-data">

		         										{{ csrf_field() }}

		                                               		<input type="file" name="fileLoc" id="changePicture1" class="hidden-input test2 form-control" onchange='this.form.submit()''>
		                                               		<input type="hidden" name="ic" value="{{$reg->ic}}">

		                                               	</form>
		                                            </p>
		                                        </div>
		                                    </div>
		                                </div>
	                                </div> <!-- end isi -->

                                <div class="col-md-2"></div>
                            </div>

                            <div class="row">
                            	<div class="col-md-2 col-md-offset-10">
                            		@if(empty($document2->doc_pdf) || empty($document1->doc_pdf))
                            		<a href="#" class="btn btn-success btn-lg" disabled onclick="myFunction()">Save</a>
                            		@else
                            		<a href="{{url('adminnew')}}" class="btn btn-success btn-lg">Save</a>
                            		@endif
                            	</div>
                            </div>
		                </div>
		            </div>
		        </div>
		    </div>



			
		</div>
	</section>

	<script type="text/javascript">
        function test1() {
            var form = document.getElementById('uploadic');
            form.submit();
        };
    </script>

    <script type="text/javascript">
        function test2() {
            var form = document.getElementById('uploadloc');
            form.submit();
        };
    </script>

    <script>
		function myFunction() {
		    alert("Please insert file IC / LOC");
		}
	</script>

@endsection