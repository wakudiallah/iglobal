@extends('vadmin.tampilan')       

@section('content')

    <!-- Bootstrap Select Css -->
    <link href="{{ asset('admin/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
    
    <section class="content">
        <div class="container-fluid">

            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">hourglass_empty</i> Pending Documentation</a></li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

            <!-- Notification -->
            <div class="row clearfix">
                @if ($message = Session::get('success')) 
                <div class="alert bg-green alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @elseif($message = Session::get('delete'))
                <div class="alert bg-pink alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @elseif($message = Session::get('update'))
                <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @endif
            </div>
            <!-- End of Notif -->
    

            <div class="row clearfix"> <!-- Task Info -->
                <div class="card">
                    <div class="header bg-red">
                        <h2>Pending Documentation</h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-hover dashboard-task-infos" id="example">
                                <thead>
                                    <tr>
                                        <th width="5%">#</th>
                                        <th width="30%">Name</th>
                                        <th width="20%">IC</th>
                                        <th>Spekar</th>
                                        <th>Status</th> 
                                        <th>Detail</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>

                                    @foreach($meetcus as $data)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$data->name}}</td>
                                        <td>{{$data->ic}}</td>
                                        <td>
                                            @if((empty($data->spekar))) 
                                                <a href="#" class="btn bg-red"><i class="material-icons disabled">do_not_disturb_alt</i> </a>
                                            @else

                                                {!! Form::open(array('url'=>'download/spekar/'.$data->id_cus, 'method'=>'post', 'files'=>'true', 'target'=>'_blank')) !!}

                                                {{ csrf_field() }}

                                                <button type="submit" name="submit" class="btn bg-light-blue" target='_blank'><i class="material-icons">library_books</i></button>
                                                <!-- <a href="{{asset('/documents/user_doc/'.$data->ic.'/'.$data->spekar)}}" class="btn bg-light-blue" target='_blank'> <i class="material-icons">library_books</i></a>-->

                                                 {{ Form::close() }}  

                                            @endif
                                        </td>
                                        <td>
                                            @include('shared.stage_new')
                                        </td>
                                         
                                           
                                        <td>
                                            @if($data->stage == 'W3') 
                                            <!-- <button type="button" class="btn bg-green waves-effect btn-sm" data-toggle="modal" data-target="#defaultModal{{$data->id}}">Detail</button> -->
                                            <!-- <button type="button" class="btn btn-success waves-effect m-r-20" data-toggle="modal" data-target="#largeModal{{$data->id}}">Detail</button>-->
                                            <a href="{{ url('view_form/'.$data->id_cus.'/second_step') }}" class="btn btn-sm bg-green"><i class="material-icons">pageview</i> View Loan Cal </a>
                                            @else
                                            @endif
                                        </td>
                                    </tr>

                                    @endforeach
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            
            <!-- #END# Task Info -->
            </div>

            
            
            @foreach($meetcus as $datas)
            <!-- Large Size -->
            <div class="modal fade" id="largeModal{{$datas->id}}" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-red">
                            <h4 class="modal-title" id="largeModalLabel">Detail</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <!-- kolom 1 -->
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>Name : </b>    
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" name="" class="form-control" value="{{$datas->name}}" readonly="">  
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>No Telf : </b>    
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" name="" class="form-control" value="{{$datas->notelp}}" readonly="">  
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>By : </b>    
                                        </div>
                                        <div class="col-md-6">
                                           <b><p style="color: red">{{$datas->user->name}} </p>  </b>
                                        </div>
                                    </div>

                                    <!-- 
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="probootstrap-date-arrival">Remark</label>
                                        </div>
                                        <div class="col-md-6">
                                           
                                            
                                                <input  class="hidden" type="text" name="name" value="{{$datas->name}} " >
                                                <input  class="hidden" type="text" name="ic" value="{{$datas->ic}} " >
                                                <input  class="hidden" type="text" name="id_cus" value="{{$datas->id_cus}} " >

                                                
                                                <select name="moremark" class="form-control" id="one{{$datas->id}}">
                                                    <option value="" selected disabled hidden>Choose Remark</option>
                                                    <option value="W3">Pass</option>
                                                    <option value="W4">Reject</option>
                                                </select>
                                        </div>                                         
                                    </div>
                                -->
                                    

                                   
                                </div> <!-- End of kolom 1 -->
                                <!-- kolom 2 -->
                                <div class="col-md-6">
                                    <!-- 
                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>Employer : {{$datas->majikan->Emp_Desc}}</b>    
                                        </div>
                                        @if($datas->emp_code == 1)
                                        <div class="col-md-6">
                                            
                                            <input name="group1" type="radio" id="radio_1{{$datas->id}}" value="1" checked />
                                            <label for="radio_1{{$datas->id}}">Crarical</label>
                                            <input name="group1" type="radio" id="radio_2{{$datas->id}}" value="0" />
                                            <label for="radio_2{{$datas->id}}">Non Crarical</label>
                                        </div>
                                        @endif
                                    </div> -->

                                    
                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>Loan Ammount : </b>    
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" name="" class="form-control" value="{{$loanammount->loanammount}}" readonly=""> 
                                            
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>Max Loan : </b>    
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" name="" class="form-control" value="{{$loanammount->maxloan}}" readonly=""> 
                                        </div>
                                    </div>
                                       
                                    </div>

                                    
                                </div> <!-- End of kolom 2 -->
                            </div>
                        </div>
                        <div class="modal-footer">
                            
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            
        </div>
    </section>

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>


    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable();
            });
    </script>
@endsection
