@extends('vadmin.tampilan_select_non')

@section('content')

<style type="text/css">
  .dropdown-toggle:after { content: none }
</style>

  <section class="content">
        <div class="container-fluid">
            
            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">create</i> Loan Eligibility</a></li>
                        
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->
            
           <div class="row clearfix">
                @if ($message = Session::get('success')) 
                <div class="alert bg-green alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @elseif($message = Session::get('delete'))
                <div class="alert bg-pink alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @elseif($message = Session::get('update'))
                <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @endif
            </div>
            <!-- Advanced Form Example With Validation -->
            <div class="row clearfix">
              <div class="col-md-2"></div>
              <div class="col-md-8">
                  <div class="card">
                      <div class="header bg-red">
                          <h2>Loan Eligibility</h2>
                      </div>
                      <div class="body">

                      

                        {!! Form::open(['url' => ['/tenos/custtenus/store'], 'class' => "probootstrap-form border border-danger", 'method' => 'post', 'id' => 'form-validate-pra']) !!}

                          {{ csrf_field() }}

                          <input type="hidden" name="id_cus" value="{{$meetcus->id_cus}}">

                        
                          <div class="form-group form-float">
                            <label class="form-label">Job Type:</label>
                              <div class="form-line">
                                  <select  id="Employment" name="Employment" required class='form-control'>
                                 <option value="">Select</option>

                                @foreach($employment as $data) 
                                        <option value="{{ $data->id }}" {{ $meetcus->employment_code == $data->id ? 'selected' : '' }}>{{$data->name}} </option>
                                        @endforeach                         
                              </select>
                              </div>
                          </div>

                          <div class="form-group form-float">
                                <label class="form-label">Package:</label>
                                <div class="form-line">
                                    <select  id="Package" name="Package" class='form-control'>
                                      <option value="">Select</option>
                                  @foreach ($loanpkg as $data)
                                  <option value="{{$data->id}}" data-parent="{{$data->EmpType}}">{{$data->Ln_Desc}} </option>
                                  @endforeach
                              </select>
                                   
                                    

                                </div>    
                            </div>
                           

                        <label for="">Basic Salary (RM):</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="gaji_asas" name="gaji_asas" class="form-control" value="{{$meetcus->gaji_asas}}" min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');"   required>
                                </div>
                            </div>
                            <label for="">Allowance (RM):</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" value="{{$meetcus->elaun}}" id="elaun" name="elaun" min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');"  required>
                                </div>
                            </div>
                            <label for="">Total Deduction (RM):</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" value="{{$meetcus->pot_bul}}" id="pot_bul" name="pot_bul" min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');"   required>
                                </div>
                            </div>
                            <label for="">Loan Amount (RM):</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" id="jml_pem" name="jml_pem" value="{{$meetcus->jml_pem}}" min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');"  required>
                                </div>
                            </div>

                           <div class="row">
                              <div class="col-md-10"></div>
                              <div class="col-md-2">
                                  <input type="submit" value="Save" class="btn btn-lg btn-success btn-block" style="cursor:pointer;">
                                      {{ csrf_field() }}
                              </div>
                          </div>
                      
                       {!! Form::close() !!}
                      </div>
                  </div>
              </div>
              <div class="col-md-2"></div>
            </div>

        </div>
    </section>



@endsection

@push('js')

<script type="text/javascript">
  $('#Employment').change(function() {
   var parent = $(this).val();
   $('#Package').children().each(function() {
      if($(this).data('parent') != parent) {
                $(this).hide();
      } else    $(this).show();
   });
});
</script>
@endpush