 @extends('vadmin.tampilan')


@section('content')
	<section class="content">
        <div class="container-fluid">

            <div class="row clearfix"> <!-- Breadcrumber -->

                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">sync_problem</i> Calculation & Changes</a></li>

                    </ol>
                </div>

            </div> <!-- End of breadcrumber -->


			<div class="row clearfix">
		        <div class="card">
		            <div class="header bg-red">
		                <h2>Calculation & Changes</h2>
		            </div>
		            <div class="body">
		                <div class="table-responsive">
		                    <table class="table table-hover dashboard-task-infos" id="example">
		                        <thead>
		                            <tr>
		                                <th>#</th>
		                                <th>Name</th>
		                                <th>IC</th>
		                                <th>Phone</th>
		                                <th>Status</th> 
		                                <th>Note</th>
		                                <th>Action</th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                            <?php $i = 1; ?>

		                            @foreach($change as $data)
		                            <tr>
		                                <td>{{$i++}}</td>
		                                <td>{{$data->name}}</td>
		                                <td>{{$data->ic}}</td>
		                                <td>{{$data->notelp}}</td>
		                                <!-- <td>
		                                     <button class="btn btn-success" aria-controls="collapse-{{$data->id}}" data-target="#collapseDetailOne{{$data->id}}" data-toggle="collapse" style="cursor:pointer;">Detail</button>
		                                </td>-->
		                                <td>
		                                     @include('shared.stage_new')
		                                </td> 
		                                <td><button type="button" class="btn btn-success waves-effect m-r-20" data-toggle="modal" data-target="#largeModal{{$data->id}}">Detail</button></td>
		                                <td>
		                                	<a href="{{url('/cal/change/'.$data->id_cus)}}" class="btn btn-sm btn-success"><i class="material-icons">sync_problem</i>Calculation & Change  </a>
		                                </td>
		                            </tr>

		                            @endforeach
		                            
		                        </tbody>
		                    </table>
		                </div>
		            </div>
		        </div>
			</div>

			
			@foreach($change as $datas)

			<?php $note = DB::table('history')->where('cus_id', $datas->id_cus)->where('remark_id', 'W6')->latest('id')->limit('1')->first(); ?>

            <!-- Large Size -->
            <div class="modal fade" id="largeModal{{$datas->id}}" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-red">
                            <h4 class="modal-title" id="largeModalLabel">Detail</h4>
                        </div>
                        <div class="modal-body">
                        	<p><b>Note :</b></p>
                            <textarea id="txtArea" name="notep6" class="form-control" rows="6" cols="5" readonly>{{$note->note}}</textarea>
                        </div>
                        <div class="modal-footer">
                            
                        </div>
                    </div>
                </div>
            </div>
            @endforeach


			<!-- ///////////////////  Data Target  //////////////// -->
		    @foreach($change as $datas)
		    <div id="collapseDetailOne{{$datas->id}}" class="collapse" aria-expanded="false" data-collapse-group="collapse-group" aria-labelledby="headingFive" data-parent="#accordionExample">
		        <div class="row clearfix">
		            <div class="card">
		                <div class="header bg-red">
		                    <h2>Detail {{$datas->name}}</h2>
		                </div>
		                <div class="body">
		                   
		                   	 

		                </div>
		            </div>
		        </div>
		    </div>
		    @endforeach
		    <!-- ///////////////////  End Data Target  //////////////// -->

		</div>
	</section>


	<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>


	<script type="text/javascript">
		$(document).ready(function() {
	        $('#example').DataTable();
	        });
	</script>
	
@endsection

    