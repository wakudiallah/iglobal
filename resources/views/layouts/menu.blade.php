@section('layouts.menu')
 <nav class="navbar navbar-expand-lg navbar-dark probootstrap_navbar" id="probootstrap-navbar">
      <div class="container">
        <a class="navbar-brand" href="/"><img src="{{ asset('assets/images/logo1.png')}}"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#probootstrap-menu" aria-controls="probootstrap-menu" aria-expanded="false" aria-label="Toggle navigation">
          <span><i class="ion-navicon"></i></span>
        </button>
        <div class="collapse navbar-collapse" id="probootstrap-menu">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item {{ Request::is('/') ? 'active' : '' }}"><a class="nav-link" href="{{url('/')}}">Home</a></li>
            <li class="nav-item {{ Request::is('about') ? 'active' : '' }}"><a class="nav-link" href="{{url('about')}}">About Us</a></li>
            <li class="nav-item {{ Request::is('service') ? 'active' : '' }}"><a class="nav-link" href="{{url('service')}}">Service</a></li>
            <li class="nav-item {{ Request::is('faq') ? 'active' : '' }}"><a class="nav-link" href="{{url('faq')}}">FAQ</a></li>
            <li class="nav-item {{ Request::is('contact') ? 'active' : '' }}"><a class="nav-link" href="{{url('contact')}}">Contact</a></li>
            @guest
            <li class="nav-item {{ Request::is('login') ? 'active' : '' }}"><a class="nav-link" href="{{url('login')}}">SIGN IN</a></li>
            @else
            <li class="nav-item {{ Request::is('adminnew') ? 'active' : '' }}"><a class="nav-link" href="{{url('adminnew')}}">ADMIN</a></li>
            @endguest
          </ul>
        </div>
      </div>
    </nav>@stop