<!DOCTYPE html>
<html lang="en">
	<head>
      
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>Welcome - Netxpert</title>
    
    <!-- Favicon -->
    <link rel="icon" href="{{ asset('admin/favicon/favicon.ico') }}" type="image/x-icon">
    
    
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,700" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap/bootstrap.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/fonts/ionicons/css/ionicons.min.css') }}">
    
    <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.min.css') }}">
    
    <link rel="stylesheet" href="{{ asset('assets/fonts/flaticon/font/flaticon.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/fonts/fontawesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/select2.css') }}">
    

    <link rel="stylesheet" href="{{ asset('assets/css/helpers.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">

    <!-- dial style-->
    <link rel="stylesheet" href="{{ asset('assets/css/dial.css') }}">

    <!-- Validate Select belum dulu -->
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css">

    <!-- jquery upload css -->
    <link rel="stylesheet" href="{{ asset('assets/css/jquery.fileupload.css') }}">

	</head>
	<body>

    <nav class="navbar navbar-expand-lg navbar-dark probootstrap_navbar" id="probootstrap-navbar">
      <div class="container">
        <a class="navbar-brand" href="/"><img src="{{ asset('assets/images/logo1.png')}}"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#probootstrap-menu" aria-controls="probootstrap-menu" aria-expanded="false" aria-label="Toggle navigation">
          <span><i class="ion-navicon"></i></span>
        </button>
        <div class="collapse navbar-collapse" id="probootstrap-menu">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item {{ Request::is('/') ? 'active' : '' }}"><a class="nav-link" href="{{url('/')}}">Home</a></li>
            <li class="nav-item {{ Request::is('about') ? 'active' : '' }}"><a class="nav-link" href="{{url('about')}}">About Us</a></li>
            <li class="nav-item {{ Request::is('service') ? 'active' : '' }}"><a class="nav-link" href="{{url('service')}}">Service</a></li>
            <li class="nav-item {{ Request::is('faq') ? 'active' : '' }}"><a class="nav-link" href="{{url('faq')}}">FAQ</a></li>
            <li class="nav-item {{ Request::is('contact') ? 'active' : '' }}"><a class="nav-link" href="{{url('contact')}}">Contact</a></li>
            @guest
            <li class="nav-item {{ Request::is('login') ? 'active' : '' }}"><a class="nav-link" href="{{url('login')}}">SIGN IN</a></li>
            @else
            <li class="nav-item {{ Request::is('adminnew') ? 'active' : '' }}"><a class="nav-link" href="{{url('adminnew')}}">ADMIN</a></li>
            @endguest
          </ul>
        </div>
      </div>
    </nav>
    <!-- END nav -->

    @yield('content')

    
    
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

    <script src="{{ asset('assets/js/popper.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>

    <script src="{{ asset('assets/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.waypoints.min.js') }}"></script>

    <script src="{{ asset('assets/js/select2.min.js') }}"></script>

    <script src="{{ asset('assets/js/main.js') }}"></script>

	<!-- JQUERY Validate Select -->
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>
	<!-- End Select -->

    @stack('script')

	</body>
</html>