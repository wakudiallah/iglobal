@extends('vadmin.tampilan')

@section('content')

    <section class="content">
        <div class="container-fluid">

            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">assignment</i> Generate & Send to MBSB</a></li>
                        
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->


        <div class="row clearfix">
            <!-- Task Info -->
            
                <div class="card">
                    <div class="header bg-red">
                        <h2>Loan Eligibility Checking and Calculation</h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-hover dashboard-task-infos" id="example">
                                <thead>
                                    <tr>
                                        <th width="10%">#</th>
                                        <th width="30%">Name</th>
                                        <th width="20%">IC</th>
                                        <th>Doc / 103 Check </th>
                                        <th>Status<th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>

                                    @foreach($process7 as $data)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$data->name}}</td>
                                        <td>{{$data->ic}}</td>
                                        
                                        <td>
                                            @if($data->doc == 1)
                                            <button class="btn btn-success" aria-controls="collapse-{{$data->id}}" data-target="#collapseDetail_103{{$data->id}}" data-toggle="collapse" style="cursor:pointer;">103 Check</button>
                                            @elseif($data->doc == 0 || empty($data->doc))
                                            <button class="btn btn-success" aria-controls="collapse-{{$data->id}}" data-target="#collapseDetailOne{{$data->id}}" data-toggle="collapse" style="cursor:pointer;">Doc</button>
                                            @else(!empty($data->process8))
                                            <button class="btn btn-success" aria-controls="collapse-{{$data->id}}" data-target="#collapseDetailKYC{{$data->id}}" data-toggle="collapse" style="cursor:pointer;">KYC</button> <!-- Tapi Belum yy -->
                                            @endif
                                        </td>

                                        <td>
                                            @include('shared.stage_new')
                                        </td>   
                                    </tr>
                                    @endforeach
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            
            <!-- #END# Task Info -->
        </div>

        <!-- ///////////////////  Data Target Document Check //////////////// -->
        @foreach($process7 as $datas)
        <div id="collapseDetailOne{{$datas->id}}" class="collapse" aria-expanded="false" data-collapse-group="collapse-group" aria-labelledby="headingFive" data-parent="#accordionExample">
            <div class="row clearfix">
                <div class="card">
                    <div class="header bg-green">
                        <h2>Doc {{$datas->name}}</h2>
                    </div>
                    <div class="body">
                        {!! Form::open(array('url'=>'save/process7/'.$datas->id_cus, 'method'=>'post', 'files'=>'true')) !!}
                        
                        <div class="row">
                            <table class="table table-responsive">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Document</th>
                                        <th scope="col">Checklist</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php $y = 1; ?>

                                    
                                    <?php
                                           $datax1 = DB::table('doc_cust')->join('praapplication', 'doc_cust.cus_id', '=', 'praapplication.id_cus')->where('cus_id', $datas->id_cus)->where('doc_cust.type','1')->latest('doc_cust.created_at')->limit('1')->first();

                                           $datax2 = DB::table('doc_cust')->join('praapplication', 'doc_cust.cus_id', '=', 'praapplication.id_cus')->where('cus_id', $datas->id_cus)->where('doc_cust.type','2')->latest('doc_cust.created_at')->limit('1')->first();

                                           $datax3 = DB::table('doc_cust')->join('praapplication', 'doc_cust.cus_id', '=', 'praapplication.id_cus')->where('cus_id', $datas->id_cus)->where('doc_cust.type','4')->latest('doc_cust.created_at')->limit('1')->first();
                                    ?>

                                    <tr>  
                                        <td>{{$y++}}</td>
                                        <td>
                                            <a href="{{asset('/documents/user_doc/'.$datas->ic.'/'.$datax1->doc_pdf)}}" target='_blank'>{{$datax1->doc_pdf}}</a>
                                        </td>
                                        <td>
                                            <input type="checkbox" id="basic_checkbox_{{$datas->id}}{{$datax1->doc_pdf}}" name="checkbox{{$datax1->id}}"   />
                                            <label for="basic_checkbox_{{$datas->id}}{{$datax1->doc_pdf}}"></label>
                                        </td>
                                    </tr>
                                    <tr>  
                                        <td>{{$y++}}</td>
                                        <td>
                                            <a href="{{asset('/documents/user_doc/'.$datas->ic.'/'.$datax2->doc_pdf)}}" target='_blank'>{{$datax2->doc_pdf}}</a>
                                        </td>
                                        <td>
                                            <input type="checkbox" id="basic_checkbox_{{$datas->id}}{{$datax2->doc_pdf}}" name="checkbox{{$datax1->id}}"  />
                                            <label for="basic_checkbox_{{$datas->id}}{{$datax2->doc_pdf}}"></label>
                                        </td>
                                    </tr>
                                    <tr>  
                                        <td>{{$y++}}</td>
                                        @if(empty($datax3->doc_pdf))
                                            <td></td>
                                        @else
                                            <td>
                                                <a href="{{asset('/documents/user_doc/'.$datas->ic.'/'.$datax3->doc_pdf)}}" target='_blank'>{{$datax3->doc_pdf}}</a>
                                            </td>
                                            <td>
                                                <input type="checkbox" id="basic_checkbox_{{$datas->id}}{{$datax3->doc_pdf}}" name="checkbox{{$datax1->id}}"  />
                                                <label for="basic_checkbox_{{$datas->id}}{{$datax3->doc_pdf}}"></label>
                                            </td>
                                        
                                        @endif
                                    </tr>
                                   
                                </tbody>
                            </table>   
                        </div>



                        <div class="row" style="margin-top: 20px">
                            <!-- kolom 1 -->
                            <div class="col-md-6">
                                
                                
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="probootstrap-date-arrival">Remark</label>
                                    </div>
                                    <div class="col-md-6">
                                        <!-- <input id="spekar" class="form-control"   type="file" name="spekar" >-->
                                        
                                            <input  class="hidden" type="text" name="name" value="{{$datas->name}} " >
                                            <input  class="hidden" type="text" name="ic" value="{{$datas->ic}} " >
                                            <input  class="hidden" type="text" name="id_cus" value="{{$datas->id_cus}} " >

                                            <select name="pro3remark" class="form-control" id="one{{$datas->id}}">
                                                <option value="W7">Doc Complete</option> <!-- Value belum -->
                                                <option value="W8">Doc Incomplete</option>
                                            </select>
                                            
                                    </div>                                         
                                </div>
                                
                               
                            </div> <!-- End of kolom 1 -->
                            <!-- kolom 2 -->
                            <div class="col-md-6">
                                
                                <?php
                                     $routomobalik = DB::table('praapplication')->where('id_cus', $datas->id_cus)->limit('1')->first();
                                ?>

                                <input type="text"  name="routomobalik" value="{{$routomobalik->process5}}" class="hidden">
                                
                                <!-- 
                                <div class="row resources" style="display: none" id="two">
                                    <div class="col-md-6"><b>Route to :</b></div>
                                    <div class="col-md-6">
                                        <select name="routetoq" class="form-control">
                                            <option value="" selected disabled hidden>Choose Processor 3</option>
                                            @foreach($workgroupprocess6 as $work)
                                            <option value="{{$work->model_id}}">{{$work->user->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                -->

                                <div class="row resources" style="display: none" id="two">
                                    <div class="col-md-6"><b>Note :</b></div>
                                    <div class="col-md-6">
                                        <textarea rows="4" cols="40" name="note"> </textarea>
                                    </div>
                                    
                                </div>



                                <div class="row">
                                    <div class="form-group">
                                    <div class="col-xs-6 col-sm-3 col-md-4 col-lg-4 col-md-offset-8">
                                        <button type="submit" class="btn bg-green btn-block btn-lg waves-effect">
                                            Save
                                        </button>
                                        {{ csrf_field() }}
                                    </div>
                                    </div>

                                    
                                </div>
                            </div> <!-- End of kolom 2 -->
                        </div>
                        {{ Form::close() }}   
                        
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        <!-- ///////////////////  End Data Target Document Check //////////////// -->


        <!-- ///////////////////  Data Target 103 Check //////////////// -->
        @foreach($process7 as $datas)
        <div id="collapseDetail_103{{$datas->id}}" class="collapse" aria-expanded="false" data-collapse-group="collapse-group" aria-labelledby="headingFive" data-parent="#accordionExample">
            <div class="row clearfix">
                <div class="card">
                    <div class="header bg-green">
                        <h2>103 Check {{$datas->name}}</h2>
                    </div>
                    <div class="body">
                        {!! Form::open(array('url'=>'save/process8/103/'.$datas->id_cus, 'method'=>'post', 'files'=>'true')) !!}
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group form-float">
                                    <div class="col-md-6">
                                        <h5>Customer Office Tlp No :</h5>
                                    </div>
                                    <div class=col-md-6>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="103checking" min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');"  value=""  required>
                                            <div class="help-info"></div> <!-- for notefoot -->
                                        </div>    
                                    </div>
                                    
                                </div>      
                            </div>


                            <div class="col-md-6"></div>
                            
                        </div>



                        <div class="row" style="margin-top: 20px">
                            <!-- kolom 1 -->
                            <div class="col-md-6">
                                
                                
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="probootstrap-date-arrival">Remark</label>
                                    </div>
                                    <div class="col-md-6">
                                        <!-- <input id="spekar" class="form-control"   type="file" name="spekar" >-->
                                        
                                            <input  class="hidden" type="text" name="name" value="{{$datas->name}} " >
                                            <input  class="hidden" type="text" name="ic" value="{{$datas->ic}} " >
                                            <input  class="hidden" type="text" name="id_cus" value="{{$datas->id_cus}} " >

                                            <select name="pro3remark103" class="form-control" id="103{{$datas->id}}">
                                                <option value="W9">103 Yes</option> <!-- Value belum -->
                                                <option value="W10" style="color: red">Tak Ada (Belum lagi tanya dlu)</option>
                                            </select>
                                            
                                    </div>                                         
                                </div>
                                
                               
                            </div> <!-- End of kolom 1 -->

                            <!-- kolom 2 -->
                            <div class="col-md-6">
                                

                                <div class="row">
                                    <div class="form-group">
                                    <div class="col-xs-6 col-sm-3 col-md-4 col-lg-4 col-md-offset-8">
                                        <button type="submit" class="btn bg-green btn-block btn-lg waves-effect">
                                            Save
                                        </button>
                                        {{ csrf_field() }}
                                    </div>
                                    </div>

                                    
                                </div>
                            </div> <!-- End of kolom 2 -->
                        </div>
                        {{ Form::close() }}   
                        
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        <!-- ///////////////////  End Data Target  //////////////// -->
@endsection

    @push('js')

    @foreach($process7 as $datas)
       <script type="text/javascript">
        var Privileges = jQuery('#one{{$datas->id}}');
        var select = this.value;
        Privileges.change(function () {
            if ($(this).val() == 'W8') {
                $('.resources').show();
            }
            else $('.resources').hide();
        });

    </script>
    @endforeach

    <!-- 
    <script src="{{ asset('admin/js/pages/forms/basic-form-elements.js') }}"></script>
    -->

    @endpush