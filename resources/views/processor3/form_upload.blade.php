@extends('vadmin.tampilan')

@section('content')

    <section class="content">
        <div class="container-fluid">

            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">offline_pin</i> Upload Amount Release</a></li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

            @include('shared.notif')

            <div class="row clearfix">
                <div class="card">
                    <div class="header bg-red">
                        <h2>Application Approval</h2>
                    </div>
                    <div class="body">
                    	{!! Form::open(['url' => ['/upload/amount/post'], 'class' => "probootstrap-form border border-danger", 'method' => 'post', 'enctype' => 'multipart/form-data', 'id' => 'form-validate-pra']) !!}

                    	{{ csrf_field() }}

                        <div class="row">
                            <div class="col-md-3"></div>
                                <div class="col-md-6">
                            		<input type="file" name="file">

                            		<div class="row">
                                    <div class="col-md-6"></div>
                                    <div class="col-md-6">
                                        <div class="col-md-6 col-md-offset-6">
                                            <input type="submit" value="Save" class="btn btn-lg btn-success btn-block" style="cursor:pointer;">
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            <div class="col-md-3"></div>
                        </div>
                                
                            {{ csrf_field() }}
                    	{!! Form::close() !!}
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection