@extends('vadmin.tampilan')

@section('content')

    <section class="content">
        <div class="container-fluid">

            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">description</i> Additional Doc</a></li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->


            <!-- Notification -->
            <div class="row clearfix">
                @if ($message = Session::get('success')) 
                <div class="alert bg-green alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @elseif($message = Session::get('delete'))
                <div class="alert bg-pink alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @elseif($message = Session::get('update'))
                <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @endif
            </div>
            <!-- End of Notif -->


            <div class="row clearfix">
                <!-- Task Info -->
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Additional Doc</h2>
                        </div>
                        <div class="body">

                        
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos" id="example">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>IC</th>
                                            <th>Phone</th>
                                            <th>Status</th>
                                            <th>Doc</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php $i = 1; ?>

                                        @foreach($approval as $data)
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{$data->name}}</td>
                                            <td>{{$data->ic}}</td>
                                            <td>{{$data->notelp}}</td> 
                                            <td>
                                                @include('shared.stage_new')
                                            </td>
                                            <td>
                                                @if(empty($data->additionaldoc->document)) 
                                                <a href="#" class="btn bg-red"><i class="material-icons disabled">do_not_disturb_alt</i> </a>
                                                @else
                                                <a href="{{asset('/additional/'.$data->ic.'/'.$data->additionaldoc->document)}}" class="btn bg-light-blue" target='_blank'> <i class="material-icons">library_books</i></a>
                                                @endif
                                                <!-- <button class="btn btn-success" aria-controls="collapse-{{$data->id}}" data-target="#collapseDetailOne{{$data->id}}" data-toggle="collapse" style="cursor:pointer;">Detail</button> -->
                                            </td> 
                                            
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
            </div>


            <!-- ///////////////////  Data Target  ///////////////// -->
            @foreach($approval as $datas)
            <div id="collapseDetailOne{{$datas->id}}" class="collapse" aria-expanded="false" data-collapse-group="collapse-group" aria-labelledby="headingFive" data-parent="#accordionExample">
                <div class="row clearfix">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Detail</h2>
                        </div>
                        <div class="body">
                            
                            <div class="row">
                                                
                                <div class="col-md-6"> <!-- kolom 1 -->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>Name  </b>    
                                        </div>
                                        <div class="col-md-6">
                                           <input  class="form-control" type="text" name="" value="{{$datas->name}} " readonly> 
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>IC  </b>    
                                        </div>
                                        <div class="col-md-6">
                                           <input  class="form-control" type="text" name="" value="{{$datas->ic}} " readonly> 
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>No Telf : </b>    
                                        </div>
                                        <div class="col-md-6">
                                            <input  class="form-control" type="text" name="" value="{{$datas->notelp}} " readonly>
                                           
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>Email : </b>    
                                        </div>
                                        <div class="col-md-6">
                                            <input  class="form-control" type="text" name="" value="{{$datas->email}} " readonly>
                                           
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>Employer : </b>    
                                        </div>
                                        <div class="col-md-6">
                                            <input  class="form-control" type="text" name="" value="{{$datas->majikan->Emp_Desc}} " readonly>
                                           
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>By : </b>    
                                        </div>
                                        <div class="col-md-6">
                                           <p style="color: red">{{$datas->user->name}} </p>  
                                        </div>
                                    </div>
                                    
                                </div> <!-- End of kolom 1 -->
                                
                                <!-- kolom 2 -->
                                <div class="col-md-6">
                                    
                                    <?php $lm = DB::table('loanammount')->where('id_praapplication', $datas->id_cus)->latest('created_at')->limit('1')->first(); ?>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>Basic Salary (RM) : </b>    
                                        </div>
                                        <div class="col-md-6">
                                           <input  class="form-control" type="text" name="" value="{{$datas->gaji_asas}} " readonly> 
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>Allowance (RM) : </b>    
                                        </div>
                                        <div class="col-md-6">
                                            <input  class="form-control" type="text" name="" value="{{$datas->elaun}}" readonly>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>Total Deduction (RM) : </b>    
                                        </div>
                                        <div class="col-md-6">
                                            <input  class="form-control" type="text" name="" value="{{$datas->pot_bul}}" readonly>
                                        </div>
                                    </div>
                                    
                                    <!--<div class="row">
                                        <div class="col-md-6">
                                            <b>Loan Amount (RM) : </b>    
                                        </div>
                                        <div class="col-md-6">
                                           <input  class="form-control" type="text" name="" value="{{$lm->loanammount}}" readonly>
                                        </div>
                                    </div>-->
                                </div> <!-- End of kolom 2 -->
                            </div>


                            <div class="row">
                                <?php
                                   $datax1 = DB::table('doc_cust')->join('praapplication', 'doc_cust.cus_id', '=', 'praapplication.id_cus')->where('cus_id', $datas->id_cus)->where('doc_cust.type','1')->latest('doc_cust.created_at')->select('doc_cust.id as iddoc', 'doc_cust.doc_pdf as doc_pdf')->limit('1')->first();

                                   $datax2 = DB::table('doc_cust')->join('praapplication', 'doc_cust.cus_id', '=', 'praapplication.id_cus')->where('cus_id', $datas->id_cus)->where('doc_cust.type','2')->latest('doc_cust.created_at')->select('doc_cust.id as iddoc', 'doc_cust.doc_pdf as doc_pdf')->limit('1')->first();

                                   $datax3 = DB::table('doc_cust')->join('praapplication', 'doc_cust.cus_id', '=', 'praapplication.id_cus')->where('cus_id', $datas->id_cus)->where('doc_cust.type','4')->latest('doc_cust.created_at')->select('doc_cust.id as iddoc', 'doc_cust.doc_pdf as doc_pdf', 'doc_cust.verification as verification')->limit('1')->first();

                                ?>

                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <table class="table table-responsive">
                                        <thead class="bg-red">
                                            <tr>
                                                <th width="5%">No</th>
                                                <th width="30%">Document</th>
                                                <th width="5%">File</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php $y = 1; ?>

                                            <tr>  
                                                <td>{{$y++}}</td>
                                                <td>IC</td>
                                                <td>
                                                    @if(empty($datax1->doc_pdf))
                                                    <a href="#" class="btn bg-red"><i class="material-icons disabled">do_not_disturb_alt</i> </a>
                                                    @else
                                                    <a href="{{asset('/documents/user_doc/'.$datas->ic.'/'.$datax1->doc_pdf)}}" target='_blank' class="btn bg-light-blue"><i class="material-icons">library_books</i></a>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>  
                                                <td>{{$y++}}</td>
                                                <td>Consent Letter</td>
                                                <td>@if(empty($datax2->doc_pdf))
                                                    <a href="#" class="btn bg-red"><i class="material-icons disabled">do_not_disturb_alt</i> </a>
                                                    @else
                                                    <a href="{{asset('/documents/user_doc/'.$datas->ic.'/'.$datax2->doc_pdf)}}" target='_blank' class="btn bg-light-blue"><i class="material-icons">library_books</i></a>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>  
                                                <td>{{$y++}}</td>
                                                <td>Spekar</td>
                                                <td>@if(empty($datax3->doc_pdf))
                                                    <a href="#" class="btn bg-red"><i class="material-icons disabled">do_not_disturb_alt</i> </a>
                                                    @else
                                                    <a href="{{asset('/documents/user_doc/'.$datas->ic.'/'.$datax3->doc_pdf)}}" target='_blank' class="btn bg-light-blue"><i class="material-icons">library_books</i></a>
                                                    @endif
                                                </td>
                                            </tr>
                                            
                                        </tbody>

                                    </table>
                                </div>
                                <div class="col-md-2"></div>

                            </div>

                            <!-- <div class="row">
                                <div class="col-md-4 col-md-offset-8">
                                    <a href="{{url('/approval-approve/'.$datas->id_cus)}}" class="btn btn-sm btn-success" style="margin-left: 20px"><i class="material-icons">done_all</i> Approval</a>
                                
                                    <a href="{{url('/approval-reject/'.$datas->id_cus)}}" class="btn btn-sm btn-warning" style="margin-left: 10px"><i class="material-icons">not_interested</i> Reject</a>

                                    <button class="btn btn-sm btn-primary" type="button" data-toggle="modal" data-target="#defaultModal" style="margin-left: 10px"><i class="material-icons">add</i> Additional Doc</button>
                                </div>
                            </div> -->


                        </div>

                    </div>
                </div>
            </div>

            @endforeach
            <!-- ///////////////////  End Data Target  //////////////// -->





        </div>
    </section>


    <!-- ================= Data Target hidden ======= -->
    <script src="https://code.jquery.com/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
     

    <script type="text/javascript">
        $("[data-collapse-group]").on('show.bs.collapse', function () {
              var $this = $(this);
              var thisCollapseAttr = $this.attr('data-collapse-group');
              $("[data-collapse-group='" + thisCollapseAttr + "']").not($this).collapse('hide');
            });
    </script>

    <!-- ================= End Data Target hidden ======= -->


    <script>
        $(document).ready(function() {
        $('#example').DataTable();
        });
    </script>

@endsection
