@extends('vadmin.tampilan')

@section('content')


    <section class="content">
        <div class="container-fluid">

            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">assignment</i> Monthly Reporting</a></li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->



            <div class="row clearfix">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Report</h2>
                        </div>

                        <div class="body">
                            <div class="table-responsive">

                                {!! Form::open(['url' => 'report_monthly','class' => 'smart-form client-form', 'id' =>'smart-form-register3' ]) !!}


                                <div class="body">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                            
                                                <h2 class="card-inside-title">Status</h2>
                                                <select name="status" class="form-control" id="one">

                                                    @if($status == 'W')

                                                        <option selected value="W">All</option>
                                                        <option value="W11">Approved </option>
                                                        <option value="W12">Rejected </option>

                                                    @elseif($status=='W11')

                                                        <option  value="W">All</option>
                                                        <option selected value="W11">Approved </option>
                                                        <option value="W12">Rejected </option>
                                                    

                                                    @elseif($status=='W12')

                                                        <option  value="W">All</option>
                                                        <option  value="W11">Approved </option>
                                                        <option selected value="W12">Rejected </option>

                                                    @endif

                                                </select>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <h2 class="card-inside-title">From Date</h2>
                                                <input type="text" class="form-control" placeholder="From Date" id="datepicker" value="{{$tanggal1}}" name="tanggal1" required/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <h2 class="card-inside-title">To Date</h2>
                                                <input type="text" class="form-control" placeholder="From Date" id="datepicker2"  value="{{$tanggal2}}" name="tanggal2" required/>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <button type="submit" name="submit" class="btn btn-lg bg-green">
                                                <i class="material-icons">visibility</i> &nbsp; Generate &nbsp;
                                            </button>
                                            <input type="hidden" name="_token" value="{{csrf_token()}}">


                                            {!! Form::close() !!} 

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>




            <div class="row clearfix">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Report {{$tanggal1}} - {{$tanggal2}}</h2>
                        </div>

                        <div class="body">
                            <div class="row">
                                <div class="col-md-3 col-md-offset-9">
                                    <h4>Total Loan Approved {{$sum_loan_approved}}</h4>
                                    <h4>Total Loan Ammount {{$sum_loan_amount}}</h4>

                                </div>
                            </div>
                           <button id="exportButton" class="btn btn-lg bg-green clearfix" style="margin-bottom: 10px !important"> Export to Excel</button>

                            <div class="table-responsive">
                                <table class="" id="example">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th width="10%">Name</th>
                                            <th>IC</th>
                                            <th>Phone</th>
                                            <th width="15%">Email</th>
                                            <th width="20%">Employer</th>
                                            <th>BasicSalary</th>
                                            <th>LoanAmount</th>
                                            <th>LoanApproved</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php $i=1; ?>

                                        @foreach($pra as $pra)

                                        <tr>

                                            <td>{{$i}}</td>
                                            <td>{{$pra->name}}</td>
                                            <td>{{$pra->ic}}</td>
                                            <td>{{$pra->notelp}}</td>
                                            <td>{{$pra->email}}</td>
                                            <td>{{$pra->majikan->Emp_Desc}}</td>
                                            <td>{{$pra->gaji_asas}}</td>
                                            <td>{{$pra->jml_pem}}</td>
                                            <td>{{$pra->loan_approve}}</td>
                                            <td>{{$pra->stages->desc}}</td>

                                        </tr>

                                        <?php $i++; ?>

                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

        </div>

    </section>




    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>



    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="http://www.shieldui.com/shared/components/latest/css/light/all.min.css" />
    <script type="text/javascript" src="http://www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>
    <script type="text/javascript" src="http://www.shieldui.com/shared/components/latest/js/jszip.min.js"></script>

    
    <script>
        $(document).ready(function() {
        $('#example').DataTable();
        });

    </script>    

    
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script>
    $(function() {
       $( "#datepicker" ).datepicker();
     });
    $(function() {
       $( "#datepicker2" ).datepicker();
     });
    </script>



    <script type="text/javascript">
        jQuery(function ($) {

            $("#exportButton").click(function () {

                // parse the HTML table element having an id=exportTable

                var dataSource = shield.DataSource.create({

                    data: "#example",

                    schema: {

                        type: "table",
                        fields: {

                           No: { type: Number },

                            Name: { type: String },

                            IC: { type: String },

                            Phone: { type: String },

                            Email: { type: String },

                            Employer: { type: String },

                            BasicSalary: { type: String },

                            LoanAmount: { type: String },

                            LoanApproved: { type: String },   

                        }

                    }

                });



                // when parsing is done, export the data to Excel

                dataSource.read().then(function (data) {

                    new shield.exp.OOXMLWorkbook({

                        author: "PrepBootstrap",

                        worksheets: [

                            {

                                name: "PrepBootstrap Table",

                                rows: [

                                    {

                                        cells: [

                                            {

                                                style: {

                                                    bold: true

                                                },

                                                type: Number,

                                                value: "No"

                                            },

                                            {

                                                style: {

                                                    bold: true

                                                },

                                                type: String,

                                                value: "Name"

                                            },

                                            {

                                                style: {

                                                    bold: true

                                                },

                                                type: String,

                                                value: "IC"

                                            },

                                            {

                                                style: {

                                                    bold: true

                                                },

                                                type: String,

                                                value: "Phone"

                                            },

                                            {

                                                style: {

                                                    bold: true

                                                },

                                                type: String,

                                                value: "Email"

                                            },

                                            {

                                                style: {

                                                    bold: true

                                                },

                                                type: String,

                                                value: "Employer"

                                            },

                                            {

                                                style: {

                                                    bold: true

                                                },

                                                type: String,

                                                value: "BasicSalary"

                                            },

                                            {

                                                style: {

                                                    bold: true

                                                },

                                                type: String,

                                                value: "LoanAmount"

                                            },

                                            {

                                                style: {

                                                    bold: true

                                                },

                                                type: String,

                                                value: "LoanApproved"

                                            },

                                        ]

                                    }

                                ].concat($.map(data, function(item) {

                                    return {
                                        cells: [
                                            { type: Number, value: item.No },
                                            { type: String, value: item.Name },
                                            { type: Number, value: item.IC },
                                            { type: String, value: item.Phone },
                                            { type: String, value: item.Email },
                                            { type: String, value: item.Employer },
                                            { type: String, value: item.BasicSalary },
                                            { type: String, value: item.LoanAmount },
                                            { type: String, value: item.LoanApproved}

                                        ]
                                    };

                                }))

                            }
                        ]

                    }).saveAs({

                        fileName: "Monthly Report"

                    });

                });

            });

        });

    </script>                             

    <style>
        #exportButton {

            border-radius: 0;
        }

    </style>



@endsection