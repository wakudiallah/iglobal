@extends('vadmin.tampilan')

@section('content')

	<section class="content">
        <div class="container-fluid">

            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">assignment</i> Reporting</a></li>
                        <li class="active"><i class="material-icons">playlist_add_check</i> Report</li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

            <div class="row clearfix">
			        <div class="card">
			            <div class="header bg-red">
			                <h2>Report</h2>
			            </div>
			            <div class="body">
			                <div class="table-responsive">
			                    <table class="table table-hover dashboard-task-infos" id="example">
			                        <thead>
			                            <tr>
			                                <th width="10%">#</th>
			                                <th width="30%">Name</th>
			                                <th width="20%">IC</th>
			                                <th width="20%">Status</th>
			                                <th width="20%">Date Received</th>
			                            </tr>
			                        </thead>
			                        <tbody>
			                            <?php $i = 1; ?>

			                            @foreach($report as $data)
			                            <tr>
			                                <td>{{$i++}}</td>
			                                <td>{{$data->name}}</td>
			                                <td>{{$data->ic}}</td>
			                                <td>@include('shared.stage_new')</td>
			                                <td>{{$data->created_at->toFormattedDateString() }}</td>
			                                 
			                            </tr>
			                            @endforeach
			                            
			                        </tbody>
			                    </table>
			                </div>
			            </div>
			        </div>
			    
			    <!-- #END# Task Info -->
			</div>
		    
        </div>
    </section>

    

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>

	
	<script type="text/javascript">
	var table = $('#example').DataTable({
	                lengthChange: false,
	                buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
	            });
	table.buttons().container().appendTo( '#example_wrapper .col-sm-6:eq(0)' );
	</script>


@endsection