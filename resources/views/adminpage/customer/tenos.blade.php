@extends('vadmin.tampilan')

@section('content')

	<section class="content">
        <div class="container-fluid">
            
            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">create</i> Financing Eligibility</a></li>
                        
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->


            @include('shared.notif')
            

			<?php
                $icnumber = $pra->icnumber;
                $tanggal  = substr($icnumber,4, 2);
                $bulan    = substr($icnumber,2, 2);
                $tahun    = substr($icnumber,0, 2); 

                if($tahun > 30) {
                    $tahun2 = "19".$tahun;
                }
                else {
                     $tahun2 = "20".$tahun;
                }
               
                $lahir = $tahun2.'-'.$bulan.'-'.$tanggal;
                $lahir =  date('Y-m-d', strtotime($lahir. ' - 16 month'));
                $oDateNow = new DateTime();
                $oDateBirth = new DateTime($lahir);
                $oDateIntervall = $oDateNow->diff($oDateBirth);

                $umur =  $oDateIntervall->y;
                $durasix = 60 - $oDateIntervall->y;
                if( $durasix  > 10){ $durasi = 10 ;} 
                else { $durasi = $durasix ;}
            ?>

            <!--- ============== Content ============ --> 
        

        <div class="row clearfix"> 
        	<div class="card">
        		<div class="header bg-red">
        			<h2>Financing Eligibility</h2>
        		</div>
        		<div class="body">
			            <div class="row">
							<!--/save_tenure-->
			            	{!! Form::open(['url' => ['/tenos/custtenus/store'], 'class' => "probootstrap-form border border-danger", 'method' => 'post', 'id' => 'form-validate-pra']) !!}

                        	{{ csrf_field() }}
                        	
			            	<div class="col-md-3"></div>

			            	<div class="col-md-6">
			            		<div class="col-md-6">
			                    	<label><b>Name</b></label>	
			                    </div>
			                    <div class="col-md-6">
			                    	<b><input name="" id="" type="text"  value="{{$pra->name}}" class="form-control" readonly></b>
			                    </div>
			                    <div class="col-md-6">
			                    	<label><b>Package</b></label>	
			                    </div>
			                    <div class="col-md-6">
			                        <b><input type="text" id="Package2" value="{{$pra->loanpkg->Ln_Desc}}"  name="Package2" placeholder="Package" disabled="disabled" class="form-control"></b>

			                        <input name="Package" id="Package" type="hidden"  value="{{$pra->loanpkg_code}}" >
			                        <input name="id_praapplication" id="id_praapplication" type="hidden"  value="{{$pra->id}}" >
			                        <input name="id_cus" id="id_cus" type="hidden"  value="{{$pra->id_cus}}" >
			                        <input name="employment_code" id="employment_code" type="hidden"  value="{{$pra->employment_code}}" >
			                           <input name="Employment" id="Employment" type="hidden"  value="{{$pra->employment_code}}" >
			                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
			                        
			                    </div>
			                    
			                    <div class="col-md-6">                    
			                    	<label> <b> <font color="red" size="3.0" >Max Financing Amount  <!--KELAYAKAN  PEMBIAYAAN  MAKSIMA -->  </font>  </b> </label>
			                	</div>

			                	<div class="col-md-6">
				                            
				                            @foreach($loan as $loan)
				                                <?php
				                                $salary_dsr = ($zbasicsalary * ($loan->dsr / 100)) - $zdeduction;
				                                $ndi = ($zbasicsalary - $zdeduction) -  1300;
				                                $max  =  $salary_dsr * 12 * 10 ;
				                                                               
				                                function pembulatan($uang) {
				                                    $puluhan = substr($uang, - 3);
				                                        if($puluhan<500) {
				                                                $akhir = $uang - $puluhan; 
				                                            }

				                                        else{
				                                                $akhir = $uang - $puluhan;
				                                            }

				                                        return $akhir;
				                                            }

				                                if(!empty($loan->max_byammount))  {
				                                  
				                                    $ansuran = intval($salary_dsr)-1;
				                                      if($pra->loanpkg_code =="1") {  //aku edit
				                                          $bunga = 3.8/100;
				                                      }
				                                      elseif($pra->loanpkg_code == "2") {
				                                          $bunga = 4.9/100;
				                                      }

				                                      else {
				                                          $bunga = 5.92/100;
				                                      }
				                                   
				                                      $pinjaman = 0;

				                                      for ($i = 0; $i <= $loan->max_byammount; $i++) {
				                                          $bungapinjaman = $i  * $bunga * $durasi;   //
				                                          $totalpinjaman = $i + $bungapinjaman ;
				                                          $durasitahun = $durasi * 12;
				                                          $ansuran2 = intval($totalpinjaman / ($durasi * 12))  ;
				                                          //echo $ansuran2."<br>";
				                                          if ($ansuran2 < $ndi)
				                                          {
				                                              $pinjaman = $i;
				                                          }
				                                      }   

				                                      if($pinjaman > 1) {

				                                          $bulat = pembulatan($pinjaman);
				                                          $loanx =  number_format($bulat, 0 , ',' , ',' ) ; 
				                                          $loanz = $bulat;
				                                      }
				                                      else {
				                                          $loanx =  number_format($loan->max_byammount, 0 , ',' , ',' ) ; 
				                                          $loanz = $loan->max_byammount;
				                                      }
				                                }
				                                else { 

				                                    $bulat = pembulatan($loan->max_bysalary * $total_salary);
				                                    $loanx =  number_format($bulat, 0 , ',' , ',' ) ; 
				                                    $loanz = $bulat;
				                                    if ($loanz > 199000) {

				                                          $loanz  = 250000;
				                                          $loanx =  number_format($loanz, 0 , ',' , ',' ) ; 
				                                    }
				                                }

				                                ?>
				                            @endforeach

				                        <b><input readonly type="text" id="MaxLoan" class="form-control" value=" RM {{$loanx}}" name="MaxLoan" placeholder="Max Loan Eligibility (RM)" class="merah" requierd> </b>
				                        <input readonly type="hidden" id="maxloanz" value="{{$loanz}}" name="maxloanz" placeholder="Max Loan Eligibility (RM)" requierd>            
			                    </div> 
			                </div>
			                <div class="col-md-3"></div> <!-- margin -->               
			            </div> <!-- end row -->
			            
			            <div class="row" style="margin-bottom: 40px !important">
			            	<div class="col-md-3"></div>
			            	<div class="col-md-6">
			            		<div class="col-md-6">
			            			<label><b>  <!-- Jumlah Pembiayaan --> Loan Amount (RM) </b> </label>	
			            		</div>
			                    <div class="col-md-6">
			                    	<label class="input state-<?php if( $pra->jml_pem <= $loanz ) { print "success"; } else { print "error"; }?>">
			                        <i class="icon-append fa fa-credit-card"></i>
			                        <input type="text" name="jml_pem" class="form-control"  id="jml_pem" onkeypress="return isNumberKey(event)" value="{{ $pra->jml_pem }}"  placeholder="RM " onkeyup="this.value = minmax(this.value, 0, {{$loanz}})">
			                        <b class="tooltip tooltip-bottom-right">Jumlah Pembiayaan</b>
			                    	</label>	
			                    </div>
			                                        

			                    <div class="col-md-6">
			                    	<label> <b>  <!-- Jumlah Pendapatan --> Total income </b> </label>	
			                    </div>
			                    
			                    <div class="col-md-6">
			                    	<input type="text" id="pendapatan" class="form-control" value="RM {{ number_format($total_salary, 0 , ',' , ',' )}}" name="pendapatan" placeholder="Loan Amount" disabled="disabled">
			                    	<input type="hidden" id="gaji_asas" class="form-control" value="{{$pra->gaji_asas}}" name="gaji_asas">
			                    	<input type="hidden" id="elaun" class="form-control" value="{{$pra->elaun}}" name="elaun">
			                    	<input type="hidden" id="pot_bul" class="form-control" value="{{$pra->pot_bul}}"" name="pot_bul">
		                         	<input type="hidden" name="_token" value="{{ csrf_token() }}">	
			                    </div>
			                    
			                    <div class="col-md-6">
			                    	<label> <b>  <!-- Ansuran Maksima --> Max Installment </b> </label>	
			                    </div>
			                    <div class="col-md-6">
			                    	<input type="text" id="ansuran_maksima" class="form-control" value="RM {{ number_format($ndi, 0 , ',' , ',' )  }}   / month" name="ansuran_maksima" placeholder="Ansuran Maksima" readonly>
			                        <b class="tooltip tooltip-bottom-right">Loan Amount</b>	
			                    </div>

			                    <!-- button -->
			                    <div class="col-md-8"></div>
	                            <div class="col-md-4">
	                            	<button class="btn btn-success" type="submit"> Recount </button>
	                            </div>
	                            <!-- end button -->

			                </div>
			                <div class="col-md-3"></div> <!-- margin -->
			                                          
			                       
			            </div>  
			            
			               {!! Form::close() !!}	


			              	
						{!! Form::open(['url' => ['/save_tenure'], 'class' => "probootstrap-form border border-danger", 'method' => 'post', 'id' => 'form-validate-pra']) !!}   

                        {{ csrf_field() }}
                         <input type='hidden' name='latitude' id='latitude' >
                            <input type='hidden' name='longitude' id='longitude'>
                            <input type='hidden' name='location' id='location' >
			            <input type="hidden" name="LoanAmount2" class="form-control"  id="LoanAmount2" onkeypress="return isNumberKey(event)" value="{{$pra->jml_pem }}"  >
			            <input type="hidden" name="maxloanz" class="form-control"  id="maxloanz" onkeypress="return isNumberKey(event)" value="{{$loanz }}"  >
			            <table class="table table-hover" style="margin-top: 30px !important">
			                <thead>
			                    <tr>
			                        <th width="20" valign="middle"> <b>  Duration   </b> </th>
			                        <th width="40"> <b> Financing Amount  </b> </th>
			                        <th width="20"><b>  Monthly installment </b> </th>
			                        <th width="20"> <b>  Profit Rate </b> </th>
			                        <th width="10" align="center"> <b>  Select </b> </th>
			                    </tr>
			                </thead>
			                <tbody>
			                     <?php if( $pra->jml_pem <= $loanz ) { $ndi_limit=$loan->ndi_limit;?>
			                            @foreach($tenure as $tenure)
			                                <?php 
			                                       $bunga2 =  $pra->jml_pem * $tenure->rate /100   ;
			                                       $bunga = $bunga2 * $tenure->years;
			                                       $total = $pra->jml_pem + $bunga ;
			                                       $bulan = $tenure->years * 12 ;
			                                       $installment =  $total / $bulan ;
			                                       $ndi_state = ($total_salary - $zdeduction) - $installment; 
			                                       
			                                       if($installment  <= $salary_dsr && $ndi_state>=$ndi_limit) {     
			                                    ?>
			                                <tr>
			                                    <td>{{$tenure->years}} years</td>
			                                    <td> RM {{ number_format( $pra->jml_pem, 0 , ',' , ',' )  }}  </td>
			                                    
			                                    <td> RM {{ number_format($installment, 0 , ',' , ',' )  }} /month

			                                   

			                                    </td>
			                                  
			                                    <td>{{$tenure->rate}} %</td>
			                                    <td>  
			                                    	<input type="hidden" name="cus_id" value="{{$pra->id_cus}}">
			                                    	<input name="tenure" type="radio" id="radio_1_{{$tenure->id}}" value="{{$tenure->id}}"  required />


                                					<label for="radio_1_{{$tenure->id}}"></label>
                                					 <input type="hidden"  name="installment_value" value="{{floor($installment) }}">
                                					 
			                                    </td>
			                                </tr>
			                                                
			                                 <?php }  ?>
			                            @endforeach
			                        <?php } ?>
			                </tbody>
			                
			            </table>

			            <div class="row">
			            	<div class="col-md-6"></div>
			                <div class="col-md-6">
			                	<div class="form-group ">
			                        <h5><b>Remark :</b></h5>
  									{{Form::select('remark', ['W3' => 'Pass', 'W5' => 'Reject'], 'W3',  ['class' => 'form-control', 'id' => 'one'])}}
			                    </div>
			                </div>

			                <div class="col-md-6"></div>
			                <div class="col-md-6">
			                    <div class="form-line resources" style="display: none; margin-top: 20px !important" id="two">
			                        <h5><b>Note :</b></h5>
			                        <textarea id="txtArea" name="notep6" class="form-control" rows="5" cols="3"></textarea>
			                    </div>
			                </div>
		                </div>

			            <div class="row">
		                	<div class="col-md-10"></div>
		                	<div class="col-md-2">
		                		<input type="submit" value="Save" class="btn btn-lg btn-success btn-block" id="submit_tenure" style="cursor:pointer;">
                        		{{ csrf_field() }}
		                	</div>
		                </div>
        		</div>

        				

		         {!! Form::close() !!}   
        	</div>
	    </div>

	    
    </div>

       

    </section>

     <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
 <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">

<script>
var apiGeolocationSuccess = function(position) {
  showLocation(position);
};
var tryAPIGeolocation = function() {
  jQuery.post( "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyATzsafy5WHjV4871j-qZmJYfZXXlrxSv0", function(success) {
    apiGeolocationSuccess({coords: {latitude: success.location.lat, longitude: success.location.lng}});
  })
  .fail(function(err) {
    // alert("API Geolocation error! \n\n"+err);
    window.location.href = "/geolocation/error/"+err;
  });
};

var browserGeolocationSuccess = function(position) {
  showLocation(position);
};

var browserGeolocationFail = function(error) {
  switch (error.code) {
    case error.TIMEOUT:
      alert("Browser geolocation error !\n\nTimeout.");
      break;
    case error.PERMISSION_DENIED:
      if(error.message.indexOf("Only secure origins are allowed") == 0) {
        tryAPIGeolocation();
      }
         window.location.href = "{{url('/')}}/geolocation/error/"+error.code;
      break;
    case error.POSITION_UNAVAILABLE:
      alert("Browser geolocation error !\n\nPosition unavailable.");
      break;
  }
};

var tryGeolocation = function() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(
      browserGeolocationSuccess,
      browserGeolocationFail,
      {maximumAge: 50000, timeout: 20000, enableHighAccuracy: true});
  }
};

tryGeolocation();

function showLocation(position) {
  var latitude = position.coords.latitude;
  var longitude = position.coords.longitude;
  var _token = $('#_token').val();
  $.ajax({
    type:'POST',
    url: "{{url('/')}}/getLocation",
    data: { latitude: latitude, _token : _token, longitude : longitude },
    success:function(data){
            if(data){
              $("#latitude").html("<input type='hidden' name='latitude' id='latitude' value='"+data.latitude+"'>");
              $("#longitude").html("<input type='hidden' name='longitude' id='longitude' value='"+data.longitude+"'>");
               $("#location").html("<input type='hidden' name='location' id='location' value='"+data.location+"'>");
                 

            }else{
                $("#location").html('Not Available');
            }
    }
  });
}
</script> 

     <script>
        $(document).ready(function() {
        $('#example').DataTable();
        });
          
    </script>


    <script type="text/javascript">
        var Privileges = jQuery('#one');
        var select = this.value;
        Privileges.change(function () {
            if ($(this).val() == 'W5') {
                $('.resources').show();
            }
            else $('.resources').hide();
        });
    </script>

    
    <!-- <script>
			$(function() {
		  $('.resources').hide();
		  $('input.prettycheckbox').prettyCheckable({});

		  $('form').on('submit', function(e) { // use #formid if you have it
		      var ok = false;
		      $('input.prettycheckbox').each(function(){
		        if ($(this).is(':checked') && $(this).is('input[value="0"]')) {
		            ok=true; // or other reason
		            $(this).attr("disabled", true);;
		            $('.result').show();
		            $('.advise').hide();
		            $(this).parent().parent().addClass('incorrect');
		          } // seems to have been missing  
		      });
		      if (!ok) { // if not ok to submit, THEN preventDefault
		         e.preventDefault();
		      } 
		    });
		});
	</script> -->
    


@endsection