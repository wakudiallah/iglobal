<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Welcome</title>
    <!-- Favicon-->

        <link rel="icon" href="{{ asset('admin/favicon/favicon.ico') }}" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{asset('admin/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{ asset('admin/plugins/node-waves/waves.css') }}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ asset('admin/plugins/animate-css/animate.css') }}" rel="stylesheet" />

    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{ asset('admin/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />

    <!-- Wait Me Css -->
    <link href="{{ asset('admin/plugins/waitme/waitMe.css') }}" rel="stylesheet" />

    <!-- Bootstrap Select Css -->
    <link href="{{ asset('admin/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{ asset('admin/css/style.css') }}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{ asset('admin/css/themes/all-themes.css') }}" rel="stylesheet" />

    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{asset('admin/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet" />

    <link href="{{ asset('//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css') }}" rel="stylesheet" />
  

</head>

<body class="theme-red">

    <!-- Page Loader -->
    <!-- <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand img-responsive" href="index.html"><img src="{{asset('assets/images/logo6.png')}}"></a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Call Search -->
                    <li><a href="javascript:void(0);" class="js-search" data-close="true"><i class="material-icons">search</i></a></li>
                    <!-- #END# Call Search -->
                    <!-- Notifications -->
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">notifications</i>
                            <span class="label-count"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">NOTIFICATIONS</li>
                            <li class="body">
                                <ul class="menu" id="markasread" onclick="markNotificationAsRead()">
                                    
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-blue-grey">
                                                <i class="material-icons">comment</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4></h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 4 hours ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>    
                                    
                                </ul>
                            </li>
                            <li class="footer">
                                <a href="javascript:void(0);">View All Notifications</a>
                            </li>
                        </ul>
                    </li>
                    <!-- #END# Notifications -->
                    <!-- Tasks -->
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">flag</i>
                            <span class="label-count">9</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">TASKS</li>
                            <li class="body">
                                <ul class="menu tasks">
                                    <li>
                                        <a href="javascript:void(0);">
                                            <h4>
                                                Footer display issue
                                                <small>32%</small>
                                            </h4>
                                            <div class="progress">
                                                <div class="progress-bar bg-pink" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 32%">
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <h4>
                                                Make new buttons
                                                <small>45%</small>
                                            </h4>
                                            <div class="progress">
                                                <div class="progress-bar bg-cyan" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <h4>
                                                Create new dashboard
                                                <small>54%</small>
                                            </h4>
                                            <div class="progress">
                                                <div class="progress-bar bg-teal" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 54%">
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <h4>
                                                Solve transition issue
                                                <small>65%</small>
                                            </h4>
                                            <div class="progress">
                                                <div class="progress-bar bg-orange" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 65%">
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <h4>
                                                Answer GitHub questions
                                                <small>92%</small>
                                            </h4>
                                            <div class="progress">
                                                <div class="progress-bar bg-purple" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 92%">
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="footer">
                                <a href="javascript:void(0);">View All Tasks</a>
                            </li>
                        </ul>
                    </li>
                    <!-- #END# Tasks -->
                    <li class="pull-right"><a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="material-icons">more_vert</i></a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->

    @include('vadmin.leftmenu')

    <section class="content">
        <div class="container-fluid">

            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">person_pin</i>Add New Customer</a></li>
                        <li class="active"><i class="material-icons">create</i> Add</li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

            <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Add Doc New Customer </h2>
                        </div>
                        <div class="body">
                      		<div class="row">
                   		  <div class="col-md-2"></div>
                   		  <div class="col-md-8">

	                       

                          <form>
	                      

	                        <div class="form-group">
	                          <div class="row mb-3">
	                            <div class="col-md">
	                              <h5 style="color:red">*Documents in PDF/JPG/JPEG/PNG</h5>
	                            </div>
	                          </div>
	                          <div class="row mb-5">
	                            <div class="col-md">
	                              
	                              <div class="form-group" >
	                                <label for="probootstrap-date-arrival">Copy of IC</label>
	                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
	                                <input type="hidden" name="id_cus" value="{{ $reg->id_cus}}">
                                    <input type="hidden" name="ic_user" value="{{ $reg->ic}}">
	                                <input type="hidden" name="type1" value="1" id="type1">

	                                <div class="probootstrap-date-wrap">
	                                  <span class="icon ion-document"></span>
	                                  <input id="fileupload1" class="form-control" required="" @if(empty($document1->name))  @endif   type="file" name="file1" >
	                                </div>

	                                <input type="hidden" name="document1"   id="documentx1"  value="IC">&nbsp; <span id="document1"> </span> 
	                                  @if(!empty($document1->name))
	                                  <span id="document1a"><a target='_blank' href="{{url('/')}}/documents/user_doc/{{$reg->ic}}/{{$document1->doc_pdf}}"> {{$document1->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
	                                  @else
	                                  <!--<a class='bnt btn-danger'>No Attachment Available</a>-->
	                                  @endif
	                                  
	                              </div>
	                            </div>
	                            <div class="col-md">
	                              <div class="form-group">
	                                <label for="probootstrap-date-arrival">Copy of Consent Letter</label>
	                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
	                                <input type="hidden" name="id_cus" value="{{ $reg->id_cus}}">
                                    <input type="hidden" name="ic_user" value="{{ $reg->ic}}">
	                                <input type="hidden" name="type2" value="2" id="type2">

	                                <div class="probootstrap-date-wrap">
	                                  <span class="icon ion-document"></span> 
	                                  <input id="fileupload2" required="" @if(empty($document2->name))  @endif    class="form-control" type="file" name="file2" >
	                                </div>

	                                <input type="hidden" name="document2"   id="documentx2"  value="Consent Letter">&nbsp; <span id="document2"> </span> 
	                                  @if(!empty($document2->name))
	                                  <span id="document2a"><a target='_blank' href="{{url('/')}}/documents/user_doc/{{$reg->ic}}/{{$document2->doc_pdf}}"> {{$document2->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
	                                  @else
	                                  <!--<a class='bnt btn-danger'>No Attachment Available</a>-->
	                                  @endif

	                              </div>
	                            </div>
	                          </div>


	                          
	                          <div class="row mb-5">
	                            <div class="col-md-10"> </div>
	                            <div class="col-md-2">
	                            
	                              <!-- Button trigger modal -->
                                    <a href="{{url('adminnew')}}" class="btn btn-lg btn-success btn-block">Save</a>
                                    
	                            </div>
	                          </div>
	               
	                        </div>
	                      </form>

	                    </div>
                        <div class="col-md-2"></div>
                    </div>
                    </div>
                </div>
            </div>



            
        </div>
    </section>

    @include('vadmin.js')

    
    <!-- upload file -->   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<!-- upload file -->
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="{{url('/')}}/assets/js/file/vendor/jquery.ui.widget.js"></script>

<script src="{{url('/')}}/assets/js/file/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="{{url('/')}}/assets/js/file/jquery.fileupload.js"></script>
<?php for ($x = 1; $x <= 12; $x++) {  ?>

<script type="text/javascript">
/*jslint unparam: true */
/*global window, $ */
$(function () {
    'use strict';

    // Change this to the location of your server-side upload handler:
    var url = window.location.hostname === 'blueimp.github.io' ?
                '//jquery-file-upload.appspot.com/' : '{{url('/')}}/assessment/kira/uploads/{{$x}}' 
    $('#fileupload{{$x}}').fileupload({
        url: url,
        dataType: 'json',
        success: function ( data) {
             var text = $('#documentx{{$x}}').val();
            $("#document{{$x}}").html("<a target='_blank' href='"+"{{url('/')}}/documents/user_doc/{{$reg->ic}}/"+data.file+"'>"+text+"</a><i class='glyphicon glyphicon-ok txt-color-green'></i>");
             $("#document{{$x}}a").hide();
             $("#a{{$x}}").val(data.file);
            $("#a{{$x}}").val(data.file);
      $("#a{{$x}}").val(data.file);

             document.getElementById("fileupload{{$x}}").required = false;
    
        }
       
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

        
});
</script>
<?php } ?>

</body>
</html>
    