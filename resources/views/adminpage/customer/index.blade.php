@extends('vadmin.tampilan')

@section('content')
    <!--  ======================== End of section ======================= -->

    <section class="content">
        <div class="container-fluid">
            
            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">person_pin</i> Add New Customer</a></li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->
            
            <!-- Advanced Form Example With Validation -->
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
            @endif

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Add New Customer</h2>
                            
                        </div>
                        <div class="body">

                            <h2 class="card-inside-title">Customer Information</h2>
                            <div class="row clearfix">
                                <div class="col-sm-12">


                                    <fieldset>

                                    {!! Form::open(['url' => ['/processor1/addcus'], 'class' => "probootstrap-form border border-danger", 'id' => 'form-validate-pra', 'novalidate']) !!}

                                    {{ csrf_field() }}

                                    <span id="latitude"></span>
                                    <span id="longitude"></span>
                                    <span id="location"></span>

                                    <div class="form-group form-float">
                                        <label class="form-label">Full Name:</label>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="name" id="name" required @if (Session::has('name'))  value="{{ Session::get('name') }}" @endif>
                                        </div>   
                                    </div>
                                    <div class="form-group form-float">
                                        <label class="form-label">IC Number :</label>
                                        <div class="form-line">
                                            <input type="text"  class="form-control" name="ic" id="ic"  min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');" maxlength="12"  required @if (Session::has('ic'))  value="{{ Session::get('ic') }}" @endif>
                                        </div>   
                                    </div>
                                    <!-- <div class="form-group form-float">
                                        <div class="form-line">
                                            <label class="form-label">Old IC (If Any) :</label>
                                            <input type="text"  class="form-control" name="old_ic" id="ic_old" value=""  min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');"  value=${var} ">
                                        </div>   
                                    </div> -->
                                    <div class="form-group form-float">
                                        <label class="form-label">Phone Number :</label>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="notelp" id="notelp"  min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');"  maxlength="15" minlength="9" required @if (Session::has('notelp'))  value="{{ Session::get('notelp') }}" @endif>
                                        </div>    
                                    </div>
                                    


                                    <div class="form-group form-float">
                                        <label class="form-label">Email :</label>
                                        <div class="form-line">
                                            <input type="email" class="form-control" name="email" id="email" required @if(Session::has('email'))  value="{{ Session::get('email') }}" @endif>
                                        </div>   
                                    </div>

                                    <div class="form-group form-float">
                                        <label class="form-label">Employer :</label>
                                        
                                            <select class="form-control show-tick js-example-basic-single js-states" data-live-search="true" name="emp_code" id="one" required>
                                                <option value="" selected disabled hidden>Choose Employer</option>
                                                @foreach ($emp as $data)
                                                  <option value="{{$data->id}}">{{$data->Emp_Desc}}</option>
                                                @endforeach  
                                            </select>
                                    </div>

                                    <div class="form-group form-float resources" style="display: none" id="two">
                                        <input name="clerical" type="radio" id="radio_1" value="1" checked />
                                        <label for="radio_1">Clerical</label>
                                        <input name="clerical" type="radio" id="radio_2" value="0" />
                                        <label for="radio_2">Non Clerical</label>
                                    </div>

                                    <div class="form-group form-float other" style="display: none" >
                                        <div class="form-group form-float">
                                            <b>Other Employer :</b>
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="others_employer" id="others_employer" required @if (Session::has('others_employer'))  value="{{ Session::get('others_employer') }}" @endif>
                                            </div>   
                                        </div>
                                    </div>

                                    
                                    <div class="form-group form-float">
                                        <label class="form-label">Biro / Non :</label>
                                        <div class="">
                                            <input name="biro" type="radio" id="radio_3" value="1" checked />
                                            <label for="radio_3">Biro</label>
                                            <input name="biro" type="radio" id="radio_4" value="0" />
                                            <label for="radio_4">Non Biro</label>
                                        </div>
                                    </div>


                                     <h2 class="card-inside-title" style="margin-top: 70px !important">MO Remark</h2>
                                    
                                    <label class="form-label">MO :</label>
                                    <select class="form-control show-tick" data-live-search="true" name="custby" required>
                                        @foreach ($workgroup as $data)
                                          <option value="{{$data->model_id}}">{{$data->user->name}}</option>
                                        @endforeach
                                    </select>

                                        <!--  <h2 class="card-inside-title" style="margin-top: 70px !important">MO Remark</h2>
                                    
                                        <div class="col-sm-12">
                                            
                                                <label class="form-label">MO:</label>
                                                <select name="custby" class="ui search dropdown form-control" id="custbyt" required>
                                                  @foreach ($workgroup as $data)
                                                  <option value="{{$data->model_id}}">{{$data->user->name}}</option>
                                                  @endforeach
                                                </select>
                                            
                                        </div> -->
                                    
                            
                                    <!-- 
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <label class="form-label">Loan Amount (RM):</label>
                                            <input type="text" class="form-control" id="jml_pem" name="jml_pem" value="" min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');"  value=${var} ">
                                        </div>    
                                    </div> -->

                                </div>
                            </div>

                           
                               


                            <div class="row clearfix">
                                <div class="col-md-10"></div>
                                <div class="col-md-2">
                                    <input type="submit" value="Save" class="btn btn-lg btn-success btn-block" style="cursor:pointer;">
                                        {{ csrf_field() }}
                                </div>
                            </div>
                            
                            
                                    
                            {!! Form::close() !!}

                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Advanced Form Example With Validation -->

        </div>
    </section>

     @push('js')

    <script type="text/javascript">
        var Privileges = jQuery('#one');
        var select = this.value;
        Privileges.change(function () {
            if ($(this).val() == '1') { //1 others  
                $('.resources').hide();
                $('.other').show();
            }
            else if($(this).val() == '2'){ //2 Maybank
                $('.resources').show();
                $('.other').hide();
            }
            else{
                $('.other').hide();
                $('.resources').hide();
            } 

        });
    </script>


    <script type="text/javascript">
        $(document).ready(function() {
       $('.selectpicker').selectpicker(); $('#form-validate-pra').bootstrapValidator({
          
          fields: {
           
            name: {
              validators: {
                notEmpty: {
                  message: 'Sila masukkan nama penuh'
                }
              }
            },
            ic: {
              validators: {
                 stringLength: {
                  min: 12,
                  max: 12,
                  message: 'Sila masukkan 12 nombor IC'
                },
                notEmpty: {
                  message: 'Sila masukan IC nombor'
                }
              }
            },
            notelp: {
              validators: {
                stringLength: {
                  min: 9,
                  max: 15,
                  message: 'Sila masukkan sekurang-kurangnya 9 nombor dan tidak lebih dari 15'
                },
                notEmpty: {
                  message: 'Sila masukan nombor telefon'
                }
              }
            },
            employment_code: {
              validators: {
                notEmpty: {
                  message: 'Sila pilih jenis pekerjaan'
                }
              }
            },        
            emp_code: {
              validators: {
                notEmpty: {
                  message: 'Sila pilih majikan'
                }
              }
            },
            elaun: {
              validators: {
                notEmpty: {
                  message: 'Sila masukkan elaun'
                }
              }
            },
            pot_bul: {
              validators: {
                notEmpty: {
                  message: 'Sila masukkan potongan bulanan'
                }
              }
            },
            loanpkg_code: {
              validators: {
                notEmpty: {
                  message: 'Sila pilih pakej'
                }
              }
            },
            gaji_asas: {
              validators: {
                notEmpty: {
                  message: 'Sila masukkan gaji asas'
                }
              }
            },
             jml_pem: {
              validators: {
                notEmpty: {
                  message: 'Sila masukkan jumlah pembiayaan'
                }
              }
            },     
          }
        })

      });
    </script>

    @endpush

    <!-- 
    <script>
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
</script>



<script language="javascript">
function getkey(e)
{
if (window.event)
   return window.event.keyCode;
else if (e)
   return e.which;
else
   return null;
}
function goodchars(e, goods, field)
{
var key, keychar;
key = getkey(e);
if (key == null) return true;
 
keychar = String.fromCharCode(key);
keychar = keychar.toLowerCase();
goods = goods.toLowerCase();
 
// check goodkeys
if (goods.indexOf(keychar) != -1)
    return true;
// control keys
if ( key==null || key==0 || key==8 || key==9 || key==27 )
   return true;
    
if (key == 13) {
    var i;
    for (i = 0; i < field.form.elements.length; i++)
        if (field == field.form.elements[i])
            break;
    i = (i + 1) % field.form.elements.length;
    field.form.elements[i].focus();
    return false;
    };
// else return false
return false;
}
</script> -->


<!-- ========   Untuk Location   =========-->

<!-- 
<input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
<script>
var apiGeolocationSuccess = function(position) {
  showLocation(position);
};

var tryAPIGeolocation = function() {
  jQuery.post( "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyDCa1LUe1vOczX1hO_iGYgyo8p_jYuGOPU", function(success) {
    apiGeolocationSuccess({coords: {latitude: success.location.lat, longitude: success.location.lng}});
  })
  .fail(function(err) {
    
  });
};

var browserGeolocationSuccess = function(position) {
  showLocation(position);
};

var browserGeolocationFail = function(error) {
  switch (error.code) {
    case error.TIMEOUT:
      alert("Browser geolocation error !\n\nTimeout.");
      break;
    case error.PERMISSION_DENIED:
      if(error.message.indexOf("Only secure origins are allowed") == 0) {
        tryAPIGeolocation();
      }
      break;
    case error.POSITION_UNAVAILABLE:
      alert("Browser geolocation error !\n\nPosition unavailable.");
      break;
  }
};

var tryGeolocation = function() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(
      browserGeolocationSuccess,
      browserGeolocationFail,
      {maximumAge: 50000, timeout: 20000, enableHighAccuracy: true});
  }
};

tryGeolocation();

function showLocation(position) {
  var latitude = position.coords.latitude;
  var longitude = position.coords.longitude;
  var _token = $('#_token').val();
  $.ajax({
    type:'POST',
    url: "{{url('/')}}/getLocation",
    data: { latitude: latitude, _token : _token, longitude : longitude },
    success:function(data){
            if(data){
              $("#latitude").html("<input type='hidden' name='latitude' id='latitude' value='"+data.latitude+"'>");
              $("#longitude").html("<input type='hidden' name='longitude' id='longitude' value='"+data.longitude+"'>");
               $("#location").html("<input type='hidden' name='location' id='location' value='"+data.location+"'>");
                 

            }else{
                $("#location").html('Not Available');
            }
    }
  });
}
</script> -->

<!-- End Location -->
@endsection