@extends('vadmin.tampilan')


@section('content')
    <section class="content">
        <div class="container-fluid">
            
            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">person</i>Add New Customer</a></li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->
            
            <!-- Advanced Form Example With Validation -->

            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
            @endif

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Add New Customer</h2>
                            
                        </div>
                        <div class="body">

                            <h1 class="card-inside-title">Customer Information</h1>
                            <div class="row clearfix">
                                <div class="col-sm-12">


                                    <fieldset>

                                    {!! Form::open(['route' => ['cus_mo.store'], 'class' => "probootstrap-form border border-danger", 'id' => 'addForm']) !!}

                                    {{ csrf_field() }}

                                    <span id="latitude"></span>
                                    <span id="longitude"></span>
                                    <span id="location"></span>

                                    <div class="form-group form-float">
                                        <b>Full Name :</b>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="name" id="name" required @if (Session::has('name'))  value="{{ Session::get('name') }}" @endif>
                                        </div>   
                                    </div>

                                    <div class="form-group form-float">
                                        <b>IC Number :</b>
                                        <div class="form-line">
                                            <input type="text"  class="form-control" name="ic" id="ic"  min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');" maxlength="12"  required @if (Session::has('ic'))  value="{{ Session::get('ic') }}" @endif>
                                        </div>   
                                    </div>
                                    <!-- <div class="form-group form-float">
                                        <div class="form-line">
                                            <label class="form-label">Old IC (If Any) :</label>
                                            <input type="text"  class="form-control" name="old_ic" id="ic" value=""  min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');"  value=${var} ">
                                        </div>   
                                    </div> -->
                                    
                                    <div class="form-group form-float">
                                        <b>Phone Number :</b>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="notelp" id="notelp"  min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');" maxlength="15" minlength="9" required @if (Session::has('notelp'))  value="{{ Session::get('notelp') }}" @endif>
                                        </div>    
                                    </div>
                                    

                                    <div class="form-group form-float">
                                        <b>Email :</b>
                                        <div class="form-line">
                                            <input type="email" class="form-control" name="email" id="email" required @if (Session::has('email'))  value="{{ Session::get('email') }}" @endif>
                                        </div>   
                                    </div>

                                    <div class="form-group form-float">
                                        <label class="form-label">Employer :</label>
                                        
                                            <select class="form-control show-tick js-example-basic-single js-states" data-live-search="true" name="emp_code" id="one" required>
                                                <option value="" selected disabled hidden>Choose Employer</option>
                                                @foreach ($emp as $data)
                                                  <option value="{{$data->id}}">{{$data->Emp_Desc}}</option>
                                                @endforeach  
                                            </select>
                                    </div>
                                   

                                    <div class="form-group form-float resources" style="display: none" id="two">
                                        <input name="clerical" type="radio" id="radio_1" value="1" checked />
                                        <label for="radio_1">Clerical</label>
                                        <input name="clerical" type="radio" id="radio_2" value="0" />
                                        <label for="radio_2">Non Clerical</label>
                                    </div>

                                   
                                   <div class="form-group form-float other" style="display: none" >
                                        <div class="form-group form-float">
                                            <b>Other Employer :</b>
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="others_employer" id="others_employer" required @if (Session::has('others_employer'))  value="{{ Session::get('others_employer') }}" @endif>
                                            </div>   
                                        </div>
                                    </div>

                                    
                                    <div class="form-group form-float">
                                        <label class="form-label">Biro / Non :</label>
                                        <div class="">
                                            <input name="biro" type="radio" id="radio_3" value="1" checked />
                                            <label for="radio_3">Biro</label>
                                            <input name="biro" type="radio" id="radio_4" value="0" />
                                            <label for="radio_4">Non Biro</label>
                                        </div>
                                    </div>


                                    
                                    <!-- 
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <label class="form-label">Loan Amount (RM):</label>
                                            <input type="text" class="form-control" id="jml_pem" name="jml_pem" value="" min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');"  value=${var} ">
                                        </div>    
                                    </div> -->

                                </div>
                            </div>

                            @can('add_customer_by_mos')
                            <div class="row clearfix">
                                <div class="col-md-10"></div>
                                <div class="col-md-2">
                                    <input type="submit" value="Save" class="btn btn-lg btn-success btn-block" style="cursor:pointer;">
                                        {{ csrf_field() }}
                                </div>
                            </div>
                            @endcan
                            
                            
                                    
                            {!! Form::close() !!}

                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Advanced Form Example With Validation -->

        </div>
    </section>


    @push('js')
    <script type="text/javascript">
        var Privileges = jQuery('#one');
        var select = this.value;
        Privileges.change(function () {
            if ($(this).val() == '1') { //1 others  
                $('.resources').hide();
                $('.other').show();
            }
            else if($(this).val() == '2'){ //2 Maybank
                $('.resources').show();
                $('.other').hide();
            }
            else{
                $('.other').hide();
                $('.resources').hide();
            } 

        });
    </script>

    <script type="text/javascript">
        $('#isAgeSelected').click(function() {
            $("#txtAge").toggle(this.checked);
        });
    </script>
    @endpush


    <script>
            

        $().ready(function() {                 
        $("#addForm").validate({
            rules:{
                name:{
                    required: true,
                    minlength: 2,
                    maxlength: 10,
                },
            },
            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            messages:{
                name:{
                    required: "This field is required",
                    minlength: "Name must be at least 2 characters",
                    maxlength: "Maximum number of characters - 10",
                },
            },
            submitHandler: function(form) { 
                  $.ajax({
                    url:'addEmpl.php',
                    type:'GET',
                    dataType: 'html',
                    success: function(data) {
                        $("#block").html(data);
                    }
                 });
                 return false; // required to block normal submit since you used ajax
             }
         });
    });
    </script>

@endsection