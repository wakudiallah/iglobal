@extends('vadmin.tampilan')

@section('content')

	<section class="content">
        <div class="container-fluid">
            
            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">person_pin</i> Customer</a></li>
                        <li class="active"><i class="material-icons">create</i> Financing Eligibility</li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->
            

        </div>




        
        <fieldset>
            <div class="row">
                <section class="col col-6">
                    <label class="label"> <b>  Pakej </b> </label>
                    <label class="input">
                        <i class="icon-append fa fa-credit-card"></i>
                        <b><input type="text" id="Package2" value="{{$pra->loanpkg_code}}"  name="Package2" placeholder="Package" disabled="disabled"></b>
                        <input name="id_praapplication" id="id_praapplication" type="hidden"  value="{{$pra->id}}" >
                         <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        
                        <b class="tooltip tooltip-bottom-right">Pakej</b>
                    </label>
                </section>
                                        
                <section class="col col-6">
                    <label class="label"> <b> <font color="red" size="2.5" > KELAYAKAN  PEMBIAYAAN  MAKSIMA  </font>  </b> </label>
                    <label class="input">
                        <i class="icon-append fa fa-credit-card"></i>
                            
                            @foreach($loan as $loan)
                                <?php
                                $salary_dsr = ($zbasicsalary * ($loan->dsr / 100)) - $zdeduction;
                                $ndi = ($zbasicsalary - $zdeduction) -  1300;
                                $max  =  $salary_dsr * 12 * 10 ;
                                                               
                                function pembulatan($uang) {
                                    $puluhan = substr($uang, -3);
                                        if($puluhan<500) {
                                                $akhir = $uang - $puluhan; 
                                            }

                                        else{
                                                $akhir = $uang - $puluhan;
                                            }

                                        return $akhir;
                                            }

                                if(!empty($loan->max_byammount))  {
                                  
                                      $ansuran = intval($salary_dsr)-1;
                                      if($pra->loanpkg_code =="1") {  //aku edit
                                          $bunga = 3.8/100;
                                      }
                                      elseif($pra->loanpkg_code == "2") {
                                          $bunga = 4.9/100;
                                      }

                                      else {
                                          $bunga = 5.92/100;
                                      }
                                   
                                      $pinjaman = 0;


                                      for ($i = 0; $i <= $loan->max_byammount; $i++) {
                                          $bungapinjaman = $i  * $bunga * $durasi ;
                                          $totalpinjaman = $i + $bungapinjaman ;
                                          $durasitahun = $durasi * 12;
                                          $ansuran2 = intval($totalpinjaman / ($durasi * 12))  ;
                                          //echo $ansuran2."<br>";
                                          if ($ansuran2 < $ndi)
                                          {
                                              $pinjaman = $i;
                                          }
                                      }   

                                      if($pinjaman > 1) {

                                          $bulat = pembulatan($pinjaman);
                                          $loanx =  number_format($bulat, 0 , ',' , ',' ) ; 
                                          $loanz = $bulat;
                                      }
                                      else {
                                          $loanx =  number_format($loan->max_byammount, 0 , ',' , ',' ) ; 
                                          $loanz = $loan->max_byammount;
                                      }
                                }
                                else { 

                                    $bulat = pembulatan($loan->max_bysalary * $total_salary);
                                    $loanx =  number_format($bulat, 0 , ',' , ',' ) ; 
                                    $loanz = $bulat;
                                    if ($loanz > 199000) {

                                          $loanz  = 250000;
                                          $loanx =  number_format($loanz, 0 , ',' , ',' ) ; 
                                    }
                                }

                                ?>
                            @endforeach

                        <b><input readonly type="text" id="MaxLoan" value=" RM {{$loanx}}" name="MaxLoan" placeholder="Max Loan Eligibility (RM)" class="merah" requierd> </b>
                        <input readonly type="hidden" id="maxloanz" value="{{$loanz}}" name="maxloanz" placeholder="Max Loan Eligibility (RM)" requierd> 
                        <b class="tooltip tooltip-bottom-right">Max Loan Eligibility (RM)</b>
                    </label>
                </section>
                                        
                                       
            </div>
            
            <div class="row">
                <section class="col col-6">
                    <label class="label"><b>  Jumlah Pembiayaan (RM) </b> </label>
                    <label class="input state-<?php if( $pra->loanamount <= $loanz ) { print "success"; } else { print "error"; }?>">
                        <i class="icon-append fa fa-credit-card"></i>
                        <input type="text" name="LoanAmount2"  id="LoanAmount2" onkeypress="return isNumberKey(event)" value="{{ $pra->loanamount }}"  placeholder="RM " onkeyup="this.value = minmax(this.value, 0, {{$loanz}})">
                        <b class="tooltip tooltip-bottom-right">Jumlah Pembiayaan</b>
                    </label>
                </section>
                                        
                <section class="col col-6">
                    <label class="label"> <b>  Jumlah Pendapatan </b> </label>
                    <label class="input">
                        <i class="icon-append fa fa-credit-card"></i>
                        <input type="text" id="pendapatan" value="RM {{ number_format($total_salary, 0 , ',' , ',' )}}" name="pendapatan" placeholder="Loan Amount" disabled="disabled">
                        <b class="tooltip tooltip-bottom-right">Loan Amount</b>
                         <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </label>
                </section>

                <section class="col col-6">
                    <label class="label"> <b>  Ansuran Maksima </b> </label>
                    <label class="input">
                        <i class="icon-append fa fa-credit-card"></i>
                        <input type="text" id="ansuran_maksima" value="RM {{ number_format($ndi, 0 , ',' , ',' )  }}   / month" name="ansuran_maksima" placeholder="Ansuran Maksima" readonly>
                        <b class="tooltip tooltip-bottom-right">Loan Amount</b>
                    </label>
                </section>
                    
                <section class="col col-6">
                    <label class="label"> &nbsp; </label>
                                         
                        <footer>
                            <button class="btn btn-primary " type="submit">
                                <b> Kira Semula </b>
                            </button>
                        </footer>
                </section>
            </div>                               
        </fieldset>
                                 
    </form> 
                                
        <fieldset>
            <table class="table table-bordered table-hover" border='1'>
                <thead>
                    <tr>
                        <th valign="middle"> <b>  Tempoh 1 </b> </th>
                        <th class="hidden-xs"> <b>  Jumlah Pembiayaan  </b> </th>
                        <th><b>  Ansuran Bulanan </b> </th>
                        <th width="40"> <b>  Kadar  Keuntungan </b> </th>
                        <th> <b>  Pilih </b> </th>
                    </tr>
                </thead>
                <tbody>
                     <?php if( $pra->loanamount <= $loanz ) { $ndi_limit=$loan->ndi_limit;?>
                            @foreach($tenure as $tenure)
                                <?php 

                                       $bunga2 =  $pra->loanamount * $tenure->rate /100   ;
                                       $bunga = $bunga2 * $tenure->years;
                                       $total = $pra->loanamount + $bunga ;
                                       $bulan = $tenure->years * 12 ;
                                       $installment =  $total / $bulan ;
                                       $ndi_state = ($total_salary - $zdeduction) - $installment; 
                                       
                                       if($installment  <= $salary_dsr && $ndi_state>=$ndi_limit) {     
                                      
                                    ?>
                                <tr>
                                    <td>{{$tenure->years}} years</td>
                                    <td class="hidden-xs"> RM {{ number_format( $pra->loanamount, 0 , ',' , ',' )  }}  </td>
                                    
                                    <td>RM {{ number_format($installment, 0 , ',' , ',' )  }} /bln</td>
                                  
                                    <td  align="center" >{{$tenure->rate}} %</td>
                                    <td align="center">  <input type="radio" name="tenure" id="tenure" class="tenure" value="{{$tenure->id}}" required> </td>
                                </tr>
                                                
                                 <?php }  ?>
                            @endforeach
                        <?php } ?>
                
                </tbody>
            </table>
        </fieldset>


       

    </section>

     <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>


     <script>
        $(document).ready(function() {
        $('#example').DataTable();
        });

            
    </script>

@endsection