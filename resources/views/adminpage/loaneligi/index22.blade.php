@extends('vadmin.tampilan')

@section('content')

    <section class="content">
        <div class="container-fluid">

            


        @can('view_loan_checks') <!-- MO Meet Cust -->

            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">layers</i> Loan Check & Calculation</a></li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

            @include('shared.notif')

            @include('mo.loan_checks')
        @endcan

        @can('view_loan_egi_processor2s')  <!-- Internal calculation checking else MO for next calculation and changes-->
            @include('processor2.loan_processor2')
        @endcan   

        <!-- @can('view_loan_check_completes')
            @include('processor3.loan_processor3')
        @endcan -->   

        @can('view_additional_doc_processor3s')
            @include('processor3.loan_processor4')
        @endcan   


            
            <!-- ///////////////////  Data Target  //////////////// -->
            @foreach($spekar as $datas)
            <div id="collapseDetailOne{{$datas->id}}" class="collapse" aria-expanded="false" data-collapse-group="collapse-group" aria-labelledby="headingFive" data-parent="#accordionExample">
                <div class="row clearfix">
                    <div class="card">
                        <div class="header bg-green">
                            <h2>Detail</h2>
                        </div>
                        <div class="body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>Name : </b>    
                                        </div>
                                        <div class="col-md-6">
                                           <p>{{$datas->name}} </p>  
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>No Telf : </b>    
                                        </div>
                                        <div class="col-md-6">
                                           <p>{{$datas->notelp}} </p>  
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>By : </b>    
                                        </div>
                                        <div class="col-md-6">
                                           <p style="color: red">{{$datas->user->name}} </p>  
                                        </div>
                                    </div>

                                   
                                </div>
                                <div class="col-md-6">
                                    
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="probootstrap-date-arrival">Doc Spekar</label>
                                        </div>
                                        <div class="col-md-6">
                                            <!-- <input id="spekar" class="form-control"   type="file" name="spekar" >-->
                                            {!! Form::open(array('url'=>'save/spekar/'.$datas->id, 'method'=>'post', 'files'=>'true')) !!}

                                                <input  class="hidden" type="text" name="name" value="{{$datas->name}} " >
                                                <input  class="hidden" type="text" name="ic" value="{{$datas->ic}} " >
                                                <input  class="hidden" type="text" name="id_cus" value="{{$datas->id_cus}} " >
                                                <input  class="hidden" type="text" name="routeto" value="{{$datas->process1}} " >
                                                
                                                <form id="uploadform" target="upiframe" action="{{url('save/spekar/'.$datas->id)}}" method="post" enctype="multipart/form-data">
                                                
                                                    <input type="file" name="fileToUpload" class="test" onchange='this.form.submit()''>

                                                </form>    
                                                 

                                        </div>                                         
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10"></div>
                                        <div class="col-md-2">
                                            <button class="btn btn-success">Save</button>
                                                {{ Form::close() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                                
                            
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            <!-- ///////////////////  End Data Target  //////////////// -->


            <!-- /////////////////// Modal //////////////// -->
            @foreach($spekar as $datax)
            <div class="modal fade" id="exampleModal{{$datax->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="color: red"><b>{{$datax->name}} <b> --( {{$datax->ic}} )</h5>
                    
                  </div>
                  <div class="modal-body">
                   
                    <div class="row">
                        <div class="col-md-2"></div>

                        <div class="col-md-8" style="margin: 30px 30px !important">
                            {!! Form::open(array('url'=>'save/spekar/'.$datax->id, 'method'=>'post', 'files'=>'true')) !!}

                            <input  class="hidden" type="text" name="name" value="{{$datax->name}} " >
                            <input  class="hidden" type="text" name="ic" value="{{$datax->ic}} " >
                            <input  class="hidden" type="text" name="id_cus" value="{{$datax->id_cus}} " >
                            <input  class="hidden" type="text" name="routeto" value="{{$datax->process1}} " >
                            
                            <form id="uploadform" target="upiframe" action="{{url('save/spekar/'.$datax->id)}}" method="post" enctype="multipart/form-data">
                            
                                <input type="file" name="fileToUpload" class="test form-control" onchange='this.form.submit()''>

                            </form>
                        </div>

                        <div class="col-md-2"></div>
                        

                    </div>
                    

                  </div>
                  <div class="modal-footer">
                    
                    <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-success waves-effect">

                    {!! Form::close() !!}   -->                  
                  </div>

                </div>
              </div>
            </div>
            
            @endforeach

            <!-- /////////////////// End Modal //////////////// -->
            
        </div>
    </section>


@include('vadmin.jsalternatif')

<!-- ================= Data Target hidden ======= -->
<script src="https://code.jquery.com/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
 

<script type="text/javascript">
    $("[data-collapse-group]").on('show.bs.collapse', function () {
          var $this = $(this);
          var thisCollapseAttr = $this.attr('data-collapse-group');
          $("[data-collapse-group='" + thisCollapseAttr + "']").not($this).collapse('hide');
        });
</script>

<!-- ================= End Data Target hidden ======= -->

    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>

    <script>
        $(document).ready(function() {
        $('#example').DataTable();
        });
    </script>

    <script type="text/javascript">
        function test() {
            var form = document.getElementById('uploadform');
            form.submit();
        };
    </script>

@endsection
    