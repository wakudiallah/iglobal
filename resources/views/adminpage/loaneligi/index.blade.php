@extends('vadmin.tampilan')

@section('content')

    <section class="content">
        <div class="container-fluid">

            


        @can('view_loan_checks') <!-- MO Meet Cust -->

            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">layers</i> Loan Check & Calculation</a></li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

            @include('shared.notif')

            @include('mo.loan_checks')
        @endcan

        @can('view_loan_egi_processor2s')  <!-- Internal calculation checking else MO for next calculation and changes-->
            @include('processor2.loan_processor2')
        @endcan   

        <!-- @can('view_loan_check_completes')
            @include('processor3.loan_processor3')
        @endcan -->   

        @can('view_additional_doc_processor3s')
            @include('processor3.loan_processor4')
        @endcan   


            
        </div>
    </section>


@include('vadmin.jsalternatif')

<!-- ================= Data Target hidden ======= -->
<script src="https://code.jquery.com/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
 

<script type="text/javascript">
    $("[data-collapse-group]").on('show.bs.collapse', function () {
          var $this = $(this);
          var thisCollapseAttr = $this.attr('data-collapse-group');
          $("[data-collapse-group='" + thisCollapseAttr + "']").not($this).collapse('hide');
        });
</script>

<!-- ================= End Data Target hidden ======= -->

    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>

    <script>
        $(document).ready(function() {
        $('#example').DataTable();
        });
    </script>

    <script type="text/javascript">
        function test() {
            var form = document.getElementById('uploadform');
            form.submit();
        };
    </script>

@endsection
    