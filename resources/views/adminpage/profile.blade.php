@extends('vadmin.tampilan')

@section('content')

    <!-- Profile Picture -->
    <link href="{{ asset('admin/css/pict.css') }}" rel="stylesheet" />

<section class="content">
	<div class="container-fluid">
		<div class="row clearfix">
			<div class="profile large">
		    <div class="cover">@if(empty($login->bc))<img src="admin/images/bck-profil.jpg"/>@else<img src="admin/images/{{Auth::user()->bc}}"/>@endif
		      <div class="layer">
		        <div class="loader"></div>
		      </div><a class="image-wrapper" href="#">
		        <form id="uploadformbackground" action="{{url('save/background/'.Auth::user()->id)}}" method="post" enctype="multipart/form-data">

		         	{{ csrf_field() }}
                    
                    <input type="file" name="fileToUpload1" id="changePicture1" class="hidden-input test1 form-control" onchange='this.form.submit()''>

                    <label class="edit glyphicon glyphicon-pencil" for="changePicture1" type="file" title="Change picture"></label>

                </form>
		    </a>


		    </div>
		    <div class="user-info">
		      <div class="profile-pic">@if(empty($login->profile))<img src="/admin/images/user.png"/>@else<img src="admin/images/profile/{{Auth::user()->profile}}"/>@endif
		        <div class="layer">
		          <div class="loader"></div>
		        </div><a class="image-wrapper" href="#">
		          
		         <form id="uploadform"  action="{{url('save/profile/'.Auth::user()->id)}}" method="post" enctype="multipart/form-data">

		         	{{ csrf_field() }}
                    
                    <input type="file" name="fileToUpload" id="changePicture" class="hidden-input test form-control" onchange='this.form.submit()''>

                    <label class="edit glyphicon glyphicon-pencil" for="changePicture" type="file" title="Change picture"></label>

                </form>
                </a>


		      </div>
		      <div class="username">
		        <div class="name"><span class="verified"></span>{{ Auth::user()->name }}</div>
		        <div class="about">{{ Auth::user()->email }}</div>
		      </div>
		    </div>
		  </div>
		</div>
	</div>
</section>


	<!-- upload automatic -->
    <script type="text/javascript">
        function test() {
            var form = document.getElementById('uploadform');
            form.submit();
        };
    </script>

    <script type="text/javascript">
        function test1() {
            var form = document.getElementById('uploadformbackground');
            form.submit();
        };
    </script>


@endsection