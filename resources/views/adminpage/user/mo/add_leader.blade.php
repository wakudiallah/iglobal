@extends('vadmin.tampilan')

@section('content')

	<section class="content">
        <div class="container-fluid">

            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">person</i> User</a></li>
                        <li class="active"><i class="material-icons">person_add</i>MO User</li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

			<!-- Horizontal Layout -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>
                                Detail MO
                            </h2>
                        </div>

                        <div class="body">
                            <form class="form-horizontal" method="post" action="{{url('mo/add_leader')}}">

                            	<input type="text" name="id" value="{{$detail->id}}" hidden>

                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="email_address_2">ID</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="email_address_2" class="form-control" value="US{{$detail->id}}" disabled>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="password_2">Name</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="password_2" class="form-control" name="name" value="{{$detail->name}}" disabled>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="password_2">Email</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="password_2" class="form-control" value="{{$detail->email}}" disabled>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="password_2">Status</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                            	@if($detail->status == 1)
                                                <input type="text" id="password_2" class="form-control" value="Aktif" disabled>
                                                @else
                                                <input type="text" id="password_2" class="form-control" value="Tidak Aktif" disabled>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="password_2" class="bg-red">Leader</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <select class="form-control" name="leader">
                                                	@foreach($leader as $data)
                                                        @if($data->model->role->id == '12')
                                                	   <option value="{{$data->id}}">US{{$data->id}} - {{$data->name}}</option>
                                                        @endif
                                                	@endforeach

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                	<div class="col-xs-6 col-sm-3 col-md-2 col-lg-2 col-md-offset-10">
                                        <button type="submit" class="btn bg-green btn-block btn-lg waves-effect">
                                            Save
                                        </button>
                                        {{ csrf_field() }}
                                    </div>
                                </div>
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Horizontal Layout -->
        </div>
    </section>

@endsection