@extends('vadmin.tampilan')

@section('content')

	<section class="content">
        <div class="container-fluid">

            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">person</i> User MO</a></li>
                        
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

            <!-- Notification -->
            <div class="row clearfix">
                @if ($message = Session::get('success')) 
                <div class="alert bg-green alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @elseif($message = Session::get('delete'))
                <div class="alert bg-pink alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @elseif($message = Session::get('update'))
                <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @endif
            </div>
            <!-- End of Notif -->

            <div class="row clearfix demo-button-sizes">
                <div class="col-md-10"></div>
                @can('add_users')
                <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2">
                    <a href="{{ route('mo.create') }}" type="submit" class="btn btn-success btn-block btn-lg waves-effect" style="cursor:pointer;">
                      Add
                    </a>
                </div>
                @endcan
            </div>

		    <div class="row clearfix">
                <div class="card">
                    <div class="header bg-red">
                        <h2>Data MO -- </h2>
                    </div>
                    <div class="body">
                        <table class="table" id="example">
                        	<thead>
                                <tr>
                                    <th>#</th>
                                    <th>ID</th>
					                <th>Name</th>
					                <th>Email</th>
					                <th>Leader</th>
                                    <th>Manager</th>
                                    <th>Workgroup</th>
					                <th>Status</th>
					                <th class="text-center">Actions</th>          
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>

                            	@foreach($result as $item)
                                @if(($item->model->role->id == 3)  || ($item->model->role->id == 12))
				                <tr>
				                    <td>{{$i++}}</td>
                                    <td>US{{ $item->id }}</td>
				                    <td>{{ $item->name }}</td>
				                    <td>{{ $item->email }}</td>
				                    
				                    <td>
										@if($item->model->role->id == 5)
                                        @else
                                            @if(empty($item->leader) )
                                            <a href="{{url('/add_leader/'.$item->id)}}" class="btn btn-primary"><i class="material-icons disabled">person</i> Add Leader </a>
                                            @elseif(!empty($item->leader) )
                                                <span class="btn btn-danger">{{$item->nama_leader->name}}</span>
                                            @else
                                                <span class="label bg-white">Follower</span>
                                            @endif
                                        @endif
				                    </td>
                                    <td>

                                        @if(empty($item->manager))
                                        <a href="{{url('/add_manager/'.$item->id)}}" class="btn btn-primary"><i class="material-icons disabled">person</i> Add Manager </a>
                                        @else
                                            <span class="btn btn-danger">{{$item->nama_manager->name}}</span>
                                        @endif
                                    </td>
                                    <td>
                                        {{ $item->roles->implode('name', ', ') }}
                                    </td>
				                    <td>
                                        <div class="switch">  <!-- update status ini belum -->
                                            @if($item->status == 0)
                                            <label>Non Act<a href="{{url('/mo/update1/'.$item->id)}}" class="lever switch-col-red" onclick="alert('User MO akan di aktif!');"></a>Active</label>
                                            @else
                                            <label>Non Act<a href="{{url('/mo/update2/'.$item->id)}}" onclick="alert('User MO akan di nonaktif!');">
                                                <input type="checkbox" id="aksi" name="status" checked><span class="lever switch-col-red"></span></a>Active</label>
                                            @endif
                                        </div>
	                            	</td>

				                    @can('edit_users')
				                    <td class="text-center">
				                        @include('shared._actions', [
				                            'entity' => 'mo',
				                            'id' => $item->id
				                        ])
				                    </td>
				                    @endcan
				                </tr>
                                @endif
				            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

		    
        </div>
    </section>
    

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>

    <script>
        $(document).ready(function() {
        $('#example').DataTable();
        });
    </script>

    <script type="text/javascript">
        
    </script>
    <!-- End status -->

@endsection