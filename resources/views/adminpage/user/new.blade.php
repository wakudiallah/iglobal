@extends('vadmin.tampilan')

@section('content')
	<section class="content">
        <div class="container-fluid">

            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="{{url('users')}}"><i class="material-icons">person</i> User</a></li>
                        <li class="active"><i class="material-icons">person_add</i> User</li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

		    <div class="row clearfix">
		        <div class="col-lg-12">
		        	<div class="card">
		        		<div class="header bg-red">Add User</div>
		        		<div class="body">
				            {!! Form::open(['route' => ['users.store'] ]) !!}
				                @include('adminpage.user._form')
				                

				                <div class="row">
									<div class="col-md-11"></div>
									<div class="col-md-1">
										<!-- Submit Form Button -->
	                            		{!! Form::submit('Save', ['class' => 'btn btn-lg btn-success']) !!}
									</div>
								</div>
				                <!-- Submit Form Button -->
				                <!-- {!! Form::submit('Create', ['class' => 'btn btn-success btn-lg']) !!} -->
				            {!! Form::close() !!}
			            </div>
		            </div>
		        </div>
		    </div>
        </div>
    </section>
@endsection