@extends('vadmin.tampilan')

@section('content')
	<section class="content">
        <div class="container-fluid">

            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">person</i> User</a></li>
                        
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

            <!-- Notification -->
            <div class="row clearfix">
                @if ($message = Session::get('success')) 
                <div class="alert bg-green alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @elseif($message = Session::get('delete'))
                <div class="alert bg-pink alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @elseif($message = Session::get('update'))
                <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @endif
            </div>
            <!-- End of Notif -->

            <div class="row clearfix demo-button-sizes">
                <div class="col-md-10"></div>
                @can('add_users')
                <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2">
                    <a href="{{ route('users.create') }}" type="submit" class="btn btn-success btn-block btn-lg waves-effect" style="cursor:pointer;">
                      Add
                    </a>
                </div>
                @endcan
            </div>

		    <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Data Users <!-- {{ $result->total() }} {{ str_plural('User', $result->count()) }} --></h2>
                        </div>
                        <div class="body">
                            <table class="table" id="example">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>ID</th>
						                <th>Name</th>
						                <th>Email</th>
						                <th>Work Group</th>
						                <th>Status</th>
						                @can('edit_users', 'delete_users')
						                <th class="text-center">Actions</th>
						                @endcan              
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>

                                	@foreach($result as $item)
					                <tr>
					                    <td>{{$i++}}</td>
                                        <td>US{{ $item->id }}</td>
					                    <td>{{ $item->name }}</td>
					                    <td>{{ $item->email }}</td>
					                    <td>{{ $item->roles->implode('name', ', ') }}</td>
					                    <td>
                                            @can('edit_users')
                                            <div class="switch">
                                                @if($item->status == 0)
                                                <label>Non Act<a href="{{url('/userdata/update/'.$item->id)}}" class="lever switch-col-red" onclick="alert('The user will be active!');"></a>Active</label>
                                                @else
                                                <label>Non Act<a href="{{url('/userdata/update1/'.$item->id)}}" onclick="alert('The user will be non active!');">
                                                    <input type="checkbox" id="aksi" name="status" checked><span class="lever switch-col-red"></span></a>Active</label>
                                                @endif
                                            </div>
                                            @endcan
		                            	</td>


					                    <td class="text-center">
					                        @can('edit_users')
                                                <a href="{{ url('/users/edit/'.$item->id) }}" class="btn btn-xs btn-info">
                                                    <i class="glyphicon glyphicon-edit"></i></a>
                                            @endcan

                                            @can('delete_users')
                                                {!! Form::open( ['method' => 'delete', 'url' => url('/users/destroy/'.$item->id), 'style' => 'display: inline', 'onSubmit' => 'return confirm("Are yous sure wanted to delete it?")']) !!}
                                                    <button type="submit" class="btn-delete btn btn-xs btn-danger">
                                                        <i class="glyphicon glyphicon-trash"></i>
                                                    </button>

                                                {!! Form::close() !!}

                                            @endcan
					                    </td>
					                </tr>
					            @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

		    
        </div>
    </section>
    

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>

    <script>
        $(document).ready(function() {
        $('#example').DataTable();
        });
    </script>

    <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script> -->

    <!-- jquery for status -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#aksi').change(function() {
            var returnVal = confirm("Are you sure?"); 
                if(returnVal){
                  postToServer($(this).prop("checked"))
                }else{
                  $(this).prop("checked", !$(this).is(":checked"));
                }       
            });
        });

        function postToServer(state){
            let value = (state) ? 1 : 0;
            alert('Posted Value: ' + value);
            $.ajax({
              type: "POST",
              url: "users/status",
              data: {"aski":value},
              success: function(response){
                //handle response
              }
            });

        }
    </script>
    <!-- End status -->

@endsection