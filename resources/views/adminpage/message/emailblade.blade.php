<div style="font-family: Helvetica Neue, Helvetica, Helvetica, Arial, sans-serif;">
    <table style="width: 100%;">
        <tr>
            <td></td>
            <td bgcolor="#FFFFFF ">
                <div style="padding: 15px; max-width: 600px;margin: 0 auto;display: block; border-radius: 0px;padding: 0px; border: 1px solid lightseagreen;">
                    <table style="width: 100%;background: #2d3def ;">
                        <tr>
                            <td></td>
                            <td>
                                <div>
                                    <table width="100%">
                                        <tr>
                                            <td rowspan="2" style="text-align:center;padding:10px;">
                                                 <img style="float:left; "  src="https://icopangkasa.com.my/public/asset/img/icopangkasa.png" width="208px" height="69px" />
                                                 <span style="color:white;float:right;font-size: 13px;font-style: italic;margin-top: 20px; padding:10px; font-size: 14px; font-weight:normal;">"Dapatkan Perbandingan Sebutharga."<span></span></span></td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                            <td></td>
                        </tr>
                    </table>
                    <table style="padding: 10px;font-size:14px; width:100%;">
                        <tr>
                            <td style="padding:10px;font-size:14px; width:100%;">
                                <p>,</p>
                                <p><br>{$coop}} telah memutuskan untuk  @if($status==1)
                                            <b>meluluskan</b>
                                        @else
                                            <b>menolak</b> permohonan pemohon berikut
                                        @endif .</p>
                                <p>Maklumat Peribadi :</p>
                                <table border="0" style="font-weight: bold">

                                    <tr>
                                        <td>Nama Pemohon</td>
                                        <td>:</td>
                                        <td>{{$fullname}}</td>
                                    </tr>
                                    <tr>
                                         <td>MyKad</td>
                                        <td>:</td>
                                        <td>{{$ic_number}}</td>
                                    </tr>
                                      <tr>
                                        <td>Status Pinjaman</td>
                                        <td>:</td>
                                        <td>
                                        @if($status==1)
                                            <b>Lulus</b>
                                        @else
                                            <b>Tolak</b>
                                        @endif
                                    </td>
                                    </tr>
                                    <tr>
                                        <td>Catatan</td>
                                        <td>:</td>
                                        <td>{{$reason_description}}</td>
                                    </tr>
                               </table>
                                <p>Sekian, terima kasih.</p>
                                <p>b/p Icopangkasa.com.my</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div align="center" style="font-size:12px; margin-top:20px; padding:5px; width:100%; background:#eee;">
                                    © {{ date('Y') }}<a href="http://icopangkasa.com.my" target="_blank" style="color:#333; text-decoration: none;">icopangkasa.com.my</a>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>