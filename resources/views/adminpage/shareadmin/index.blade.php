<div class="body">
    <table class="table" id="example">
        <thead>
            <tr>
                
                <th>#</th>
                <th>By Hand</th>
                <th>Customer Name</th>
                <th>New IC</th>
                <th>Old IC (if Any)</th>
                <th>Company</th>
                <th>Status</th>
                
            </tr>
        </thead>
        <tbody>
            <?php $i = 1; ?>

            @foreach($assessment as $data)
            <tr>
                <td>{{$i++}}</td>
                <td>{{ $data->created_at->toFormattedDateString() }}</td>
                <td>{{ $data->name }}</td>
                <td>{{ $data->ic }}</td>
                <td>{{ $data->ic }}</td>
                <td>{{ $data->majikan->Emp_Desc }}</td>
                
                <td>
                    @include('shared.remarkdashboard')
                </td>

            </tr>
        @endforeach
        </tbody>
    </table>
</div>