@extends('vadmin.tampilan')

@section('content')

	<section class="content">
        <div class="container-fluid">        

	    	<!-- Tabs With Only Icon Title -->
	        <div class="row clearfix">
	            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                <div class="card">
	                    <div class="header bg-red">
	                        <h2>
	                            Verifikasi
	                        </h2>
	                    </div>
	                    <div class="body">
	                    	
	                    	<!-- Applikasi baru -->
	                    	<p>{{$pra->name}}</p>

	                    	

	                    	<div class="col-sm-12">
	                    		<div class="form-group form-float">
	                    			<div class="form-line">
		                    			{!! Form::label('role_id', 'Pengguna') !!}
										<select name="carlist" form="form-control">
											@foreach($remark as $item)
										  <option value="volvo">{{$item -> desc}}</option>
										  @endforeach	
										</select>
									</div>
								</div>
							</div>

	                    	            	
	                    </div>
	                </div>
	            </div>
	        </div>

	    </div>
	</section>

@endsection