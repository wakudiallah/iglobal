@extends('vadmin.tampilanmin');

@section('content')
	<script src="https://maps.google.com/maps/api/js"></script>
  	<script src="gmaps.js"></script>
  	<style>
      #map {
        width: 100%;
        height: 400px;
        background-color: grey;
      }
    </style>

	<section class="content">
        <div class="container-fluid">        

	    	<!-- Tabs With Only Icon Title -->
	        <div class="row clearfix">
	            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                <div class="card">
	                    <div class="header bg-red">
	                        <h2>
	                            Detail Lokasi
	                        </h2>
	                    </div>
	                    <div class="body">
	                        <!-- Nav tabs -->
	                        <ul class="nav nav-tabs tab-nav-right" role="tablist">
	                            <li role="presentation" class="active">
	                                <a href="#home_only_icon_title" data-toggle="tab">
	                                    <i class="material-icons">location_searching</i>
	                                </a>
	                            </li>
	                            <li role="presentation">
	                                <a href="#profile_only_icon_title" data-toggle="tab">
	                                    <i class="material-icons">face</i>
	                                </a>
	                            </li>
	                            
	                        </ul>

	                        <!-- Tab panes -->
	                        <div class="tab-content">
	                            <div role="tabpanel" class="tab-pane fade in active" id="home_only_icon_title">
	                                <b>Location</b>
	                                
	                                <div id="map"></div>
	                                <script>
										// Initialize and add the map
										function initMap() {
										  // The location of Uluru
										  var uluru = {lat: {{$location->latitude}}, lng: {{$location->longitude}}};
										  // The map, centered at Uluru
										  var map = new google.maps.Map(
										      document.getElementById('map'), {zoom: 15, center: uluru});
										  // The marker, positioned at Uluru
										  var marker = new google.maps.Marker({position: uluru, map: map});
										}
									</script>
	                            </div>
	                            <div role="tabpanel" class="tab-pane fade" id="profile_only_icon_title">
	                                <b>Customer Information</b>
	                                <ul class="list-group" style="margin-top: 30px">
		                                <li class="list-group-item">Name : <b>{{$location->name}}</b></li>
		                                <li class="list-group-item">IC : <b>{{$location->ic}}</b></li>
		                                
		                            </ul>
	                            </div>
	                            
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    	<!-- #END# Tabs With Only Icon Title -->

    	</div>
    </section>

    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjyNfeJKhNl43VJox5jX0uRuf4N6jlHPA&callback=initMap">
    </script>
@endsection