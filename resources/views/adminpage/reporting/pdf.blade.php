<!DOCTYPE html>

<html>

<head>

	<title>Batch Header</title>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</head>

<body>
<!-- <img src="http://global.insko.my/admin/images/header.png" /> -->

	

	<table>
		<tr>
			<td width="30%" align="center"><img src="{{asset('admin/images/global1.png')}}" /></td>
		</tr>
	</table>

	<table width="100%">
		<tr>
			<td width="40%" align="center" colspan="5"><h3 style="margin-bottom: 20px !important"><u>Listing Checklist </u></h3>
				<p>Batch : {{$pra->uniq_no}}</p>
			</td>
		</tr>

		<tr>
			<td colspan="5"> &nbsp;</td>
		</tr>
		<tr>
			<td colspan="5"> &nbsp;</td>
		</tr>

		<tr>
			<td width="50%">Date : <?php echo date("Y/m/d") ?></td>
			<td></td>
			<td align="left" style="padding-left: 10px">Name </td>
			<td width="5%">:</td>
			<td width="20%">{{$pra->name}}</td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td align="left" style="padding-left: 10px">IC </td>
			<td width="5%">:</td>
			<td width="20%">{{$pra->ic}}</td>
		</tr>
	</table>

	<div class="row" style="margin-top: 50px !important; margin-bottom: 100px !important">
		<table class="table table-striped table-responsive"  width="100%"  	>
			<thead>
				<tr>
					<th>ID</th>
				    <th>Document</th>
				    <th >Check</th>
				</tr>
			</thead>
			<tbody>
			    <tr>
					<td>1</td>
					<td>IC</td>
					<td ><input type="checkbox"> </td>
			    </tr>
			    <tr>
			    	<td>2</td>
			    	<td>Consent Letter</td>
			    	<!--<td>@if(empty($datax2->doc_pdf) ) X @else <input type="checkbox" checked> @endif</td> -->
			    	<td ><input type="checkbox"> </td>
			    </tr>
			    <tr>
			     	<td>3</td>
			     	<td>Spekar</td>
			     	<td ><input type="checkbox"> </td>
			     	<!-- <td>@if(empty($datax3->doc_pdf) ) X @else <input type="checkbox" checked> @endif</td> -->
			    	
			    </tr>
			    <tr>
			     	<td>4</td>
			     	<td>Personal Financing Schema</td>
			     	<td ><input type="checkbox"> </td>
			     	<!-- <td>@if(empty($datax3->doc_pdf) ) X @else <input type="checkbox" checked> @endif</td> -->
			    	
			    </tr>
		    
		 	</tbody>
		</table>
	</div>



	<table width="100%">
		
		<tr>
			<td>By : {{$pra->user->name}}</td>
		</tr>
	</table>


</body>
</html>