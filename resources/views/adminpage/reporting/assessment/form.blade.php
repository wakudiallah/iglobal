
<!-- 
<div class="col-md-12">
    <h5>Message :</h5>
    <div class="form-group @if ($errors->has('message')) has-error @endif">
        {!! Form::textarea('message', null, ['class' => 'form-control ckeditor', 'id' => 'reply']) !!}
        @if ($errors->has('message')) <p class="help-block">{{ $errors->first('message') }}</p> @endif
    </div>
</div> -->  


<div class="col-sm-6">
    <h5>Attachment:</h5>
    <div class="form-group form-float @if ($errors->has('attach')) has-error @endif">
            
        {!! Form::file('attach', null, ['class' => 'form-control']) !!}
        @if ($errors->has('attach')) <p class="help-block">{{ $errors->first('attach') }}</p> @endif
            
    </div>
</div>


@push('scripts')
<script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>

<script type="text/javascript">  
    CKEDITOR.replace( 'reply', { 
    enterMode: CKEDITOR.ENTER_BR, 
    on: {'instanceReady': function (evt) { evt.editor.execCommand('');     }},
    });      
</script>
@endpush

