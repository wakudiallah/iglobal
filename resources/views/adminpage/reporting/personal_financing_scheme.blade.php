<!DOCTYPE html>

<html>

<head>

	<title>Batch Header</title>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<style type="text/css">
		p{
			font-size: 12px;
			margin-bottom: 10px;
		}
		
	</style>

</head>

<body>
<!-- <img src="http://global.insko.my/admin/images/header.png" /> -->

	
	<table width="100%">
		<tr>
			<td width="40%" align="center" colspan="5"><h3 style="margin-bottom: 10px !important"><img src="{{asset('admin/images/mbsb.png')}}"  width="200px" height="80px"/></h3></td>
		</tr>

		<tr>
			<td width="20%">
				<h5><b>PERSONAL FINANCING SCHEME</b> <br>
				- DAILY BRANCH REGISTER <br></h5>
				<p>Region :</p>
				<p>Branch Name :</p>
				<p>Branch Code :</p>
			</td>
			<td width="40%">
				<h5><b>AFDHAL (WITH CONVERAGE & BUNDLING)</b><br></h5>
				<b>AGENT NAME : <u>Global I Exceed Management Sdn Bhd (Global)</u></b>
			</td>
			<td width="20%">

			</td>
			
		</tr>

		
	</table>

	<div class="row use" style="margin-top: 50px !important; margin-bottom: 100px !important; ">
		<table class="table table-striped table-responsive"  width="100%" border="1">
			<thead style="text-align: center">
				<tr>
					<td colspan="6">
						
					</td>
					<td colspan="6">Personal Financing Department Use</td>
				</tr>
				<tr>
					<td rowspan="3">No</td>
					<td rowspan="3">Date</td>
					<td rowspan="3">Borrowers Name</td>
					<td></td>
					<td></td>
					<td rowspan="3">Agents Name</td>
					<td colspan="6">Loan Status</td>
				</tr>
				<tr>
					<td>IC Number</td>
					<td>Amount (RM)</td>
					<td rowspan="2">Approved (Date)</td>
					<td colspan="2">Disbursed</td>
					<td colspan="2">Rejected</td>
					<td rowspan="2">Pending</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td>Date</td>
					<td>Amount</td>
					<td>Date</td>
					<td>Amount</td>
				</tr>
			</thead>
			<tbody>
			    <tr>
					<td>No</td>
					<td><?php echo date("Y/m/d") ?></td>
					<td>Name</td>
					<td>IC</td>
					<td>Amount</td>
					<td>GLOBAL</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
			    </tr>
			    <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
			    </tr>
			    <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
			    </tr>
			    <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
			    </tr>
			    <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
			    </tr>
			    <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>Amount</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
			    </tr>
			    
			    
		    
		 	</tbody>
		</table>
	</div>



	<table width="100%">
		
		<tr>
			<td>By : {{$pra->p2->name}}</td>
		</tr>
	</table>


</body>
</html>