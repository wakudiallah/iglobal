<div class="col-sm-12">
    <div class="form-group form-float @if ($errors->has('folder')) has-error @endif">
        <h5>Folder</h5>
        <div class="form-line">
            <select class="form-control show-tick js-example-basic-single js-states" data-live-search="true" name="emp">
	            @foreach($file as $data) 
	                <option value="{{ $data->id }}" {{ $data->id == $data->id ? 'selected' : '' }}>{{ $data->folder }}</option>
	            @endforeach 
        	</select>
        </div>
        @if ($errors->has('folder')) <p class="help-block">{{ $errors->first('folder') }}</p> @endif
    </div>  
</div>

<div class="col-sm-12">
    <h5>Folder :</h5>
    <div class="form-group form-float @if ($errors->has('folder')) has-error @endif">
        <div class="form-line">
            
            {!! Form::label('folder', 'Folder ', ['class' => 'form-label']) !!}
            {!! Form::text('folder', null, ['class' => 'form-control']) !!}
            @if ($errors->has('folder')) <p class="help-block">{{ $errors->first('folder') }}</p> @endif
            
        </div>
    </div>
</div>

<div class="col-sm-6">
    <h5>Attachment:</h5>
    <div class="form-group form-float @if ($errors->has('attach')) has-error @endif">
            
        {!! Form::file('attach', null, ['class' => 'form-control']) !!}
        @if ($errors->has('attach')) <p class="help-block">{{ $errors->first('attach') }}</p> @endif
            
    </div>
</div>


