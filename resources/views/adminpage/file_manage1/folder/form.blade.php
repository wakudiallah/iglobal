<div class="col-sm-12">
    <h5>Folder :</h5>
    <div class="form-group form-float @if ($errors->has('folder')) has-error @endif">
        <div class="form-line">
            
            {!! Form::label('folder', 'Folder ', ['class' => 'form-label']) !!}
            {!! Form::text('folder', null, ['class' => 'form-control']) !!}
            @if ($errors->has('folder')) <p class="help-block">{{ $errors->first('folder') }}</p> @endif
            
        </div>
    </div>
</div>


