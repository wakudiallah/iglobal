@extends('vadmin.tampilan')

@section('content')
	
	<!-- Dropzone Css -->
    <link href="{{asset('admin/plugins/dropzone/dropzone.css')}}" rel="stylesheet">

	<section class="content">
        <div class="container-fluid">
            
            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">build</i> Attribute</a></li>
                        <li><a href="{{ route('homeimage.index') }}"><i class="material-icons">add_a_photo</i> Home Image</a></li>
                        <li class="active"><i class="material-icons">create</i> Tambah</li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

            <!-- File Upload | Drag & Drop OR With Click & Choose -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>
                                Tambah
                            </h2>
                        </div>
                        <div class="body">
                         	<!-- <form action="/homeimage" id="" class="dropzone" method="post" enctype="multipart/form-data">
                           		{{ csrf_field() }}
                                <div class="dz-message">
                                    <div class="drag-icon-cph">
                                        <i class="material-icons">touch_app</i>
                                    </div>
                                    <h3>Drop files here or click to upload.</h3>
                                    	
                                </div>
                                <div class="fallback">
                                    <input name="file" type="file" multiple />
                                </div>

                                <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2 col-md-offset-10">
				                    <button type="submit" class="btn bg-red btn-block btn-lg waves-effect" style="margin-top: 35px; cursor: pointer">
	                                    Simpan
	                                </button>
				                </div>
                            </form> -->

                            <form action="{{ route('homeimage.store') }}" method="post" enctype="multipart/form-data">
				                {{ csrf_field() }}
				                
				                <div class="form-line">
                                    <div class="form-group form-float @if ($errors->has('image')) has-error @endif">
				                        <label for="email">File:</label>
				                        <input type="file" class="form-control" id="email" name="image">
                                        @if ($errors->has('image')) <p class="help-block">{{ $errors->first('image') }}</p> @endif
                                    </div>
				                </div>


                                <div class="form-line">
                                    <div class="form-group form-float @if ($errors->has('status')) has-error @endif">
                                        <h5>Status</h5>
                                        <div class="">
                                            {!! Form::select('status', array('1' => 'Aktif', '0' => 'Non Aktif'), '1'); !!}
                                        </div>
                                        @if ($errors->has('status')) <p class="help-block">{{ $errors->first('status') }}</p> @endif
                                    </div>  
                                </div>

				               
				                <div class="form-line">
                                    <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2 col-md-offset-10">
    				                    <button type="submit" class="btn bg-cyan btn-block btn-lg waves-effect">
    	                                    Simpan
    	                                </button>
    				                </div>
                                </div>

				            </form>
						</div>
							
                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# File Upload | Drag & Drop OR With Click & Choose -->
        </div>
    </section>

    <!-- Dropzone Plugin Js -->
    <!-- <script src="{{asset('admin/plugins/dropzone/dropzone.js')}}"></script> -->
	

    
@endsection