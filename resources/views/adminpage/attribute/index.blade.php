@extends('vadmin.tampilan')

@section('content')
	<section class="content">
        <div class="container-fluid">
            
            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">build</i> Attribute</a></li>
                        <li><a href="javascript:void(0);"><i class="material-icons">add_a_photo</i> Home Image</a></li>
                        @if(Request::segment(3) === 'tambah')
                        <li class="active"><i class="material-icons">create</i> Create</li>
                        @elseif(Request::segment(3) == 'kemaskini')
                        <li class="active"><i class="material-icons">edit</i> Kemaskini</li>
                        @endif
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

            <!-- Notification -->
            <div class="row clearfix">
                @if ($message = Session::get('success')) 
                <div class="alert bg-green alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @elseif($message = Session::get('delete'))
                <div class="alert bg-pink alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @elseif($message = Session::get('update'))
                <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @endif
            </div>
            <!-- End of Notif -->

            <div class="row clearfix demo-button-sizes">
                <div class="col-md-10"></div>
                
                <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2">
                    <a href="{{ route('homeimage.create') }}" type="submit" class="btn btn-success btn-block btn-lg waves-effect" style="cursor:pointer;">
                      Tambah
                    </a>
                </div>
                
            </div>
            
            <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Home Images</h2>
                        </div>
                        <div class="body">
                            <table class="table" id="data-table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Image</th>
                                        <th>Status</th>          
                                    </tr>
                                </thead>
                                <tbody>
                                	<?php $i = 1; ?>

                                	@foreach($image as $data)
                                	<tr>
                                		<td>{{$i++}}</td>
                                		<td><img src="uploads/{{$data->image}}" style="width: 90px; height: 40px"></td>
                                		<td>
                                		  <div class="switch">
                                                @if($data->status == 0)
                                                <label>Non Act<a href="{{url('/homeimage/update1/'.$data->id)}}" class="lever switch-col-red" onclick="alert('Image akan di aktif!');"></a>Active</label>
                                                @else
                                                <label>Non Act<a href="{{url('/homeimage/update2/'.$data->id)}}" onclick="alert('Image akan di nonaktif!');">
                                                    <input type="checkbox" id="aksi" name="status" checked><span class="lever switch-col-red"></span></a>Active</label>
                                                @endif
                                            </div>
			                            </td>
                                	</tr>
                                	
                                	@endforeach
                                	
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div><!-- End of table -->
        </div>
    </section>


    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>

    <script>
	    $(document).ready(function() {
	    $('#data-table').DataTable();
	    });
	</script>

    <!-- Tombol ON OFF -->
    <script type="text/javascript">
        $(document).ready(function(){
        $('#aksi').change(function() {
        var returnVal = confirm("Are you sure?"); 
            if(returnVal){
              postToServer($(this).prop("checked"))
            }else{
              $(this).prop("checked", !$(this).is(":checked"));
            }       
        });
    });

    function postToServer(state){
        let value = (state) ? 1 : 0;
        alert('Posted Value: ' + value);
        $.ajax({
          type: "POST",
          url: "testUrl/{id}",
          data: {"aski":value},
          success: function(response){
            //handle response
          }
        });

    }
    </script> <!-- end of tombol on off -->
@endsection