@extends('vadmin.tampilan')

@section('content')

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
    

    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix"><!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li class="active"><a href="{{route('announc.index')}}"><i class="material-icons">mic</i> Announcement Group</a></li>
                    </ol>
                </div>
            </div><!-- End of breadcrumber -->

            <!-- Notification -->
            <div class="row clearfix">
                <div class="col-md-12">
                    @if ($message = Session::get('success')) 
                    <div class="alert bg-green alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                    </div>
                    @elseif($message = Session::get('delete'))
                    <div class="alert bg-pink alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                    </div>
                    @elseif($message = Session::get('update'))
                    <div class="alert alert-warning alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                    </div>
                </div>
                @endif
            </div>
            <!-- End of Notif -->

            <div class="row clearfix demo-button-sizes"><!-- Button tambah -->
                <div class="col-md-10"></div>                
                <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2">
                    <a href="{{ route('announcementgroups.create') }}" type="submit" class="btn btn-success btn-block btn-lg waves-effect" style="cursor:pointer;">
                      Add
                    </a>
                </div>
            </div> <!-- End of button tambah -->
           
            <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Announcement Group</h2>
                        </div>
                        <div class="body">
                            <table class="table" id="example">
                                <thead> 
                                    <tr>
                                        <th width="5%">#</th>
                                        <th width="20%">Title</th>
                                        <th width="40%">Desc</th>
                                        <th width="10%">Attach</th>
                                        <th width="10%">Group</th>
                                        <th width="20%">Status</th>
                                        <th>Action</th>             
                                    </tr>
                                </thead>
                                <tbody>
                                	<?php $i = 1; ?>

                                	@foreach($announc as $data)
                                	<tr>
                                		<td>{{$i++}}</td>
                                		<td>{{$data->title}}</td>
                                		<td>{{$data->description}}</td>
                                		<td>
                                            @if($data->attach == "NULL")
                                                {{$data->attach}}
                                            @else
                                                <a href="file_announcement/{{$data->attach}}" class='btn btn-warning' target="_blank">{{$data->attach}}</a>
                                            @endif
                                        </td>
                                        <td>@if($data->group_id == 'A')
                                                All
                                            @else
                                            {{$data->announcformanager->name}}
                                            @endif
                                        </td>
                                        <td>
                                            <div class="switch">
                                                @if($data->status == 0)
                                                <label>Non Act<a href="{{url('/announcementgroups/update1/'.$data->id)}}" class="lever switch-col-red" onclick="alert('Announcement will be active!');"></a>Active</label>
                                                @else
                                                <label>Non Act<a href="{{url('/announcementgroups/update0/'.$data->id)}}" onclick="alert('Announcement will be non active!');">
                                                    <input type="checkbox" id="aksi" name="status" checked><span class="lever switch-col-red"></span></a>Active</label>
                                                @endif
                                            </div>
                                        </td>
                                        <td>
                                            @include('shared._actions', [
                                                'entity' => 'announcementgroups',
                                                'id' => $data->id
                                            ])
                                        </td>
                                	</tr>
                                	
                                	@endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
	
	<script>
	    $(document).ready(function() {
	    $('#example').DataTable();
	    });
	</script>

	<script>
	    $(document).ready(function() {
	    $('#example1').DataTable( {
	        dom: 'Bfrtip',
	        buttons: [
	            'copyHtml5',
	            'excelHtml5',
	            'csvHtml5',
	            'pdfHtml5'
	        ]
	    } );
	} );
	</script>

    
@endsection
