@extends('vadmin.tampilan')

@section('content')

	<section class="content">
        <div class="container-fluid">
            <div class="row clearfix"><!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li class="active"><a href="{{route('announc.index')}}"><i class="material-icons">mic</i> Announcement Group</a></li>
                    </ol>
                </div>
            </div><!-- End of breadcrumber -->

            <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Announcment Group</h2>
                        </div>
                        <div class="body">
                        	<div class="row">
                        		
                        		<div class="col-md-8">
                        			 
	                        		<h2>{{$announc->title}}</h2>
	                        	
	                                <p>{{$announc->desc}}</p>
		                            <blockquote>
		                                <footer>{{$announc->user->name}} 
		                                
		                                <p><cite title="Source Title"> {{ $announc->created_at->toFormattedDateString() }}</cite></p>
		                                </footer>
		                            </blockquote>
		                            	
                                        @if($announc->attach != 'NULL')
		                                <a href="{{url('/')}}/file_announcement/{{$announc->attach}}" class='btn btn-warning' target="_blank" style="margin-top: 30px">{{$announc->attach}}</a>
                                        @endif
		                                
	                        	</div>

	                        	

	                            <div class="col-md-4">
		                            		
	                        		<ul>
	                        			@foreach ($other as $data)

	                        			<li style="color:red"><a href="{{url('/announcgroups/show/'.$data->id)}}" style="color: red; text-decoration: none">{{ $announc->created_at->toFormattedDateString() }}-- {{$data->title}}</a></li>

	                        			@endforeach
	                        		</ul>
		                            		
	                            </div>
                        	</div>

                            
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>    

@endsection