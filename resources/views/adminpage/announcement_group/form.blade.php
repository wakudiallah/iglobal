<div class="col-sm-12">
    <h5>Title Announcement:</h5>
    <div class="form-group form-float @if ($errors->has('title')) has-error @endif">
        <div class="form-line">
            
            {!! Form::label('title', 'Title Announcement', ['class' => 'form-label']) !!}
            {!! Form::text('title', null, ['class' => 'form-control']) !!}
            @if ($errors->has('title')) <p class="help-block">{{ $errors->first('title') }}</p> @endif
            
        </div>
    </div>
</div>

<div class="col-md-12">
    <h5>Description :</h5>
    <div class="form-group @if ($errors->has('description')) has-error @endif">
        {!! Form::textarea('description', null, ['class' => 'form-control ckeditor', 'id' => 'desc']) !!}
        @if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
    </div>
</div>



<div class="col-md-6">
    <h5>Attachment:</h5>
    <div class="form-group form-float @if ($errors->has('attach')) has-error @endif">
            
        {!! Form::file('attach', null, ['class' => 'form-control']) !!}
        @if ($errors->has('attach')) <p class="help-block">{{ $errors->first('attach') }}</p> @endif
            
    </div>
</div>  


<div class="col-md-6">
    <div class="form-group form-float @if ($errors->has('group_id')) has-error @endif">
        <h5>Manager </h5>
        <select class="form-control show-tick" data-live-search="true" name="group_id" required>
            @foreach ($manager as $data)
              <option value="{{$data->model_id}}">{{$data->user->name}} (Manager)</option>
            @endforeach
        </select>
        @if ($errors->has('group_id')) <p class="help-block">{{ $errors->first('group_id') }}</p> @endif
    </div>  
</div>

<div class="col-md-6">
    <div class="form-group form-float @if ($errors->has('status')) has-error @endif">
        <h5>Status </h5>
        <div class="">
            {!! Form::select('status', array('1' => 'Activ', '0' => 'Non Activ'), ['class' => 'form-control show-tick']); !!}
        </div>
        @if ($errors->has('status')) <p class="help-block">{{ $errors->first('status') }}</p> @endif
    </div>  
</div>


@push('scripts')
<script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>

<script type="text/javascript">  
    CKEDITOR.replace( 'desc', { 
    shiftEnterMode : CKEDITOR.ENTER_P,
    enterMode: CKEDITOR.ENTER_BR, 
    on: {'instanceReady': function (evt) { evt.editor.execCommand('');     }},
    });      
</script>

@endpush
