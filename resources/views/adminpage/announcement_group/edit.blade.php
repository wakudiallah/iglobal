@extends('vadmin.tampilan')

@section('content')

	<!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{asset('admin/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet" />

	<section class="content">
        <div class="container-fluid">
            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="{{route('announcementgroups.index')}}"><i class="material-icons">mic</i> Announcement Group</a></li>
                        <li class="active"><i class="material-icons">create</i> Edit</li>

                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

            <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Edit</h2>
                        </div>

                        <div class="body">
                            <div class="row clearfix">
                                
                                {!! Form::model($announc, ['method' => 'PUT','files' => true, 'route' => ['announcementgroups.update',  $announc->id ] ]) !!}
                                    
                                    @include('adminpage.announcement_group.form')
                                     

                                    <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2 col-md-offset-10">
                                        <button type="submit" class="btn bg-green btn-block btn-lg waves-effect">
                                            Save
                                        </button>
                                        {{ csrf_field() }}
                                    </div>
                                    
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>

        </div>
    </section>

	<!-- Jquery Core Js -->
    <script src="{{asset('admin/plugins/jquery/jquery.min.js')}}"></script>

    <!-- Custom Js -->
    <script src="{{asset('admin/js/pages/forms/basic-form-elements.js')}}"></script>

    
@endsection()
