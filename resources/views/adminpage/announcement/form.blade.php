<div class="col-sm-12">
    <h5>Title Announcement:</h5>
    <div class="form-group form-float @if ($errors->has('title')) has-error @endif">
        <div class="form-line">
            
            {!! Form::label('title', 'Title Announcement', ['class' => 'form-label']) !!}
            {!! Form::text('title', null, ['class' => 'form-control']) !!}
            @if ($errors->has('title')) <p class="help-block">{{ $errors->first('title') }}</p> @endif
            
        </div>
    </div>
</div>

<div class="col-md-12">
    <h5>Desc :</h5>
    <div class="form-group @if ($errors->has('desc')) has-error @endif">
        {!! Form::textarea('desc', null, ['class' => 'form-control ckeditor', 'id' => 'desc']) !!}
        @if ($errors->has('desc')) <p class="help-block">{{ $errors->first('desc') }}</p> @endif
    </div>
</div>



<div class="col-sm-6">
    <h5>Attachment:</h5>
    <div class="form-group form-float @if ($errors->has('attach')) has-error @endif">
            
        {!! Form::file('attach', null, ['class' => 'form-control']) !!}
        @if ($errors->has('attach')) <p class="help-block">{{ $errors->first('attach') }}</p> @endif
            
    </div>
</div>

<div class="col-md-6">
    <div class="form-group form-float @if ($errors->has('act')) has-error @endif">
        <h5>Status</h5>
        <div class="">
            {!! Form::select('act', array('1' => 'Actif', '0' => 'Non Actif'), '1'); !!}
        </div>
        @if ($errors->has('act')) <p class="help-block">{{ $errors->first('act') }}</p> @endif
    </div>  
</div>


@push('scripts')
<script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>

<script type="text/javascript">  
    CKEDITOR.replace( 'desc', { 
    shiftEnterMode : CKEDITOR.ENTER_P,
    enterMode: CKEDITOR.ENTER_BR, 
    on: {'instanceReady': function (evt) { evt.editor.execCommand('');     }},
    });      
</script>

@endpush
