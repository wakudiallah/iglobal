@extends('vadmin.tampilan')


@section('content')
	<section class="content">
        <div class="container-fluid">
            <div class="row clearfix"><!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">home</i> Dashboard</a></li>
                        <li class="active"><i class="material-icons">file_upload</i> upload</li>
                    </ol>
                </div>
            </div><!-- End of breadcrumber -->
           
            <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Upload Document Customers</h2>
                        </div>
                        
                        <div class="body">
                        	<div class="row">
                        		<div class="col-md-6">
                        			<label for="probootstrap-date-arrival">Salinan Penyata Pengenalan</label>
                        			<input type="hidden" name="_token" value="{{ csrf_token() }}">
				                    <input type="text" name="id_cus" value="{{ $reg->id_cus}}">
                    <input type="hidden" name="type1" value="1" id="type1">

                    <div class="probootstrap-date-wrap">
                      <span class="icon ion-document"></span>
                      <input id="fileupload1"  @if(empty($document1->name))  @endif    class="form-control" type="file" name="file1" >
                    </div>

                    <input type="hidden" name="document1"   id="documentx1"  value="Salinan Pengenal">&nbsp; <span id="document1"> </span> 
                      @if(!empty($document1->name))
                      <span id="document1a"><a target='_blank' href="{{url('/')}}/documents/user_doc/{{$reg->ic}}/{{$document1->doc_pdf}}"> {{$document1->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
                      @else
                      <!--<a class='bnt btn-danger'>No Attachment Available</a>-->
                      @endif
                        		</div>
                        		<div class="col-md-6">
                        			<label for="probootstrap-date-arrival">Salinan Penyata Gaji Untuk 3 Bulan Terkini</label>
                        			
                        		</div>
                        	</div>

                        	<div class="row">
                        		<div class="col-md-12">
                        			<p style="color: green !important; margin-top: 30px">Jika dokumen penyata gaji lebih daripada satu, sila muat naik dibawah</p>
                        		</div>
                        		<div class="col-md-6">
                        			
                        		</div>
                        		<div class="col-md-6">
                        			
                        		</div>
                        	</div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </section>


@endsection

@push('js')

<!-- upload file -->
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="{{url('/')}}/assets/js/file/vendor/jquery.ui.widget.js"></script>

<script src="{{url('/')}}/assets/js/file/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="{{url('/')}}/assets/js/file/jquery.fileupload.js"></script>
<?php for ($x = 1; $x <= 12; $x++) {  ?>

<script type="text/javascript">
/*jslint unparam: true */
/*global window, $ */
$(function () {
    'use strict';

    // Change this to the location of your server-side upload handler:
    var url = window.location.hostname === 'blueimp.github.io' ?
                '//jquery-file-upload.appspot.com/' : '{{url('/')}}/doc/submit/uploads/{{$x}}'
    $('#fileupload{{$x}}').fileupload({
        url: url,
        dataType: 'json',
        success: function ( data) {
             var text = $('#documentx{{$x}}').val();
            $("#document{{$x}}").html("<a target='_blank' href='"+"{{url('/')}}/documents/user_doc/{{$reg->ic}}/"+data.file+"'>"+text+"</a><i class='glyphicon glyphicon-ok txt-color-green'></i>");
             $("#document{{$x}}a").hide();
             $("#a{{$x}}").val(data.file);
            $("#a{{$x}}").val(data.file);
      $("#a{{$x}}").val(data.file);

             document.getElementById("fileupload{{$x}}").required = false;
             
          
            

        }
       
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

        
});
</script>
<?php } ?>

<!-- end of upload file -->


@endpush