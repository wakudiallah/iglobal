@extends('vadmin.tampilan')

@section('content')

	<section class="content">
        <div class="container-fluid">
            
            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">text_fields</i> Setup Module</a></li>
                        <li><a href="{{route('employment.index')}}"><i class="material-icons">next_week</i> Job Type</a></li>
                        @if(Request::segment(2) === 'create')
                        <li class="active"><i class="material-icons">create</i>Add</li>
                        @else
                        <li class="active"><i class="material-icons">edit</i> Edit</li>
                        @endif
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

            <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Add</h2>
                        </div>

                        <div class="body">
                            <div class="row clearfix">
                                {!! Form::open(['route' => ['employment.store'] ]) !!}
                                    @include('adminpage.setupmodule.jenispekerjaan.form')
                                    
                                    <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2 col-md-offset-10">
                                        <button type="submit" class="btn bg-green btn-block btn-lg waves-effect">
                                            Save
                                        </button>
                                        {{ csrf_field() }}
                                    </div>
                                    
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>

        </div>
    </section>

@endsection