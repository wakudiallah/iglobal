<div class="col-sm-12">
    <h5>Job Type Code:</h5>
    <div class="form-group form-float @if ($errors->has('code_employment')) has-error @endif">
        <div class="form-line">            
            {!! Form::label('title', 'Job Type Code', ['class' => 'form-label']) !!}
            {!! Form::text('code_employment', null, ['class' => 'form-control']) !!}
            @if ($errors->has('code_employment')) <p class="help-block">{{ $errors->first('code_employment') }}</p> @endif            
            <div class="help-info">Max 4 char</div>
        </div>
    </div>
</div>


<div class="col-sm-12">
    <h5>Job Type:</h5>
    <div class="form-group form-float @if ($errors->has('name')) has-error @endif">
        <div class="form-line">            
            {!! Form::label('title', 'Job Type', ['class' => 'form-label']) !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
            @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif            
        </div>
    </div>
</div>

