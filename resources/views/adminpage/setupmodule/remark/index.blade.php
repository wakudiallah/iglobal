@extends('vadmin.tampilan')

@section('content')

	<section class="content">
        <div class="container-fluid">
            
            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">text_fields</i> Setup Module</a></li>
                        <li class="active"><i class="material-icons">looks</i> Remark</li>
                        
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

             <!-- Notification -->
            <div class="row clearfix">
                @if ($message = Session::get('success')) 
                <div class="alert bg-green alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @elseif($message = Session::get('delete'))
                <div class="alert bg-pink alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @elseif($message = Session::get('update'))
                <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @endif
            </div>
            <!-- End of Notif -->

            <div class="row clearfix demo-button-sizes"><!-- Button tambah -->
                <div class="col-md-10"></div>
                <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2">
                    <a href="{{ route('remark.create') }}" type="submit" class="btn btn-success btn-block btn-lg waves-effect" style="cursor:pointer;">
                      Add
                    </a>
                </div>
                
            </div> <!-- End of button tambah -->

            <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header bg-red">
                            Remark
                        </div>
                        <div class="body">
                            <table class="table" id="data-table">
                                <thead>
                                    <tr><div class="col-md-12">
                                            
                                            <span class="label bg-black">black</span>
                                            <span class="label bg-blue-grey">blue-grey</span>
                                            <span class="label bg-red">red</span>
                                            <span class="label bg-pink">pink</span>
                                            <span class="label bg-purple">purple</span>
                                            <span class="label bg-deep-purple">deep-purple</span>
                                            <span class="label bg-indigo">indigo</span>
                                            <span class="label bg-blue">blue</span>
                                            <span class="label bg-light-blue">light-blue</span>
                                            <span class="label bg-light-green">light-green</span>
                                            <span class="label bg-cyan">cyan</span>
                                            <span class="label bg-teal">teal</span>
                                            <span class="label bg-lime">lime</span>
                                            <span class="label bg-yellow">yellow</span>
                                            <span class="label bg-amber">amber</span>
                                            <span class="label bg-orange">orange</span>
                                            <span class="label bg-deep-orange">deep-orange</span>
                                            <span class="label bg-brown">brown</span>
                                            <span class="label bg-grey">grey</span>
                                        </div>
                                    </tr>
                                    <tr>
                                        <th width="5%">#</th>
                                        <th width="15%">Remark Code</th>
                                        <th>Desc</th>
                                        <th>Color</th>
                                        <th>View</th>
                                        <th>Workgroup</th>
                                        <th width="10%">Action</th>            
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>

                                    @foreach($remark as $data)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$data -> id_remark}}</td>
                                        <td>{{$data -> desc}}</td>
                                        <td>
                                            <div class="progress">
                                                    <div class="progress-bar bg-{{$data -> color}}" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                                                </div>
                                        </td>
                                        <td><span class="label bg-{{$data -> color}}">{{$data -> desc}}</span></td>
                                        <td>{{$data->role->name}}</td>
                                        <td>
                                            @include('shared._actions', [
					                            'entity' => 'remark',
					                            'id' => $data->id
					                        ])
                                         </td>
                                    </tr>
                                    @endforeach

        
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>

    <script>
        $(document).ready(function() {
        $('#data-table').DataTable();
        });
    </script>

@endsection