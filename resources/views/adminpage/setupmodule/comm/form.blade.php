	<div class="col-sm-12">
        <h5>Kod Loan Pakej:</h5>
        <div class="form-group form-float @if ($errors->has('LnPkg_Code')) has-error @endif">
            <div class="form-line">
                <select class="form-control show-tick" name="LnPkg_Code">
                    
                    @foreach ($loan as $data)
                    <option value="{{$data->LnPkg_Code}}">{{$data->LnPkg_Code}} - {{$data->Ln_Desc}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    
	<div class="col-sm-6">
		<h5>Date Start:</h5>
		<div class="form-group form-float @if ($errors->has('Dt_Start')) has-error @endif">
			<div class="form-line">
				{!! Form::label('title', 'Date Start', ['class' => 'form-label']) !!}
	            {!! Form::text('Dt_Start', null, ['class' => 'form-control', 'id' => 'datepicker']) !!}
	            @if ($errors->has('Dt_Start')) <p class="help-block">{{ $errors->first('Dt_Start') }}</p> @endif
			</div>
		</div>
	</div>
	<div class="col-sm-6">
		<h5>Date End:</h5>
		<div class="form-group form-float @if ($errors->has('Dt_Ent')) has-error @endif">
			<div class="form-line">
				{!! Form::label('title', 'Date End', ['class' => 'form-label']) !!}
	            {!! Form::text('Dt_Ent', null, ['class' => 'form-control', 'id' => 'datepicker']) !!}
	            @if ($errors->has('Dt_Ent')) <p class="help-block">{{ $errors->first('Dt_Ent') }}</p> @endif
			</div>
		</div>
	</div>
	
		<!-- 'class' => 'datepicker form-control -->

	<div class="col-sm-4">
	    <h5>Comm MO:</h5>
	    <div class="form-group form-float @if ($errors->has('Comm_MO')) has-error @endif">
	        <div class="form-line">
	            
	            {!! Form::label('title', 'Comm MO', ['class' => 'form-label']) !!}
	            {!! Form::text('Comm_MO', null, ['class' => 'form-control']) !!}
	            @if ($errors->has('Comm_MO')) <p class="help-block">{{ $errors->first('Comm_MO') }}</p> @endif
	            
	        </div>
	    </div>
	</div>

	<div class="col-sm-4">
		<h5>Comm Team Lead:</h5>
		<div class="form-group form-float @if ($errors->has('Comm_TeamLEad')) has-error @endif">
			<div class="form-line">
				{!! Form::label('title', 'Comm Team Lead', ['class' => 'form-label']) !!}
	            {!! Form::text('Comm_TeamLEad', null, ['class' => 'form-control']) !!}
	            @if ($errors->has('Comm_TeamLEad')) <p class="help-block">{{ $errors->first('Comm_TeamLEad') }}</p> @endif
			</div>
		</div>
	</div>
	<div class="col-sm-4">
		<h5>Comm Manager:</h5>
		<div class="form-group form-float @if ($errors->has('Comm_Manager')) has-error @endif">
			<div class="form-line">
				{!! Form::label('title', 'Comm Manager', ['class' => 'form-label']) !!}
	            {!! Form::text('Comm_Manager', null, ['class' => 'form-control']) !!}
	            @if ($errors->has('Comm_Manager')) <p class="help-block">{{ $errors->first('Comm_Manager') }}</p> @endif
			</div>
		</div>
	</div>
