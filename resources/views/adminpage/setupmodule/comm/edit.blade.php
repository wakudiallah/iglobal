@extends('vadmin.tampilan')

@section('content')
	<section class="content">
        <div class="container-fluid">
            
            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">text_fields</i> Attribute</a></li>
                        <li><a href="{{route('comm.index')}}"><i class="material-icons">money</i>Komisyen</a></li>
                        <li class="active"><i class="material-icons">create</i>Kemaskini</li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

            <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Tambah</h2>
                        </div>

                        <div class="body">
                            <div class="row clearfix">
                                {!! Form::model($comm, ['method' => 'PUT', 'route' => ['comm.update',  $comm->id ] ]) !!}
                                    
                                    @include('adminpage.setupmodule.comm.form')
                                    
                                    <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2 col-md-offset-10">
                                        <button type="submit" class="btn bg-red btn-block btn-lg waves-effect">
                                            Simpan
                                        </button>
                                        {{ csrf_field() }}
                                    </div>
                                    
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>                
            </div>

        </div>
    </section>

@endsection