

<div class="col-md-12">
    <h5>Question :</h5>
    <div class="form-group @if ($errors->has('question')) has-error @endif">
        {!! Form::textarea('question', null, ['class' => 'form-control ckeditor', 'id' => 'question']) !!}
        @if ($errors->has('question')) <p class="help-block">{{ $errors->first('question') }}</p> @endif
    </div>
</div>


<div class="col-md-6">
    <div class="form-group form-float @if ($errors->has('status')) has-error @endif">
        <h5>Status</h5>
        <div class="">
            {!! Form::select('status', array('1' => 'Activ', '0' => 'Non Activ')); !!}
        </div>
        @if ($errors->has('status')) <p class="help-block">{{ $errors->first('status') }}</p> @endif
    </div>  
</div>


@push('scripts')
<script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>

<script type="text/javascript">  
    CKEDITOR.replace( 'question', { 
    shiftEnterMode : CKEDITOR.ENTER_P,
    enterMode: CKEDITOR.ENTER_BR, 
    on: {'instanceReady': function (evt) { evt.editor.execCommand('');     }},
    });      
</script>

@endpush
