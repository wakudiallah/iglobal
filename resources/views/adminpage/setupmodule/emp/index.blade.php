@extends('vadmin.tampilan')

@section('content')

	<section class="content">
        <div class="container-fluid">

            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">text_fields</i> Attribute</a></li>
                        <li class="active"><i class="material-icons">work</i> Employer</li>

                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

            <!-- Notification -->
            <div class="row clearfix">
                @if ($message = Session::get('success')) 
                <div class="alert bg-green alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @elseif($message = Session::get('delete'))
                <div class="alert bg-pink alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @elseif($message = Session::get('update'))
                <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @endif
            </div>
            <!-- End of Notif -->

            @can('add_setupmodules')
           	<div class="row clearfix demo-button-sizes"><!-- Button tambah -->
                <div class="col-md-10"></div>                
                <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2">
                    <a href="{{ route('emp.create') }}" type="submit" class="btn btn-success btn-block btn-lg waves-effect" style="cursor:pointer;">
                      Add
                    </a>
                </div>
            </div> <!-- End of button tambah -->
            @endcan

            <div class="row clearfix">
        		<div class="card">
        			<div class="header bg-red">
        				Employer
	        		</div>
	        		<div class="body">
	        			<table class="table" id="data-table">
                            <thead>
                                <tr>
                                    <th width="5px">#</th>
                                    <th width="20px">Employer Code</th>
                                    <th width="30px">Employer</th>
                                    <th width="30px">Status</th>                   
                                    <th width="5px" align="center">Action</th>   
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>

                                @foreach($emp as $data)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$data -> Emp_Code}}</td>
                                    <td>{{$data -> Emp_Desc}}</td>
                                    <td>
                                        @can('edit_setupmodules')
                                        <div class="switch">
                                            @if($data->Act == 0)
                                            <label>Non Act<a href="{{url('/employer/update/'.$data->id)}}" class="lever switch-col-red" onclick="alert('Employer will be anable!');"></a>Active</label>
                                            @else
                                            <label>Non Act<a href="{{url('/employer/update1/'.$data->id)}}" onclick="alert('Employer will be disable!');">
                                                <input type="checkbox" id="aksi" name="status" checked><span class="lever switch-col-red"></span></a>Active</label>
                                            @endif
                                        </div>
                                        @endcan
                                    </td>
                                    
                                    <td align="center">
                                        @include('shared._actions', [
                                            'entity' => 'emp',
                                            'id' => $data->id
                                        ])
                                     </td>
                                </tr>
                                @endforeach

    
                            </tbody>
                        </table>
	        		</div>
        		</div>
        	</div>
    
    	</div>
    </section>

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>

    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

    <script>
        $(document).ready(function() {
        $('#data-table').DataTable();
        });
    </script>
 @foreach($emp as $datas)
    <script type="text/javascript">
        $(document).ready(function(){
        $('#aksi{{$datas->id}}').change(function() {
        var returnVal = confirm("Are you sure?"); 
            if(returnVal){
              postToServer($(this).prop("checked"))
            }else{
              $(this).prop("checked", !$(this).is(":checked"));
            }       
        });
    });

    function postToServer(state){
        let value = (state) ? 1 : 0;
        var id = $('#ids').val();
        alert('Posted Value: ' + value);
        $.ajax({
          type: "POST",
          url: "{{url('/')}}/emp/show/"+id,
          data: {
            "_token": "{{ csrf_token() }}",
            "status": value,
            "id": id,
        },
          success: function(response){
            //handle response
          }
        });

    }
    </script>
    @endforeach
@endsection