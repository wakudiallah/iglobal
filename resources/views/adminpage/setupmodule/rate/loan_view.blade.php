@extends('vadmin.tampilan')

@section('content')

	<section class="content">
        <div class="container-fluid">
            
            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">text_fields</i> Attribute</a></li>
                        <li class="active"><i class="material-icons">domain</i>Rate</li>
                        
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

            <!-- Notification -->
            <div class="row clearfix">
                @if ($message = Session::get('success')) 
                <div class="alert bg-green alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @elseif($message = Session::get('delete'))
                <div class="alert bg-pink alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @elseif($message = Session::get('update'))
                <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @endif
            </div>
            <!-- End of Notif -->
             <div class="row clearfix">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="card">
                        <div class="header bg-red">
                           <h2>Rate</h2>
                        </div>

                        <div class="body">
                            <div class="table-responsive">

                               <a href='' data-toggle='modal' data-target='#addparameter' class='btn btn-success'><i class='fa fa-bank'></i> Add Parameter</a>
                                <div class="modal fade" id="addparameter" tabindex="-1" role="dialog" aria-labelledby="addparameter" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                    &times;
                                                </button>
                                                <h4 class="modal-title" id="addparameter">Add parameter</h4>
                                            </div>
                                            <div class="modal-body">
                                              <form method="POST"  id="smart-form-register3" class="smart-form client-form" action="{{ url('admin/addparameter')}}">
                                              {{ csrf_field() }}
                                              <fieldset>
                                               
                                <div class="body">
                                   

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <h2 class="card-inside-title">Job Type</h2>

                                            <select class="form-control show-tick js-example-basic-single js-states" data-live-search="true" name="emp">
                                                <option value="W">All </option>
                                                @foreach($employment as $data) 
                                                     <option value="{{$data->id}}">{{$data->name}} </option>
                                                @endforeach 
                                            </select>

                                        </div>

                                    </div>

                                   <div class="col-sm-12">
                                        <div class="form-group">
                                            <h2 class="card-inside-title">Package</h2>

                                            <select class="form-control show-tick js-example-basic-single js-states" data-live-search="true" name="pkg">
                                                
                                                @foreach($pkg as $data) 
                                                     <option value="{{$data->id}}">{{$data->Ln_Desc}} </option>
                                                @endforeach 
                                            </select>
                                        </div>
                                    </div>



                                  

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <button type="submit" name="submit" class="btn btn-lg bg-green">
                                                <i class="material-icons">visibility</i> &nbsp; Generate &nbsp;
                                            </button>
                                            <input type="hidden" name="_token" value="{{csrf_token()}}">

                                            {!! Form::close() !!} 

                                        </div>

                                    </div>
                                                           
                                                   </form> 
                                            </div>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->
                            
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
        	<div class="row clearfix demo-button-sizes"><!-- Button tambah -->
                <div class="col-md-10"></div>
                
                <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2">
                    <a href="{{ route('rate.create') }}" type="submit" class="btn btn-success btn-block btn-lg waves-effect" style="cursor:pointer;">
                      Add
                    </a>
                </div>
            </div> <!-- End of button tambah -->

        	<div class="row clearfix">
        		<div class="card">
        			<div class="header bg-red">
        				Rate
	        		</div>
	        		<div class="body">
	        			<table class="table" id="data-table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>ID Loan</th>
                                    <th>Emp Type</th>
                                    <th>Loan Package</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>

                                @foreach($loan as $loan)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$loan->id}}</td>
                                    <td>{{$loan->types->name}}</td>
                                    <td>{{$loan->packages->Ln_Desc}}</td>
                                    <td>
                                        @include('shared._actions', [
                                            'entity' => 'rate',
                                            'id' => $loan->id
                                        ])
                                     </td>
                                </tr>
                                @endforeach

    
                            </tbody>
                        </table>
	        		</div>
        		</div>
        	</div>

        </div>
    </section>

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>

    <script>
        $(document).ready(function() {
        $('#data-table').DataTable();
        });
    </script>

@endsection