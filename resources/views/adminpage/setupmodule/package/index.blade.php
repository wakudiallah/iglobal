@extends('vadmin.tampilan')

@section('content')
	<section class="content">
        <div class="container-fluid">
            
            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">text_fields</i> Attribute</a></li>
                        <li class="active"><i class="material-icons">bookmarks</i> Packages</li>
                        
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

            <div class="row clearfix demo-button-sizes"><!-- Button tambah -->
                <div class="col-md-10"></div>
                
                <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2">
                    <a href="{{ route('package.create') }}" type="submit" class="btn btn-success btn-block btn-lg waves-effect" style="cursor:pointer;">
                      Add
                    </a>
                </div>
            </div> <!-- End of button tambah -->

            <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header bg-red">
                            Packages
                        </div>
                        <div class="body">
                            <table class="table" id="data-table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name Packages</th>
                                        <th width="20%">Aksi</th>            
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>

                                    @foreach($package as $data)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$data -> name}}</td>
                                        <td>
                                            @include('shared._actions', [
                                                'entity' => 'package',
                                                'id' => $data->id
                                            ])
                                         </td>
                                    </tr>
                                    @endforeach

        
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>

    <script>
        $(document).ready(function() {
        $('#data-table').DataTable();
        });
    </script>
@endsection