

    <div class="col-sm-12">
	    <h5>Kod Loan Pakej:</h5>
	    <div class="form-group form-float @if ($errors->has('LnPkg_Code')) has-error @endif">
	        <div class="form-line">
	            
	            {!! Form::label('title', 'Kod Loan Pakej', ['class' => 'form-label']) !!}
	            {!! Form::text('LnPkg_Code', null, ['class' => 'form-control']) !!}
	            @if ($errors->has('LnPkg_Code')) <p class="help-block">{{ $errors->first('LnPkg_Code') }}</p> @endif
	            
	        </div>
	    </div>
	</div>

	<div class="col-sm-12">
	    <h5>Pakej Deskripsi:</h5>
	    <div class="form-group form-float @if ($errors->has('Ln_Desc')) has-error @endif">
	        <div class="form-line">
	            
	            {!! Form::label('title', 'Pakej Deskripsi', ['class' => 'form-label']) !!}
	            {!! Form::text('Ln_Desc', null, ['class' => 'form-control']) !!}
	            @if ($errors->has('Ln_Desc')) <p class="help-block">{{ $errors->first('Ln_Desc') }}</p> @endif
	            
	        </div>
	    </div>
	</div>

	<div class="col-sm-6">
		<h5>Min Ten:</h5>
		<div class="form-group form-float @if ($errors->has('Min_Ten')) has-error @endif">
			<div class="form-line">
				{!! Form::label('title', 'Min Ten', ['class' => 'form-label']) !!}
	            {!! Form::text('Min_Ten', null, ['class' => 'form-control']) !!}
	            @if ($errors->has('Min_Ten')) <p class="help-block">{{ $errors->first('Min_Ten') }}</p> @endif
			</div>
		</div>
	</div>
	<div class="col-sm-6">
		<h5>Max Ten:</h5>
		<div class="form-group form-float @if ($errors->has('Max_Ten')) has-error @endif">
			<div class="form-line">
				{!! Form::label('title', 'Max Ten', ['class' => 'form-label']) !!}
	            {!! Form::text('Max_Ten', null, ['class' => 'form-control']) !!}
	            @if ($errors->has('Max_Ten')) <p class="help-block">{{ $errors->first('Max_Ten') }}</p> @endif
			</div>
		</div>
	</div>

	<div class="col-sm-6">
		<h5>Min Amt:</h5>
		<div class="form-group form-float @if ($errors->has('Mln_amt')) has-error @endif">
			<div class="form-line">
				{!! Form::label('title', 'Min Ten', ['class' => 'form-label']) !!}
	            {!! Form::text('Mln_amt', null, ['class' => 'form-control']) !!}
	            @if ($errors->has('Mln_amt')) <p class="help-block">{{ $errors->first('Mln_amt') }}</p> @endif
			</div>
			<div class="help-info">Decimal xx.xx</div>
		</div>
	</div>
	<div class="col-sm-6">
		<h5>Max Amt:</h5>
		<div class="form-group form-float @if ($errors->has('Max_amt')) has-error @endif">
			<div class="form-line">
				{!! Form::label('title', 'Max Amt', ['class' => 'form-label']) !!}
	            {!! Form::text('Max_amt', null, ['class' => 'form-control']) !!}
	            @if ($errors->has('Max_amt')) <p class="help-block">{{ $errors->first('Max_amt') }}</p> @endif
			</div>
			<div class="help-info">Decimal xx.xx</div>
		</div>
	</div>
	

	<div class="col-sm-12">
	    <h5>Min Service:</h5>
	    <div class="form-group form-float @if ($errors->has('Min_Service')) has-error @endif">
	        <div class="form-line">
	            
	            {!! Form::label('title', 'Min Service', ['class' => 'form-label']) !!}
	            {!! Form::text('Min_Service', null, ['class' => 'form-control']) !!}
	            @if ($errors->has('Min_Service')) <p class="help-block">{{ $errors->first('Min_Service') }}</p> @endif
	            
	        </div>
	    </div>
	</div>

	<div class="col-sm-6">
		<h5>Min Age:</h5>
		<div class="form-group form-float @if ($errors->has('Min_Age')) has-error @endif">
			<div class="form-line">
				{!! Form::label('title', 'Min Age', ['class' => 'form-label']) !!}
	            {!! Form::text('Min_Age', null, ['class' => 'form-control']) !!}
	            @if ($errors->has('Min_Age')) <p class="help-block">{{ $errors->first('Min_Age') }}</p> @endif
			</div>
		</div>
	</div>
	<div class="col-sm-6">
		<h5>Max Age:</h5>
		<div class="form-group form-float @if ($errors->has('Max_Age')) has-error @endif">
			<div class="form-line">
				{!! Form::label('title', 'Max Age', ['class' => 'form-label']) !!}
	            {!! Form::text('Max_Age', null, ['class' => 'form-control']) !!}
	            @if ($errors->has('Max_Age')) <p class="help-block">{{ $errors->first('Max_Age') }}</p> @endif
			</div>
		</div>
	</div>

	<div class="col-sm-12">
		<h5>Loan Int:</h5>
		<div class="form-group form-float @if ($errors->has('Ln_Int')) has-error @endif">
			<div class="form-line">
				{!! Form::label('title', 'Loan Int', ['class' => 'form-label']) !!}
	            {!! Form::text('Ln_Int', null, ['class' => 'form-control']) !!}
	            @if ($errors->has('Ln_Int')) <p class="help-block">{{ $errors->first('Ln_Int') }}</p> @endif
			</div>
			<div class="help-info">Decimal xx.xx</div>
		</div>
	</div>
