
	<div class="col-sm-12">
	    <h5>Package:</h5>
	    <div class="form-group form-float @if ($errors->has('Ln_Desc')) has-error @endif">
	        <div class="form-line">
	            
	            {!! Form::label('title', 'Package', ['class' => 'form-label']) !!}
	            {!! Form::text('Ln_Desc', null, ['class' => 'form-control']) !!}
	            @if ($errors->has('Ln_Desc')) <p class="help-block">{{ $errors->first('Ln_Desc') }}</p> @endif
	            
	        </div>
	    </div>
	</div>

	
