@extends('vadmin.tampilan')

@section('content')

	<section class="content">
        <div class="container-fluid">
            
            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">text_fields</i> Attribute</a></li>
                        <li class="active"><i class="material-icons">domain</i>Loan Package</li>
                        
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

            <!-- Notification -->
            <div class="row clearfix">
                @if ($message = Session::get('success')) 
                <div class="alert bg-green alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @elseif($message = Session::get('delete'))
                <div class="alert bg-pink alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @elseif($message = Session::get('update'))
                <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @endif
            </div>
            <!-- End of Notif -->

            @can('add_setupmodules')
        	<div class="row clearfix demo-button-sizes"><!-- Button tambah -->
                <div class="col-md-10"></div>
                
                <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2">
                    <a href="{{ route('loanpkg.create') }}" type="submit" class="btn btn-success btn-block btn-lg waves-effect" style="cursor:pointer;">
                      Add
                    </a>
                </div>
            </div> <!-- End of button tambah -->
            @endcan

        	<div class="row clearfix">
        		<div class="card">
        			<div class="header bg-red">
        				Loan Package
	        		</div>
	        		<div class="body">
	        			<table class="table" id="data-table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <!-- <th>Loan Package Code</th>-->
                                    
                                    <th>Loan Desc</th>
                                    <!--<th>Min Ten</th>
                                    <th>Max Ten</th>
                                    <th>Mln Amt</th>
                                    <th>Max Amt</th>
                                    <th>Min Service</th>
                                    <th>Min Age</th>
                                    <th>Max Age</th>
                                    <th>Ln Int</th> -->
                                    <th>Act</th> 
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>

                                @foreach($package as $data)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <!--<td>{{$data -> LnPkg_Code}}</td>-->
                                     <td>{{$data -> Ln_Desc}}</td>
                                    <!--<td>{{$data -> Min_Ten}}</td>
                                    <td>{{$data -> Max_Ten}}</td>
                                    <td>{{$data -> Mln_amt}}</td>
                                    <td>{{$data -> Max_amt}}</td>
                                    <td>{{$data -> Min_Service}}</td>
                                    <td>{{$data -> Min_Age}}</td>
                                    <td>{{$data -> Max_Age}}</td>
                                    <td>{{$data -> Ln_Int}}</td> -->
                                    <td>
                                        @can('edit_setupmodules')
                                        <div class="switch">
                                            @if($data->Act == 0)
                                            <label>Non Act<a href="{{url('/loanpackage/update/'.$data->id)}}" class="lever switch-col-red" ></a>Active</label>
                                            @else
                                            <label>Non Act<a href="{{url('/loanpackage/update1/'.$data->id)}}" >
                                                <input type="checkbox" id="aksi" name="status" checked><span class="lever switch-col-red"></span></a>Active</label>
                                            @endif
                                        </div>
                                        @endcan
                                    </td>
                                    <td>
                                        @include('shared._actions', [
                                            'entity' => 'loanpkg',
                                            'id' => $data->id
                                        ])
                                     </td>
                                </tr>
                                @endforeach

    
                            </tbody>
                        </table>
	        		</div>
        		</div>
        	</div>

        </div>
    </section>

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>

    <script>
        $(document).ready(function() {
        $('#data-table').DataTable();
        });
    </script>

@endsection