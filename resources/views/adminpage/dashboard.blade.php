@extends('vadmin.tampilan')

@section('content')

    <section class="content">
        <div class="container-fluid">

            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">home</i> Dashboard</a></li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

            <!-- Running Text -->
                <div class="info-box-3 bg-red hover-zoom-effect">
                    <div class="icon">
                        <i class="material-icons">email</i>
                    </div>
                    <div class="content">
                        <div class="headline-text">
                            @if(empty($announc->id))
                                <marquee scrollamount="5" width="1000px" onmouseover="stop();" onmouseout="start();" ></marquee>
                            @else
                                <marquee scrollamount="5" width="1000px" onmouseover="stop();" onmouseout="start();" ><a href="{{url('/announc/show/'.$announc->id)}}" style="color:white; font-size: 20px; line-height: 60px !important; text-decoration: none;" onMouseOver="this.style.color='#a39999'" onMouseOut="this.style.color='#FFF'" > {{$announc->title}} </a></marquee>
                            @endif
                        </div>
                    </div>
                    
                </div> <!-- End of running text -->
                
        <!-- =====================  Task Admin  ================== -->
            
            @if( $roless == '1') 
            <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-pink hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">forum</i>
                        </div>

                        <div class="content">
                            <div class="text">Assessment</div>
                            <div class="number count-to" data-from="0" data-to="{{ $count_submission }}" data-speed="15" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">person_add</i>
                        </div>
                        <div class="content">
                            <div class="text">Submission</div>
                            <div class="number count-to" data-from="0" data-to="{{ $countsub }}" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">playlist_add_check</i>
                        </div>
                        <div class="content">
                            <div class="text">Disbursement</div>
                            <div class="number count-to" data-from="0" data-to="0" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-orange hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">help</i>
                        </div>
                        <div class="content">
                            <div class="text">Pending</div>
                            <div class="number count-to" data-from="0" data-to="0" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
            </div>  
        <!-- ===================== End Task Admin ==================== -->

            
        <!-- =====================  Task MO  ================== -->    
            @elseif($roless=='3')

            <!-- Running Text Per Manager -->
            <div class="info-box-3 bg-red hover-zoom-effect">
                <div class="icon">
                    <i class="material-icons">email</i>
                </div>
                <div class="content">
                    <div class="headline-text">
                        @if(empty($announcgroup->id))
                            <marquee scrollamount="5" width="1000px" onmouseover="stop();" onmouseout="start();" ></marquee>
                        @else
                            <marquee scrollamount="5" width="1000px" onmouseover="stop();" onmouseout="start();" ><a href="{{url('/announcgroups/show/'.$announcgroup->id)}}" style="color:white; font-size: 20px; line-height: 60px !important; text-decoration: none;" onMouseOver="this.style.color='#a39999'" onMouseOut="this.style.color='#FFF'" > {{$announcgroup->title}} </a></marquee>
                        @endif
                    </div>
                </div>
                
            </div> <!-- End of running text -->


            <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-pink hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">person_add</i>
                        </div>

                        <div class="content">
                            <div class="text">Submission</div>
                            <div class="number count-to" data-from="0" data-to="{{ $count_submission }}" data-speed="15" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" onclick="location.href='{{url('mo_cal_check')}}';">
                    <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">work</i>
                        </div>
                        <div class="content">
                            <div class="text">Loan Eligibility & Scoring</div>
                            <div class="number count-to" data-from="0" data-to="{{ $countmo_calculation }}" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" onclick="location.href='{{url('nextcalchange')}}';">
                    <div class="info-box bg-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">sync_problem</i>
                        </div>
                        <div class="content">
                            <div class="text">Calculation & Change</div>
                            <div class="number count-to" data-from="0" data-to="{{ $countmo_calculation_n_change }}" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" onclick="location.href='{{url('pendingdoc')}}';" >
                    <div class="info-box bg-lime hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">hourglass_empty</i>
                        </div>
                        <div class="content">
                            <div class="text">Pending Documentation </div>
                            <div class="number count-to" data-from="0" data-to={{$count_pending_doc}} data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-purple hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">update</i>
                        </div>
                        <div class="content">
                            <div class="text">Recommend New Submission</div>
                            <div class="number count-to" data-from="0" data-to={{$count_recommend_new_sub}} data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" onclick="location.href='{{url('docincomplete_menu')}}';" >
                    <div class="info-box bg-orange hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">cancel</i>
                        </div>
                        <div class="content">
                            <div class="text">Doc Incomplete </div>
                            <div class="number count-to" data-from="0" data-to={{$count_doc_incomplete}} data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" onclick="location.href='{{url('additional_doc')}}';" >
                    <div class="info-box bg-indigo hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">help</i>
                        </div>
                        <div class="content">
                            <div class="text">Additional Doc </div>
                            <div class="number count-to" data-from="0" data-to={{$count_add_doc}} data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">local_offer</i>
                        </div>
                        <div class="content">
                            <div class="text">Approved</div>
                            <div class="number count-to" data-from="0" data-to="{{$count_disbursement}}" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>

            </div>


            <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Task list</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos" id="example">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>IC</th>
                                            <th>Phone</th>
                                            <th>Date</th>
                                            <th class="hidden">Diff hari</th>
                                            <th>Status</th>
                                            <th>Activity</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php $i = 1; ?>

                                        @foreach($tasklist_mo as $data)

                                            <?php 
                                                /*$date_rev =  date('Y-m-d H:i:s ', strtotime($data->created_at));
                                                $today    =  date('Y-m-d H:i:s');
                                                $to_time = strtotime($today);
                                                $from_time = strtotime($date_rev);
                                                $di= ($to_time - $from_time) / (60*60);*/ //selisih 3 jam

                                                $key_in = strtotime($data->created_at);
                                                $sekarang      = time(); // Waktu sekarang
                                                $diff          = $sekarang - $key_in;
                                                $hari          = floor($diff / (60 * 60 * 24));
                                            ?> 

                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{$data->name}}</td>
                                            <td>{{$data->ic}} </td>
                                            <td>{{$data->notelp}}</td>

                                            <?php
                                                 
                                                if(!empty($data->date_w2->last()->id)) {
                                               
                                                    foreach ($data->date_w2 as $log) {  

                                                        if($log->remark_id=='W2'){
                                                            $last_act_w2 = $log->created_at ;   
                                                             $cs = $log->cus_id ;    
                                                        }
                                                        
                                                    }
                                                } 
                                                if(!empty($data->date_w3->last()->id)) {
                                               
                                                    foreach ($data->date_w3 as $log) {  

                                                        if($log->remark_id=='W3'){
                                                            $last_act_w3 = $log->created_at ;      
                                                        }
                                                        
                                                    }
                                                }
                                                
                                                if(!empty($data->date_w6->last()->id)) {
                                               
                                                    foreach ($data->date_w6 as $log) {  

                                                        if($log->remark_id=='W6'){
                                                            $last_act_w6 = $log->created_at ;      
                                                        }
                                                        
                                                    }
                                                }
                                                
                                                if(!empty($data->date_w7->last()->id)) {
                                               
                                                    foreach ($data->date_w7 as $log) {  

                                                        if($log->remark_id=='W7'){
                                                            $last_act_w7 = $log->created_at ;      
                                                        }
                                                        
                                                    }
                                                } 
                                                
                                                if(!empty($data->date_w13->last()->id)) {
                                               
                                                    foreach ($data->date_w13 as $log) {  

                                                        if($log->remark_id=='W13'){
                                                            $last_act_w13 = $log->created_at ;      
                                                        }
                                                        
                                                    }
                                                }

                                                if(!empty($data->date_w14->last()->id)) {
                                               
                                                    foreach ($data->date_w14 as $log) {  

                                                        if($log->remark_id=='W14'){
                                                            $last_act_w14 = $log->created_at ;      
                                                        }
                                                        
                                                    }
                                                }


                                                if($data->stage == 'W15')  {   
                                                    if(!empty($data->date_w15->last()->cus_id)) {
                                                   
                                                        foreach ($data->date_w15 as $log) {
                                                         
                                                                $last_act_w15 = $log->created_at ;       
                                                            
                                                        }
                                                    }
                                                }
                                            ?>

                                            <td> 
                                                <?php
                                                    $s0 = $data->stage == 'W0';
                                                    $s2 = $data->stage == 'W2';
                                                    $s3 = $data->stage == 'W3';
                                                    $s6 = $data->stage == 'W6';
                                                    $s7 = $data->stage == 'W7';
                                                    $s13 = $data->stage == 'W13';
                                                    $s14 = $data->stage == 'W14';
                                                    $s15 = $data->stage == 'W15';
                                                 ?>

                                                @if($s2 || $s3 || $s6 || $s7 || $s13 || $s14 || $s15)
                                                    @if($s2) 
                                                        @if(!empty($data->date_w2->last()->id))
                                                            {{$last_act_w2->toDayDateTimeString()}}
                                                        @else
                                                            {{$data->created_at->toDayDateTimeString()}}
                                                        @endif

                                                    @elseif($s3) 
                                                        @if(!empty($data->date_w3->last()->id))
                                                            {{$last_act_w3->toDayDateTimeString()}}
                                                        @else
                                                            {{$data->created_at->toDayDateTimeString()}}
                                                        @endif

                                                    @elseif($s6) 
                                                        @if(!empty($data->date_w6->last()->id))
                                                            {{$last_act_w6->toDayDateTimeString()}}
                                                        @else
                                                            {{$data->created_at->toDayDateTimeString()}}
                                                        @endif

                                                    @elseif($s7) 
                                                        @if(!empty($data->date_w7->last()->id))
                                                            {{$last_act_w7->toDayDateTimeString()}}
                                                        @else
                                                            {{$data->created_at->toDayDateTimeString()}}
                                                        @endif

                                                    @elseif($s13) 
                                                        @if(!empty($data->date_w13->last()->id))
                                                            {{$last_act_w13->toDayDateTimeString()}}
                                                        @else
                                                            {{$data->created_at->toDayDateTimeString()}}
                                                        @endif

                                                    @elseif($s14) 
                                                        @if(!empty($data->date_w14->last()->id))
                                                            {{$last_act_w14->toDayDateTimeString()}}
                                                        @else
                                                            {{$data->created_at->toDayDateTimeString()}}
                                                        @endif

                                                    @elseif($s15) 
                                                        @if(!empty($data->date_w15->last()->id))
                                                            {{$last_act_w15->toDayDateTimeString()}}
                                                        @else
                                                            {{$data->created_at->toDayDateTimeString()}}
                                                        @endif
                                                    @endif
                                                @else

                                                    {{$data->created_at->toDayDateTimeString()}}  <!-- kecuali date_create di tb pra -->
                                                @endif

                                            </td> 

                                             

                                            <td class="hidden">
                                                @include('shared.diff_mo') 
                                            </td>

                                            <td>
                                                <span class="label bg-{{$data->stages->color}}">{{$data->stages->desc}}</span>
                                            </td>
                                            <td class="text-center">
                                                <a href="{{url('/history/show/'.$data->id_cus)}}" class="btn bg-orange btn-circle waves-effect waves-circle waves-float" onclick="window.open('{{url('/history/show/'.$data->id_cus)}}', 'newwindow', 'width=750,height=400'); return false;"> <i class="material-icons">history</i> </a>
                                            </td>
                                            
                                            <td>
                                                @if($data->stage == 'W2')
                                                <a href="{{url('/loan/eli/scoring/'.$data->id_cus)}}" class="btn btn-sm btn-primary"><i class="material-icons">work</i>Meet Customer  </a>

                                                @elseif($data->stage == 'W0')
                                                <a href="{{ url('view_form/'.$data->id_cus.'/first_step') }}" class="btn btn-sm bg-pink"><i class="material-icons">pageview</i> View Form </a>

                                                @elseif($data->stage == 'W3')
                                                <a href="{{ url('view_form/'.$data->id_cus.'/second_step') }}" class="btn btn-sm bg-green"><i class="material-icons">pageview</i> View Loan Cal </a>

                                                @elseif($data->stage == 'W6')
                                                <a href="{{url('/cal/change/'.$data->id_cus)}}" class="btn btn-sm btn-success"><i class="material-icons">sync_problem</i>Calculation & Change  </a>
                                                @elseif($data->stage == 'W7')
                                                <a href="{{url('/doc/incomplete/'.$data->id_cus)}}" class="btn btn-sm btn-warning"><i class="material-icons">cancel</i> Doc Incomplete  </a>
                                                @elseif($data->stage == 'W13')
                                                <!-- <a href="{{url('/doc/incomplete/'.$data->id_cus)}}" class="btn btn-sm bg-indigo"><i class="material-icons">help</i>Additional Doc Req  </a> -->
                                                @elseif($data->stage == 'W14')
                                                <a href="{{url('/doc/icloc/'.$data->id_cus)}}" class="btn btn-sm btn-warning"><i class="material-icons">description</i>Doc LOC / IC </a>
                                                @elseif($data->stage == 'W15')
                                                <a href="{{url('/doc/icloc/'.$data->id_cus)}}" class="btn btn-sm bg-purple"><i class="material-icons">updated</i>Recommend New Submission </a>
                                                @elseif($data->stage == 'W11')
                                                <a href="{{ url('view_form/'.$data->id_cus.'/fourth_step') }}" class="btn btn-sm bg-green"><i class="material-icons">pageview</i> View Loan </a> 
                                                @endif
                                            </td> 


                                            
                                        </tr>
                                        @endforeach
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
            </div>  
        <!-- ===================== End Task MO ==================== -->


        <!-- =====================  Task Manager  ================== -->
            @elseif($roless=='5') 

            <!-- Running Text Per Manager -->
            <div class="info-box-3 bg-red hover-zoom-effect">
                <div class="icon">
                    <i class="material-icons">email</i>
                </div>
                <div class="content">
                    <div class="headline-text">
                        @if(empty($announcgroup->id))
                            <marquee scrollamount="5" width="1000px" onmouseover="stop();" onmouseout="start();" ></marquee>
                        @else
                            <marquee scrollamount="5" width="1000px" onmouseover="stop();" onmouseout="start();" ><a href="{{url('/announcgroups/show/'.$announcgroup->id)}}" style="color:white; font-size: 20px; line-height: 60px !important; text-decoration: none;" onMouseOver="this.style.color='#a39999'" onMouseOut="this.style.color='#FFF'" > {{$announcgroup->title}} </a></marquee>
                        @endif
                    </div>
                </div>
                
            </div> <!-- End of running text -->


            <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="info-box bg-pink hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">person_add</i>
                        </div>
                        <div class="content">
                            <div class="text">Submission</div>
                            <div class="number count-to" data-from="0" data-to="{{ $countsub_manager }}" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" onclick="location.href='{{url('report-manager-failed-bymo')}}';" style="cursor:pointer;" >
                    <div class="info-box bg-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">close</i>
                        </div>

                        <div class="content">
                            <div class="text">Failed By MO</div>
                            <div class="number count-to" data-from="0" data-to="{{ $count_failed_mo }}" data-speed="15" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" onclick="location.href='{{url('report-manager-rejected')}}';" style="cursor:pointer;" >
                    <div class="info-box bg-red hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">help</i>
                        </div>
                        <div class="content">
                            <div class="text">Rejected</div>
                            <div class="number count-to" data-from="0" data-to="{{$count_rejected}}" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="info-box bg-orange hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">playlist_add_check</i>
                        </div>
                        <div class="content">
                            <div class="text">Approved</div>
                            <div class="number count-to" data-from="0" data-to="{{$count_approved}}" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                
                
            </div>


            <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Task list</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos" id="example">
                                    <thead>
                                        <tr>
                                            <th class="hidden"></th>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>IC</th>
                                            <th>Date</th>
                                            <th>Status</th>
                                            <th>Activity</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php $i = 1; ?>

                                        @foreach($tasklist_manager as $data)
                                            <?php 
                                                $date_rev =  date('Y-m-d H:i:s ', strtotime($data->created_at));
                                                $today    =  date('Y-m-d H:i:s');
                                                $to_time = strtotime($today);
                                                $from_time = strtotime($date_rev);
                                                $di= ($to_time - $from_time) / (60*60);
                                            ?> 

                                        <tr>
                                            <td class="hidden">{{number_format($di,0,'.','.')}}</td>
                                            <td>{{$i++}}</td>
                                            <td>{{$data->name}}</td>
                                            <td>{{$data->ic}}</td>
                                            
                                            <?php
                                                 
                                                if(!empty($data->date_w5->last()->id)) {
                                               
                                                    foreach ($data->date_w5 as $log) {  

                                                        if($log->remark_id=='W5'){
                                                            $last_act_w5 = $log->created_at ; 
                                                            $note = $log->note ; 
                                                        }
                                                        
                                                    }
                                                } 
                                                if(!empty($data->date_w12->last()->id)) {
                                               
                                                    foreach ($data->date_w12 as $log) {  

                                                        if($log->remark_id=='W12'){
                                                            $last_act_w12 = $log->created_at ; 
                                                            $note = $log->note ; 
                                                        }
                                                        
                                                    }
                                                } 
                                                if(!empty($data->date_w11->last()->id)) {
                                               
                                                    foreach ($data->date_w11 as $log) {  

                                                        if($log->remark_id=='W11'){
                                                            $last_act_w11 = $log->created_at ; 
                                                            $note = $log->note ; 
                                                        }
                                                        
                                                    }
                                                }
                                                
                                            ?>

                                            <?php
                                                $s5 = $data->stage == 'W5';
                                                $s12 = $data->stage == 'W12';
                                                $s11 = $data->stage == 'W11';
                                             ?> 
                                            <td>
                                                @if($s5) 
                                                    @if(!empty($s5))
                                                        {{$last_act_w5->toDayDateTimeString()}}
                                                    @endif

                                                @elseif($s12) 
                                                    @if(!empty($s12))
                                                        {{$last_act_w12->toDayDateTimeString()}}
                                                    @endif

                                                 @elseif($s11) 
                                                    @if(!empty($s11))
                                                        {{$last_act_w11->toDayDateTimeString()}}
                                                    @endif 

                                                @endif
                                            </td>
                                            <td>
                                                <span class="label bg-{{$data->stages->color}}">{{$data->stages->desc}}</span>
                                            </td>
                                            
                                            
                                            <td class="text-center">
                                                <a href="{{url('/history/show/'.$data->id_cus)}}" class="btn bg-orange btn-circle waves-effect waves-circle waves-float" onclick="window.open('{{url('/history/show/'.$data->id_cus)}}', 'newwindow', 'width=750,height=400'); return false;"> <i class="material-icons">history</i> </a>
                                            </td>
                                            <td>
                                                @if($data->stage == 'W5')
                                                    <a href="{{ url('make-new-cus-detail/'.$data->id_cus) }}" class="btn btn-sm bg-orange"><i class="material-icons">border_color</i> Recommend new submission  </a>
                                                    <!-- <a href="{{ url('view_form/'.$data->id_cus.'/first_step') }}" class="btn btn-sm bg-pink"><i class="material-icons">pageview</i> Recommend new submission Beneri </a>-->
                                                @elseif($data->stage == 'W12')
                                                   <a href="{{ url('detail_processor2/'.$data->id_cus) }}" class="btn btn-sm bg-green"><i class="material-icons">pageview</i> View Loan  </a>
                                                @elseif($data->stage == 'W11')
                                                    <a href="{{ url('view_form/'.$data->id_cus.'/fourth_step') }}" class="btn btn-sm bg-green"><i class="material-icons">pageview</i> View Loan </a>
                                                @endif
                                            </td>
                                               
                                        </tr>
                                        @endforeach
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->                
            </div>
        <!-- =====================  End Task Manager  ================== -->

        <!-- ======================= Team Lead =================== -->
            
            @elseif($roless=='12')

            <!-- Running Text Per Manager -->
            <div class="info-box-3 bg-red hover-zoom-effect">
                <div class="icon">
                    <i class="material-icons">email</i>
                </div>
                <div class="content">
                    <div class="headline-text">
                        @if(empty($announcgroup->id))
                            <marquee scrollamount="5" width="1000px" onmouseover="stop();" onmouseout="start();" ></marquee>
                        @else
                            <marquee scrollamount="5" width="1000px" onmouseover="stop();" onmouseout="start();" ><a href="{{url('/announcgroups/show/'.$announcgroup->id)}}" style="color:white; font-size: 20px; line-height: 60px !important; text-decoration: none;" onMouseOver="this.style.color='#a39999'" onMouseOut="this.style.color='#FFF'" > {{$announcgroup->title}} </a></marquee>
                        @endif
                    </div>
                </div>
                
            </div> <!-- End of running text -->

            <div class="row clearfix">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" onclick="location.href='{{url('submission-team-lead')}}';" style="cursor:pointer;"> 
                    <div class="info-box bg-pink hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">person_add</i>
                        </div>
                        <div class="content">
                            <div class="text">Submission</div>
                            <div class="number count-to" data-from="0" data-to="{{ $count_submission_tl }}" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" onclick="location.href='{{url('rejected-team-lead')}}';" style="cursor:pointer;" >
                    <div class="info-box bg-red hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">help</i>
                        </div>
                        <div class="content">
                            <div class="text">Rejected</div>
                            <div class="number count-to" data-from="0" data-to="{{$count_rejected_tl}}" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" onclick="location.href='{{url('approved-team-lead')}}';" style="cursor:pointer;">
                    <div class="info-box bg-orange hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">playlist_add_check</i>
                        </div>
                        <div class="content">
                            <div class="text">Approved</div>
                            <div class="number count-to" data-from="0" data-to="{{$count_approved_tl}}" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                
                
            </div>


            <div class="row clearfix">
                
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" onclick="location.href='{{url('mo_cal_check')}}';">
                    <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">work</i>
                        </div>
                        <div class="content">
                            <div class="text">Loan Eligibility & Scoring</div>
                            <div class="number count-to" data-from="0" data-to="{{ $countmo_calculation }}" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" onclick="location.href='{{url('nextcalchange')}}';">
                    <div class="info-box bg-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">sync_problem</i>
                        </div>
                        <div class="content">
                            <div class="text">Calculation & Change</div>
                            <div class="number count-to" data-from="0" data-to="{{ $countmo_calculation_n_change }}" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" onclick="location.href='{{url('pendingdoc')}}';" >
                    <div class="info-box bg-lime hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">hourglass_empty</i>
                        </div>
                        <div class="content">
                            <div class="text">Pending Documentation </div>
                            <div class="number count-to" data-from="0" data-to={{$count_pending_doc}} data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-purple hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">update</i>
                        </div>
                        <div class="content">
                            <div class="text">Recommend New Submission</div>
                            <div class="number count-to" data-from="0" data-to={{$count_recommend_new_sub}} data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" onclick="location.href='{{url('docincomplete_menu')}}';" >
                    <div class="info-box bg-orange hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">cancel</i>
                        </div>
                        <div class="content">
                            <div class="text">Doc Incomplete </div>
                            <div class="number count-to" data-from="0" data-to={{$count_doc_incomplete}} data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" onclick="location.href='{{url('additional_doc')}}';" >
                    <div class="info-box bg-indigo hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">help</i>
                        </div>
                        <div class="content">
                            <div class="text">Additional Doc </div>
                            <div class="number count-to" data-from="0" data-to={{$count_add_doc}} data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                

            </div>


            <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Task list</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos" id="example">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>IC</th>
                                            <th>Phone</th>
                                            <th>Date</th>
                                            <th class="hidden">Diff hari</th>
                                            <th>Status</th>
                                            <th>Activity</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php $i = 1; ?>

                                        @foreach($tasklist_mo as $data)

                                            <?php 
                                                /*$date_rev =  date('Y-m-d H:i:s ', strtotime($data->created_at));
                                                $today    =  date('Y-m-d H:i:s');
                                                $to_time = strtotime($today);
                                                $from_time = strtotime($date_rev);
                                                $di= ($to_time - $from_time) / (60*60);*/ //selisih 3 jam

                                                $key_in = strtotime($data->created_at);
                                                $sekarang      = time(); // Waktu sekarang
                                                $diff          = $sekarang - $key_in;
                                                $hari          = floor($diff / (60 * 60 * 24));
                                            ?> 

                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{$data->name}}</td>
                                            <td>{{$data->ic}} </td>
                                            <td>{{$data->notelp}}</td>

                                            <?php
                                                 
                                                if(!empty($data->date_w2->last()->id)) {
                                               
                                                    foreach ($data->date_w2 as $log) {  

                                                        if($log->remark_id=='W2'){
                                                            $last_act_w2 = $log->created_at ;   
                                                             $cs = $log->cus_id ;    
                                                        }
                                                        
                                                    }
                                                } 
                                                if(!empty($data->date_w3->last()->id)) {
                                               
                                                    foreach ($data->date_w3 as $log) {  

                                                        if($log->remark_id=='W3'){
                                                            $last_act_w3 = $log->created_at ;      
                                                        }
                                                        
                                                    }
                                                }
                                                
                                                if(!empty($data->date_w6->last()->id)) {
                                               
                                                    foreach ($data->date_w6 as $log) {  

                                                        if($log->remark_id=='W6'){
                                                            $last_act_w6 = $log->created_at ;      
                                                        }
                                                        
                                                    }
                                                }
                                                
                                                if(!empty($data->date_w7->last()->id)) {
                                               
                                                    foreach ($data->date_w7 as $log) {  

                                                        if($log->remark_id=='W7'){
                                                            $last_act_w7 = $log->created_at ;      
                                                        }
                                                        
                                                    }
                                                } 
                                                
                                                if(!empty($data->date_w13->last()->id)) {
                                               
                                                    foreach ($data->date_w13 as $log) {  

                                                        if($log->remark_id=='W13'){
                                                            $last_act_w13 = $log->created_at ;      
                                                        }
                                                        
                                                    }
                                                }

                                                if(!empty($data->date_w14->last()->id)) {
                                               
                                                    foreach ($data->date_w14 as $log) {  

                                                        if($log->remark_id=='W14'){
                                                            $last_act_w14 = $log->created_at ;      
                                                        }
                                                        
                                                    }
                                                }


                                                if($data->stage == 'W15')  {   
                                                    if(!empty($data->date_w15->last()->cus_id)) {
                                                   
                                                        foreach ($data->date_w15 as $log) {
                                                         
                                                                $last_act_w15 = $log->created_at ;       
                                                            
                                                        }
                                                    }
                                                }
                                            ?>

                                            <td> 
                                                <?php
                                                    $s0 = $data->stage == 'W0';
                                                    $s2 = $data->stage == 'W2';
                                                    $s3 = $data->stage == 'W3';
                                                    $s6 = $data->stage == 'W6';
                                                    $s7 = $data->stage == 'W7';
                                                    $s13 = $data->stage == 'W13';
                                                    $s14 = $data->stage == 'W14';
                                                    $s15 = $data->stage == 'W15';
                                                 ?>

                                                @if($s2 || $s3 || $s6 || $s7 || $s13 || $s14 || $s15)
                                                    @if($s2) 
                                                        @if(!empty($data->date_w2->last()->id))
                                                            {{$last_act_w2->toDayDateTimeString()}}
                                                        @else
                                                            {{$data->created_at->toDayDateTimeString()}}
                                                        @endif

                                                    @elseif($s3) 
                                                        @if(!empty($data->date_w3->last()->id))
                                                            {{$last_act_w3->toDayDateTimeString()}}
                                                        @else
                                                            {{$data->created_at->toDayDateTimeString()}}
                                                        @endif

                                                    @elseif($s6) 
                                                        @if(!empty($data->date_w6->last()->id))
                                                            {{$last_act_w6->toDayDateTimeString()}}
                                                        @else
                                                            {{$data->created_at->toDayDateTimeString()}}
                                                        @endif

                                                    @elseif($s7) 
                                                        @if(!empty($data->date_w7->last()->id))
                                                            {{$last_act_w7->toDayDateTimeString()}}
                                                        @else
                                                            {{$data->created_at->toDayDateTimeString()}}
                                                        @endif

                                                    @elseif($s13) 
                                                        @if(!empty($data->date_w13->last()->id))
                                                            {{$last_act_w13->toDayDateTimeString()}}
                                                        @else
                                                            {{$data->created_at->toDayDateTimeString()}}
                                                        @endif

                                                    @elseif($s14) 
                                                        @if(!empty($data->date_w14->last()->id))
                                                            {{$last_act_w14->toDayDateTimeString()}}
                                                        @else
                                                            {{$data->created_at->toDayDateTimeString()}}
                                                        @endif

                                                    @elseif($s15) 
                                                        @if(!empty($data->date_w15->last()->id))
                                                            {{$last_act_w15->toDayDateTimeString()}}
                                                        @else
                                                            {{$data->created_at->toDayDateTimeString()}}
                                                        @endif
                                                    @endif
                                                @else

                                                    {{$data->created_at->toDayDateTimeString()}}  <!-- kecuali date_create di tb pra -->
                                                @endif

                                            </td> 

                                             

                                            <td class="hidden">
                                                @include('shared.diff_mo') 
                                            </td>

                                            <td>
                                                <span class="label bg-{{$data->stages->color}}">{{$data->stages->desc}}</span>
                                            </td>
                                            <td class="text-center">
                                                <a href="{{url('/history/show/'.$data->id_cus)}}" class="btn bg-orange btn-circle waves-effect waves-circle waves-float" onclick="window.open('{{url('/history/show/'.$data->id_cus)}}', 'newwindow', 'width=750,height=400'); return false;"> <i class="material-icons">history</i> </a>
                                            </td>
                                            
                                            <td>
                                                @if($data->stage == 'W2')
                                                <a href="{{url('/loan/eli/scoring/'.$data->id_cus)}}" class="btn btn-sm btn-primary"><i class="material-icons">work</i>Meet Customer  </a>

                                                @elseif($data->stage == 'W0')
                                                <a href="{{ url('view_form/'.$data->id_cus.'/first_step') }}" class="btn btn-sm bg-pink"><i class="material-icons">pageview</i> View Form </a>

                                                @elseif($data->stage == 'W3')
                                                <a href="{{ url('view_form/'.$data->id_cus.'/second_step') }}" class="btn btn-sm bg-green"><i class="material-icons">pageview</i> View Loan Cal </a>

                                                @elseif($data->stage == 'W6')
                                                <a href="{{url('/cal/change/'.$data->id_cus)}}" class="btn btn-sm btn-success"><i class="material-icons">sync_problem</i>Calculation & Change  </a>
                                                @elseif($data->stage == 'W7')
                                                <a href="{{url('/doc/incomplete/'.$data->id_cus)}}" class="btn btn-sm btn-warning"><i class="material-icons">cancel</i> Doc Incomplete  </a>
                                                @elseif($data->stage == 'W13')
                                                <!-- <a href="{{url('/doc/incomplete/'.$data->id_cus)}}" class="btn btn-sm bg-indigo"><i class="material-icons">help</i>Additional Doc Req  </a> -->
                                                @elseif($data->stage == 'W14')
                                                <a href="{{url('/doc/icloc/'.$data->id_cus)}}" class="btn btn-sm btn-warning"><i class="material-icons">description</i>Doc LOC / IC </a>
                                                @elseif($data->stage == 'W15')
                                                <a href="{{url('/recommend-new-submission/'.$data->id_cus)}}" class="btn btn-sm bg-purple"><i class="material-icons">updated</i>Recommend New Submission </a>
                                                @elseif($data->stage == 'W11')
                                                <a href="{{ url('view_form/'.$data->id_cus.'/fourth_step') }}" class="btn btn-sm bg-green"><i class="material-icons">pageview</i> View Loan </a> 
                                                @endif
                                            </td> 


                                            
                                        </tr>
                                        @endforeach
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
            </div>
            

        <!-- ======================= End Team Lead =================== -->

        <!-- ===================== Task Customer Service ==================== -->
            @elseif($roless=='11') 
                <!-- 
                <div class="row clearfix">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" onclick="location.href='{{route('assessment1.index')}}';" style="cursor:pointer; ">
                            <div class="info-box bg-pink hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">playlist_add_check</i>
                                </div>
                                
                                <div class="content">
                                    <div class="text">Loan Tracking</div>
                                    <div class="number count-to" data-from="0" data-to="{{ $count_admin }}" data-speed="1000" data-fresh-interval="20"></div>
                                </div>
                            </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" onclick="location.href='{{url('ol/loan')}}';">
                        <div class="info-box bg-light-green hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons">network_wifi</i>
                            </div>
                            <div class="content">
                                <div class="text">Loan Online</div>
                                <div class="number count-to" data-from="0" data-to={{$count_loan_online}} data-speed="1000" data-fresh-interval="20"></div>
                            </div>
                        </div>
                    </div> 
                    
                </div>
                -->

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Loan Search </h2>
                        </div>
                        <div class="body">
                            <div class="row">
                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <form action="{{url('/search-loan')}}" method="post">
                                        {{ csrf_field() }}

                                        <div class="">
                                            Search :
                                        </div>

                                        <div class="">
                                            <div id="gform">
                                              <label class="screen-reader-text" for="s"></label>
                                              <input type="text" value="" name="search_loan" id="s">
                                              <button type="submit" class="btn btn-xs bg-red" style="font-family: FontAwesome;"><i class="material-icons" >search</i></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-md-4"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        <!-- ===================== End Task Customer Service ==================== -->

        <!-- =====================Task Management ==================== -->
            
            @elseif($roless=='10')
                
                <div class="row clearfix">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" onclick="location.href='{{url('check-loan-cal-p2')}}';" style="cursor:pointer; ">
                            <div class="info-box bg-pink hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">border_color</i>
                                </div>
                                
                                    <div class="content">
                                        <div class="text">Loan Calculation</div>
                                        <div class="number count-to" data-from="0" data-to="{{ $pending_doc_p2 }}" data-speed="1000" data-fresh-interval="20"></div>
                                    </div>
                            </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" onclick="location.href='{{url('doc-check')}}';">
                        <div class="info-box bg-blue hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons">rate_review</i>
                            </div>
                            <div class="content">
                                <div class="text">Doc Check</div>
                                <div class="number count-to" data-from="0" data-to={{$pen_doc_check}} data-speed="1000" data-fresh-interval="20"></div>
                            </div>
                        </div>
                    </div> 
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" onclick="location.href='{{url('call')}}';" style="cursor:pointer; ">
                        <div class="info-box bg-green hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons">call</i>
                            </div>
                            <div class="content">
                                <div class="text">Call 103</div>
                                <div class="number count-to" data-from="0" data-to="{{ $pen_103 }}" data-speed="1000" data-fresh-interval="20"></div>
                            </div>
                        </div>
                    </div> 
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" onclick="location.href='{{url('docincomplete/p2')}}';" style="cursor:pointer; ">
                        <div class="info-box bg-lime hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons">pan_tool</i>
                            </div>
                            <div class="content">
                                <div class="text">Document Incomplete</div>
                                <div class="number count-to" data-from="0" data-to="{{ $doc_incom_p2 }}" data-speed="1000" data-fresh-interval="20"></div>
                            </div>
                        </div>
                    </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" onclick="location.href='{{url('batch-to-mbsb-process-2')}}';" style="cursor:pointer; ">
                                <div class="info-box bg-black hover-expand-effect">
                                    <div class="icon">
                                        <i class="material-icons">donut_small</i>
                                    </div>
                                    
                                    <div class="content">
                                        <div class="text">Ready to MBSB</div>
                                        <div class="number count-to" data-from="0" data-to="{{ $count_mbsb_processing_p3 }}" data-speed="1000" data-fresh-interval="20"></div>
                                    </div>
                                </div>
                        </div> 
                </div>


                
        <!--  ============= End of Management ===========-->

        <!-- ===================== Task Processor 1 ==================== -->
            @elseif($roless=='4') 

                <div class="row clearfix">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" onclick="location.href='{{route('assessment1.index')}}';" style="cursor:pointer; ">
                            <div class="info-box bg-red hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">playlist_add_check</i>
                                </div>
                                
                                    <div class="content">
                                        <div class="text">Send to MBSB</div>
                                        <div class="number count-to" data-from="0" data-to="{{ $countw0 }}" data-speed="1000" data-fresh-interval="20"></div>
                                    </div>
                            </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" onclick="location.href='{{url('uploadspekar_p1')}}';">
                        <div class="info-box bg-light-green hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons">file_upload</i>
                            </div>
                            <div class="content">
                                <div class="text">Upload Spekar</div>
                                <div class="number count-to" data-from="0" data-to={{$countp1upload_spekar}} data-speed="1000" data-fresh-interval="20"></div>
                            </div>
                        </div>
                    </div> 
                     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" onclick="location.href='{{url('route_to_mo')}}';" style="cursor:pointer; ">
                        <div class="info-box bg-blue hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons">exit_to_app</i>
                            </div>
                            <div class="content">
                                <div class="text">Route Mo</div>
                                <div class="number count-to" data-from="0" data-to="{{ $count_pen_mo }}" data-speed="1000" data-fresh-interval="20"></div>
                            </div>
                        </div>
                    </div> 
                </div>

            <div class="row clearfix">
                
                <div class="card">
                    <div class="header bg-red">
                        <h2>Task List </h2>
                    </div>
                    
                   <div class="body">
                                <!-- <input  value="Generate" class="btn btn-success" > -->
                                <table class="table" id="example">
                                    <thead>
                                        <tr>
                                            
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>IC</th>
                                            <th>Phone</th>
                                            <th>Date</th>
                                            <th class="hidden">Diff Hari</th>
                                            <th>Status</th>
                                            <th>MO</th>
                                            <th>Activity</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1; ?>

                                        @foreach($tasklist_p1 as $data)

                                            
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{ $data->name }}</td>
                                            <td>{{ $data->ic }}</td>
                                            <td>{{ $data->notelp }}</td>

                                            <?php 
                                                if(!empty($data->date_w0->last()->id)) {
                                               
                                                    foreach ($data->date_w0 as $log) {  

                                                        if($log->remark_id=='W0'){
                                                            $last_act_w0 = $log->created_at ;     
                                                        }
                                                        
                                                    }
                                                }

                                                if(!empty($data->date_w1->last()->id)) {
                                               
                                                    foreach ($data->date_w1 as $log) {  

                                                        if($log->remark_id=='W1'){
                                                            $last_act_w1 = $log->created_at ;     
                                                        }
                                                        
                                                    }
                                                }
                                            ?>

                                            <?php
                                                $s0 = $data->stage == 'W0';
                                                $s1 = $data->stage == 'W1';
                                             ?>
 
                                            <td>

                                                @if($s0) 
                                                    @if(!empty($s0))
                                                        {{$last_act_w0->toDayDateTimeString()}}
                                                    @endif
                                                @elseif($s1) 
                                                    @if(!empty($s1))
                                                        {{$last_act_w1->toDayDateTimeString()}}
                                                    @endif
                                                @endif
                                            </td>
                                            <td  class="hidden">
                                                @include('shared.diff_p1') 
                                            </td>
                                            <td>
                                                @include('shared.stage')
                                            </td>
                                            <td>{{ $data->p2->name }}</td>
                                            <td>
                                               <a href="{{url('/history/show/'.$data->id_cus)}}" class="btn bg-orange btn-circle waves-effect waves-circle waves-float" onclick="window.open('{{url('/history/show/'.$data->id_cus)}}', 'newwindow', 'width=750,height=400'); return false;"> <i class="material-icons">history</i> </a>
                                            </td>
                                            <td>
                                                @if($data->stage == 'W0')
                                                    <a href="{{ url('view_form/'.$data->id_cus.'/first_step') }}" class="btn btn-sm bg-pink"><i class="material-icons">pageview</i> View Form </a>
                                                @endif
                                            </td>

                                        </tr>
                                    @endforeach
                                    

                                </form>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>  
        <!-- ===================== End Task Processor 1 ==================== -->

        <!-- =====================Task Processor2 ==================== -->
            
            @elseif($roless=='6')
                
                <div class="row clearfix">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" onclick="location.href='{{url('check-loan-cal-p2')}}';" style="cursor:pointer; ">
                            <div class="info-box bg-pink hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">border_color</i>
                                </div>
                                
                                    <div class="content">
                                        <div class="text">Loan Calculation</div>
                                        <div class="number count-to" data-from="0" data-to="{{ $pending_doc_p2 }}" data-speed="1000" data-fresh-interval="20"></div>
                                    </div>
                            </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" onclick="location.href='{{url('doc-check')}}';">
                        <div class="info-box bg-blue hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons">rate_review</i>
                            </div>
                            <div class="content">
                                <div class="text">Doc Check</div>
                                <div class="number count-to" data-from="0" data-to={{$pen_doc_check}} data-speed="1000" data-fresh-interval="20"></div>
                            </div>
                        </div>
                    </div> 
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" onclick="location.href='{{url('call')}}';" style="cursor:pointer; ">
                        <div class="info-box bg-green hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons">call</i>
                            </div>
                            <div class="content">
                                <div class="text">Call 103</div>
                                <div class="number count-to" data-from="0" data-to="{{ $pen_103 }}" data-speed="1000" data-fresh-interval="20"></div>
                            </div>
                        </div>
                    </div> 
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" onclick="location.href='{{url('docincomplete/p2')}}';" style="cursor:pointer; ">
                        <div class="info-box bg-lime hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons">pan_tool</i>
                            </div>
                            <div class="content">
                                <div class="text">Document Incomplete</div>
                                <div class="number count-to" data-from="0" data-to="{{ $doc_incom_p2 }}" data-speed="1000" data-fresh-interval="20"></div>
                            </div>
                        </div>
                    </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" onclick="location.href='{{url('batch-to-mbsb-process-2')}}';" style="cursor:pointer; ">
                                <div class="info-box bg-black hover-expand-effect">
                                    <div class="icon">
                                        <i class="material-icons">donut_small</i>
                                    </div>
                                    
                                    <div class="content">
                                        <div class="text">Ready to MBSB</div>
                                        <div class="number count-to" data-from="0" data-to="{{ $count_mbsb_processing_p3 }}" data-speed="1000" data-fresh-interval="20"></div>
                                    </div>
                                </div>
                        </div> 
                </div>


                <div class="row clearfix">
                <!-- Task Info -->
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Task List</h2>
                        </div>
                        <div class="body">

                        
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos" id="example">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>IC</th>
                                            <th>Phone</th>
                                            <th width="20%">Date</th>
                                            <th class="hidden">Diff hari</th>
                                            <th>Status</th>
                                            <th>Activity</th>
                                            <th>Loc</th>
                                            <th>Action</th>
                                            <th>Listing Checklist</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php $i = 1; ?>

                                        @foreach($tasklist_p2 as $data)
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{$data->name}}</td>
                                            <td>{{$data->ic}}</td>
                                            <td>{{$data->notelp}}</td> 

                                            <?php 
                                                if(!empty($data->date_w3->last()->id)) {
                                               
                                                    foreach ($data->date_w3 as $log) {  

                                                        if($log->remark_id=='W3'){
                                                            $last_act_w3 = $log->created_at ;     
                                                        }
                                                        
                                                    }
                                                }

                                                if(!empty($data->date_w7->last()->id)) {
                                               
                                                    foreach ($data->date_w7 as $log) {  

                                                        if($log->remark_id == 'W7'){
                                                            $last_act_w7 = $log->created_at ;     
                                                        }
                                                        
                                                    }
                                                }

                                                if(!empty($data->date_w8->last()->id)) {
                                               
                                                    foreach ($data->date_w8 as $log) {  

                                                        if($log->remark_id =='W8'){
                                                            $last_act_w8 = $log->created_at ;     
                                                        }
                                                        
                                                    }
                                                }

                                                if(!empty($data->date_w9->last()->id)) {
                                               
                                                    foreach ($data->date_w9 as $log) {  

                                                        if($log->remark_id=='W9'){
                                                            $last_act_w9 = $log->created_at ;     
                                                        }
                                                        
                                                    }
                                                }

                                                if(!empty($data->date_w10->last()->id)) {
                                               
                                                    foreach ($data->date_w10 as $log) {  

                                                        if($log->remark_id=='W10'){
                                                            $last_act_w10 = $log->created_at ;     
                                                        }
                                                        
                                                    }
                                                }
                                                if(!empty($data->date_w17->last()->id)) {
                                               
                                                    foreach ($data->date_w17 as $log) {  

                                                        if($log->remark_id=='W17'){
                                                            $last_act_w17 = $log->created_at ;     
                                                        }
                                                        
                                                    }
                                                }
                                            ?>

                                            <?php
                                                $s3 = $data->stage == 'W3';
                                                $s7 = $data->stage == 'W7';
                                                $s8 = $data->stage == 'W8'; // Ready to batch header
                                                $s9 = $data->stage == 'W9';
                                                $s10 = $data->stage == 'W10';
                                                $s17 = $data->stage == 'W17'; //MBSB Processing
                                             ?>

                                            <td>
                                                @if($s3) 
                                                    @if(!empty($data->date_w3->last()->id))
                                                        {{$last_act_w3->toDayDateTimeString()}}
                                                    @else
                                                        {{$data->created_at->toDayDateTimeString()}}
                                                    @endif
                                                @elseif($s7) 
                                                    @if(!empty($data->date_w7->last()->id))
                                                        {{$last_act_w7->toDayDateTimeString()}}
                                                    @else
                                                        {{$data->created_at->toDayDateTimeString()}}
                                                    @endif
                                                @elseif($s8) 
                                                    @if(!empty($data->date_w8->last()->id))
                                                        {{$last_act_w8->toDayDateTimeString()}}
                                                    @else
                                                        {{$data->created_at->toDayDateTimeString()}}
                                                    @endif
                                                @elseif($s9) 
                                                    @if(!empty($data->date_w9->last()->id))
                                                        {{$last_act_w9->toDayDateTimeString()}}
                                                    @else
                                                        {{$data->created_at->toDayDateTimeString()}}
                                                    @endif
                                                @elseif($s10) 
                                                    @if(!empty($data->date_w10->last()->id))
                                                        {{$last_act_w10->toDayDateTimeString()}}
                                                    @else
                                                        {{$data->created_at->toDayDateTimeString()}}
                                                    @endif
                                                @elseif($s17) 
                                                    @if(!empty($data->date_w17->last()->id))
                                                        {{$last_act_w17->toDayDateTimeString()}}
                                                    @else
                                                        {{$data->created_at->toDayDateTimeString()}}
                                                    @endif
                                                @endif
                                            </td> 
                                            <td class="hidden">@include('shared.diff_p2')</td> 
                                            <td>
                                                @if($data->stage == 'W8')
                                                    @include('shared.stage_new')
                                                    <span class="label bg-pink">Make Batch Header</span>
                                                @else
                                                    @include('shared.stage_new')
                                                @endif

                                            </td>
                                            <td>
                                                <a href="{{url('/history/show/'.$data->id_cus)}}" class="btn bg-orange btn-circle waves-effect waves-circle waves-float" onclick="window.open('{{url('/history/show/'.$data->id_cus)}}', 'newwindow', 'width=750,height=400'); return false;"> <i class="material-icons">history</i> </a>
                                            </td>
                                            <td>
                                                <a href="{{url('/location/show/'.$data->id)}}" class="btn bg-light-blue btn-circle waves-effect waves-circle waves-float" onclick="window.open('{{url('/location/show/'.$data->id)}}', 'newwindow', 'width=600,height=400'); return false;"> <i class="material-icons">place</i> </a>
                                            </td>
                                            <td>
                                               @if($data->stage == 'W3')
                                                    <a href="{{ url('processor2/'.$data->id_cus.'/step1') }}" class="btn btn-sm bg-pink"><i class="material-icons">border_color</i> Loan Calculation </a>
                                                    <a href="{{ url('view_form/'.$data->id_cus.'/second_step') }}" class="btn btn-sm bg-green"><i class="material-icons">pageview</i> View Loan Cal </a>
                                                @elseif($data->stage == 'W7')
                                                    <a href="{{ url('loan-eli/'.$data->id_cus.'/step2') }}" class="btn btn-sm bg-lime"><i class="material-icons">rate_review</i> Doc Check </a>
                                                @elseif($data->stage == 'W9')
                                                    <a href="{{ url('loan-eli/'.$data->id_cus.'/step2') }}" class="btn btn-sm bg-lime"><i class="material-icons">rate_review</i> Doc Check </a>
                                                @elseif($data->stage == 'W10')
                                                    <a href="{{ url('loan-eli/'.$data->id_cus.'/step3') }}" class="btn btn-sm bg-green"><i class="material-icons">rate_review</i> 103 check </a>
                                                @elseif(($data->stage == 'W8') || ($data->stage == 'W17'))
                                                    <a href="{{ url('detail_processor2/'.$data->id_cus) }}" class="btn btn-sm bg-green"><i class="material-icons">pageview</i> Detail </a>

                                                @endif
                                            </td>
                                            <td>
                                                @if(($data->doc == 1) &&  ($data->stage == 'W17'))
                                                <a href="{{url('/pdfbatchheader/'.$data->id_cus)}}" class="btn bg-pink btn-circle waves-effect" target="_blank"> <i class="material-icons">dns</i> </a>
                                                @else
                                                -
                                                @endif
                                            </td>
                                            
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

        <!--  ============= End of Processor 2 ===========-->

        <!--  =======================  Processor 3 =======================-->
            @elseif($roless=='7')
            <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" onclick="location.href='{{url('update-approval')}}';" style="cursor:pointer; ">
                        <div class="info-box bg-pink hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons">donut_small</i>
                            </div>
                            
                            <div class="content">
                                <div class="text">MBSB Processing</div>
                                <div class="number count-to" data-from="0" data-to="{{ $count_mbsb_processing_p3 }}" data-speed="1000" data-fresh-interval="20"></div>
                            </div>
                        </div>
                </div>  
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" onclick="location.href='{{url('add-doc-from-mbsb')}}';">
                    <div class="info-box bg-orange hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">rate_review</i>
                        </div>
                        <div class="content">
                            <div class="text">Additional Document</div>
                            <div class="number count-to" data-from="0" data-to={{$count_mbsb_processing_add_doc}} data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div> 
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" onclick="location.href='{{url('approve-fix')}}';" style="cursor:pointer; ">
                    <div class="info-box bg-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">check</i>
                        </div>
                        <div class="content">
                            <div class="text">Approved</div>
                            <div class="number count-to" data-from="0" data-to="{{ $count_mbsb_processing_app }}" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" onclick="location.href='{{url('reject-fix')}}';" style="cursor:pointer; ">
                    <div class="info-box bg-red hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">cancel</i>
                        </div>
                        <div class="content">
                            <div class="text">Rejected</div>
                            <div class="number count-to" data-from="0" data-to="{{ $count_mbsb_processing_reject }}" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <!-- Task Info -->
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Task List</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos" id="example">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>IC</th>
                                            <th>Phone</th>
                                            <th width="20%">Date</th>
                                            <th class="hidden">Diff hari</th>
                                            <th>Status</th>
                                            <th>Activity</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php $i = 1; ?>

                                        @foreach($tasklist_p3 as $data)
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{$data->name}}</td>
                                            <td>{{$data->ic}}</td>
                                            <td>{{$data->notelp}}</td> 

                                            <?php 
                                                if(!empty($data->date_w8->last()->id)) {
                                               
                                                    foreach ($data->date_w8 as $log) {  

                                                        if($log->remark_id=='W8'){
                                                            $last_act_w8 = $log->created_at ;     
                                                        } 
                                                    }
                                                }

                                                if(!empty($data->date_w11->last()->id)) {
                                               
                                                    foreach ($data->date_w11 as $log) {  

                                                        if($log->remark_id=='W11'){
                                                            $last_act_w11 = $log->created_at ;     
                                                        }  
                                                    }
                                                }

                                                if(!empty($data->date_w12->last()->id)) {
                                               
                                                    foreach ($data->date_w12 as $log) {  

                                                        if($log->remark_id=='W12'){
                                                            $last_act_w12 = $log->created_at ;     
                                                        }
                                                        
                                                    }
                                                }

                                                if(!empty($data->date_w13->last()->id)) {
                                               
                                                    foreach ($data->date_w13 as $log) {  

                                                        if($log->remark_id=='W13'){
                                                            $last_act_w13 = $log->created_at ;     
                                                        } 
                                                    }
                                                }

                                                if(!empty($data->date_w17->last()->id)) {
                                               
                                                    foreach ($data->date_w17 as $log) {  

                                                        if($log->remark_id=='W17'){
                                                            $last_act_w17 = $log->created_at ;     
                                                        } 
                                                    }
                                                }

                                            ?>

                                            <?php
                                                $s8 = $data->stage == 'W8';
                                                $s13 = $data->stage == 'W13';
                                                $s11 = $data->stage == 'W11';
                                                $s12 = $data->stage == 'W12';
                                                $s17 = $data->stage == 'W17';
                                            ?>

                                            <td>
                                                @if($s8) 
                                                    @if(!empty($s8))
                                                        {{$last_act_w8->toDayDateTimeString()}}
                                                    @endif
                                                @elseif($s13) 
                                                    @if(!empty($s13))
                                                        {{$last_act_w13->toDayDateTimeString()}}
                                                    @endif
                                                @elseif($s11) 
                                                    @if(!empty($s11))
                                                        {{$last_act_w11->toDayDateTimeString()}}
                                                    @endif
                                                @elseif($s12) 
                                                    @if(!empty($s12))
                                                        {{$last_act_w12->toDayDateTimeString()}}
                                                    @endif
                                                @elseif($s17) 
                                                    @if(!empty($data->date_w17->last()->id))
                                                        {{$last_act_w17->toDayDateTimeString()}}
                                                    @else
                                                        {{$data->created_at->toDayDateTimeString()}}
                                                    @endif
                                                @endif

                                            </td> 
                                            <td class="hidden">
                                                @include('shared.diff_p3')
                                            </td>
                                            <td>
                                                @include('shared.stage_new')
                                            </td>
                                            <td>
                                                <a href="{{url('/history/show/'.$data->id_cus)}}" class="btn bg-orange btn-circle waves-effect waves-circle waves-float" onclick="window.open('{{url('/history/show/'.$data->id_cus)}}', 'newwindow', 'width=750,height=400'); return false;"> <i class="material-icons">history</i> </a>
                                            </td>
                                            <td>
                                               @if($data->stage == 'W17')  
                                                    <a href="{{ url('/update-approval_perone/'.$data->id_cus) }}" class="btn btn-sm bg-pink"><i class="material-icons">border_color</i> Update Approval </a>
                                                    <a href="{{ url('detail_processor2/'.$data->id_cus) }}" class="btn btn-sm bg-green"><i class="material-icons">pageview</i> Detail </a>
                                                @elseif($data->stage == 'W13')
                                                    @if(empty($data->additionaldoc->document)) 
                                                       
                                                    @else
                                                        <a href="{{asset('/additional/'.$data->ic.'/'.$data->additionaldoc->document)}}" class="btn bg-light-blue" target='_blank'> <i class="material-icons">library_books</i></a>
                                                        <a href="{{url('/additional_confirm/'.$data->id_cus)}}" class="btn btn-sm bg-orange"><i class="material-icons">help</i>Confirm Doc Received  </a>
                                                    @endif
                                                    
                                                @elseif($data->stage == 'W11')
                                                    <a href="{{ url('view_form/'.$data->id_cus.'/fourth_step') }}" class="btn btn-sm bg-green"><i class="material-icons">pageview</i> View Loan </a>
                                                @elseif($data->stage == 'W12')
                                                    <a href="{{ url('detail_processor2/'.$data->id_cus) }}" class="btn btn-sm bg-green"><i class="material-icons">pageview</i> Detail </a>
                                                @elseif($data->stage == 'W16')
                                                    <a href="{{ url('/update-approval_perone/'.$data->id_cus) }}" class="btn btn-sm bg-pink"><i class="material-icons">border_color</i> Update Approval </a>
                                                    <a href="{{ url('detail_processor2/'.$data->id_cus) }}" class="btn btn-sm bg-green"><i class="material-icons">pageview</i> Detail </a>
                                                @endif
                                            </td> 
                                            
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            @endif


        </div>

        <!-- Gak jadi dulu -->
        <!-- modal detail -->
        @include('shared._modaldetail')
        <!-- end of modal detail -->


    </section>

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>

    <script type="text/javascript">     
        $(document).ready( function () {
          var table = $('#example').DataTable({
            "createdRow": function( row, data, dataIndex ) {
                     if ( data[5] >= {{ config('maxday.max_day') }} ) {        
                 $(row).css('color', 'Red');
               }
               
            }
          });
        } );

    </script>


@endsection