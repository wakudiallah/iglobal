@extends('vadmin.tampilan')

@section('content')

    <section class="content">
        <div class="container-fluid">

            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">pageview</i> Loan Search</a></li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Loan Search </h2>
                        </div>
                        <div class="body">
                        	<div class="row">
	                        	<div class="col-md-4"></div>
	                        	<div class="col-md-4">
	                        		<form action="{{url('/search-loan')}}" method="post">
	                        			{{ csrf_field() }}

			                        	<div class="">
			                        		Search :
			                        	</div>

			                        	<div class="">
				                        	<div id="gform">
											  <label class="screen-reader-text" for="s"></label>
											  <input type="text" value="" name="search_loan" id="s">
											  <button type="submit" class="btn btn-xs bg-red" style="font-family: FontAwesome;"><i class="material-icons" >search</i></button>
											</div>
										</div>
									</form>
		                        </div>
	                        	<div class="col-md-4"></div>
	                        </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </section>

@endsection