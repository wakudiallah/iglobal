@extends('vadmin.tampilan')

@section('content')

    <section class="content">
        <div class="container-fluid">

            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">network_wifi</i> Loan Application Online</a></li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->
                

            <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Loan Application Online</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos" id="example">
                                    <thead>
                                        <tr>
                                            <th class="hidden"></th>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>IC</th>
                                            <th>Date Received</th>
                                            <th>Status</th>
                                            <th>Activity</th>

                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php $i = 1; ?>

                                        @foreach($praaplication as $data)

                                            <?php 
                                                $date_rev =  date('Y-m-d H:i:s ', strtotime($data->created_at));
                                                $today    =  date('Y-m-d H:i:s');
                                                $to_time = strtotime($today);
                                                $from_time = strtotime($date_rev);
                                                $di= ($to_time - $from_time) / (60*60);
                                            ?>

                                        <tr>
                                            <td class="hidden">{{number_format($di,0,'.','.')}}</td>
                                            <td>{{$i++}}</td>
                                            <td>{{$data->name}}</td>
                                            <td>{{$data->ic}}</td>

                                            <td>
                                                {{$data->created_at->toDayDateTimeString()}}
                                            </td>
                                            <td>
                                                <span class="label bg-{{$data->stages->color}}">{{$data->stages->desc}}</span>
                                            </td>
                                            
                                            
                                            <td class="text-center">
                                                @can('add_loan_onlines')
                                                <a href="{{url('/history/show/'.$data->id_cus)}}" class="btn bg-orange btn-circle waves-effect waves-circle waves-float" onclick="window.open('{{url('/history/show/'.$data->id_cus)}}', 'newwindow', 'width=750,height=400'); return false;"> <i class="material-icons">history</i> </a>
                                                @endcan
                                            </td>

                                               
                                        </tr>
                                        @endforeach
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
                
            </div>
        </div>

        <!-- Gak jadi dulu -->
        <!-- modal detail -->
        @include('shared._modaldetail')
        <!-- end of modal detail -->


    </section>

    
    <!-- Slimscroll Plugin Js -->
    <script src="{{ asset('admin/js/pages/forms/basic-form-elements.js') }}"></script>

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    
    <?php
    $today= date('dmy');
    ?>
    <script type="text/javascript">     
        $(document).ready( function () {
          var table = $('#example').DataTable({
            "createdRow": function( row, data, dataIndex ) {
                     if ( (data[0]) > 900 ) {        
                 $(row).css('color', 'Red');
               }
               else {        
                 $(row).css('color', 'Black');
               }
            }
          });
        } );

    </script>

@endsection