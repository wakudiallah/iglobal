 @extends('vadmin.tampilan')


@section('content')
	<section class="content">
        <div class="container-fluid">

			
			<div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">pageview</i> Loan Search</a></li>
                        
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->


			<div class="row clearfix">
                <div class="card">
                    <div class="header bg-red">
                        <h2>Loan Search </h2>
                    </div>
                    <div class="body">
                    	<div class="row">
                        	<div class="col-md-4"></div>
                        	<div class="col-md-4">
                        		<form action="{{url('/search-loan')}}" method="post">
                        			{{ csrf_field() }}

		                        	<div class="">
		                        		Search :
		                        	</div>

		                        	<div class="">
			                        	<div id="gform">
										  <label class="screen-reader-text" for="s"></label>
										  <input type="text" value="" name="search_loan" id="s">
										  <button type="submit" class="btn btn-xs bg-red" style="font-family: FontAwesome;"><i class="material-icons" >search</i></button>
										</div>
									</div>
								</form>
	                        </div>
                        	<div class="col-md-4"></div>
                        </div>
                    </div>
                </div>
            </div>

			<div class="row clearfix">
	    	<!-- Task Info -->
	        <div class="card">
	            <div class="header bg-red">
	                <h2>Detail</h2>
	            </div>
	            <div class="body">
	                <div class="table-responsive">
	                    <table class="table table-hover dashboard-task-infos" id="example">
	                        <thead>
	                            <tr>
	                                <th width="5%">#</th>
	                                <th>Name</th>
	                                <th>IC</th>
	                                <th>Email</th> 
	                                <th>No Telp</th> 
	                                <th>Employer</th> 
	                                <th>Status</th> 
	                                <th>Detail</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                            <?php $i = 1; ?>

	                            @foreach($pra as $data)
	                            <tr>
	                                <td>{{$i++}}</td>
	                                <td>{{$data->name}}</td>
	                                <td>{{$data->ic}}</td>
	                                <td>{{$data->email}}</td>
	                                <td>{{$data->notelp}}</td>
	                                <td>{{$data->majikan->Emp_Desc}}</td>

	                                
	                                <td>
	                                    @include('shared.stage_new')
	                                </td> 
	                                
	                                	

	                                <td>
                                       		<button type="button" class="btn btn-sm bg-orange" aria-controls="collapse-{{$data->id}}" data-target="#collapseDetailOne{{$data->id}}" data-toggle="collapse" style="cursor:pointer;"><i class="material-icons">pageview</i> Work Flow</button>

                                       @if($data->stage == 'W11')
                                       		<a href="{{ url('view_form/'.$data->id_cus.'/fourth_step') }}" class="btn btn-sm bg-green"><i class="material-icons">pageview</i> View Loan </a> 
                                       @elseif($data->stage == 'W17')  
                                       		<a href="{{ url('detail_processor2/'.$data->id_cus) }}" class="btn btn-sm bg-red"><i class="material-icons">pageview</i> Detail </a>
                                       	@elseif($data->stage == 'W3')
                                       		<a href="{{ url('view_form/'.$data->id_cus.'/second_step') }}" class="btn btn-sm bg-indigo"><i class="material-icons">pageview</i> View Loan Cal </a>
                                       	@elseif($data->stage == 'W0')
                                            <a href="{{ url('view_form/'.$data->id_cus.'/first_step') }}" class="btn btn-sm bg-blue"><i class="material-icons">pageview</i> View Form </a>
                                                
                                       	@endif                                        
	                                </td>
	                                
	                            </tr>

	                            @endforeach
	                            
	                        </tbody>
	                    </table>
	                </div>
	            </div>
	        </div>
	    
	    <!-- #END# Task Info -->
			</div>

			

			<!-- ///////////////////  Data Target  //////////////// -->
		    @foreach($pra as $datas)
		    <div id="collapseDetailOne{{$datas->id}}" class="collapse" aria-expanded="false" data-collapse-group="collapse-group" aria-labelledby="headingFive" data-parent="#accordionExample">
		        <div class="row clearfix">
		            <div class="card">
		                <div class="header bg-red">
		                    <h2>Detail {{$datas->name}}</h2>
		                </div>
		                <div class="body">
		                   
		                   <div class="table-responsive">
	                            <table class="table table-hover dashboard-task-infos" id="example1">
	                                <thead>
	                                    <tr>
	                                        <th>Date</th>
	                                        <th>Status</th>
	                                        <th>By</th>
	                                        <th>Note</th>
	                                    </tr>
	                                </thead>
	                                <tbody>

	                                	@foreach($datas->activity as $data)
	                                    <tr>
	                                        <!-- {{Carbon\Carbon::parse($data->created_at)	->format('d-m-Y i')}} -->
	                                        <td>{{$data->created_at->toDayDateTimeString()}}</td>
	                                        <td>
	                                        	<span class="label bg-{{$data->stage->color}}">{{$data->stage->desc}}</span>
	                                        </td>
	                                        <td>{{$data->user->name}}</td>
	                                        <td>{{$data->note}}</td>
	                                    </tr>
	                                    @endforeach
	                                </tbody>
	                            </table>
	                        </div>

		                </div>
		            </div>
		        </div>
		    </div>
		    @endforeach
		    <!-- ///////////////////  End Data Target  //////////////// -->

		</div>
	</section>


	<!-- ================= Data Target hidden ======= -->
    <script src="https://code.jquery.com/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
     

    <script type="text/javascript">
        $("[data-collapse-group]").on('show.bs.collapse', function () {
              var $this = $(this);
              var thisCollapseAttr = $this.attr('data-collapse-group');
              $("[data-collapse-group='" + thisCollapseAttr + "']").not($this).collapse('hide');
            });
    </script>

    <!-- ================= End Data Target hidden ======= -->
	

	<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>


	<script type="text/javascript">
		$(document).ready(function() {
	        $('#example').DataTable();
	        });
	</script>

	<script type="text/javascript">
		$(document).ready(function() {
	        $('#example1').DataTable();
	        });
	</script>
	
@endsection

    