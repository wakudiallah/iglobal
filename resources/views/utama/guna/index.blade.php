@extends('layouts.main')

@section('content')
    <section class="probootstrap-cover overflow-hidden relative"  @if(empty($images->image)) style="background-image: url('uploads/kl.jpg');" @else style="background-image: url('uploads/{{$images->image}}');" @endif data-stellar-background-ratio="0.5"  id="section-home">
      <div class="overlay"></div>
      <div class="container">
        <div class="row align-items-center">
          <div class="col-md">
            <h2 class="heading mb-2 display-4 font-light probootstrap-animate">Global I Exceed Management Sdn.Bhd</h2> 
            <p class="lead mb-5 probootstrap-animate">
              

            <!-- </p>
              <a href="onepage.html" role="button" class="btn btn-primary p-3 mr-3 pl-5 pr-5 text-uppercase d-lg-inline d-md-inline d-sm-block d-block mb-3">See OnePage Verion</a> 
            </p> -->
          </div> 
          <div class="col-md probootstrap-animate">
            {!! Form::open(['route' => ['cif.store'], 'class' => "probootstrap-form border border-danger", 'id' => 'form-validate-pra']) !!}

            {{ csrf_field() }}
            <span id="latitude"></span>
            <span id="longitude"></span>
            <span id="location"></span>


              <div class="form-group">
                <div class="row mb-3">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="probootstrap-date-departure">Full Name</label>
                      <div class="probootstrap-date-wrap">
                        <span class="icon ion-person"></span> 
                        <input type="text" id="name" name="name" class="form-control" placeholder="" required>
                      </div>
                    </div>
                  </div>
                  
                </div>
                <div class="row mb-3">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="probootstrap-date-arrival">Email</label>
                      <div class="probootstrap-date-wrap">
                        <span class="icon ion-card"></span> 
                        <input type="email" id="email" name="email" class="form-control" placeholder="" required >
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row mb-5">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="probootstrap-date-departure">Nombor Telefon Bimbit</label>
                      <div class="probootstrap-date-wrap">
                        <span class="icon ion-clipboard"></span> 
                        <input type="text" name="notelp" id="notelp" class="form-control" placeholder="" required onKeyPress="return goodchars(event,'1234567890',this)" >
                      </div>
                    </div>
                  </div>
                </div>

                
                <!-- END row -->
                <div class="row">
                  <div class="col-md">
                    
                  </div>
                  <div class="col-md">
                    <input type="submit" value="Submit" class="btn btn-danger btn-block" style="cursor:pointer;">
                  </div>
                  {{ csrf_field() }}
                </div>
              </div>
            {!! Form::close() !!}
          </div>



        </div>
      </div>
    
    </section>
    <!-- END section image -->
    
    <section class="probootstrap_section">
      <div class="container">
        <div class="row text-center mb-5 probootstrap-animate">
          <div class="col-md-12">
            <h2 class="display-4 border-bottom probootstrap-section-heading">Berita</h2>
          </div>
        </div>
        
        <div class="row probootstrap-animate">
          <div class="col-md-12">
            <div class="owl-carousel js-owl-carousel-2">
              <div>
                <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">
                  <img src="assets/images/sq_img_2.jpg" alt="Free Template by ProBootstrap" class="img-fluid">
                  <div class="media-body">
                    <h5 class="mb-3">02. Service Title Here</h5>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                  </div>
                </div>
              </div>
              <!-- END slide item -->

              <div>
                <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">
                  <img src="assets/images/sq_img_1.jpg" alt="Free Template by ProBootstrap" class="img-fluid">
                  <div class="media-body">
                    <h5 class="mb-3">02. Service Title Here</h5>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                  </div>
                </div>
              </div>
              <!-- END slide item -->

              <div>
                <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">
                  <img src="assets/images/sq_img_3.jpg" alt="Free Template by ProBootstrap" class="img-fluid">
                  <div class="media-body">
                    <h5 class="mb-3">02. Service Title Here</h5>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                  </div>
                </div>
              </div>
              <!-- END slide item -->

              <div>
                <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">
                  <img src="assets/images/sq_img_4.jpg" alt="Free Template by ProBootstrap" class="img-fluid">
                  <div class="media-body">
                    <h5 class="mb-3">02. Service Title Here</h5>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                  </div>
                </div>
              </div>
              <!-- END slide item -->

              <div>
                <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">
                  <img src="assets/images/sq_img_5.jpg" alt="Free Template by ProBootstrap" class="img-fluid">
                  <div class="media-body">
                    <h5 class="mb-3">02. Service Title Here</h5>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                  </div>
                </div>
              </div>
              <!-- END slide item -->


              <div>
                <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">
                  <img src="assets/images/sq_img_2.jpg" alt="Free Template by ProBootstrap" class="img-fluid">
                  <div class="media-body">
                    <h5 class="mb-3">02. Service Title Here</h5>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                  </div>
                </div>
              </div>
              <!-- END slide item -->

              <div>
                <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">
                  <img src="assets/images/sq_img_1.jpg" alt="Free Template by ProBootstrap" class="img-fluid">
                  <div class="media-body">
                    <h5 class="mb-3">02. Service Title Here</h5>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                  </div>
                </div>
              </div>
              <!-- END slide item -->

              <div>
                <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">
                  <img src="assets/images/sq_img_3.jpg" alt="Free Template by ProBootstrap" class="img-fluid">
                  <div class="media-body">
                    <h5 class="mb-3">02. Service Title Here</h5>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                  </div>
                </div>
              </div>
              <!-- END slide item -->

              <div>
                <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">
                  <img src="assets/images/sq_img_4.jpg" alt="Free Template by ProBootstrap" class="img-fluid">
                  <div class="media-body">
                    <h5 class="mb-3">02. Service Title Here</h5>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                  </div>
                </div>
              </div>
              <!-- END slide item -->

              <div>
                <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">
                  <img src="assets/images/sq_img_5.jpg" alt="Free Template by ProBootstrap" class="img-fluid">
                  <div class="media-body">
                    <h5 class="mb-3">02. Service Title Here</h5>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                  </div>
                </div>
              </div>
              <!-- END slide item -->
              
            </div>
          </div>
        </div>
      </div>
    </section> <!-- END section -->  

    <section class="probootstrap_section">
      <div class="container">
        <div class="row text-center mb-5 probootstrap-animate">
          <div class="col-md-12">
            <h2 class="display-4 border-bottom probootstrap-section-heading">Pelayanan Kami</h2>
          </div>
        </div>
      </div>
    </section>
        

    <section class="probootstrap-section-half d-md-flex" id="section-about">
      <div class="probootstrap-image probootstrap-animate" data-animate-effect="fadeIn" style="background-image: url(assets/images/img_2.jpg)"></div>
      <div class="probootstrap-text">
        <div class="probootstrap-inner probootstrap-animate" data-animate-effect="fadeInRight">
          <h2 class="heading mb-4">Konsep Pembiayaan i-Lestari</h2>
          <p>Pembiayaan i-Lestari adalah berkonsepkan Tawarruq</p>
        </div>
      </div>
    </section>


    <section class="probootstrap-section-half d-md-flex">
      <div class="probootstrap-image order-2 probootstrap-animate" data-animate-effect="fadeIn" style="background-image: url(assets/images/img_3.jpg)"></div>
      <div class="probootstrap-text order-1">
        <div class="probootstrap-inner probootstrap-animate" data-animate-effect="fadeInLeft">
          <h2 class="heading mb-4">Kategori Pelanggan </h2>
            <ol>
              <li>Persendirian-i Lestari Awam</li>
              <ol type="i">
                <li>Sektor Kerajaan dan Badan Berkanun berstatus tetap dengan tempoh berkhidmat melebihi 3 bulan dan kakitangan status kontrak bersambung tidak kurang 2 tahun berkhidmat hanya kepada Kakitangan KEMAS dan Jabatan Perpaduan sahaja.</li>
                <li>Anak Syarikat Kerajaan / GLC</li>
              </ol>
              <li>i-Lestari Swasta</li>
              <ol>
                <li>Sektor Swasta / Syarikat Terpilh (Panel Majikan Dan Bukan Panel Majikan)</li>
              </ol>
            </ol>
        </div>
      </div>
    </section>

    <section class="probootstrap-section-half d-md-flex" id="section-about">
      <div class="probootstrap-image probootstrap-animate" data-animate-effect="fadeIn" style="background-image: url(assets/images/img_2.jpg)"></div>
      <div class="probootstrap-text">
        <div class="probootstrap-inner probootstrap-animate" data-animate-effect="fadeInRight">
          <h2 class="heading mb-4">Konsep Pembiayaan i-Lestari</h2>
          <p>Pembiayaan i-Lestari adalah berkonsepkan Tawarruq</p>
        </div>
      </div>
    </section><!-- END section -->

    <section class="probootstrap_section" id="section-feature-testimonial">
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-12 text-center mb-5 probootstrap-animate">
            <h2 class="display-4 border-bottom probootstrap-section-heading">Pelayanan Kami</h2>
            <div class="row justify-content-center mb-5 probootstrap-animate">
              <div class="col-md-6 text-justify">
                <ol>
                  <h2 class="heading mb-3"><li>Konsep Pembiayaan i-Lestari </li></h2> 
                  <ol type="i">
                    <h5 class="mb-5"><li>Pembiayaan i-Lestari adalah berkonsepkan Tawarruq</li></h5>
                  </ol>
                  
                    <h2 class="heading mb-3"><li>Kategori Pelanggan</li></h2>
                    <ol type="i">
                      <h5><li>Persendirian-i Lestari Awam</li></h5>
                      <p>Sektor Kerajaan dan Badan Berkanun berstatus tetap dengan tempoh berkhidmat melebihi 3 bulan dan kakitangan status kontrak bersambung tidak kurang 2 tahun berkhidmat hanya kepada Kakitangan KEMAS dan Jabatan Perpaduan sahaja. Anak Syarikat Kerajaan / GLC</p>
                      <h5><li>i-Lestari Swasta</li></h5>
                      <p class="mb-5">Sektor Swasta / Syarikat Terpilh (Panel Majikan Dan Bukan Panel Majikan)</p>
                    </ol>

     
                </ol>
                
              </div>
              <div class="col-md-6"></div>
            </div>


          </div>
        </div> <!-- end -->
        
      </div>
    </section>
    <!-- END section -->

    <section class="probootstrap_section bg-light">
      <div class="container">
        <div class="row text-center mb-5 probootstrap-animate">
          <div class="col-md-12">
            <h2 class="display-4 border-bottom probootstrap-section-heading">News</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">

            <div class="media probootstrap-media d-flex align-items-stretch mb-4 probootstrap-animate">
              <div class="probootstrap-media-image" style="background-image: url(assets/images/img_1.jpg)">
              </div>
              <div class="media-body">
                <span class="text-uppercase">January 1st 2018</span>
                <h5 class="mb-3">Travel To United States Without Visa</h5>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                <p><a href="#">Read More</a></p>
              </div>
            </div>

            <div class="media probootstrap-media d-flex align-items-stretch mb-4 probootstrap-animate">
              <div class="probootstrap-media-image" style="background-image: url(assets/images/img_2.jpg)">
              </div>
              <div class="media-body">
                <span class="text-uppercase">January 1st 2018</span>
                <h5 class="mb-3">Travel To United States Without Visa</h5>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                <p><a href="#">Read More</a></p>
              </div>
            </div>

          </div>
          <div class="col-md-6">
            
            <div class="media probootstrap-media d-flex align-items-stretch mb-4 probootstrap-animate">
              <div class="probootstrap-media-image" style="background-image: url(assets/images/img_4.jpg)">
              </div>
              <div class="media-body">
                <span class="text-uppercase">January 1st 2018</span>
                <h5 class="mb-3">Travel To United States Without Visa</h5>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                <p><a href="#">Read More</a></p>
              </div>
            </div>

            <div class="media probootstrap-media d-flex align-items-stretch mb-4 probootstrap-animate">
              <div class="probootstrap-media-image" style="background-image: url(assets/images/img_5.jpg)">
              </div>
              <div class="media-body">
                <span class="text-uppercase">January 1st 2018</span>
                <h5 class="mb-3">Travel To United States Without Visa</h5>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                <p><a href="#">Read More</a></p>
              </div>
            </div>

          </div>
        </div>
      </div>
    </section> <!-- END section -->


@endsection

@push('script')

<script type="text/javascript">
    $(document).ready(function() {
   $('.selectpicker').selectpicker(); $('#form-validate-pra').bootstrapValidator({
      
      fields: {
       
        name: {
          validators: {
            notEmpty: {
              message: 'Sila masukkan nama penuh'
            }
          }
        },
        email: {
          validators: {

            notEmpty: {
              message: 'Sila masukan Email'
            }
          }
        },
        ic: {
          validators: {
             stringLength: {
              min: 12,
              max: 12,
              message: 'Sila masukkan 12 nombor IC'
            },
            notEmpty: {
              message: 'Sila masukan IC nombor'
            }
          }
        },
        notelp: {
          validators: {
            stringLength: {
              min: 9,
              max: 15,
              message: 'Sila masukkan sekurang-kurangnya 9 nombor dan tidak lebih dari 15'
            },
            notEmpty: {
              message: 'Sila masukan nombor telefon'
            }
          }
        },
        employment_code: {
          validators: {
            notEmpty: {
              message: 'Sila pilih jenis pekerjaan'
            }
          }
        },        
        emp_code: {
          validators: {
            notEmpty: {
              message: 'Sila pilih majikan'
            }
          }
        },
        elaun: {
          validators: {
            notEmpty: {
              message: 'Sila masukkan elaun'
            }
          }
        },
        pot_bul: {
          validators: {
            notEmpty: {
              message: 'Sila masukkan potongan bulanan'
            }
          }
        },
        loanpkg_code: {
          validators: {
            notEmpty: {
              message: 'Sila pilih pakej'
            }
          }
        },
        gaji_asas: {
          validators: {
            notEmpty: {
              message: 'Sila masukkan gaji asas'
            }
          }
        },
         jml_pem: {
          validators: {
            notEmpty: {
              message: 'Sila masukkan jumlah pembiayaan'
            }
          }
        },     
      }
    })

  });
</script>

<script>
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
</script>



<script language="javascript">
function getkey(e)
{
if (window.event)
   return window.event.keyCode;
else if (e)
   return e.which;
else
   return null;
}
function goodchars(e, goods, field)
{
var key, keychar;
key = getkey(e);
if (key == null) return true;
 
keychar = String.fromCharCode(key);
keychar = keychar.toLowerCase();
goods = goods.toLowerCase();
 
// check goodkeys
if (goods.indexOf(keychar) != -1)
    return true;
// control keys
if ( key==null || key==0 || key==8 || key==9 || key==27 )
   return true;
    
if (key == 13) {
    var i;
    for (i = 0; i < field.form.elements.length; i++)
        if (field == field.form.elements[i])
            break;
    i = (i + 1) % field.form.elements.length;
    field.form.elements[i].focus();
    return false;
    };
// else return false
return false;
}
</script>


<!-- Untuk Location -->
<input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
<script>
var apiGeolocationSuccess = function(position) {
  showLocation(position);
};

var tryAPIGeolocation = function() {
  jQuery.post( "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyDCa1LUe1vOczX1hO_iGYgyo8p_jYuGOPU", function(success) {
    apiGeolocationSuccess({coords: {latitude: success.location.lat, longitude: success.location.lng}});
  })
  .fail(function(err) {
    
  });
};

var browserGeolocationSuccess = function(position) {
  showLocation(position);
};

var browserGeolocationFail = function(error) {
  switch (error.code) {
    case error.TIMEOUT:
      alert("Browser geolocation error !\n\nTimeout.");
      break;
    case error.PERMISSION_DENIED:
      if(error.message.indexOf("Only secure origins are allowed") == 0) {
        tryAPIGeolocation();
      }
      break;
    case error.POSITION_UNAVAILABLE:
      alert("Browser geolocation error !\n\nPosition unavailable.");
      break;
  }
};

var tryGeolocation = function() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(
      browserGeolocationSuccess,
      browserGeolocationFail,
      {maximumAge: 50000, timeout: 20000, enableHighAccuracy: true});
  }
};

tryGeolocation();

function showLocation(position) {
  var latitude = position.coords.latitude;
  var longitude = position.coords.longitude;
  var _token = $('#_token').val();
  $.ajax({
    type:'POST',
    url: "{{url('/')}}/getLocation",
    data: { latitude: latitude, _token : _token, longitude : longitude },
    success:function(data){
            if(data){
              $("#latitude").html("<input type='hidden' name='latitude' id='latitude' value='"+data.latitude+"'>");
              $("#longitude").html("<input type='hidden' name='longitude' id='longitude' value='"+data.longitude+"'>");
               $("#location").html("<input type='hidden' name='location' id='location' value='"+data.location+"'>");
                 

            }else{
                $("#location").html('Not Available');
            }
    }
  });
}
</script>

<!-- End Location -->

@endpush