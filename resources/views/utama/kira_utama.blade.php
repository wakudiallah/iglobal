@extends('layouts.main')



@section('content')

    <section class="probootstrap-cover overflow-hidden relative"  style="background-image: url('{{url('/')}}/uploads/{{$image->image}}');" data-stellar-background-ratio="0.5"  id="section-home">
      <div class="overlay"></div>
      <div class="container">
        <div class="row align-items-center">
          <div class="col-md">
            <h2 class="heading mb-2 display-4 font-light probootstrap-animate">Global I Exceed Management Sdn.Bhd</h2> 
            <p class="lead mb-5 probootstrap-animate">

            <!-- </p>

              <a href="onepage.html" role="button" class="btn btn-primary p-3 mr-3 pl-5 pr-5 text-uppercase d-lg-inline d-md-inline d-sm-block d-block mb-3">See OnePage Verion</a> 

            </p> -->



          </div> 
          <div class="col-md probootstrap-animate">

            <div class="probootstrap-form border border-danger" >

              {!! Form::open(['url' => '/praapplication/just_calculate','class' => 'smart-form client-form', 'id' =>'smart-form-register3' ]) !!}


              @if(Session::has('flash_message'))
              <div class="alert alert-success alert-dismissible fade show" role="alert">
                {!! session('flash_message') !!}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            @endif

              <input type="hidden" name="id_cus" value="{{$pra->id_cus}}" class="form-control" placeholder="" >

            {{ csrf_field() }}





              <?php

                $icnumber = $pra->ic;
                $tanggal  = substr($icnumber,4, 2);
                $bulan    = substr($icnumber,2, 2);
                $tahun    = substr($icnumber,0, 2); 


                if($tahun > 30) {
                    $tahun2 = "19".$tahun;
                }

                else {
                     $tahun2 = "20".$tahun;
                }

               
                $lahir = $tahun2.'-'.$bulan.'-'.$tanggal;
                $lahir =  date('Y-m-d', strtotime($lahir. ' - 16 month'));
                $oDateNow = new DateTime();
                $oDateBirth = new DateTime($lahir);
                $oDateIntervall = $oDateNow->diff($oDateBirth);



                $umur =  $oDateIntervall->y;
                $durasix = 60 - $oDateIntervall->y;
                if( $durasix  > 10){ $durasi = 10 ;} 
                else { $durasi = $durasix ;}

            ?>



             <span id="latitude"></span>
              <span id="longitude"></span>
              <span id="location"></span>

              <div class="row mb-5">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="probootstrap-date-departure" style="color: red; font-size: 15px">Max Financing Amount :</label>
                      <div class="probootstrap-date-wrap">

 

                        @foreach($loan as $loan)
                                        <?php

                                        $salary_dsr = ($zbasicsalary * ($loan->dsr / 100)) - $zdeduction;
                                        $ndi = ($zbasicsalary - $zdeduction) -  1300;
                                        $max  =  $salary_dsr * 12 * 10 ;

                                                                       

                                        function pembulatan($uang) {

                                            $puluhan = substr($uang, - 3);
                                                if($puluhan<500) {
                                                        $akhir = $uang - $puluhan; 

                                                    }


                                                else{
                                                        $akhir = $uang - $puluhan;
                                                    }



                                                return $akhir;
                                                    }



                                        if(!empty($loan->max_byammount))  {

                                            $ansuran = intval($salary_dsr)-1;
                                              if($pra->loanpkg_code =="1") {  //aku edit
                                                  $bunga = 3.8/100;

                                              }

                                              elseif($pra->loanpkg_code == "2") {
                                                  $bunga = 4.9/100;

                                              }

                                              else {
                                                  $bunga = 5.92/100;
                                              }

                                              $pinjaman = 0;



                                              for ($i = 0; $i <= $loan->max_byammount; $i++) {
                                                  $bungapinjaman = $i  * $bunga * $durasi;   //
                                                  $totalpinjaman = $i + $bungapinjaman ;
                                                  $durasitahun = $durasi * 12;
                                                  $ansuran2 = intval($totalpinjaman / ($durasi * 12))  ;

                                                  //echo $ansuran2."<br>";

                                                  if ($ansuran2 < $ndi)
                                                  {
                                                      $pinjaman = $i;
                                                  }

                                              }   


                                              if($pinjaman > 1) {

                                                  $bulat = pembulatan($pinjaman);
                                                  $loanx =  number_format($bulat, 0 , ',' , ',' ) ;
                                                  $loanz = $bulat;
                                              }

                                              else {
                                                  $loanx =  number_format($loan->max_byammount, 0 , ',' , ',' ) ; 
                                                  $loanz = $loan->max_byammount;
                                              }

                                        }

                                        else { 


                                            $bulat = pembulatan($loan->max_bysalary * $total_salary);

                                            $loanx =  number_format($bulat, 0 , ',' , ',' ) ; 

                                            $loanz = $bulat;

                                            if ($loanz > 199000) {

                                                  $loanz  = 250000;
                                                  $loanx =  number_format($loanz, 0 , ',' , ',' ) ;
                                            }

                                        }



                                        ?>

                                    @endforeach

                        
                                    
                        <input type="text" id="name" name="maxloan" value="RM {{ number_format($loanz, 0 , ',' , ',' )  }} " class="form-control" placeholder="" readonly>
                        <input type="text" id="name" name="maxloanx" value="{{$loanz}}" class="form-control" placeholder="" hidden>

                      </div>

                    </div>

                  </div>

                </div>



              



              <div class="form-group">

                <div class="row mb-3">

                  <div class="col-md-6">

                    <div class="form-group">

                      <label for="probootstrap-date-departure">Package : </label>

                      <div class="probootstrap-date-wrap">

                        <span class="icon ion-card"></span> 

                        <input type="text" id="name" name="package" value=" {{$pra->loanpkg->Ln_Desc}}" class="form-control" placeholder="" readonly>

                      </div>

                    </div>

                  </div>

                  <div class="col-md-6">

                    <div class="form-group">

                      <label for="probootstrap-date-arrival">Max Installment :</label>

                      <div class="probootstrap-date-wrap">

                        <span class="icon ion-card"></span> 

                        <input type="text" id="ansuran_maksima" class="form-control" value="RM {{ number_format($ndi, 0 , ',' , ',' )  }}   / month" name="ansuran_maksima" placeholder="Ansuran Maksima" readonly>

                      </div>

                    </div>

                  </div>

                </div>



                <div class="row mb-5">

                  <div class="col-md-6">

                    <div class="form-group">

                      <label for="probootstrap-date-departure">Loan Amount (RM) :</label>

                      <div class="probootstrap-date-wrap">

                        <span class="icon ion-clipboard"></span> 

                        
                        <input type="text" name="jml_pem" id="jml_pem" value=" RM {{ number_format($pra->jml_pem, 0 , ',' , ',' )}}" class="form-control" placeholder="RM" required onKeyPress="return goodchars(event,'1234567890',this)" readonly="">

                        <input type="hidden" name="loanamount" id="" value=" {{$pra->jml_pem}}" class="form-control" >

                      </div>

                    </div>

                  </div>

                  <div class="col-md-6">

                    <div class="form-group">

                      <label for="probootstrap-date-departure">Total income :</label>

                      <div class="probootstrap-date-wrap">

                        <input type="text" id="pendapatan" class="form-control" value="RM {{ number_format($total_salary, 0 , ',' , ',' )}}" name="pendapatan" placeholder="Loan Amount" disabled="disabled">

                      </div>
                    </div>
                  </div>

                </div>


                <!-- END row -->



                <!-- END row -->
                <div class="row">
                  <div class="col-md">

                      <table class="table" style="font-size: 13px !important">
                          <thead>
                            <tr>
                              <th>Duration</th>
                              <th>Financing Amount </th>
                              <th>Monthly installment</th>
                              <th>Profit Rate </th>
                              <th>Select</th>
                            </tr>

                          </thead>

                     <tbody>

                     <?php if( $pra->loanamount <= $loanz ) { $ndi_limit=$loan->ndi_limit;?>
                            @foreach($tenure as $tenure)

                               <?php 

                                   $bunga2 =  $pra->jml_pem * $tenure->rate /100   ;
                                   $bunga = $bunga2 * $tenure->years;
                                   $total = $pra->jml_pem + $bunga ;
                                   $bulan = $tenure->years * 12 ;
                                   $installment =  $total / $bulan ;
                                   $ndi_state = ($total_salary - $zdeduction) - $installment; 

                                   
                                   if($installment  <= $salary_dsr && $ndi_state>=$ndi_limit) {
                                ?>

                                <tr>

                                    <td>{{$tenure->years}} years</td>
                                    <td class="hidden-xs"> RM {{ number_format( $pra->jml_pem, 0 , ',' , ',' )  }}  </td>

                                    <td>RM {{ number_format($installment, 0 , ',' , ',' )  }} /month</td>

                                  

                                    <td  align="center" >{{$tenure->rate}} %</td>
                                    <td align="center">  <input type="radio" name="tenure" id="tenure" value="{{$tenure->id}}" required> </td>

                                </tr>

                                       
                                 <?php }  ?>
                            @endforeach
                        <?php } ?>
                  </tbody>

                        </table>



                  </div>


                  <div class="col-md-6">
                    

                    <button type="submit" class="btn btn-danger btn-block" id="submit_tenure" style="cursor:pointer;">
                        <b>  Submit </b>
                    </button>
                  </div>


                  {{ csrf_field() }}

                </div>
              </div>
            </div>
          </div>




          <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Register</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <div class="form-group">
                      <label for="probootstrap-date-departure">Email  :</label>
                      <div class="probootstrap-date-wrap">
                        <span class="icon ion-mail"></span> 
                        <input type="email" name="email" id="Email" value="" class="form-control" placeholder="Email" required>
                      </div>
                    </div>
                    <!-- <div class="form-group">
                      <label for="probootstrap-date-departure">Password  :</label>
                      <div class="probootstrap-date-wrap">
                        <span class="icon ion-key"></span> 
                        <input type="password" name="password" id="password" value="" class="form-control" placeholder="Pasword" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="probootstrap-date-departure">Password  :</label>
                      <div class="probootstrap-date-wrap">
                        <span class="icon ion-key"></span> 
                        <input type="password" name="confirm_password" id="password_confirmation" value="" class="form-control" placeholder="Pasword" required>
                      </div>
                    </div> -->


                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Register</button>
                  </div>
                </div>
              </div>
            </div>

            {!! Form::close() !!} 

        </div>
      </div>


    </section>

    





@endsection


@push('script')



<script type="text/javascript">

    $(document).ready(function() {

   $('.selectpicker').selectpicker(); $('#form-validate-pra').bootstrapValidator({
      fields: {
        name: {
          validators: {
            notEmpty: {
              message: 'Sila masukkan nama penuh'

            }

          }

        },

        ic: {

          validators: {

             stringLength: {
              min: 12,
              max: 12,
              message: 'Sila masukkan 12 nombor IC'

            },

            notEmpty: {

              message: 'Sila masukan IC nombor'

            }

          }

        },

        notelp: {

          validators: {

            stringLength: {
              min: 9,
              max: 15,
              message: 'Sila masukkan sekurang-kurangnya 9 nombor dan tidak lebih dari 15'

            },

            notEmpty: {

              message: 'Sila masukan nombor telefon'

            }

          }

        },

        employment_code: {
          validators: {
            notEmpty: {
              message: 'Sila pilih jenis pekerjaan'

            }

          }

        },        

        emp_code: {
          validators: {
            notEmpty: {
              message: 'Sila pilih majikan'

            }

          }

        },

        elaun: {
          validators: {
            notEmpty: {
              message: 'Sila masukkan elaun'

            }

          }

        },

        pot_bul: {

          validators: {

            notEmpty: {

              message: 'Sila masukkan potongan bulanan'

            }

          }

        },

        loanpkg_code: {

          validators: {

            notEmpty: {

              message: 'Sila pilih pakej'

            }

          }

        },

        gaji_asas: {

          validators: {

            notEmpty: {

              message: 'Sila masukkan gaji asas'

            }

          }

        },

         jml_pem: {

          validators: {

            notEmpty: {

              message: 'Sila masukkan jumlah pembiayaan'

            }

          }

        },     

      }

    })



  });

</script>



<script>
    $(document).ready(function(){
        $("#submit_tenure").click(function(){
      var tenure = $('input[name=tenure]:checked').val();
         
            if (tenure>0) { 
          $('#myModal').modal('show');
        }
        else{
          
          alert("Please select the Tenur.");
          
        }
        });
    });
</script>

<script>
  $('#password, #confirm_password').on('keyup', function () {
    if ($('#password').val() == $('#confirm_password').val()) {
      $('#message').html('Matching').css('color', 'green');
    } else 
      $('#message').html('Not Matching').css('color', 'red');
  });
</script>


<script type="text/javascript">
   
    // Validation
    $(function() {
         runAllForms();
        $("#smart-form-register3").validate({

            // Rules for form validation
           rules : {
                
                Email2 : {
                    required : true,
                    email : true
                },
                BasicSalary : {
                    required : true,
                    min :2000

                    
                },
                password : {
                    required : true,
                    minlength : 8,
                    maxlength : 36
                },
                password_confirmation : {
                    required : true,
                   
                    maxlength : 36,
                    equalTo : '#password'
                }
            },

            // Messages for form validation
             messages : {

                    email : {
                    required : 'Please enter your email address',
                    Email2 : 'Please enter a VALID email address'
                },
                    BasicSalary : {
                    required : 'Please enter your email address'
                },
                    password : {
                    required : 'Please enter your password'
                },
                    password_confirmation : {
                    required : 'Please enter your password one more time',
                    equalTo : 'Please enter the same password as above'
                }
                    }
        });

    });
</script>



@endpush