@extends('layouts.main')

@section('content')
    <section class="probootstrap-cover overflow-hidden relative"  @if(empty($images->image)) style="background-image: url('uploads/kl.jpg');" @else style="background-image: url('uploads/{{$images->image}}');" @endif data-stellar-background-ratio="0.5"  id="section-home">
      <div class="overlay"></div>
      <div class="container">
        <div class="row align-items-center text-center">
          <div class="col-md">
            <h2 class="heading mb-2 display-4 font-light probootstrap-animate">Global I Exceed Management Sdn.Bhd</h2> 
            <p class="lead mb-5 probootstrap-animate">
              
            <!-- </p>
              <a href="onepage.html" role="button" class="btn btn-primary p-3 mr-3 pl-5 pr-5 text-uppercase d-lg-inline d-md-inline d-sm-block d-block mb-3">See OnePage Verion</a> 
            </p> -->
          </div> 
        </div>
      </div>
    
    </section><!-- END section image -->


    <section class="probootstrap_section bg-light" id="section-contact">
      <div class="container">
        <div class="row text-center mb-5 probootstrap-animate">
          <div class="col-md-12">
            <h2 class="display-4 border-bottom probootstrap-section-heading">Frequently Asked Question</h2>
          </div>
        </div>

        <div class="accordion" id="accordionExample">
            <div class="card">
            <div class="card-header" id="headingOne">
              <h5 class="mb-0">
                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  Apakah syarat-syarat untuk memohon pembiayaan peribadi?
                </button>
              </h5>
            </div>

            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
              <div class="card-body">
                <ol type="1">
                    <li>Warga negara Malaysia dan berumur 18 tahun ke atas;</li>
                    <li>Pemohon mestilah seorang pekerja kepada mana-mana sektor kerajaan atau sektor swasta (berkelayakan) yang stabil dengan pendapatan tetap setiap bulan.</li>
                </ol>
              </div>
            </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingTwo">
                  <h5 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                      Bagaimana untuk membuat semakan kelayakan ?
                    </button>
                  </h5>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                  <div class="card-body">
                    Semakan boleh dibuat secara online. Sila klik di sini untuk maklumat lanjut.
                  </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingThree">
                  <h5 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                      Bagaimana untuk membuat permohonan pembiayaan di Co-opbank Persatuan?
                    </button>
                  </h5>
                </div>
                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                  <div class="card-body">
                    Permohonan boleh dibuat secara online. Sila klik di sini untuk maklumat lanjut.
                  </div>
                </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingFour">
                  <h5 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseTwo">
                      Apakah dokumen yang diperlukan?
                    </button>
                  </h5>
                </div>
                <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                  <div class="card-body">
                    <p>Dokumen yang diperlukan adalah seperti berikut: </p>
                    <ol type="1">
                        <li>Salinan Kad Pengenalan pemohon (disahkan oleh pihak majikan)</li>
                        <li>Salinan slip gaji 3 bulan yang terkini (disahkan oleh pihak majikan)</li>
                        <li>Surat pengesahan jawatan oleh majikan</li>
                        <li>Penyata penyelesaian awal (jika berkaitan)</li>
                        <li>Penyata bank untuk 3 bulan gaji yang terkini</li>
                        <li>Borang Biro (BPA) – untuk pembiayaan yang telah diluluskan</li>
                        <li>Bil utiliti</li>
                        <li>Lain-lain dokumen sokongan (jika diperlukan)</li>
                    </ol>
 
                  </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingFive">
                  <h5 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseTwo">
                      Adakah saya layak memohon sekiranya saya disenaraihitam di dalam CTOS?
                    </button>
                  </h5>
                </div>
                <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                  <div class="card-body">
                    Anda layak membuat permohonan dengan syarat anda tidak berstatus bankrap. Pemohon perlu memberikan sebarang surat pelepasan dari institusi yang berkenaan dan dokumen-dokumen yang berkaitan untuk memudahkan proses permohonan.
 
                  </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingSix">
                  <h5 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                      Siapakah yang boleh dihubungi bagi tujuan pertanyaan tentang pembiayaan?
                    </button>
                  </h5>
                </div>
                <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
                  <div class="card-body">
                    Pemohon boleh menghubungi no talian 03-4144 4075 atau emel di loan@ezlestari.com.my untuk maklumat lanjut mengenai pembiayaan.
 
                  </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingSeven">
                  <h5 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                      Berapa lamakah tempoh proses permohonan pembiayaan peribadi-i?
                    </button>
                  </h5>
                </div>
                <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionExample">
                  <div class="card-body">
                    Permohonan akan diproses dalam tempoh 3 hari bekerja selepas penerimaan lengkap borang permohonan 
                  </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingEight">
                  <h5 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                      Adakah penjamin diperlukan?
                    </button>
                  </h5>
                </div>
                <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordionExample">
                  <div class="card-body">
                    <p>Penjamin hanya diperlukan sekiranya pelanggan tidak memenuhi syarat pembiayaan.</p>
                  </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingNine">
                  <h5 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                      Adakah yuran proses akan dikenakan?
                    </button>
                  </h5>
                </div>
                <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordionExample">
                  <div class="card-body">
                    <p>Tiada yuran proses akan dikenakan kepada pemohon sektor kerajaan, badan berkanun dan GLC. Yuran proses sebanyak 0.5% akan dikenakan untuk kategori majikan swasta.</p>
                  </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingTen">
                  <h5 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                      Adakah akan dikenakan bayaran awal / pendahuluan?
                    </button>
                  </h5>
                </div>
                <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordionExample">
                  <div class="card-body">
                    <p>Tiada bayaran awal / pendahuluan untuk pemohon sektor kerajaan, badan berkanun dan GLC dengan cara bayaran balik melalui BPA. Untuk lain-lain cara bayaran balik dan kategori majikan, bayaran awal / pendahuluan 1 bulan akan ditolak dari jumlah pembiayaan dan dikreditksn ke dalam akaun pembiayaan yang berkenaan. </p>
                  </div>
                </div>
            </div>
      </div>
    </section>
    <!-- END section of alamat-->

@endsection