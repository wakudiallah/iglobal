@extends('layouts.main')



@section('content')

    <section class="probootstrap-cover overflow-hidden relative"  style="background-image: url('uploads/{{$image->image}}');" data-stellar-background-ratio="0.5"  id="section-home">
      <div class="overlay"></div>
      <div class="container">
        <div class="row align-items-center">
          <div class="col-md">

            <h2 class="heading mb-2 display-4 font-light probootstrap-animate">Global I Exceed Management Sdn.Bhd</h2> 

            <p class="lead mb-5 probootstrap-animate">

              

            <!-- </p>

              <a href="onepage.html" role="button" class="btn btn-primary p-3 mr-3 pl-5 pr-5 text-uppercase d-lg-inline d-md-inline d-sm-block d-block mb-3">See OnePage Verion</a> 

            </p> -->



          </div> 

          <div class="col-md probootstrap-animate">

            {!! Form::open(['url' => ['praaplication/store'], 'method' => 'POST', 'class' => "probootstrap-form border border-danger", 'id' => 'form-validate-pra']) !!}


            @if(Session::has('flash_message'))
              <div class="alert alert-success alert-dismissible fade show" role="alert">
                {!! session('flash_message') !!}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            @endif

    @if (Session::has('message'))



                <script>

           

                       function pesan() {

                            bootbox.alert("<b>{{Session::get('message')}}</b>");

                        }

                        window.onload = pesan;



                        

                </script>

            @endif             

            @if (count($errors) > 0)

                <script>

   



                     function pesan() {

                          bootbox.alert("<b>@foreach ($errors->all() as $error) {{ $error }} <br> @endforeach</b>");

                      }

                      window.onload = pesan;



          

              </script>



          @endif



            {{ csrf_field() }}

             <span id="latitude"></span>

              <span id="longitude"></span>

              <span id="location"></span>

              <div class="form-group">

                <div class="row mb-3">

                  <div class="col-md-6">

                    <div class="form-group">

                      <label for="probootstrap-date-departure">Full Name :</label>

                      <div class="probootstrap-date-wrap">

                        <span class="icon ion-person"></span> 

                        <input type="text" id="name" name="name" class="form-control" placeholder="" required  @if (Session::has('fullname'))  value="{{ Session::get('fullname') }}" @endif>

                      </div>

                    </div>

                  </div>

                  <div class="col-md-6">

                    <div class="form-group">

                      <label for="probootstrap-date-arrival">IC Number :</label>

                      <div class="probootstrap-date-wrap">

                        <span class="icon ion-card"></span> 

                        <input type="text" id="ic" name="ic" class="form-control" placeholder="" required onKeyPress="return goodchars(event,'1234567890',this)" maxlength="12"  @if (Session::has('icnumber'))  value="{{ Session::get('icnumber') }}" @endif>

                      </div>

                    </div>

                  </div>

                </div>



                <div class="row mb-5">

                  <div class="col-md-6">

                    <div class="form-group">

                      <label for="probootstrap-date-departure">Phone Number :</label>

                      <div class="probootstrap-date-wrap">

                        <span class="icon ion-clipboard"></span> 

                        <input type="text" name="notelp" id="notelp" class="form-control" placeholder="" required onKeyPress="return goodchars(event,'1234567890',this)"  value="{{ Session::get('phone') }}">

                      </div>

                    </div>

                  </div>

                  <div class="col-md-6">

                    <div class="form-group">

                      <label for="probootstrap-date-departure">Job Type :</label>

                      <div class="probootstrap-date-wrap">

                        <select  id="Employment" name="Employment" class="js-example-basic-single js-states form-control" required>

                          @if (Session::has('employment')) 

                          <option  value="{{ Session::get('employment') }}">{{ Session::get('employment2') }}</option>

                           @endif

                             <option value="" selected="selected">Select...</option>

                          @foreach ($employment as $data)

                          <option value="{{$data->id}}">{{$data->name}}</option>

                          @endforeach                          

                        </select>

                      </div>





                    </div>

                  </div>

                </div>



                <div class="row mb-5">

                  <div class="col-md-6">

                    <div class="form-group">

                      <label for="probootstrap-date-departure">Employer :</label>

                      <div class="probootstrap-date-wrap">

                        <select class="js-example-basic-single js-states form-control" id="emp_code" name="emp_code">

                           @if (Session::has('employer')) 

                           <option  value="{{ Session::get('employer') }}">{{ Session::get('employer2') }}</option>

                           @endif

                          @foreach ($emp as $data)

                          <option value="{{$data->id}}">{{$data->Emp_Desc}}</option>

                          @endforeach                          

                        </select>

                      </div>

                    </div>

                  </div>

                  

                  <div class="col-md-6">

                    <div class="form-group">

                      <label for="probootstrap-date-departure">Basic Salary (RM) :</label>

                      <div class="probootstrap-date-wrap">

                        <span class="icon ion-card"></span> 

                        <input type="text" id="gaji_asas" name="gaji_asas" class="form-control" placeholder="" required onKeyPress="return goodchars(event,'1234567890',this)" @if (Session::has('basicsalary'))  value="{{ Session::get('basicsalary') }}" @endif >

                      </div>

                    </div>

                  </div>



                </div>

                <!-- END row -->



                <div class="row mb-3">

                  <div class="col-md-6">

                    <div class="form-group">

                      <label for="probootstrap-date-arrival">Allowance (RM) :</label>

                      <div class="probootstrap-date-wrap">

                        <span class="icon ion-card"></span> 

                        <input type="text" id="elaun" name="elaun" class="form-control" placeholder="" required onKeyPress="return goodchars(event,'1234567890',this)" @if (Session::has('allowance'))  value="{{ Session::get('allowance') }}" @endif >

                      </div>

                    </div>

                  </div>

                  <div class="col-md-6">

                    <div class="form-group">

                      <label for="probootstrap-date-departure">Existing Total Deduction (RM) :</label>

                      <div class="probootstrap-date-wrap">

                        <span class="icon ion-card"></span> 

                        <input type="text" id="pot_bul" name="pot_bul" class="form-control" placeholder="" required onKeyPress="return goodchars(event,'1234567890',this)" @if (Session::has('deduction'))  value="{{ Session::get('deduction') }}" @endif>

                      </div>

                    </div>

                  </div>

                </div>



                

                <div class="row mb-5">

                  <div class="col-md-6">

                    <div class="form-group">

                      <label for="probootstrap-date-departure">Package :</label>

                      <select  id="loanpkg_code" name="loanpkg_code" class="js-states form-control">

                           <option value="" selected="selected">Select...</option>

                          @foreach ($loanpkg as $data)

                          <option value="{{$data->id}}" data-parent="{{$data->EmpType}}">{{$data->Ln_Desc}}</option>

                          @endforeach

                      </select>



                      <!-- <input type="text" name="Package" id="Package" @if (Session::has('package_name'))  value="{{ Session::get('package_name') }}" @else value="Mumtaz-i" @endif class="form-control" disabled> -->



                    </div>

                  </div>

                  <div class="col-md-6">

                    <div class="form-group">

                      <label for="probootstrap-date-arrival">Loan Amount (RM) :</label>

                      <div class="probootstrap-date-wrap">

                        <span class="icon ion-card"></span> 

                        <input type="text" id="jml_pem" name="jml_pem" class="form-control" placeholder="" required onKeyPress="return goodchars(event,'1234567890',this)" @if (Session::has('loanAmount'))  value="{{ Session::get('loanAmount') }}" @endif>

                      </div>

                    </div>

                  </div>

                </div>

                

                <!-- END row -->

                <div class="row">

                  <div class="col-md">

                    

                  </div>

                  <div class="col-md">

                    <input type="submit" value="Submit" class="btn btn-danger btn-block" style="cursor:pointer;">

                  </div>

                  {{ csrf_field() }}

                </div>

              </div>

            {!! Form::close() !!}

          </div>

        </div>

      </div>

    

    </section>

    <!-- END section image -->

    

    <section class="probootstrap_section">

      <div class="container">

        <div class="row text-center mb-5 probootstrap-animate">

          <div class="col-md-12">

            <h2 class="display-4 border-bottom probootstrap-section-heading">Berita</h2>

          </div>

        </div>

        

        <div class="row probootstrap-animate">

          <div class="col-md-12">

            <div class="owl-carousel js-owl-carousel-2">

              <div>

                <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">

                  <img src="assets/images/sq_img_2.jpg" alt="Free Template by ProBootstrap" class="img-fluid">

                  <div class="media-body">

                    <h5 class="mb-3">02. Service Title Here</h5>

                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>

                  </div>

                </div>

              </div>

              <!-- END slide item -->



              <div>

                <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">

                  <img src="assets/images/sq_img_1.jpg" alt="Free Template by ProBootstrap" class="img-fluid">

                  <div class="media-body">

                    <h5 class="mb-3">02. Service Title Here</h5>

                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>

                  </div>

                </div>

              </div>

              <!-- END slide item -->



              <div>

                <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">

                  <img src="assets/images/sq_img_3.jpg" alt="Free Template by ProBootstrap" class="img-fluid">

                  <div class="media-body">

                    <h5 class="mb-3">02. Service Title Here</h5>

                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>

                  </div>

                </div>

              </div>

              <!-- END slide item -->



              <div>

                <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">

                  <img src="assets/images/sq_img_4.jpg" alt="Free Template by ProBootstrap" class="img-fluid">

                  <div class="media-body">

                    <h5 class="mb-3">02. Service Title Here</h5>

                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>

                  </div>

                </div>

              </div>

              <!-- END slide item -->



              <div>

                <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">

                  <img src="assets/images/sq_img_5.jpg" alt="Free Template by ProBootstrap" class="img-fluid">

                  <div class="media-body">

                    <h5 class="mb-3">02. Service Title Here</h5>

                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>

                  </div>

                </div>

              </div>

              <!-- END slide item -->





              <div>

                <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">

                  <img src="assets/images/sq_img_2.jpg" alt="Free Template by ProBootstrap" class="img-fluid">

                  <div class="media-body">

                    <h5 class="mb-3">02. Service Title Here</h5>

                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>

                  </div>

                </div>

              </div>

              <!-- END slide item -->



              <div>

                <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">

                  <img src="assets/images/sq_img_1.jpg" alt="Free Template by ProBootstrap" class="img-fluid">

                  <div class="media-body">

                    <h5 class="mb-3">02. Service Title Here</h5>

                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>

                  </div>

                </div>

              </div>

              <!-- END slide item -->



              <div>

                <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">

                  <img src="assets/images/sq_img_3.jpg" alt="Free Template by ProBootstrap" class="img-fluid">

                  <div class="media-body">

                    <h5 class="mb-3">02. Service Title Here</h5>

                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>

                  </div>

                </div>

              </div>

              <!-- END slide item -->



              <div>

                <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">

                  <img src="assets/images/sq_img_4.jpg" alt="Free Template by ProBootstrap" class="img-fluid">

                  <div class="media-body">

                    <h5 class="mb-3">02. Service Title Here</h5>

                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>

                  </div>

                </div>

              </div>

              <!-- END slide item -->



              <div>

                <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">

                  <img src="assets/images/sq_img_5.jpg" alt="Free Template by ProBootstrap" class="img-fluid">

                  <div class="media-body">

                    <h5 class="mb-3">02. Service Title Here</h5>

                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>

                  </div>

                </div>

              </div>

              <!-- END slide item -->

              

            </div>

          </div>

        </div>

      </div>

    </section> <!-- END section -->  



    <section class="probootstrap_section">

      <div class="container">

        <div class="row text-center mb-5 probootstrap-animate">

          <div class="col-md-12">

            <h2 class="display-4 border-bottom probootstrap-section-heading">Pelayanan Kami</h2>

          </div>

        </div>

      </div>

    </section>

        



    <section class="probootstrap-section-half d-md-flex" id="section-about">

      <div class="probootstrap-image probootstrap-animate" data-animate-effect="fadeIn" style="background-image: url(assets/images/img_2.jpg)"></div>

      <div class="probootstrap-text">

        <div class="probootstrap-inner probootstrap-animate" data-animate-effect="fadeInRight">

          <h2 class="heading mb-4">Konsep Pembiayaan i-Lestari</h2>

          <p>Pembiayaan i-Lestari adalah berkonsepkan Tawarruq</p>

        </div>

      </div>

    </section>





    <section class="probootstrap-section-half d-md-flex">

      <div class="probootstrap-image order-2 probootstrap-animate" data-animate-effect="fadeIn" style="background-image: url(assets/images/img_3.jpg)"></div>

      <div class="probootstrap-text order-1">

        <div class="probootstrap-inner probootstrap-animate" data-animate-effect="fadeInLeft">

          <h2 class="heading mb-4">Kategori Pelanggan </h2>

            <ol>

              <li>Persendirian-i Lestari Awam</li>

              <ol type="i">

                <li>Sektor Kerajaan dan Badan Berkanun berstatus tetap dengan tempoh berkhidmat melebihi 3 bulan dan kakitangan status kontrak bersambung tidak kurang 2 tahun berkhidmat hanya kepada Kakitangan KEMAS dan Jabatan Perpaduan sahaja.</li>

                <li>Anak Syarikat Kerajaan / GLC</li>

              </ol>

              <li>i-Lestari Swasta</li>

              <ol>

                <li>Sektor Swasta / Syarikat Terpilh (Panel Majikan Dan Bukan Panel Majikan)</li>

              </ol>

            </ol>

        </div>

      </div>

    </section>



    <section class="probootstrap-section-half d-md-flex" id="section-about">

      <div class="probootstrap-image probootstrap-animate" data-animate-effect="fadeIn" style="background-image: url(assets/images/img_2.jpg)"></div>

      <div class="probootstrap-text">

        <div class="probootstrap-inner probootstrap-animate" data-animate-effect="fadeInRight">

          <h2 class="heading mb-4">Konsep Pembiayaan i-Lestari</h2>

          <p>Pembiayaan i-Lestari adalah berkonsepkan Tawarruq</p>

        </div>

      </div>

    </section><!-- END section -->



    <section class="probootstrap_section" id="section-feature-testimonial">

      <div class="container">

        <div class="row justify-content-center mb-5">

          <div class="col-md-12 text-center mb-5 probootstrap-animate">

            <h2 class="display-4 border-bottom probootstrap-section-heading">Pelayanan Kami</h2>

            <div class="row justify-content-center mb-5 probootstrap-animate">

              <div class="col-md-6 text-justify">

                <ol>

                  <h2 class="heading mb-3"><li>Konsep Pembiayaan i-Lestari </li></h2> 

                  <ol type="i">

                    <h5 class="mb-5"><li>Pembiayaan i-Lestari adalah berkonsepkan Tawarruq</li></h5>

                  </ol>

                  

                    <h2 class="heading mb-3"><li>Kategori Pelanggan</li></h2>

                    <ol type="i">

                      <h5><li>Persendirian-i Lestari Awam</li></h5>

                      <p>Sektor Kerajaan dan Badan Berkanun berstatus tetap dengan tempoh berkhidmat melebihi 3 bulan dan kakitangan status kontrak bersambung tidak kurang 2 tahun berkhidmat hanya kepada Kakitangan KEMAS dan Jabatan Perpaduan sahaja. Anak Syarikat Kerajaan / GLC</p>

                      <h5><li>i-Lestari Swasta</li></h5>

                      <p class="mb-5">Sektor Swasta / Syarikat Terpilh (Panel Majikan Dan Bukan Panel Majikan)</p>

                    </ol>



     

                </ol>

                

              </div>

              <div class="col-md-6"></div>

            </div>





          </div>

        </div> <!-- end -->

        

      </div>

    </section>

    <!-- END section -->



    <section class="probootstrap_section bg-light">

      <div class="container">

        <div class="row text-center mb-5 probootstrap-animate">

          <div class="col-md-12">

            <h2 class="display-4 border-bottom probootstrap-section-heading">News</h2>

          </div>

        </div>

        <div class="row">

          <div class="col-md-6">



            <div class="media probootstrap-media d-flex align-items-stretch mb-4 probootstrap-animate">

              <div class="probootstrap-media-image" style="background-image: url(assets/images/img_1.jpg)">

              </div>

              <div class="media-body">

                <span class="text-uppercase">January 1st 2018</span>

                <h5 class="mb-3">Travel To United States Without Visa</h5>

                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>

                <p><a href="#">Read More</a></p>

              </div>

            </div>



            <div class="media probootstrap-media d-flex align-items-stretch mb-4 probootstrap-animate">

              <div class="probootstrap-media-image" style="background-image: url(assets/images/img_2.jpg)">

              </div>

              <div class="media-body">

                <span class="text-uppercase">January 1st 2018</span>

                <h5 class="mb-3">Travel To United States Without Visa</h5>

                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>

                <p><a href="#">Read More</a></p>

              </div>

            </div>



          </div>

          <div class="col-md-6">

            

            <div class="media probootstrap-media d-flex align-items-stretch mb-4 probootstrap-animate">

              <div class="probootstrap-media-image" style="background-image: url(assets/images/img_4.jpg)">

              </div>

              <div class="media-body">

                <span class="text-uppercase">January 1st 2018</span>

                <h5 class="mb-3">Travel To United States Without Visa</h5>

                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>

                <p><a href="#">Read More</a></p>

              </div>

            </div>



            <div class="media probootstrap-media d-flex align-items-stretch mb-4 probootstrap-animate">

              <div class="probootstrap-media-image" style="background-image: url(assets/images/img_5.jpg)">

              </div>

              <div class="media-body">

                <span class="text-uppercase">January 1st 2018</span>

                <h5 class="mb-3">Travel To United States Without Visa</h5>

                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>

                <p><a href="#">Read More</a></p>

              </div>

            </div>



          </div>

        </div>

      </div>

    </section> <!-- END section -->





@endsection



@push('script')

<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript" src="{{url('/js/bootbox.min.js')}}"></script>

<script type="text/javascript">
  $('#Employment').change(function() {
   var parent = $(this).val();
   $('#loanpkg_code').children().each(function() {
      if($(this).data('parent') != parent) {

                $(this).hide();

      } else    $(this).show();
   });
});
</script>

<script type="text/javascript">

    $(document).ready(function() {

   $('.selectpicker').selectpicker(); $('#form-validate-pra').bootstrapValidator({

      

      fields: {

       

        name: {

          validators: {

            notEmpty: {

              message: 'Please enter the full name'

            }

          }

        },

        ic: {

          validators: {

             stringLength: {

              min: 12,

              max: 12,

              maxlength: 12,

              message: 'Please enter 12 IC Number'

            },

            notEmpty: {

              message: 'Please enter IC nombor'

            }

          }

        },

        notelp: {

          validators: {

            stringLength: {

              min: 9,

              max: 15,

              message: 'Please enter at least 9 numbers and not more than 15'

            },

            notEmpty: {

              message: 'Please enter phone number'

            }

          }

        },

        employment_code: {

          validators: {

            notEmpty: {

              message: 'Please select a job type'

            }

          }

        },        

        emp_code: {

          validators: {

            notEmpty: {

              message: 'Please enter employer'

            }

          }

        },

        elaun: {

          validators: {

            notEmpty: {

              message: 'Please enter elaun'

            }

          }

        },

        pot_bul: {

          validators: {

            notEmpty: {

              message: 'Please enter total deduction'

            }

          }

        },

        loanpkg_code: {

          validators: {

            notEmpty: {

              message: 'Please select Package'

            }

          }

        },

        gaji_asas: {

          validators: {

            notEmpty: {

              message: 'Please enter basic salary'

            }

          }

        },

         jml_pem: {

          validators: {

            notEmpty: {

              message: 'Please enter loan emount'

            }

          }

        },     

      }

    })



  });

</script>



<script>

function isNumberKey(evt){

    var charCode = (evt.which) ? evt.which : event.keyCode

    if (charCode > 31 && (charCode < 48 || charCode > 57))

        return false;

    return true;

}

</script>



<script language="javascript">

function getkey(e)

{

if (window.event)

   return window.event.keyCode;

else if (e)

   return e.which;

else

   return null;

}

function goodchars(e, goods, field)

{

var key, keychar;

key = getkey(e);

if (key == null) return true;

 

keychar = String.fromCharCode(key);

keychar = keychar.toLowerCase();

goods = goods.toLowerCase();

 

// check goodkeys

if (goods.indexOf(keychar) != -1)

    return true;

// control keys

if ( key==null || key==0 || key==8 || key==9 || key==27 )

   return true;

    

if (key == 13) {

    var i;

    for (i = 0; i < field.form.elements.length; i++)

        if (field == field.form.elements[i])

            break;

    i = (i + 1) % field.form.elements.length;

    field.form.elements[i].focus();

    return false;

    };

// else return false

return false;

}

</script>





<!-- Untuk Location -->

<input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">

<script>

var apiGeolocationSuccess = function(position) {

  showLocation(position);

};



var tryAPIGeolocation = function() {

  jQuery.post( "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyDCa1LUe1vOczX1hO_iGYgyo8p_jYuGOPU", function(success) {

    apiGeolocationSuccess({coords: {latitude: success.location.lat, longitude: success.location.lng}});

  })

  .fail(function(err) {

    

  });

};



var browserGeolocationSuccess = function(position) {

  showLocation(position);

};



var browserGeolocationFail = function(error) {

  switch (error.code) {

    case error.TIMEOUT:

      alert("Browser geolocation error !\n\nTimeout.");

      break;

    case error.PERMISSION_DENIED:

      if(error.message.indexOf("Only secure origins are allowed") == 0) {

        tryAPIGeolocation();

      }

      break;

    case error.POSITION_UNAVAILABLE:

      alert("Browser geolocation error !\n\nPosition unavailable.");

      break;

  }

};



var tryGeolocation = function() {

  if (navigator.geolocation) {

    navigator.geolocation.getCurrentPosition(

      browserGeolocationSuccess,

      browserGeolocationFail,

      {maximumAge: 50000, timeout: 20000, enableHighAccuracy: true});

  }

};



tryGeolocation();



function showLocation(position) {

  var latitude = position.coords.latitude;

  var longitude = position.coords.longitude;

  var _token = $('#_token').val();

  $.ajax({

    type:'POST',

    url: "{{url('/')}}/getLocation",

    data: { latitude: latitude, _token : _token, longitude : longitude },

    success:function(data){

            if(data){

              $("#latitude").html("<input type='hidden' name='latitude' id='latitude' value='"+data.latitude+"'>");

              $("#longitude").html("<input type='hidden' name='longitude' id='longitude' value='"+data.longitude+"'>");

               $("#location").html("<input type='hidden' name='location' id='location' value='"+data.location+"'>");

                 



            }else{

                $("#location").html('Not Available');

            }

    }

  });

}

</script>







@endpush