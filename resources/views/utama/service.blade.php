@extends('layouts.main')

@section('content')
  <section class="probootstrap-cover overflow-hidden relative"  @if(empty($images->image)) style="background-image: url('uploads/kl.jpg');" @else style="background-image: url('uploads/{{$images->image}}');" @endif data-stellar-background-ratio="0.5"  id="section-home">
      <div class="overlay"></div>
      <div class="container">
        <div class="row align-items-center text-center">
          <div class="col-md">
            <h2 class="heading mb-2 display-4 font-light probootstrap-animate">Global I Exceed Management Sdn.Bhd</h2> 
            <p class="lead mb-5 probootstrap-animate">
              

            <!-- </p>
              <a href="onepage.html" role="button" class="btn btn-primary p-3 mr-3 pl-5 pr-5 text-uppercase d-lg-inline d-md-inline d-sm-block d-block mb-3">See OnePage Verion</a> 
            </p> -->
          </div> 
        </div>
      </div>
    
    </section>
    <!-- END section image -->

    <section class="probootstrap_section bg-light">
      <div class="container">
        <div class="row text-center mb-5 probootstrap-animate">
          <div class="col-md-12">
            <h2 class="display-4 border-bottom probootstrap-section-heading">Service</h2>
          </div>
        </div>

        <div class="row probootstrap-animate">
          <div class="col-md-4">
            <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">
              <img src="assets/images/sq_img_2.jpg" alt="Free Template by ProBootstrap" class="img-fluid">
              <div class="media-body">
                <h2 class="mb-3 text-center">MUMTAZ-I</h2>
                <p class="text-center">Goverment Sector and selected GLC.<br>
                Sector Awam dan GLC terpilih. </p>
                <div class="batas">
                  <button class="btn btn-danger" data-target="#collapseDetailOne" data-toggle="collapse" style="cursor:pointer;">Detail</button>
                  <button class="btn btn-danger" style="cursor:pointer;">Form</button>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-4">
            <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">
              <img src="assets/images/sq_img_1.jpg" alt="Free Template by ProBootstrap" class="img-fluid">
              <div class="media-body">
                <h2 class="mb-3 text-center">AFDHAL-I</h2>
                <p class="text-center">Goverment Sector and selected GLC.<br>
                  Sector Awam dan GLC terpilih</p>
                <div class="batas">
                  <button class="btn btn-danger" data-target="#collapseDetailTwo" data-toggle="collapse" style="cursor:pointer;">Detail</button>
                  <button class="btn btn-danger" style="cursor:pointer;">Form</button>

                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">
              <img src="assets/images/sq_img_3.jpg" alt="Free Template by ProBootstrap" class="img-fluid">
              <div class="media-body">
                <h2 class="mb-5 text-center">PRIVATE SECTOR</h2>
                <p class="text-center">Sektor Swasta.</p>
                  <div class="batas">
                    <button class="btn btn-danger" data-target="#collapseDetailThree" data-toggle="collapse" style="cursor:pointer;">Detail</button>
                    <button class="btn btn-danger" style="cursor:pointer;">Form</button>
                  </div>
              </div>
            </div>
          </div>
        </div> <!-- end item -->

        <div class="row probootstrap-animate"> <!-- Row for detail -->
            <div class="col-md-12">
              <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">
                <div id="collapseDetailOne" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                  <div class="media-body text-center">
                    <h4 class="display-5 border-bottom probootstrap-section-heading">MUMTAZ-I</h4>
                    <p>Mumtaz-i Mumtaz-i is a special personal financing-i package with lower rates. This package is offered to public servants and employees of companies having salary deduction arrangement with MBSB. •  Financing amount of up to RM250,000 • Competitive financing rates as low as 0.20% below MBSB’s ECOF-i (equivalent to 3.66% flat rate at 10 years tenure) •  Financing tenure of up to 10 years •  Takaful coverage is optional •  No guarantor required • Financing payment through salary deduction via Biro Angkasa /Accountant General or employer salary deduction Who Can Apply •  Malaysian citizen aged 19 years old and not exceeding 60 years upon expiry of facility or optional retirement age, whichever is earlier • Permanent employee of Government, GLCs, local councils and selected companies having salary deduction arrangements with MBSB with at least 12 months in service Terms and Conditions apply. Notes: ECOF-i = Effective Cost of Fund-i, GLC = Government Linked Company MBSB's current ECOF-i is 6.75% </p>
 
                  </div>
                </div>
              </div>    
            </div>

            <div class="col-md-12">
              <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">
                <div id="collapseDetailTwo" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                  <div class="media-body text-center">
                    <h4 class="display-5 border-bottom probootstrap-section-heading">AFDHAL-I</h4>
                    <p>AFDHAL-i is a special personal financing-i package with lower rates. This package is offered to public servants and employees of companies having salary deduction arrangement with MBSB. •  Financing amount of up to RM250,000 • Competitive financing rates as low as 0.20% below MBSB’s ECOF-i (equivalent to 3.66% flat rate at 10 years tenure) •  Financing tenure of up to 10 years •  Takaful coverage is optional •  No guarantor required • Financing payment through salary deduction via Biro Angkasa /Accountant General or employer salary deduction Who Can Apply •  Malaysian citizen aged 19 years old and not exceeding 60 years upon expiry of facility or optional retirement age, whichever is earlier • Permanent employee of Government, GLCs, local councils and selected companies having salary deduction arrangements with MBSB with at least 12 months in service Terms and Conditions apply. Notes: ECOF-i = Effective Cost of Fund-i, GLC = Government Linked Company MBSB's current ECOF-i is 6.75% </p>
 
                  </div>
                </div>
              </div> 
            </div>

            <div class="col-md-12">
              <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">
                <div id="collapseDetailThree" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                  <div class="media-body text-center">
                    <h4 class="display-5 border-bottom probootstrap-section-heading">PRIVATE SECTOR</h4>
                    <p>Personal Financing-i Learn about MBSB's Shariah compliant personal financing solutions through our host of attractive packages that best suit your financing needs. •  * Competitive financing rates • * Financing amount of up to RM400,000 • * Financing tenure of up to 10 years •  * No guarantor required • * No hidden charges • * Fast approval • * Applicable for public servant and private sector employees Terms and conditions apply Please visit our offices or contact our Customer Service at 03-9999 9999 for more information about the product. On Monday, 8 January 2018, 12:57, Omar Aziz wrote: Mumtaz-i Mumtaz-i is a special personal financing-i package with lower rates. This package is offered to public servants and employees of companies having salary deduction arrangement with MBSB. •  Financing amount of up to RM250,000 • Competitive financing rates as low as 0.20% below MBSB’s ECOF-i (equivalent to 3.66% flat rate at 10 years tenure) •  Financing tenure of up to 10 years •  Takaful coverage is optional •  No guarantor required • Financing payment through salary deduction via Biro Angkasa /Accountant General or employer salary deduction Who Can Apply •  Malaysian citizen aged 19 years old and not exceeding 60 years upon expiry of facility or optional retirement age, whichever is earlier • Permanent employee of Government, GLCs, local councils and selected companies having salary deduction arrangements with MBSB with at least 12 months in service Terms and Conditions apply. Notes: ECOF-i = Effective Cost of Fund-i, GLC = Government Linked Company MBSB's current ECOF-i is 6.75% </p>
 
                  </div>
                </div>
              </div> 
            </div>
        </div> <!-- End Row for detail -->
        


      </div>
    </section> <!-- END section -->  


@endsection