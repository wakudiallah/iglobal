<style type="text/css">
    .sidebar .user-info {
    padding: 13px 15px 12px 15px;
    white-space: nowrap;
    position: relative !important;
    border-bottom: 1px solid #e9e9e9;
    background:  @if(empty(Auth::user()->bc)) url("{{url('/')}}/admin/images/bck-profil.jpg") @else url("{{url('/')}}/admin/images/{{Auth::user()->bc}}") @endif no-repeat no-repeat !important;
    height: 135px; }
</style>


    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    @if(empty(Auth::user()->profile))<img src="{{url('/')}}/admin/images/user.png" width="48" height="48" alt="User">@else<img src="{{url('/')}}/admin/images/profile/{{Auth::user()->profile}}" width="48" height="48" alt="User"/>@endif
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}{{ Auth::user()->id }}</div>
                    <div class="email">{{ Auth::user()->email }}</div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="{{route('profile1.index')}}"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <!-- <li><a href="javascript:void(0);"><i class="material-icons">group</i>Day Off</a></li> -->
                            <li>
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    Logout <i class="material-icons">input</i>
                                    {{ csrf_field() }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
                    <li class="active">
                        <a href="{{ url('adminnew') }}">
                            <i class="material-icons">home</i>
                            <span>Dashboard</span>
                        </a>
                    </li>

                    @can('view_tests')
                    <li>
                        <a href="{{ route('test.index') }}">
                            <i class="material-icons">person_pin</i>
                            <span>Test</span>
                        </a>
                    </li>
                    @endcan

                    @can('view_customers', 'add_customers')
                    <li>
                        <a href="{{ url('/assess_addcus') }}">
                            <i class="material-icons">person_pin</i>
                            <span>Add New Customer</span>
                        </a>
                    </li>
                    @endcan

                    @can('view_customer_by_mos','add_customer_by_mos')
                    <li>
                        <a href="{{ url('/cus_mo/create') }}">
                            <i class="material-icons">person</i>
                            <span>Add New Customer By MO</span>
                        </a>
                    </li>
                    @endcan

                    @can('view_send_mbsb_ics')
                    <li>
                        <a href="{{route('assessment1.index')}}">
                            <i class="material-icons">send</i>
                            <span>Send To MBSB</span>
                        </a>
                    </li>  
                    @endcan

                    @can('view_loan_eligibility_scorings') 
                    <li>
                        <a href="{{url('/mo_cal_check')}}">
                            <i class="material-icons">work</i>
                            <span>Loan Eligibility & Scoring</span>
                        </a>
                    </li>
                    @endcan

                    @can('view_loan_eligibility_scorings')
                    <li>
                        <a href="{{url('/nextcalchange')}}">
                            <i class="material-icons">sync_problem</i>
                            <span>Calculation & Change</span>
                        </a>
                    </li>
                    @endcan

                    @can('view_pending_documens')
                    <li>
                        <a href="{{url('/pendingdoc')}}">
                            <i class="material-icons">hourglass_empty</i>
                            <span>Pending Documentation</span>
                        </a>
                    </li>
                    @endcan

                    @can('view_doc_incompletes')
                    <li>
                        <a href="{{url('/docincomplete_menu')}}">
                            <i class="material-icons">cancel</i>
                            <span>Doc Incomplete</span>
                        </a>
                    </li>
                    @endcan

                    @can('view_additional_docs')
                    <li>
                        <a href="{{url('/additional_doc')}}">
                            <i class="material-icons">help</i>
                            <span>Additional Document</span>
                        </a>
                    </li>
                    @endcan


                    @can('view_upload_spekars')
                    <li>
                        <a href="{{url('/uploadspekar_p1')}}">
                            <i class="material-icons">file_upload</i>
                            <span>Upload Spekar</span>
                        </a>
                    </li>  
                    @endcan

                    @can('view_route_to_mos')
                    <li>
                        <a href="{{url('/route_to_mo')}}">
                            <i class="material-icons">reply_all</i>
                            <span>Route to MO</span>
                        </a>
                    </li>  
                    @endcan

                    

                    @can('view_document_incompletes')
                    <li>
                        <a href="{{url('pendingdoc')}}">
                            <i class="material-icons">hourglass_empty</i>
                            <span>Pending Documentation</span>
                        </a>
                    </li>
                    @endcan
                    
                    @can('view_document_incompletes')
                    <li>
                        <a href="{{route('docincomplete.index')}}">
                            <i class="material-icons">description</i>
                            <span>Document Incomplete</span>
                        </a>
                    </li>
                    @endcan

                    @can('view_loan_egi_checkings')
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">widgets</i>
                            <span>Loan Eligibility Checking and Calculation</span>
                        </a>
                        <ul class="ml-menu">
                            <li class="{{ Request::is('check-loan-cal-p2') ? 'active' : '' }}">
                                <a href="{{url('check-loan-cal-p2')}}">Loan Cal</a>
                            </li>
                            <li class="{{ Request::is('doc-check') ? 'active' : '' }}">
                                <a href="{{ url('doc-check') }}">Doc Check</a>
                            </li>
                            <li class="{{ Request::is('call') ? 'active' : '' }}">
                                <a href="{{url('call')}}">Call 103</a>
                            </li> 
                        </ul>
                    </li>

                    <li>
                        <a href="{{url('docincomplete/p2')}}">
                            <i class="material-icons">close</i>
                            <span>Document Incomplete</span>
                        </a>
                    </li>

                    @endcan

                    @can('view_batch_to_mbsbs')
                        <li>
                            <a href="{{url('batch-to-mbsb-process-2')}}">
                                <i class="material-icons">donut_small</i>
                                <span>Ready to MBSB</span>
                            </a>
                        </li>
                    @endcan

                    @can('view_list_submissions')
                        <li>
                            <a href="{{url('get-submission-list')}}">
                                <i class="material-icons">print</i>
                                <span>Submission List</span>
                            </a>
                        </li>
                    @endcan

                    @can('view_assessment_lists')
                        <li>
                            <a href="{{url('get-assessment-list')}}">
                                <i class="material-icons">print</i>
                                <span>Assessment List</span>
                            </a>
                        </li>
                    @endcan

                    @can('view_report_managers')
                        <li>
                            <a href="{{url('report-manager-failed-bymo')}}">
                                <i class="material-icons">close</i>
                                <span>Failed By MO</span>
                            </a>
                        </li>
                    @endcan

                    @can('view_report_managers')
                        
                        <li>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="material-icons">offline_pin</i>
                                <span>Application Approval</span>
                            </a>
                            <ul class="ml-menu">
                                
                                <li class="{{ Request::is('report-manager-approved') ? 'active' : '' }}">
                                    <a href="{{ url('report-manager-approved') }}">Approved Loan</a>
                                </li>
                                <li class="{{ Request::is('report-manager-rejected') ? 'active' : '' }}">
                                    <a href="{{url('report-manager-rejected')}}">Rejected Loan</a>
                                </li> 
                            </ul>
                        </li>
                    @endcan

                    @can('view_report_managers')
                        <li>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="material-icons">print</i>
                                <span>Report</span>
                            </a>
                            <ul class="ml-menu">
                                
                                <li class="{{ Request::is('report-manager-for-employer') ? 'active' : '' }}">
                                    <a href="{{ url('report-manager-for-employer') }}">Report Employer</a>
                                </li>
                                <li class="{{ Request::is('report-manager-for-mo') ? 'active' : '' }}">
                                    <a href="{{url('report-manager-for-mo')}}">Report MO</a>
                                </li> 
                            </ul>
                        </li>    

                    @endcan

                    
                    @can('view_approve_disbursement_processor3s')
                    <!-- <li>
                        <a href="{{url('update-approval')}}">
                            <i class="material-icons">offline_pin</i>
                            <span>Application Approval</span>
                        </a>
                    </li> -->

                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">offline_pin</i>
                            <span>Application Approval</span>
                        </a>
                        <ul class="ml-menu">
                            
                            <li class="{{ Request::is('approve-fix') ? 'active' : '' }}">
                                <a href="{{ url('approve-fix') }}">Approved Loan</a>
                            </li>
                            <li class="{{ Request::is('reject-fix') ? 'active' : '' }}">
                                <a href="{{url('reject-fix')}}">Rejected Loan</a>
                            </li>

                            <li class="{{ Request::is('add-doc-from-mbsb') ? 'active' : '' }}">
                                <a href="{{url('add-doc-from-mbsb')}}">Additional Doc Loan</a>
                            </li>

                        </ul>
                    </li>
                    @endcan
                    
                    @can('view_moaqs')
                        <li>
                            <a href="{{url('upload/amount')}}">
                                <i class="material-icons">offline_pin</i>
                                <span>Upload Amount Release</span>
                            </a>
                        </li>
                    @endcan

                    @can('view_moaqs')
                        <li>
                            <a href="{{url('moaqs/test')}}">
                                <i class="material-icons">find_in_page</i>
                                <span>Moaqs</span>
                            </a>
                        </li>
                    @endcan

                    @can('view_monthly_report_processor3s')
                        <li>
                            <a href="{{url('report/month')}}">
                                <i class="material-icons">assignment</i>
                                <span>Monthly Reporting</span>
                            </a>
                        </li>
                    @endcan

                    
                    @can('view_file_managers')
                    <!-- <li>
                        <a href="{{ route('file-manager.index') }}">
                            <i class="material-icons">folder_open</i>
                            <span>File Manager</span> 
                        </a>
                    </li> -->

                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">folder_open</i>
                            <span>File Manager</span>
                        </a>
                        <ul class="ml-menu">
                            
                            <li class="{{ Request::is('list-folder') ? 'active' : '' }}">
                                <a href="{{ url('list-folder') }}">Folder Name</a>
                            </li>
                            <li class="{{ Request::is('file-manager') ? 'active' : '' }}">
                                <a href="{{url('file-manager')}}">Document</a>
                            </li>
                        </ul>
                    </li>
                    @endcan
                    
                    @can('view_file_manager_users')
                    <li>
                        <a href="{{ url('detail-folder') }}">
                            <i class="material-icons">folder_open</i>
                            <span>File Manager User</span> 
                        </a>
                    </li>
                    @endcan


                    @can('view_loan_app_trackings')
                    <li>
                        <a href="{{ route('loan.index') }}">
                            <i class="material-icons">layers</i>
                            <span>Loan Application Tracking</span>  <!-- chart itu berada pada loan -->
                        </a>
                    </li>
                    @endcan

                    @can('view_loan_onlines')
                    <li>
                        <a href="{{ url('ol/loan') }}">
                            <i class="material-icons">network_wifi</i>
                            <span>Loan Application Online </span>  <!-- chart itu berada pada loan -->
                        </a>
                    </li>
                    @endcan

                    @can('view_search_loans', 'add_search_loans')
                    <li>
                        <a href="{{ url('search/loan') }}">
                            <i class="material-icons">pageview</i>
                            <span>Search Loan</span>  <!-- chart itu berada pada loan -->
                        </a>
                    </li>
                    @endcan

                
                    @can('view_activity')
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">build</i>
                            <span>Footel Activity</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{route('message.index')}}">Message By Prospect</a>
                            </li>
                            <li>
                                <a href="{{route('homeimage.index')}}">Image Home</a>
                            </li>
                        </ul>
                    </li>
                    @endcan

                    @can('view_announcements')
                    <li>
                        <a href="{{ url('announc') }}">
                            <i class="material-icons">mic</i>
                            <span>Announcement</span>
                        </a>
                    </li>
                    @endcan

                    @can('view_announcement_to_groups')
                    <li>
                        <a href="{{ route('announcementgroups.index') }}">
                            <i class="material-icons">mic</i>
                            <span>Announcement Group</span>
                        </a>
                    </li>
                    @endcan

                    @can('view_setupmodules')
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">text_fields</i>
                            <span>Setup Module</span>
                        </a>
                        <ul class="ml-menu">
                            <!-- <li class="{{ Request::is('package') ? 'active' : '' }}">
                                <a href="{{ route('package.index') }}">Pakej</a>
                            </li> -->
                            <li class="{{ Request::is('employment') ? 'active' : '' }}">
                                <a href="{{ route('employment.index') }}">Job Type</a>
                            </li>
                            <li class="{{ Request::is('loanpkg') ? 'active' : '' }}">
                                <a href="{{route('loanpkg.index')}}">Loan Package</a>
                            </li>

                            <li class="{{ Request::is('loan_detail') ? 'active' : '' }}">
                                <a href="{{url('loan_detail')}}">Rate</a>
                            </li>

                            <li class="{{ Request::is('comm') ? 'active' : '' }}">
                                <a href="{{route('comm.index')}}">Commissions</a>
                            </li>
                            <li class="{{ Request::is('emp') ? 'active' : '' }}">
                                <a href="{{route('emp.index')}}">Employer</a>
                            </li>
                            <li class="{{ Request::is('question') ? 'active' : '' }}">
                                <a href="{{route('question.index')}}">Question</a>
                            </li>
                            <li class="{{ Request::is('file-calculation') ? 'active' : '' }}">
                                <a href="{{route('file-calculation.index')}}">File Calculation</a>
                            </li>
                            <!-- <li class="{{ Request::is('remark') ? 'active' : '' }}">
                                <a href="{{route('remark.index')}}">Remark</a>
                            </li> -->
                            <li class="{{ Request::is('stage') ? 'active' : '' }}">
                                <a href="{{route('stage.index')}}">Stage</a>
                            </li>
                        </ul>
                    </li>
                    @endcan

                    @can('view_users')
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">person</i>
                            <span>User</span>
                        </a>
                        <ul class="ml-menu">
                            <li class="{{ Request::is('users') ? 'active' : '' }}">
                                <a href="{{ route('users.index') }}">User</a>
                            </li>
                            <li class="{{ Request::is('mo') ? 'active' : '' }}">
                                <a href="{{ route('mo.index') }}">Marketing Officer</a>
                            </li>
                            <li class="{{ Request::is('roles') ? 'active' : '' }}">
                                <a href="{{ route('roles.index') }}">Work Group</a>
                            </li>
                        </ul>
                    </li>
                    @endcan
                    
                    
                    @can('view_commissions')
                    <!-- <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">pie_chart</i>
                            <span class="bg-red">Commission Calculator</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{ url('commission') }}">Commision</a>
                            </li>
                            <li>
                                <a href="pages/forms/advanced-form-elements.html">Advanced Form Elements</a>
                            </li>
                        </ul>
                    </li> -->
                    @endcan

                     
                    
                    
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2018 <a href="javascript:void(0);">Admin - Netxpert</a>.
                </div>
                <div class="version">
                    <b>Version: </b> 1.0.0
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->
        <aside id="rightsidebar" class="right-sidebar">
            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                <li role="presentation" class="active"><a href="#skins" data-toggle="tab">SKINS</a></li>
                <li role="presentation"><a href="#settings" data-toggle="tab">SETTINGS</a></li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active in active" id="skins">
                    <ul class="demo-choose-skin">
                        <li data-theme="red" class="active">
                            <div class="red"></div>
                            <span>Red</span>
                        </li>
                        <li data-theme="pink">
                            <div class="pink"></div>
                            <span>Pink</span>
                        </li>
                        <li data-theme="purple">
                            <div class="purple"></div>
                            <span>Purple</span>
                        </li>
                        <li data-theme="deep-purple">
                            <div class="deep-purple"></div>
                            <span>Deep Purple</span>
                        </li>
                        <li data-theme="indigo">
                            <div class="indigo"></div>
                            <span>Indigo</span>
                        </li>
                        <li data-theme="blue">
                            <div class="blue"></div>
                            <span>Blue</span>
                        </li>
                        <li data-theme="light-blue">
                            <div class="light-blue"></div>
                            <span>Light Blue</span>
                        </li>
                        <li data-theme="cyan">
                            <div class="cyan"></div>
                            <span>Cyan</span>
                        </li>
                        <li data-theme="teal">
                            <div class="teal"></div>
                            <span>Teal</span>
                        </li>
                        <li data-theme="green">
                            <div class="green"></div>
                            <span>Green</span>
                        </li>
                        <li data-theme="light-green">
                            <div class="light-green"></div>
                            <span>Light Green</span>
                        </li>
                        <li data-theme="lime">
                            <div class="lime"></div>
                            <span>Lime</span>
                        </li>
                        <li data-theme="yellow">
                            <div class="yellow"></div>
                            <span>Yellow</span>
                        </li>
                        <li data-theme="amber">
                            <div class="amber"></div>
                            <span>Amber</span>
                        </li>
                        <li data-theme="orange">
                            <div class="orange"></div>
                            <span>Orange</span>
                        </li>
                        <li data-theme="deep-orange">
                            <div class="deep-orange"></div>
                            <span>Deep Orange</span>
                        </li>
                        <li data-theme="brown">
                            <div class="brown"></div>
                            <span>Brown</span>
                        </li>
                        <li data-theme="grey">
                            <div class="grey"></div>
                            <span>Grey</span>
                        </li>
                        <li data-theme="blue-grey">
                            <div class="blue-grey"></div>
                            <span>Blue Grey</span>
                        </li>
                        <li data-theme="black">
                            <div class="black"></div>
                            <span>Black</span>
                        </li>
                    </ul>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="settings">
                    <div class="demo-settings">
                        <p>GENERAL SETTINGS</p>
                        <ul class="setting-list">
                            <li>
                                <span>Report Panel Usage</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                            <li>
                                <span>Email Redirect</span>
                                <div class="switch">
                                    <label><input type="checkbox"><span class="lever"></span></label>
                                </div>
                            </li>
                        </ul>
                        <p>SYSTEM SETTINGS</p>
                        <ul class="setting-list">
                            <li>
                                <span>Notifications</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                            <li>
                                <span>Auto Updates</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                        </ul>
                        <p>ACCOUNT SETTINGS</p>
                        <ul class="setting-list">
                            <li>
                                <span>Offline</span>
                                <div class="switch">
                                    <label><input type="checkbox"><span class="lever"></span></label>
                                </div>
                            </li>
                            <li>
                                <span>Location Permission</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </aside>
        <!-- #END# Right Sidebar -->
    </section>