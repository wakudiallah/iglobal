@extends('vadmin.tampilan')

@section('content')

    <section class="content">
        <div class="container-fluid">

        

            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">help</i> Rejected </a></li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

            @include('shared.notif')
            


            <div class="row clearfix">
                
                <div class="card">
                    <div class="header bg-red">
                        <h2>Rejected </h2>
                    </div>
                    
                   <div class="body">
                                <!-- <input  value="Generate" class="btn btn-success" > -->
                                <table class="table" id="example">
                                    <thead>
                                        <tr>
                                            
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>IC</th>
                                            <th>Phone</th>
                                            <th>Date</th>
                                            <th>Status</th>
                                            <th>MO</th>
                                            <th>Activity</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1; ?>

                                        @foreach($rejected as $data)

                                            
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{ $data->name }}</td>
                                            <td>{{ $data->ic }}</td>
                                            <td>{{ $data->notelp }}</td>

                                            <td>{{ $data->created_at->toDayDateTimeString() }}</td>
                                            <td>
                                                @include('shared.stage')
                                            </td>
                                            <td>{{ $data->p2->name }}</td>
                                            <td>
                                               <a href="{{url('/history/show/'.$data->id_cus)}}" class="btn bg-orange btn-circle waves-effect waves-circle waves-float" onclick="window.open('{{url('/history/show/'.$data->id_cus)}}', 'newwindow', 'width=750,height=400'); return false;"> <i class="material-icons">history</i> </a>
                                            </td>
                                            <td>
                                                @if($data->stage == 'W0')
                                                    <a href="{{ url('view_form/'.$data->id_cus.'/first_step') }}" class="btn btn-sm bg-pink"><i class="material-icons">pageview</i> View Form </a>
                                                @endif
                                            </td>

                                        </tr>
                                    @endforeach
                                    

                                </form>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div> 
        <!-- =========================== End Upload Spekar / Processor 1 Status : Pending Spekar ========================= -->

            
           


            <!-- /////////////////// Modal //////////////// -->
            @foreach($rejected as $datax)
            <div class="modal fade" id="exampleModal{{$datax->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="color: red"><b>{{$datax->name}} <b> --( {{$datax->ic}} )</h5>
                    
                  </div>
                  <div class="modal-body">
                   
                    <div class="row">
                        <div class="col-md-2"></div>

                        <div class="col-md-8" style="margin: 30px 30px !important">
                            {!! Form::open(array('url'=>'save/spekar/'.$datax->id, 'method'=>'post', 'files'=>'true')) !!}

                            <input  class="hidden" type="text" name="name" value="{{$datax->name}} " >
                            <input  class="hidden" type="text" name="ic" value="{{$datax->ic}} " >
                            <input  class="hidden" type="text" name="id_cus" value="{{$datax->id_cus}} " >
                            <input  class="hidden" type="text" name="routeto" value="{{$datax->process1}} " >
                            
                            <form id="uploadform" target="upiframe" action="{{url('save/spekar/'.$datax->id)}}" method="post" enctype="multipart/form-data">
                            
                                <input type="file" name="fileToUpload" class="test form-control" onchange='this.form.submit()''>

                            </form>
                        </div>

                        <div class="col-md-2"></div>
                        

                    </div>
                    

                  </div>
                  <div class="modal-footer">
                    
                    <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-success waves-effect">

                    {!! Form::close() !!}   -->                  
                  </div>

                </div>
              </div>
            </div>
            
            @endforeach

            <!-- /////////////////// End Modal //////////////// -->
            
        </div>
    </section>


@include('vadmin.jsalternatif')

<!-- ================= Data Target hidden ======= -->
<script src="https://code.jquery.com/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
 

<script type="text/javascript">
    $("[data-collapse-group]").on('show.bs.collapse', function () {
          var $this = $(this);
          var thisCollapseAttr = $this.attr('data-collapse-group');
          $("[data-collapse-group='" + thisCollapseAttr + "']").not($this).collapse('hide');
        });
</script>

<!-- ================= End Data Target hidden ======= -->

    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>

    <script>
        $(document).ready(function() {
        $('#example').DataTable();
        });
    </script>


    <!-- upload automatic -->
    <script type="text/javascript">
        function test() {
            var form = document.getElementById('uploadform');
            form.submit();
        };
    </script>


    <!-- Checkbox detect -->
    <script type="text/javascript">
        $( '#popup-validation' ).on('submit', function(e) {
           if($( 'input[class^="invitation-friends"]:checked' ).length === 0) {
              alert( 'Please! Select the application' );
              e.preventDefault();
           }
        });
    </script>    


@endsection
    