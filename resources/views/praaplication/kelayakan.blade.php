@extends('layouts.main')

@section('content')
  <section class="probootstrap-cover overflow-hidden relative"  style="background-image: url('{{ asset('assets/images/kl.jpg') }}');" data-stellar-background-ratio="0.5"  id="section-home">
    <div class="overlay"></div>
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-5">
          <h2 class="heading mb-2 display-4 font-light probootstrap-animate">Global I Exceed Management Sdn.Bhd</h2> 
          <p class="lead mb-5 probootstrap-animate">
          <!-- </p>
            <a href="onepage.html" role="button" class="btn btn-primary p-3 mr-3 pl-5 pr-5 text-uppercase d-lg-inline d-md-inline d-sm-block d-block mb-3">See OnePage Verion</a> 
          </p> -->
        </div> 
        <div class="col-md-7 probootstrap-animate">
          <form action="{{ url('upload-praaplication') }}" class="probootstrap-form border border-danger" method="post">
            <div class="form-group">
              <div class="row mb-3">
                <div class="col-md">
                @if(Session::has('flash_message'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                  {!! session('flash_message') !!}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                </div>
              </div>
              <div class="row mb-3">
                <div class="col-md">
                  <div class="form-group">
                    <label for="probootstrap-date-departure">Package</label>
                    <div class="probootstrap-date-wrap">
                      <span class="icon ion-card"></span> 
                      <input type="text" id="" class="form-control" placeholder="" value="{{ $pra->loanpkg->Ln_Desc }}" disabled>
                    </div>
                  </div>
                </div>
                <div class="col-md">
                  <div class="form-group">
                    <label for="probootstrap-date-arrival">FUNDAMENTAL FINANCING QUALIFICATIONS<!-- Kelayakan Pembiayaan Maksima --></label>
                     @foreach($loan as $loan)
                                        <?php
                                        $salary_dsr = ($zbasicsalary * ($loan->dsr / 100)) - $zdeduction;
                                        $ndi = ($zbasicsalary - $zdeduction) -  1300;
                                        $max  =  $salary_dsr * 12 * 10 ;
                                                                       
                                        function pembulatan($uang) {
                                            $puluhan = substr($uang, - 3);
                                                if($puluhan<500) {
                                                        $akhir = $uang - $puluhan; 
                                                    }

                                                else{
                                                        $akhir = $uang - $puluhan;
                                                    }

                                                return $akhir;
                                                    }

                                        if(!empty($loan->max_byammount))  {
                                          
                                            $ansuran = intval($salary_dsr)-1;
                                              if($pra->loanpkg_code =="1") {  //aku edit
                                                  $bunga = 3.8/100;
                                              }
                                              elseif($pra->loanpkg_code == "2") {
                                                  $bunga = 4.9/100;
                                              }

                                              else {
                                                  $bunga = 5.92/100;
                                              }
                                           
                                              $pinjaman = 0;

                                              for ($i = 0; $i <= $loan->max_byammount; $i++) {
                                                  $bungapinjaman = $i  * $bunga * $durasi;   //
                                                  $totalpinjaman = $i + $bungapinjaman ;
                                                  $durasitahun = $durasi * 12;
                                                  $ansuran2 = intval($totalpinjaman / ($durasi * 12))  ;
                                                  //echo $ansuran2."<br>";
                                                  if ($ansuran2 < $ndi)
                                                  {
                                                      $pinjaman = $i;
                                                  }
                                              }   

                                              if($pinjaman > 1) {

                                                  $bulat = pembulatan($pinjaman);
                                                  $loanx =  number_format($bulat, 0 , ',' , ',' ) ; 
                                                  $loanz = $bulat;
                                              }
                                              else {
                                                  $loanx =  number_format($loan->max_byammount, 0 , ',' , ',' ) ; 
                                                  $loanz = $loan->max_byammount;
                                              }
                                        }
                                        else { 

                                            $bulat = pembulatan($loan->max_bysalary * $total_salary);
                                            $loanx =  number_format($bulat, 0 , ',' , ',' ) ; 
                                            $loanz = $bulat;
                                            if ($loanz > 199000) {

                                                  $loanz  = 250000;
                                                  $loanx =  number_format($loanz, 0 , ',' , ',' ) ; 
                                            }
                                        }

                                        ?>
                                    @endforeach

                    
                    <div class="probootstrap-date-wrap">
                      <span class="icon ion-card"></span> 
                      <input type="text" id="" class="form-control" placeholder="">
                    </div>
                  </div>
                </div>
              </div>

              <div class="row mb-3">
                <div class="col-md">
                  <div class="form-group">
                    <label for="probootstrap-date-departure"> Total Financing (RM) <!-- Jumlah Pembiayaan (RM) --></label>
                    <div class="probootstrap-date-wrap">
                      <span class="icon ion-card"></span> 
                      <input type="text" id="" class="form-control" placeholder="" value="{{$pra->jml_pem}}" disabled>
                    </div>
                  </div>
                </div>
                <div class="col-md">
                  <div class="form-group">
                    <label for="probootstrap-date-arrival"> <!-- Jumlah Pendapatan--> Total income</label>
                    <div class="probootstrap-date-wrap">
                      <span class="icon ion-card"></span> 
                      <input type="text" id="" class="form-control" placeholder="" value="{{$pra-> gaji_asas}}" disabled>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row mb-3">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="probootstrap-date-departure"><!-- Ansuran Maksima--> Max Installment</label>
                    <div class="probootstrap-date-wrap">
                      <span class="icon ion-card"></span> 
                      <input type="text" id="" class="form-control" placeholder="">
                    </div>
                  </div>
                </div>                
              </div> 
              <!-- END row -->
              <div class="row mb-5">
                <div class="col-md">
                  
                </div>
                <div class="col-md">
                  
                  <!-- Button trigger modal -->
                  <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal" style="cursor: pointer;">
                    Recount
                  </button>

                </div>
              </div>

              <div class="row">
                <table class="table table-hover" style="margin-top: 30px !important">
                      <thead>
                          <tr>
                              <th width="20" valign="middle"> <b>  Duration {{$id_loan}}  </b> </th>
                              <th width="40"> <b> Financing Amount  </b> </th>
                              <th width="20"><b>  Monthly installment </b> </th>
                              <th width="20"> <b>  Profit Rate </b> </th>
                              <th width="10" align="center"> <b>  Select </b> </th>
                          </tr>
                      </thead>
                      <tbody>
                           <?php if( $pra->jml_pem <= $loanz ) { $ndi_limit=$loan->ndi_limit;?>
                                  @foreach($tenure as $tenure)
                                      <?php 
                                             $bunga2 =  $pra->jml_pem * $tenure->rate /100   ;
                                             $bunga = $bunga2 * $tenure->years;
                                             $total = $pra->jml_pem + $bunga ;
                                             $bulan = $tenure->years * 12 ;
                                             $installment =  $total / $bulan ;
                                             $ndi_state = ($total_salary - $zdeduction) - $installment; 
                                             
                                             if($installment  <= $salary_dsr && $ndi_state>=$ndi_limit) {     
                                          ?>
                                      <tr>
                                          <td>{{$tenure->years}} years</td>
                                          <td> RM {{ number_format( $pra->jml_pem, 0 , ',' , ',' )  }}  </td>
                                          
                                          <td> RM {{ number_format($installment, 0 , ',' , ',' )  }} /month</td>
                                        
                                          <td>{{$tenure->rate}} %</td>
                                          <td>  
                                            <input type="hidden" name="cus_id" value="{{$pra->id_cus}}">
                                            <input name="tenure" type="radio" id="radio_1_{{$tenure->id}}" value="{{$tenure->id}}"  required />
                                          <label for="radio_1_{{$tenure->id}}"></label>
                                          </td>
                                      </tr>
                                                      
                                       <?php }  ?>
                                  @endforeach
                              <?php } ?>
                      
                      </tbody>
                      

                  </table>
              </div>

              <!-- Button trigger modal -->
                   <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal" style="cursor: pointer;">
                    Hantar
                  </button>

            </div>
          </form>
        </div>



      <!-- Modal -->
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Pendaftaran</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">

              {!! Form::open(['url' => ['praaplication/kelayakan'] ]) !!}

              {{ csrf_field() }}

              <input type="hidden" name=" id_cus" value="{{$kelayakan->id_cus}}" id="" class="form-control" placeholder="" >

              <input type="hidden" name="name" value="{{$kelayakan->name}}" id="" class="form-control" placeholder="" >
              <div class="col-md">
                <div class="form-group">
                  <label for="probootstrap-date-arrival"> Emel</label>
                  <div class="probootstrap-date-wrap">
                    <span class="icon ion-card"></span> 
                    <input type="email" name="email" id="email" class="form-control" placeholder="" required>
                  </div>
                </div>
              </div>
              <div class="col-md">
                <div class="form-group">
                  <label for="probootstrap-date-arrival"> Kata Laluan</label>
                  <div class="probootstrap-date-wrap">
                    <span class="icon ion-key"></span> 
                    <input type="password" name="password" id="password" class="form-control" onkeyup='check();' required>
                  </div>
                </div>
              </div>
              <div class="col-md">
                <div class="form-group">
                  <label for="probootstrap-date-arrival"> Pengesahan Kata Laluan</label>
                  <div class="probootstrap-date-wrap">
                    <span class="icon ion-key"></span> 
                    <input type="password" class="form-control" name="confirm_password" id="confirm_password"  onkeyup='check();'>
                  </div>
                  <span id='message'></span>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <!-- <button type="button" class="btn btn-secondary" style="cursor: pointer;" data-dismiss="modal">Batal</button> -->
              
              <input type="submit" value="Submit" class="btn btn-xs btn-danger btn-block" style="cursor:pointer;">

            </div>
          </div>
          {!! Form::close() !!}
        </div>
      </div>

      </div>



    </div>
  </section>

@push('script')

<script type="text/javascript">
    $(document).ready(function() {
   $('.selectpicker').selectpicker(); $('#form-validate-pra').bootstrapValidator({
      
      fields: {
       
        email: {
          validators: {
            notEmpty: {
              message: 'Sila masukkan emel'
            },
            emailAddress: {
            message: 'Sila masukkan alamat emel yang sah'
            }
          }
        },
        password: {
          validators: {
            stringLength: {
              min: 6,
              message: 'Sila masukkan sekurang-kurangnya 6 char kata laluan'
            },
            notEmpty: {
              message: 'Sila masukan kata laluan'
            }
          }
        },
             
      }
    })

  });
</script>
@endpush

<!-- Matching password -->
<script type="text/javascript">
  var check = function() {
    if (document.getElementById('password').value ==
      document.getElementById('confirm_password').value) {
      document.getElementById('message').style.color = 'green';
      document.getElementById('message').innerHTML = 'matching';
    } else {
      document.getElementById('message').style.color = 'red';
      document.getElementById('message').innerHTML = 'not matching';
    }
  }
</script>

@endsection
