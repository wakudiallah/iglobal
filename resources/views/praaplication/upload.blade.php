@extends('layouts.main')

@section('content')
  <section class="probootstrap-cover overflow-hidden relative"  style="background-image: url('{{ asset('assets/images/kl.jpg') }}');" data-stellar-background-ratio="0.5"  id="section-home">
    <div class="overlay"></div>
    <div class="container">
      <div class="row align-items-center">
         
        <div class="col-md-12 probootstrap-animate">
         
          {!! Form::open(['url' => ['praaplication/finish'], 'id' => 'form-validate-pra', 'class' => 'probootstrap-form border border-danger','enctype' => 'multipart/form-data']) !!}

          {{ csrf_field() }}

            <div class="form-group">
              <div class="row mb-3">
                <div class="col-md">
                  <h5 style="color:red">*Dokumen dalam PDF/JPG/JPEG/PNG</h5>
                </div>
              </div>
              <div class="row mb-5">
                <div class="col-md">
                  
                  <div class="form-group" >
                    <label for="probootstrap-date-arrival">Salinan Penyata Pengenalan</label>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id_cus" value="{{ $reg->id_cus}}">
                    <input type="hidden" name="type1" value="1" id="type1">

                    <div class="probootstrap-date-wrap">
                      <span class="icon ion-document"></span>
                      <input id="fileupload1"  @if(empty($document1->name))  @endif    class="form-control" type="file" name="file1" >
                    </div>

                    <input type="hidden" name="document1"   id="documentx1"  value="Salinan Pengenal">&nbsp; <span id="document1"> </span> 
                      @if(!empty($document1->name))
                      <span id="document1a"><a target='_blank' href="{{url('/')}}/documents/user_doc/{{$reg->ic}}/{{$document1->doc_pdf}}"> {{$document1->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
                      @else
                      <!--<a class='bnt btn-danger'>No Attachment Available</a>-->
                      @endif
                      
                  </div>
                </div>
                <div class="col-md">
                  <div class="form-group">
                    <label for="probootstrap-date-arrival">Salinan Consent Letter</label>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id_cus" value="{{ $reg->id_cus}}">
                    <input type="hidden" name="type2" value="2" id="type2">

                    <div class="probootstrap-date-wrap">
                      <span class="icon ion-document"></span> 
                      <input id="fileupload2"  @if(empty($document2->name))  @endif    class="form-control" type="file" name="file2" >
                    </div>

                    <input type="hidden" name="document2"   id="documentx2"  value="Salinan Consent Letter">&nbsp; <span id="document2"> </span> 
                      @if(!empty($document2->name))
                      <span id="document2a"><a target='_blank' href="{{url('/')}}/documents/user_doc/{{$reg->ic}}/{{$document2->doc_pdf}}"> {{$document2->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
                      @else
                      <!--<a class='bnt btn-danger'>No Attachment Available</a>-->
                      @endif

                  </div>
                </div>
              </div>


              <div class="row mb-5">
                <div class="col-md-10"> </div>
                <div class="col-md-2">
                
                  <!-- Button trigger modal -->
                  
                  <input type="submit" value="Hantar" class="btn btn-xs btn-danger btn-block" style="cursor:pointer;">

                </div>
              </div>
   
            </div>
          {!! Form::close() !!}
        </div>   
      </div>
    </div>
 
     
        <!-- Modal -->
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        
        @include('praaplication._modal')

      </div> <!-- End of Modal -->
  </section><!-- END section -->

<!-- Check -->
<script>
  function check() {
      document.getElementById("myCheck").checked = true;
  }           
</script>
<!-- End of Check -->  



@endsection

@push('script')
<!-- upload file -->
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="{{url('/')}}/assets/js/file/vendor/jquery.ui.widget.js"></script>

<script src="{{url('/')}}/assets/js/file/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="{{url('/')}}/assets/js/file/jquery.fileupload.js"></script>
<?php for ($x = 1; $x <= 12; $x++) {  ?>

<script type="text/javascript">
/*jslint unparam: true */
/*global window, $ */
$(function () {
    'use strict';

    // Change this to the location of your server-side upload handler:
    var url = window.location.hostname === 'blueimp.github.io' ?
                '//jquery-file-upload.appspot.com/' : '{{url('/')}}/praaplication/upload-file/uploads/{{$x}}'
    $('#fileupload{{$x}}').fileupload({
        url: url,
        dataType: 'json',
        success: function ( data) {
             var text = $('#documentx{{$x}}').val();
            $("#document{{$x}}").html("<a target='_blank' href='"+"{{url('/')}}/documents/user_doc/{{$reg->ic}}/"+data.file+"'>"+text+"</a><i class='glyphicon glyphicon-ok txt-color-green'></i>");
             $("#document{{$x}}a").hide();
             $("#a{{$x}}").val(data.file);
            $("#a{{$x}}").val(data.file);
      $("#a{{$x}}").val(data.file);

             document.getElementById("fileupload{{$x}}").required = false;
             
          
            

        }
       
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

        
});
</script>
<?php } ?>

<!-- end of upload file -->

@endpush

