<!-- CSS Step =-->
<link href="{{ asset('admin/css/tabsteptype.css') }}" rel="stylesheet" />


<div class="row clearfix">
    <!-- Task Info -->
        <div class="row clearfix"> <!-- Breadcrumber -->
            <div class="col-md-6">
                <ol class="breadcrumb breadcrumb-col-pink">
                    <li><a href="javascript:void(0);"><i class="material-icons">layers</i> Loan Check & Calculation</a></li>
                </ol>
            </div>
        </div> <!-- End of breadcrumber -->

        @include('shared.notif')
    

    <!-- #END# Task Info -->
</div>

    <!-- ///////////////////  Data Target  ///////////////// -->
   
   
        <div class="row clearfix">
            <div class="card">
                <div class="header bg-red">
                    <h2>Detail {{$step1->name}} </h2>
                </div>
                <div class="body">
                    
                    <!-- start step -->
                   <section class="design-process-section" id="process-tab">
                        <div class="row">
                          <div class="col-xs-12"> 
                            <!-- design process steps--> 
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs process-model more-icon-preocess" role="tablist">
                              <li role="presentation" class="active"><a href="#discover{{$step1->id}}" aria-controls="discover{{$step1->id}}" role="tab" data-toggle="tab"> <i class="material-icons" aria-hidden="true">credit_card</i>
                                <p>Loan Calculation</p>
                                </a></li>
                              <li role="presentation"><a href="#strategy{{$step1->id}}" aria-controls="strategy{{$step1->id}}" role="tab" data-toggle="tab"><i class="material-icons" aria-hidden="true">note</i>
                                <p>Document</p>
                                </a></li>
                              <li role="presentation"><a href="#optimization{{$step1->id}}" aria-controls="optimization{{$step1->id}}" role="tab" data-toggle="tab"><i class="material-icons" aria-hidden="true">call</i>
                                <p>Call 103</p>
                                </a></li>
                             
                            </ul>
                            <!-- end design process steps--> 
                            <!-- Tab panes -->
                            <div class="tab-content">
                              <div role="tabpanel" class="tab-pane active" id="discover{{$step1->id}}">
                                <div class="design-process-content">
                                    
                                    {!! Form::open(array('url'=>'save/process6/'.$step1->id_cus, 'method'=>'post', 'files'=>'true')) !!}

                                    {{ csrf_field() }}
                                    
                                    <input type="hidden" name="process5" value="{{$step1->process5}}">
                                    <div class="row">
                                        
                                        <div class="col-md-6"> <!-- kolom 1 -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Name  </b>    
                                                </div>
                                                <div class="col-md-6">
                                                   <input  class="form-control" type="text" name="" value="{{$step1->name}} " readonly> 
                                                    <input  class="form-control" type="hidden" name="id_praapplication" value="{{$step1->id_cus}} " readonly> 
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Phone : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                    <input  class="form-control" type="text" name="" value="{{$step1->notelp}} " readonly>
                                                   
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Employer : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                    <input  class="form-control" type="text" name="" value="{{$step1->majikan->Emp_Desc}} " readonly>
                                                   
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>By : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                   <p style="color: red">{{$step1->user->name}} </p>  
                                                </div>
                                            </div>
                                            
                                        </div> <!-- End of kolom 1 -->
                                        <!-- kolom 2 -->
                                        <div class="col-md-6">
                                            
                                            <?php $lm = DB::table('loanammount')->where('id_praapplication', $step1->id_cus)->latest('created_at')->limit('1')->first(); ?>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Basic Salary (RM) : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                   <input  class="form-control" type="text" name="" value="{{$step1->gaji_asas}} " readonly> 
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Allowance (RM) : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                    <input  class="form-control" type="text" name="" value="{{$step1->elaun}}" readonly>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Total Deduction (RM) : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                    <input  class="form-control" type="text" name="" value="{{$step1->pot_bul}}" readonly>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Loan Amount (RM) : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                   <input  class="form-control" type="text" name="" value="{{$lm->loanammount}}" readonly>
                                                </div>
                                            </div>

                                            

                                        </div> <!-- End of kolom 2 -->
                                    </div>
                                 </div>
                              </div><!-- end loan calculation -->


 
                            <fieldset><br>
                                <div class="container" style="">
                                    <div class="row">
                                        <div class='col-lg-10'>
                                            <div class="form-group">
                                                <div id="add_redemption_button" style='display:block' >
                                                    <div class="form-group">
                                                        <label><b>Settlement Instruction</b></label><br>
                                                            <table id="setData" border='1' class='table table-hover table-borderd col-md-11 col-lg-11'> 
                                                                <tbody>
                                                                    <tr bgcolor="#e6f3ff">
                                                                        <td>Institusi Kewangan / Koperasi</td> 
                                                                        <td>Ansuran Bulanan (RM)</td>
                                                                        <td>Baki Pembayaran (RM)</td>
                                                                    </tr> 
                                                                </tbody>
                                                            </table><br>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-11">
                                            <p><h3><b>PENGIRAAN NISBAH KHIDMAT HUTANG [DEBT SERVICE RATIO (DSR)]</b></h3></p>
                                            <p><b>1. PENDAPATAN DAN PEMBIAYAAN (BULANAN)</b></p><br>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class='col-md-11'>
                                            <table width="71%" class='table table-hover' border='0' cellspacing="2" cellpadding="2">
                                                <tbody>
                                                    <tr>
                                                        <td>Gaji Pokok</td>
                                                        <td>
                                                            <input onchange="toFloat('fn_monthly_basic')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_monthly_basic" value="{{number_format((float)$step1->gaji_asas, 2, '.', '')}}" id="fn_monthly_basic">                   
                                                        </td>
                                                        <td>KWSP</td>
                                                        <td> 
                                                            <input onchange="toFloat('fn_knwsp')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_knwsp" value="{{number_format((float)$financial->fn_knwsp, 2, '.', '')}}" id="fn_knwsp">                                          
                                                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Imbuhan Tetap Perumahan</td>
                                                        <td> 
                                                            <input onchange="toFloat('fn_housing_allowance')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_housing_allowance" value="{{number_format((float)$financial->fn_housing_allowance, 2, '.', '')}}" id="fn_housing_allowance">
                                                        </td>
                                                        <td>PERKESO</td>
                                                        <td>
                                                            <input onchange="toFloat('fn_perkeso')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_perkeso" value="{{number_format((float)$financial->fn_perkeso, 2, '.', '')}}" id="fn_perkeso">                                          
                                                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Imbuhan Tetap Khidmat Awam</td>
                                                        <td> 
                                                            <input onchange="toFloat('fn_govt_service')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_govt_service" value="{{number_format((float)$financial->fn_govt_service, 2, '.', '')}}" id="fn_govt_service">     
                                                       </td>
                                                        <td>Cukai Pendapatan</td>
                                                        <td>
                                                            <input onchange="toFloat('fn_income_tax')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_income_tax" value="{{number_format((float)$financial->fn_income_tax, 2, '.', '')}}" id="fn_income_tax">         
                                                       </td>
                                                       <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Bantuan Sara Hidup</td>
                                                        <td>
                                                            <input onchange="toFloat('fn_cost_of')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_cost_of" value="{{number_format((float)$financial->fn_cost_of, 2, '.', '')}}" id="fn_cost_of">                                          
                                                        </td>
                                                        <td>Zakat Pendapatan</td>
                                                        <td>
                                                            <input onchange="toFloat('fn_zakat')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_zakat" value="{{number_format((float)$financial->fn_zakat, 2, '.', '')}}" id="fn_zakat">                                          
                                                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Bayaran Insentif Tugas Kewangan</td>
                                                        <td>
                                                            <input onchange="toFloat('fn_financial_incentive')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_financial_incentive" value="{{number_format((float)$financial->fn_financial_incentive, 2, '.', '')}}" id="fn_financial_incentive">
                                                        </td>
                                                        <td>ASB/ASN</td>
                                                        <td>
                                                            <input onchange="toFloat('fn_asb')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_asb" value="{{number_format((float)$financial->fn_asb, 2, '.', '')}}" id="fn_asb">                                          
                                                       </td>
                                                       <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Elaun Khas</td>
                                                        <td>
                                                            <input onchange="toFloat('fn_special_allowance')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_special_allowance" value="{{number_format((float)$financial->fn_special_allowance, 2, '.', '')}}" id="fn_special_allowance">
                                                        </td>
                                                        <td>Lembaga Tabung Haji</td>
                                                        <td>
                                                            <input onchange="toFloat('fn_lembaga_tabung')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_lembaga_tabung" value="{{number_format((float)$financial->fn_lembaga_tabung, 2, '.', '')}}" id="fn_lembaga_tabung">     
                                                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Elaun Tetap (Lain-lain)</td>
                                                        <td>
                                                            <input onchange="toFloat('fn_fixed_allowance')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_fixed_allowance" value="{{number_format((float)$financial->fn_fixed_allowance, 2, '.', '')}}" id="fn_fixed_allowance">     
                                                        </td>
                                                        <td>Pembiayaan BR/MBSB/MUAMALAT</td>
                                                        <td>
                                                            <input onchange="toFloat('fn_house_financing')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_house_financing" value="{{number_format((float)$financial->fn_house_financing, 2, '.', '')}}" id="fn_house_financing">  
                                                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>Biro Perkhidmatan Angkasa</td>
                                                        <td>
                                                            <input onchange="toFloat('fn_bpa')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_bpa" value="{{number_format((float)$financial->fn_bpa, 2, '.', '')}}" id="fn_bpa">                                          
                                                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>TIDAK TETAP:</strong></td>
                                                        <td>&nbsp;</td>
                                                        <td>Potongan Insuran/Takaful</td>
                                                        <td>
                                                            <input onchange="toFloat('fn_insurance')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_insurance" value="{{number_format((float)$financial->fn_insurance, 2, '.', '')}}" id="fn_insurance">                               
                                                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Elaun Lebih Masa</td>
                                                        <td>
                                                            <input onchange="toFloat('fn_overtime_allowance')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_overtime_allowance" value="{{number_format((float)$financial->fn_overtime_allowance, 2, '.', '')}}" id="fn_overtime_allowance">         
                                                        </td>
                                                        <td>Lain-lain (sila nyatakan)</td>
                                                        <td>
                                                            <input onchange="toFloat('fn_others')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_others" value="{{number_format((float)$financial->fn_others, 2, '.', '')}}" id="fn_others">                                          
                                                        </td>
                                                        <td>
                                                            <input type="text" maxlength="21"  class="form-control" placeholder="Remark" name="fn_remark" value="{{$financial->remark}}" id="fn_remark">                                          
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Elaun Makan</td>
                                                        <td>
                                                            <input onchange="toFloat('fn_food_allowance')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_food_allowance" value="{{number_format((float)$financial->fn_food_allowance, 2, '.', '')}}" id="fn_food_allowance">       
                                                        </td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Elaun Perjalanan/Perbatuan /Pengangkutan</td>
                                                        <td>
                                                            <input onchange="toFloat('fn_travel')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_travel" value="{{number_format((float)$financial->fn_travel, 2, '.', '')}}" id="fn_travel">                                          
                                                        </td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Bonus Kontraktual Tahunan</td>
                                                        <td>
                                                            <input onchange="toFloat('fn_yearly_contractual')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_yearly_contractual" value="{{number_format((float)$financial->fn_yearly_contractual, 2, '.', '')}}" id="fn_yearly_contractual">
                                                        </td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Komisyen</td>
                                                        <td>
                                                            <input onchange="toFloat('fn_commission')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_commission" value="{{number_format((float)$financial->fn_commission, 2, '.', '')}}" id="fn_commission">                          
                                                        </td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Pendapatan Hasil Sewa Rumah /Bangunan</td>
                                                        <td>
                                                            <input onchange="toFloat('fn_housing_building')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_housing_building" value="{{number_format((float)$financial->fn_housing_building, 2, '.', '')}}" id="fn_housing_building">    
                                                        </td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Pendapatan Hasil Perniagaan</td>
                                                        <td> 
                                                            <input onchange="toFloat('fn_business_income')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_business_income" value="{{number_format((float)$financial->fn_business_income, 2, '.', '')}}" id="fn_business_income">
                                                        </td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Elaun Lembaga Pengarah</td>
                                                        <td>
                                                            <input onchange="toFloat('fn_non_fixed')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_non_fixed" value="{{number_format((float)$financial->fn_non_fixed, 2, '.', '')}}" id="fn_non_fixed">        
                                                        </td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Pendapatan Dividen Tahunan</td>
                                                        <td>
                                                            <input onchange="toFloat('fn_yearly_devidend')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_yearly_devidend" value="{{number_format((float)$financial->fn_yearly_devidend, 2, '.', '')}}" id="fn_yearly_devidend">  
                                                        </td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Pendapatan Saham/Syer</td>
                                                        <td>
                                                            <input onchange="toFloat('fn_share_income')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_share_income" value="{{number_format((float)$financial->fn_share_income, 2, '.', '')}}" id="fn_share_income">            
                                                        </td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>(A) Jumlah Pendapatan</strong></td>
                                                        <td>
                                                            <input readonly onchange="toFloat('fn_total_income')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_total_income" value="{{number_format((float)$financial->fn_total_income, 2, '.', '')}}" id="fn_total_income">    
                                                        </td>
                                                        <td><strong>Jumlah Potongan</strong></td>
                                                        <td>
                                                            <input readonly onchange="toFloat('fn_total_deductions')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_total_deductions" value="{{number_format((float)$financial->fn_total_deductions, 2, '.', '')}}" id="fn_total_deductions">
                                                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td><strong>Jumlah Bersih </strong></td>
                                                        <td>
                                                            <input readonly onchange="toFloat('fn_total_net')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_total_net" value="{{number_format((float)$financial->fn_total_net, 2, '.', '')}}" id="fn_total_net">        
                                                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <p><b>BAYARAN BALIK PEMBIAYAAN LAIN<br>
                                            (yang tidak dinyatakan dalam penyata pendapatan & wajib diisytihar )</b></p><br>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-9">
                                            <table border="0" class='table table-hover' cellspacing="2" cellpadding="2">
                                                <tbody>
                                                    <tr>
                                                        <td width="45%" align="center"><strong>PERKARA</strong></td>
                                                        <td  align="center"><strong>RM</strong></td>
                                                        <td width="35%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>1. Pembiayaan Perumahan 1 (cth: dari RHB Bank)</td>
                                                        <td><input name="pemrumah1" value="{{number_format((float)$financial->pemrumah1, 2, '.', '')}}" type="text" required class="form-control fn" id="pemrumah1" onchange="toFloat('pemrumah1')" onkeypress="return isNumberKey(event)"></td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>2. Pembiayaan Perumahan 2 (cth: dari Maybank Berhad)</td>
                                                        <td><input name="pemrumah2" value="{{number_format((float)$financial->pemrumah2, 2, '.', '')}}" type="text" required class="form-control fn" id="pemrumah2" onchange="toFloat('pemrumah2')" onkeypress="return isNumberKey(event)"></td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>3. Pembiayaan Perumahan 3 (cth: dari CIMB Bank)</td>
                                                        <td><input name="pemrumah3" value="{{number_format((float)$financial->pemrumah3, 2, '.', '')}}" type="text" required class="form-control fn" id="pemrumah3" onchange="toFloat('pemrumah3')" onkeypress="return isNumberKey(event)"></td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>4. Kad Kredit 1</td>
                                                        <td><input name="kadkredit1" value="{{number_format((float)$financial->kadkredit1, 2, '.', '')}}" type="text" required class="form-control fn" id="kadkredit1" onchange="toFloat('kadkredit1')" onkeypress="return isNumberKey(event)"></td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>5. Kad Kredit 2</td>
                                                        <td><input name="kadkredit2" value="{{number_format((float)$financial->kadkredit2, 2, '.', '')}}" type="text" required class="form-control fn" id="kadkredit2" onchange="toFloat('kadkredit2')" onkeypress="return isNumberKey(event)"></td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                         <td>6. Kad Kredit 3</td>
                                                        <td><input name="kadkredit3" value="{{number_format((float)$financial->kadkredit3, 2, '.', '')}}" type="text" required class="form-control fn" id="kadkredit3" onchange="toFloat('kadkredit3')" onkeypress="return isNumberKey(event)"></td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>7. Pembiayaan Peribadi (cth dari Bank Komersil)</td>
                                                        <td><input name="pempribadi" value="{{number_format((float)$financial->pempribadi, 2, '.', '')}}" type="text" required class="form-control fn" id="pempribadi" onchange="toFloat('pempribadi')" onkeypress="return isNumberKey(event)"></td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>8. Pembiayaan Perabot (cth: dari Courts,AEON)</td>
                                                        <td><input name="pemperabot" value="{{number_format((float)$financial->pemperabot, 2, '.', '')}}" type="text" required class="form-control fn" id="pemperabot" onchange="toFloat('pemperabot')" onkeypress="return isNumberKey(event)"></td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>9. Pembiayaan Kenderaan (cth: dari MBB,CIMB)</td>
                                                        <td><input name="pemkenderaan" value="{{number_format((float)$financial->pemkenderaan, 2, '.', '')}}" type="text" required class="form-control fn" id="pemkenderaan" onchange="toFloat('pemkenderaan')" onkeypress="return isNumberKey(event)"></td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>10. Pembiayaan Lain (sila nyatakan)</td>
                                                        <td><input name="pemlain" value="{{number_format((float)$financial->pemlain, 2, '.', '')}}" type="text" required class="form-control fn" id="pemlain" onchange="toFloat('pemlain')" onkeypress="return isNumberKey(event)"></td>
                                                        <td><input name="pemlain_nyatakan" maxlength="55" placeholder="Remark" value="{{$financial->pemlain_nyatakan}}" type="text" class="form-control"></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center"><strong>(B) Jumlah Pembiayaan</strong></td>
                                                        <td><strong>
                                                            <input name="jumlahpembiayaan" value="{{number_format((float)$financial->jumlahpembiayaan, 2, '.', '')}}" readonly type="text" required class="form-control fn" id="jumlahpembiayaan" onchange="toFloat('jumlahpembiayaan')" onkeypress="return isNumberKey(event)">
                                                            </strong></td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                             
                             <div class="row">
                               <div class="col-md-8">
                                 <p><b>2. PENGIRAAN DSR<br>
                                Formula DSR = (JUMLAH BAYARAN BALIK PEMBIAYAAN/ (PENDAPATAN - AMAUN STATUTORI)) x 100%</b></p><br>
                               </div>
                             </div>
                             <div class="row">
                               <div class="col-md-8">
                                <table width="71%" class="table table-hover" border="0" cellspacing="2" cellpadding="2">
  <tbody>
    <tr>
      <td width="19%"><strong>Langkah 1:</strong></td>
      <td colspan="2"><strong>Pengiraaan Pendapatan</strong></td>
      <td width="19%" align="center"><strong>RM</strong></td>
      <td  align="center"><strong>RM</strong></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td width="12%">&nbsp;</td>
      <td width="30%">&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td colspan="2">Pendapatan dari (A)- spt Penyata Pendapatan</td>
      <td><input name="pendapatan" type="text" value="{{number_format((float)$financial->fn_total_income, 2, '.', '')}}" required class="form-control fn" id="pendapatan" onchange="toFloat('pendapatan')" onkeypress="return isNumberKey(event)"></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td colspan="2">tolak: Amaun Statutori iaitu:</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>KWSP</td>
      <td><input name="kwsp" type="text" required value="{{number_format((float)$financial->fn_knwsp, 2, '.', '')}}" class="form-control fn" id="kwsp" onchange="toFloat('kwsp')" onkeypress="return isNumberKey(event)"></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>PERKESO</td>
      <td><input name="perkeso" type="text" required value="{{number_format((float)$financial->fn_perkeso, 2, '.', '')}}"class="form-control fn" id="perkeso" onchange="toFloat('perkeso')" onkeypress="return isNumberKey(event)"></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>Cukai Pendapatan</td>
      <td><input name="cukai_pendapatan" type="text" required value="{{number_format((float)$financial->fn_income_tax, 2, '.', '')}}" class="form-control fn" id="cukai_pendapatan" onchange="toFloat('cukai_pendapatan')" onkeypress="return isNumberKey(event)"></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>Zakat Pendapatan</td>
      <td><input name="zakat_pendapatan" type="text" required value="{{number_format((float)$financial->fn_zakat, 2, '.', '')}}" class="form-control fn" id="zakat_pendapatan" onchange="toFloat('zakat_pendapatan')" onkeypress="return isNumberKey(event)"></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>Lain-lain (sila nyatakan)</td>
      <td><input name="lainlain1" type="text" required value="{{number_format((float)$financial->fn_others, 2, '.', '')}}" class="form-control fn" id="lainlain1" onchange="toFloat('lainlain1')" onkeypress="return isNumberKey(event)"></td>
      <td><input name="lainlain2" type="text" required value="{{number_format((float)$financial->lainlain2, 2, '.', '')}}" class="form-control fn" id="lainlain2" onchange="toFloat('lainlain2')" onkeypress="return isNumberKey(event)"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td colspan="2">( C ) JUMLAH PENDAPATAN</td>
      <td>&nbsp;</td>
      <td><input name="jumlah_pendapatan" type="text" required value="{{number_format((float)$financial->jumlah_pendapatan, 2, '.', '')}}" class="form-control fn" id="jumlah_pendapatan" onchange="toFloat('jumlah_pendapatan')" onkeypress="return isNumberKey(event)"></td>
    </tr>
  </tbody>
</table>
                                
                                
                                
                              </div>
                             </div>
                             
                             <div class="row">
                               <div class="col-md-8">
                                  <table width="71%" border="0" class="table table-hover" cellspacing="2" cellpadding="2">
  <tbody>
    <tr>
      <td width="19%"><strong>Langkah 2:</strong></td>
      <td colspan="2"><strong>Pengiraan Bayaran Balik Pembiayaan</strong></td>
      <td width="19%" align="center"><strong>RM</strong></td>
      <td  align="center"><strong>RM</strong></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td width="12%">&nbsp;</td>
      <td width="30%">&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td colspan="2">Pembiayaan (dalam Penyata Pendapatan) iatu:</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>Pembiayaan Perumahan</td>
      <td><input name="l2_pemrumah" type="text" required value="{{number_format((float)$financial->fn_house_financing, 2, '.', '')}}" class="form-control fn" id="l2_pemrumah" onchange="toFloat('l2_pemrumah')" onkeypress="return isNumberKey(event)"></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>Biro Perkhidmatan Angkasa</td>
      <td><input name="l2_bps" type="text" required value="{{number_format((float)$financial->fn_bpa, 2, '.', '')}}" class="form-control fn" id="l2_bps" onchange="toFloat('l2_bps')" onkeypress="return isNumberKey(event)"></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td colspan="2">Pembiayaan dari ( B )</td>
      <td><input name="l2_pemdarib" type="text" required value="{{number_format((float)$financial->jumlahpembiayaan, 2, '.', '')}}" class="form-control fn" id="l2_pemdarib" onchange="toFloat('l2_pemdarib')" onkeypress="return isNumberKey(event)"></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td colspan="2">(D) Jumlah Bayaran Balik Pembiayaan </td>
      <td>&nbsp;</td>
      <td><input name="l2_jumlahbayaran" type="text" required value="{{number_format((float)$financial->l2_jumlahbayaran , 2, '.', '')}}" class="form-control fn" id="l2_jumlahbayaran" onchange="toFloat('l2_jumlahbayaran')" onkeypress="return isNumberKey(event)"></td>
    </tr>
  </tbody>
</table>
                              </div>
                             </div>
                              <div class="row">
                               <div class="col-md-8">
                                <table width="71%" border="0" class="table table-hover" cellspacing="2" cellpadding="2">
  <tbody>
    <tr>
      <td width="19%"><strong>Langkah 3:</strong></td>
      <td colspan="2"><strong>Penyelesaian Awal/Lain</strong></td>
      <td width="19%" align="center"><strong>RM</strong></td>
      <td  align="center"><strong>RM</strong></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td width="12%">&nbsp;</td>
      <td width="30%">&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td colspan="2">1. BKR</td>
      <td><input name="l3_bkr" type="text" required value="{{number_format((float)$financial->l3_bkr, 2, '.', '')}}" class="form-control fn" id="l3_bkr" onchange="toFloat('l3_bkr')" onkeypress="return isNumberKey(event)"></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td colspan="2">2. Koperasi</td>
      <td><input name="l3_koperasi" type="text" required value="{{number_format((float)$financial->l3_koperasi, 2, '.', '')}}" class="form-control fn" id="l3_koperasi" onchange="toFloat('l3_koperasi')" onkeypress="return isNumberKey(event)"></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td colspan="2">3. Bank</td>
      <td><input name="l3_bank" type="text" required value="{{number_format((float)$financial->l3_bank, 2, '.', '')}}" class="form-control fn" id="l3_bank" onchange="toFloat('l3_bank')" onkeypress="return isNumberKey(event)"></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td colspan="2">4. Lain-Lain</td>
      <td><input name="l3_lainlain" type="text" required value="{{number_format((float)$financial->l3_lainlain, 2, '.', '')}}" class="form-control fn" id="l3_lainlain" onchange="toFloat('l3_lainlain')" onkeypress="return isNumberKey(event)"></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td colspan="2">&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td colspan="2">(E) Jumlah Penyelesaian Lain/Awal</td>
      <td>&nbsp;</td>
      <td><input readonly name="l3_jumlah" type="text" required value="{{number_format((float)$financial->l3_jumlah, 2, '.', '')}}" class="form-control fn" id="l3_jumlah" onchange="toFloat('l3_jumlah')" onkeypress="return isNumberKey(event)"></td>
    </tr>
  </tbody>
</table>
                                
                                
                              </div>
                             </div>
                             <div class="row">
                               <div class="col-md-12">
                               <hr>
                               <br>
                                <table width="92%" border="0" class="" cellspacing="2" cellpadding="2">
  <tbody>
    <tr>
      <td colspan="5"><strong>Langkah 4: Pengiraan DSR</strong></td>
    </tr>
    <tr>
      <td width="12%">&nbsp;</td>
      <td width="10%">&nbsp;</td>
      <td width="52%">&nbsp;</td>
      <td width="13%">&nbsp;</td>
      <td width="13%">&nbsp;</td>
    </tr>
    <tr>
      <td>DSR </td>
      <td><p>= 
      </p></td>
      <td><strong>
        <input name="l4_jumlahbayaran" value="{{number_format((float)$financial->l4_jumlahbayaran, 2, '.', '')}}" readonly size='8' type="text"  class="fn" id="l4_jumlahbayaran" onchange="toFloat('l4_jumlahbayaran')" onkeypress="return isNumberKey(event)">
    /
        <input name="l4_jumlah_pendapatan" value="{{number_format((float)$financial->l4_jumlah_pendapatan, 2, '.', '')}}" readonly size='8' type="text"  class="fn" id="l4_jumlah_pendapatan" onchange="toFloat('l4_jumlah_pendapatan')" onkeypress="return isNumberKey(event)">
x 100<br>
      </strong></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>=</td>
      <td><strong>
        <input name="l4_dsr" value="{{number_format((float)$financial->l4_dsr, 2, '.', '')}}" readonly type="text" size='8'  class="fn" id="l4_dsr" onchange="toFloat('l4_dsr')" onkeypress="return isNumberKey(event)">
%</strong></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2"><strong>Pembiayaan Baharu</strong></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td colspan="4">*Sekiranya peminjam memohon pembiayaan baharu sejumlah RM
        <strong><input name="l4_jumlahpinjam" value="{{number_format((float)$loanammount->loanammount, 2, '.', '')}}" type="text" size='10' class="fn" id="l4_jumlahpinjam" onchange="toFloat('l4_jumlahpinjam')" onkeypress="return isNumberKey(event)"></strong>
    dengan ciri ciri berikut :</td>
   
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
        <?php
            $month = $loanammount->Tenure->years;
            $m = $month * 12;
            $kadar = $loanammount->Tenure->rate;
        ?>
      <td height="26" colspan="2">Tempoh Pembiayaan</td>
       <td><strong><input name="l4_tempohbulan" value="{{$m}}" size='8' maxlength="2" type="text"   required class="fn" id="l4_tempoh"  onkeypress="return isNumberKey(event)"> </strong>
     
bulan</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2">Kadar Pulangan</td>
      <td>
    @if($financial->l4_kadar==0.00)
      <?php $l4_kadar = 8.00; ?>
    @else 
      <?php $l4_kadar = number_format((float)$financial->l4_kadar, 2, '.', ''); ?>
    @endif
    
    <strong><input name="l4_kadar" value="{{$kadar}}" type="text" size='8' class="fn" id="l4_kadar" onchange="toFloat('l4_kadar')" onkeypress="return isNumberKey(event)"></strong>
      % setahun (kadar tetap)</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2">Ansuran bayaran balik bulanan      RM</td>
      <td><strong><input name="l4_ansuran" value="{{number_format((float)$financial->l4_ansuran, 2, '.', '')}}" size='8' readonly type="text"  class="fn" id="l4_ansuran" onchange="toFloat('l4_ansuran')" onkeypress="return isNumberKey(event)"></strong></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td colspan="3">* Pengiraan DSR dengan mengambilkira pembiayaan baharu yang dipohon:</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>DSR</td>
      <td> =</td>
      <td colspan="3"><strong>Jumlah Bayaran Balik Pembiayaan (sedia ada + baharu) - Jumlah Penyelesaian Awal/Lain</strong></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td colspan="2"><hr></td>
      <td><strong>x 100</strong></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td><strong>Pendapatan-Amaun Statutori</strong></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>=</td>
      <td><strong>(
        <input name="l4_bayaranbalik" value="{{number_format((float)$financial->l4_bayaranbalik, 2, '.', '')}}" readonly type="text" size='8' class="fn" id="l4_bayaranbalik" onchange="toFloat('l4_bayaranbalik')" onkeypress="return isNumberKey(event)">
/ 
<input name="l4_jumlah_pendapatan2" value="{{number_format((float)$financial->l4_jumlah_pendapatan2, 2, '.', '')}}" readonly type="text" size='8' class="fn" id="l4_jumlah_pendapatan2" onchange="toFloat('l4_jumlah_pendapatan2')" onkeypress="return isNumberKey(event)">
) X 100</strong></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>=</td>
      <td><strong>
        &nbsp;&nbsp;<input name="l4_dsr2" value="{{number_format((float)$financial->l4_dsr2, 2, '.', '')}}" readonly type="text" size='8' class="fn" id="l4_dsr2" onchange="toFloat('l4_dsr2')" onkeypress="return isNumberKey(event)">
      %</strong></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    
    <tr>
      <td>&nbsp;</td>
      <td colspan="4">&nbsp;</td>
    </tr>
  </tbody>
</table>
   <!--<table width="41%" class='table table-hover' border='0' cellspacing="2" cellpadding="2">
                <tbody>
                    <tr>
      <td colspan="4"><strong>Langkah 5: IDSB Loan Calculator</strong></td>
    </tr>
   
                    <tr>
                        <td width="20%">&nbsp;</td>
                        <td width="20%">&nbsp;</td>
                        <td width="10%">&nbsp;</td>
                        <td width="20%">&nbsp;</td>
                         <td width="10%">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Insurance and Roadtax</td>
                        <td>
                            <input name="insurance_premium" type="text"  onchange="toFloat('insurance_premium')" onkeypress="return isNumberKey(event)" required="" id="insurance_premium" class="form-control fn" value="">
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>  
                        <td>&nbsp;</td> 
                    </tr>
                    <tr>
                        <td >Roadtax</td>
                        <td >
                            <input name="roadtax" type="text"  class="form-control fn" value="0">
                        </td>
                        <td >&nbsp;</td>
                        <td >&nbsp;</td>  
                        <td>&nbsp;</td> 
                    </tr>
                    <tr>
                        <td >Admin Fee</td>
                        <td >
                            <input name="admin_fee" type="text"   onchange="toFloat('admin_fee')" onkeypress="return isNumberKey(event)"  id="admin_fee"  class="form-control fn" readonly="">
                        </td>
                        <td >&nbsp;</td>
                        <td >&nbsp;</td> 
                         <td >&nbsp;</td>  
                    </tr>
                    <tr>
                        <td>GST 6%</td>
                        <td>&nbsp;</td>
                        <td><input  type="text"  name="gst_psn" value="6 %" class="form-control" disabled=""></td>
                        <td><input name="gst" type="text" class="form-control fn" id="gst" onchange="toFloat('gst')" onkeypress="return isNumberKey(event)" readonly=""></td>
                         <td >&nbsp;</td> 
                    </tr>
                     <tr>
                        <td >Loan Amaun<td>
                        <td >&nbsp;</td> 
                        <td >
                            <input name="loan_amaun" type="text"   onchange="toFloat('loan_amaun')" onkeypress="return isNumberKey(event)"   id="loan_amaun" class="form-control fn" readonly="">
                        </td>
                        <td >&nbsp;</td>
                    </tr>
                       <tr>
                        <td >Tenure (dalam bulan)<td>
                        <td >
                             <input name="tenure" type="text"  onkeypress="return isNumberKey(event)" required="" id="tenure" class="form-control fn" value="{{$financial->l4_tempohbulan}}"> 
                        </td>
                        <td ><b>Bulan</b></td>
                        <td >&nbsp;</td> 
                    </tr>
                       <tr>
                        <td >Rate<td>
                        <td >
                            <input name="rate" type="text"   onchange="toFloat('rate')" onkeypress="return isNumberKey(event)"  id="rate" class="form-control fn" value="8 %" readonly=""> 
                        </td>
                        <td >&nbsp;</td>
                        <td >&nbsp;</td> 
                    </tr>
                       <tr>
                        <td >Total Repayment<td>
                            <td >&nbsp;</td>
                        <td >
                            <input name="total_repayment" type="text"   onchange="toFloat('total_repayment')" onkeypress="return isNumberKey(event)"  id="total_repayment" class="form-control fn" readonly="">
                        </td>
                        <td>&nbsp;</td> 
                    </tr>
                       <tr>
                        <td >Installment<td>
                       <td >&nbsp;</td> 
                        <td >
                            <input name="installment" type="text" min="100"  onchange="toFloat('installment')" onkeypress="return isNumberKey(event)"  id="installment"  class="form-control fn" readonly="">
                        </td>
                        <td ><b>/ Bulan</b></td>
                    </tr>
                       <tr>
                        <td >Deduction Fee<td>
                            <td >&nbsp;</td>
                        <td >
                            <input name="deduction_fee" type="text"  onchange="toFloat('deduction_fee')" onkeypress="return isNumberKey(event)"  id="deduction_fee" class="form-control fn" readonly="">
                        </td>
                         <td >&nbsp;</td>

                    </tr>
                     <tr>
                        <td>GST 6%</td>
                        <td>&nbsp;</td>
                        <td><input  type="text"   name="gst_psn_fee" value="6 %"  class="form-control"  disabled=""></td>
                        <td><input name="gst_fee" type="text" class="form-control fn" id="gst_fee" onchange="toFloat('gst_fee')" onkeypress="return isNumberKey(event)" readonly=""></td>
                         <td >&nbsp;</td> 
                       
                    </tr>
                       <tr>
                        <td >Salary Deduction<td>
                            <td >&nbsp;</td>
                        <td >
                            <input name="salary_deduction" type="text"   onchange="toFloat('salary_deduction')" onkeypress="return isNumberKey(event)"  id="salary_deduction"  class="form-control fn" readonly="">
                        </td>
                         <td ><b>/ Bulan</b></td>
                    </tr>
                </tbody>
            </table>-->



        <div class="row">
            <div class="col-md-6"></div>
            <div class="col-md-6">
                <div class="row" style="margin-top: 30px !important">
                    <div class="col-md-6">
                        <b>Remark:</b>
                    </div>
                    <div class="col-md-6">
                        <select name="remarkp6" class="form-control" id="one{{$step1->id}}">
                           @if(!empty($data->remark6))
                                @if($data->remark6 =='W6')
                                <option value="W6"  selected>Reject</option>
                                <option value="W9">Pass </option>
                                @else
                                 <option value="W9" selected>Pass </option>
                                 <option value="W6">Reject</option>
                                @endif
                            @else
                            <option value="" selected disabled hidden>Choose Remark</option>
                            <option value="W9">Pass </option>
                            <option value="W6">Reject <!-- Calculation & Changes --></option>
                            @endif
                            
                        </select>
                    </div>
                </div>


                <div class="row">
                    <div class="resources1" style="display: none" id="two{{$step1->id}}">
                        <div class="col-md-6"><b>Note :</b></div>
                        <div class="col-md-6">
                            ​<textarea id="txtArea" name="notep6" class="form-control" rows="3" cols="3"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>

                                                                
                                                            </div>
                                                         </div>
                                                    
                                            </div>
                                             <div class="row">
                                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-md-offset-10">
                                                    <button type="submit" class="btn bg-green btn-block btn-lg waves-effect">
                                                        Save
                                                    </button>
                                                    {{ csrf_field() }}
                                                </div>
                                            </div>
                                        
                                        {{ Form::close() }}
                                        </fieldset>
                           
                            </div>
                          </div>
                        </div>
                    </section><!-- end of step -->

                </div>

            </div>
        </div>
    </div>


    <!-- ///////////////////  End Data Target  //////////////// -->


    @push('js')

        @foreach($process6 as $step1)
        <script type="text/javascript">
            var Privileges = jQuery('#one{{$step1->id}}');
            var select = this.value;
            Privileges.change(function () {
                if ($(this).val() == 'W6' ) {
                    $('.resources1').show();
                }
                else $('.resources1').hide();
            });

            var Privileges = jQuery('#three{{$step1->id}}');
            var select = this.value;
            Privileges.change(function () {
                if ($(this).val() == 'W7' ) {
                    $('.resources2').show();
                }
                else $('.resources2').hide();
            });

        


            //form step 

        // script for tab steps
    $('a[data-toggle="tab{{$step1->id}}"]').on('shown.bs.tab', function (e) {

        var href = $(e.target).attr('href');
        var $curr = $(".process-model{{$step1->id}}  a[href='" + href + "']").parent();

        $('.process-model{{$step1->id}} li').removeClass();

        $curr.addClass("active");
        $curr.prevAll().addClass("visited");
    });
// end  script for tab steps

            //end form step 



        </script>
        @endforeach
<script type="text/javascript">
    
$(document).ready(function() {

var found = [];
    $("select[name='remarkp6'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

});

</script>
<script type="text/javascript">

$( ".fn" ).keyup(function() {
  
// Income
var fn_monthly_basic = $('#fn_monthly_basic').val();
var fn_govt_service = $('#fn_govt_service').val();
var fn_financial_incentive = $('#fn_financial_incentive').val();
var fn_fixed_allowance = $('#fn_fixed_allowance').val();
var fn_housing_allowance = $('#fn_housing_allowance').val();
var fn_cost_of = $('#fn_cost_of').val();
var fn_special_allowance = $('#fn_special_allowance').val();

var fn_gross_income = parseFloat(fn_monthly_basic) + parseFloat(fn_govt_service)  + parseFloat(fn_financial_incentive)
      + parseFloat(fn_fixed_allowance) + parseFloat(fn_housing_allowance) + parseFloat(fn_cost_of) + parseFloat(fn_special_allowance)  ;
    
    
// Non fixed Variable
var fn_overtime_allowance = $('#fn_overtime_allowance').val();
var fn_travel = $('#fn_travel').val();
var fn_commission = $('#fn_commission').val();
var fn_business_income = $('#fn_business_income').val();
var fn_yearly_devidend = $('#fn_yearly_devidend').val();
var fn_food_allowance = $('#fn_food_allowance').val();
var fn_yearly_contractual = $('#fn_yearly_contractual').val();
var fn_housing_building = $('#fn_housing_building').val();
var fn_non_fixed = $('#fn_non_fixed').val();
var fn_share_income = $('#fn_share_income').val();

  
var fn_total_income = parseFloat(fn_overtime_allowance) + parseFloat(fn_travel)  + parseFloat(fn_commission)
      + parseFloat(fn_business_income) + parseFloat(fn_yearly_devidend) + parseFloat(fn_food_allowance) + parseFloat(fn_yearly_contractual)
      + parseFloat(fn_housing_building) + parseFloat(fn_non_fixed) + parseFloat(fn_share_income) + parseFloat(fn_gross_income) ;
    $("#fn_total_income").val(parseFloat(fn_total_income).toFixed(2));
  

// Deductions Variable
var fn_knwsp = $('#fn_knwsp').val();
var fn_income_tax = $('#fn_income_tax').val();
var fn_house_financing = $('#fn_house_financing').val();
var fn_others = $('#fn_others').val();
var fn_lembaga_tabung = $('#fn_lembaga_tabung').val();
var fn_insurance = $('#fn_insurance').val();
var fn_perkeso = $('#fn_perkeso').val();
var fn_zakat = $('#fn_zakat').val();
var fn_bpa = $('#fn_bpa').val();
var fn_asb = $('#fn_asb').val();

// Total Deductions
var fn_total_deductions = parseFloat(fn_knwsp) + parseFloat(fn_income_tax) + parseFloat(fn_house_financing)
      + parseFloat(fn_others) + parseFloat(fn_lembaga_tabung) + parseFloat(fn_insurance) + parseFloat(fn_perkeso)
      + parseFloat(fn_zakat) + parseFloat(fn_bpa) + parseFloat(fn_asb);

    $("#fn_total_deductions").val(parseFloat(fn_total_deductions).toFixed(2));
    
// Total net income
var fn_total_income = $('#fn_total_income').val();
var fn_total_deductions = $('#fn_total_deductions').val();

var fn_total_net = parseFloat(fn_total_income) - parseFloat(fn_total_deductions);
    $("#fn_total_net").val(parseFloat(fn_total_net).toFixed(2));
  
// Bayaran Balik
var pemrumah1 = $('#pemrumah1').val();
var pemrumah2 = $('#pemrumah2').val();
var pemrumah3 = $('#pemrumah3').val();
var kadkredit1 = $('#kadkredit1').val();
var kadkredit2 = $('#kadkredit2').val();
var kadkredit3 = $('#kadkredit3').val();
var pempribadi = $('#pempribadi').val();
var pemperabot = $('#pemperabot').val();
var pemkenderaan = $('#pemkenderaan').val();
var pemlain = $('#pemlain').val();
var jumlahpembiayaan = parseFloat(pemrumah1) + parseFloat(pemrumah2)
          + parseFloat(pemrumah3) + parseFloat(kadkredit1)
          + parseFloat(kadkredit2) + parseFloat(kadkredit3)
          + parseFloat(pempribadi) + parseFloat(pemperabot)
          + parseFloat(pemkenderaan) + parseFloat(pemlain);
          
    $("#jumlahpembiayaan").val(parseFloat(jumlahpembiayaan).toFixed(2));

// Langkah 1
$("#pendapatan").val(parseFloat(fn_total_income).toFixed(2));
$("#kwsp").val(parseFloat(fn_knwsp).toFixed(2));
$("#perkeso").val(parseFloat(fn_perkeso).toFixed(2));
$("#cukai_pendapatan").val(parseFloat(fn_income_tax).toFixed(2));
$("#zakat_pendapatan").val(parseFloat(fn_zakat).toFixed(2));
$("#lainlain1").val(parseFloat(fn_others).toFixed(2));

var lainlain2= parseFloat(fn_knwsp) + parseFloat(fn_perkeso) + parseFloat(fn_income_tax)
      + parseFloat(fn_zakat) + parseFloat(fn_others);
    
$("#lainlain2").val(parseFloat(lainlain2).toFixed(2));

var jumlah_pendapatan= parseFloat(fn_total_income) - parseFloat(lainlain2);
    $("#jumlah_pendapatan").val(parseFloat(jumlah_pendapatan).toFixed(2));
    
// Langkah 2

$("#l2_pemrumah").val(parseFloat(fn_house_financing).toFixed(2));
$("#l2_bps").val(parseFloat(fn_bpa).toFixed(2));
$("#l2_pemdarib").val(parseFloat(jumlahpembiayaan).toFixed(2));

var l2_jumlahbayaran= parseFloat(fn_house_financing) + parseFloat(fn_bpa) + parseFloat(jumlahpembiayaan);
$("#l2_jumlahbayaran").val(parseFloat(l2_jumlahbayaran).toFixed(2));

// Langkah 3
var l3_bkr = $('#l3_bkr').val();
var l3_koperasi = $('#l3_koperasi').val();
var l3_bank = $('#l3_bank').val();
var l3_lainlain = $('#l3_lainlain').val();

var l3_jumlah = parseFloat(l3_bkr) + parseFloat(l3_koperasi) + parseFloat(l3_bank)  + parseFloat(l3_lainlain);
$("#l3_jumlah").val(parseFloat(l3_jumlah).toFixed(2));


// Langkah 4


$("#l4_jumlahbayaran").val(parseFloat(l2_jumlahbayaran).toFixed(2));
$("#l4_jumlah_pendapatan").val(parseFloat(jumlah_pendapatan).toFixed(2));

var l4_dsr  = (parseFloat(l2_jumlahbayaran) / parseFloat(jumlah_pendapatan)) * 100;
$("#l4_dsr").val(parseFloat(l4_dsr).toFixed(2));

var l4_jumlahpinjam = $('#l4_jumlahpinjam').val();
var l4_jumlahpinjam2 = $('#l4_jumlahpinjam2').val();
var l4_tempoh = $('#l4_tempoh').val();
var l4_kadar = $('#l4_kadar').val();


var l4_tempohbulan = parseFloat(l4_tempoh) * 12;
$("#l4_tempohbulan").val(parseInt(l4_tempohbulan));


var l4_ansuran_total = parseFloat(l4_jumlahpinjam) * parseFloat(l4_tempoh)* (parseFloat(l4_kadar) / 100) + parseFloat(l4_jumlahpinjam);
var l4_ansuran = parseFloat(l4_ansuran_total) / parseFloat(l4_tempoh);


$("#l4_ansuran").val(parseFloat(l4_ansuran).toFixed(2));


var l4_bayaranbalik = parseFloat(l2_jumlahbayaran) + parseFloat(l4_ansuran)- parseFloat(l3_jumlah);
$("#l4_bayaranbalik").val(parseFloat(l4_bayaranbalik).toFixed(2));

var l4_jumlah_pendapatan2 = parseFloat(jumlah_pendapatan);
$("#l4_jumlah_pendapatan2").val(parseFloat(l4_jumlah_pendapatan2).toFixed(2));


var l4_dsr2 = (parseFloat(l4_bayaranbalik) / parseFloat(l4_jumlah_pendapatan2)) * 100;
$("#l4_dsr2").val(parseFloat(l4_dsr2).toFixed(2));

});
</script>
<script type="text/javascript">

$( ".fn" ).keyup(function() {

var insurance_premium = $('#insurance_premium').val();
var roadtax = $('#roadtax').val();

//Admin fee
var admin_fee =(parseFloat(insurance_premium)) * 0.1;
$("#admin_fee").val(parseFloat(admin_fee).toFixed(2));


var gst= (parseFloat(admin_fee) * 6) /100;
$("#gst").val(parseFloat(gst).toFixed(2));

var loan_amaun= parseFloat(insurance_premium)  + parseFloat(admin_fee) + parseFloat(gst) ;
$("#loan_amaun").val(parseFloat(loan_amaun).toFixed(2));

var tenure = $('#tenure').val();


var total_repayment= parseFloat(loan_amaun) * 0.08/12* parseFloat(tenure) +parseFloat(loan_amaun); 
$("#total_repayment").val(parseFloat(total_repayment).toFixed(2));


var installment= parseFloat(total_repayment) / parseFloat(tenure) ; 
$("#installment").val(parseFloat(installment).toFixed(2));

var deduction_fee= parseFloat(installment) * 0.020412; 
$("#deduction_fee").val(parseFloat(deduction_fee).toFixed(2));

var gst_fee= (parseFloat(deduction_fee) * 6) /100;
$("#gst_fee").val(parseFloat(gst_fee).toFixed(2));

var salary_deduction= parseFloat(gst_fee) + parseFloat(installment) + parseFloat(deduction_fee) ; 
$("#salary_deduction").val(parseFloat(salary_deduction).toFixed(2));

var total_repayment= parseFloat(loan_amaun) * 0.08/12* parseFloat(tenure) +parseFloat(loan_amaun); 
$("#total_repayment").val(parseFloat(total_repayment).toFixed(2));
});
</script>
    @endpush