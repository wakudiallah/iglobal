<!-- CSS Step =-->
<link href="{{ asset('admin/css/tabsteptype.css') }}" rel="stylesheet" />


<div class="row clearfix">
    <!-- Task Info -->
        <div class="row clearfix"> <!-- Breadcrumber -->
            <div class="col-md-6">
                <ol class="breadcrumb breadcrumb-col-pink">
                    <li><a href="javascript:void(0);"><i class="material-icons">layers</i> Loan Check & Calculation</a></li>
                </ol>
            </div>
        </div> <!-- End of breadcrumber -->

        @include('shared.notif')
    

    <!-- #END# Task Info -->
</div>

    <!-- ///////////////////  Data Target  ///////////////// -->
   
   
        <div class="row clearfix">
            <div class="card">
                <div class="header bg-red">
                    <h2>Document {{$step1->name}} </h2>
                </div>
                <div class="body">
                    
                    <!-- start step -->
                   <section class="design-process-section" id="process-tab">
                        <div class="row">
                          <div class="col-xs-12"> 
                            <!-- design process steps--> 
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs process-model more-icon-preocess" role="tablist">
                              <li role="presentation" ><a href="#discover{{$step1->id}}" aria-controls="discover{{$step1->id}}" role="tab" data-toggle="tab"> <i class="material-icons" aria-hidden="true">credit_card</i>
                                <p>Loan Calculation</p>
                                </a></li>
                              <li role="presentation" class="active"><a href="#strategy{{$step1->id}}" aria-controls="strategy{{$step1->id}}" role="tab" data-toggle="tab"><i class="material-icons" aria-hidden="true">note</i>
                                <p>Document</p>
                                </a></li>
                              <li role="presentation"><a href="#optimization{{$step1->id}}" aria-controls="optimization{{$step1->id}}" role="tab" data-toggle="tab"><i class="material-icons" aria-hidden="true">call</i>
                                <p>Call 103</p>
                                </a></li>
                             
                            </ul>
                            <!-- end design process steps--> 
                            <!-- Tab panes -->
                            <div class="tab-content">
                              <div role="tabpanel" class="tab-pane active" id="discover{{$step1->id}}">
                                <div class="design-process-content">
                                    
                                    {!! Form::open(array('url'=>'save/process7/'.$step1->id_cus, 'method'=>'post', 'files'=>'true')) !!}

                                    {{ csrf_field() }}
                                    
                                    <input type="hidden" name="process5_7" value="{{$step1->process5}}">

                                        <table class="table table-responsive">
                                            <thead>
                                                <tr>
                                                    <th width="10%">No</th>
                                                    <th width="30%">Document</th>
                                                    <th>File</th>
                                                    <th width="10%">Checklist</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <?php $y = 1; ?>

                                                <?php
                                                       $datax1 = DB::table('doc_cust')->join('praapplication', 'doc_cust.cus_id', '=', 'praapplication.id_cus')->where('cus_id', $step1->id_cus)->where('doc_cust.type','1')->latest('doc_cust.created_at')->select('doc_cust.id as iddoc', 'doc_cust.doc_pdf as doc_pdf')->limit('1')->first();

                                                       $datax2 = DB::table('doc_cust')->join('praapplication', 'doc_cust.cus_id', '=', 'praapplication.id_cus')->where('cus_id', $step1->id_cus)->where('doc_cust.type','2')->latest('doc_cust.created_at')->select('doc_cust.id as iddoc', 'doc_cust.doc_pdf as doc_pdf')->limit('1')->first();

                                                       $datax3 = DB::table('doc_cust')->join('praapplication', 'doc_cust.cus_id', '=', 'praapplication.id_cus')->where('cus_id', $step1->id_cus)->where('doc_cust.type','4')->latest('doc_cust.created_at')->select('doc_cust.id as iddoc', 'doc_cust.doc_pdf as doc_pdf')->limit('1')->first();
                                                ?>

                                                <tr>  
                                                    <td>{{$y++}}</td>
                                                    <td>IC</td>
                                                    <td>
                                                        <a href="{{asset('/documents/user_doc/'.$step1->ic.'/'.$datax1->doc_pdf)}}" target='_blank'>{{$datax1->doc_pdf}}</a>
                                                    </td>
                                                    <td>
                                                        <input type="hidden" name="iddatadoc1" value="{{$datax1->iddoc}}">
                                                        
                                                        <!-- <input type="hidden" id="basic_checkbox_{{$step1->id}}{{$datax1->doc_pdf}}" name="checkbox1" value="0"  />
                                                        <label for="basic_checkbox_{{$step1->id}}{{$datax1->doc_pdf}}"></label> -->

                                                        <input type="checkbox" id="basic_checkbox_{{$step1->id}}{{$datax1->doc_pdf}}" name="checkbox1"  />
                                                        
                                                        <label for="basic_checkbox_{{$step1->id}}{{$datax1->doc_pdf}}"></label>
                                                    </td>
                                                    
                                                </tr>
                                                <tr>  
                                                    <td>{{$y++}}</td>
                                                    <td>LOC</td>
                                                    <td>
                                                        <a href="{{asset('/documents/user_doc/'.$step1->ic.'/'.$datax2->doc_pdf)}}" target='_blank'>{{$datax2->doc_pdf}} </a>
                                                    </td>
                                                    <td>
                                                        <input type="hidden" name="iddatadoc2" value="{{$datax2->iddoc}}">


                                                        <!-- <input type="hidden" id="basic_checkbox_{{$step1->id}}{{$datax2->doc_pdf}}" name="checkbox2" value="0"  />
                                                        <label for="basic_checkbox_{{$step1->id}}{{$datax2->doc_pdf}}"></label> -->

                                                        <input type="checkbox" id="basic_checkbox_{{$step1->id}}{{$datax2->doc_pdf}}" name="checkbox2"  value="1" />
                                                        <label for="basic_checkbox_{{$step1->id}}{{$datax2->doc_pdf}}"></label>
                                                         <input  class="form-control" type="hidden" name="id_praapplication" value="{{$step1->id_cus}} " readonly> 
                                                    </td>
                                                </tr>
                                                <tr>  
                                                    <td>{{$y++}}</td>
                                                    @if(empty($datax3->doc_pdf))
                                                        <td></td>
                                                    @else
                                                        <td>Spekar</td>
                                                        <td>
                                                            <a href="{{asset('/documents/user_doc/'.$step1->ic.'/'.$datax3->doc_pdf)}}" target='_blank'>{{$step1->ic}}-Spekar.pdf</a>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="iddatadoc3" value="{{$datax3->iddoc}}">

                                                           <!-- <input type="hidden" id="basic_checkbox_{{$step1->id}}{{$datax3->doc_pdf}}" name="checkbox3" value="0" />
                                                            <label for="basic_checkbox_{{$step1->id}}{{$datax3->doc_pdf}}"></label> -->

                                                            <input type="checkbox" id="basic_checkbox_{{$step1->id}}{{$datax3->doc_pdf}}" name="checkbox3" value="1" />
                                                            <label for="basic_checkbox_{{$step1->id}}{{$datax3->doc_pdf}}"></label>

                                                            
                                                        </td>
                                                    @endif
                                                </tr>
                                                <tr>
                                                    <td>{{$y++}}</td>
                                                    <td>Personal Financing Scheme</td>
                                                    <td>
                                                        
                                                        <a href="{{url('/personal_financing/'.$step1->id_cus)}}"  target="_blank"> <i class="material-icons"></i>{{$step1->ic}}-Personal Financing Scheme </a>
                                                    </td>
                                                    <td>
                                                        
                                                        <input type="checkbox" id="basic_checkbox_er1" name="checkbox2"  value="1" checked/>
                                                        <label for="basic_checkbox_er1"></label>
                                                    </td>
                                                </tr>
                                               

                                            </tbody>

                                        </table>  

                                        <div class="row">
                                            <div class="col-md-6 col-md-offset-6">
                                                <input  class="hidden" type="text" name="name" value="{{$step1->name}} " >

                                                <div class="col-md-3">
                                                    <b>Remark:</b>
                                                </div>
                                                <div class="col-md-9">
                                                    <select name="stagep7" class="form-control" id="three" required>
                                                        <option value="" selected disabled hidden>Choose Remark</option>
                                                        <option value="W10">Pass</option>
                                                        <option value="W7">Reject <!-- Document Check --></option>
                                                    </select>
                                                </div>

                                                <div class="resources2" style="display: none" id="two{{$step1->id}}">
                                                    <div class="col-md-3"><b>Note :</b></div>
                                                    <div class="col-md-9">
                                                        ​<textarea id="txtArea" name="notep7" class="form-control" rows="6" cols="3"></textarea>
                                                    </div>
                                                </div>

                                            </div>
                                            
                                        </div> 

                                                                
                                                            </div>
                                                         </div>
                                                    
                                            </div>
                                             <div class="row">
                                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-md-offset-10">
                                                    <button type="submit" class="btn bg-green btn-block btn-lg waves-effect">
                                                        Save
                                                    </button>
                                                    {{ csrf_field() }}
                                                </div>
                                            </div>
                                        
                                        {{ Form::close() }}
                                        </fieldset>
                           
                            </div>
                          </div>
                        </div>
                    </section><!-- end of step -->

                </div>

            </div>
        </div>
    </div>


    <!-- ///////////////////  End Data Target  //////////////// -->


    @push('js')

        <script>
            var Privileges = jQuery('#three');
            var select = this.value;
            Privileges.change(function () {
                if ($(this).val() == 'W7' ) {
                    $('.resources2').show();
                }
                else $('.resources2').hide();
            });
        </script>

        @foreach($process6 as $step1)
        <script type="text/javascript">
            var Privileges = jQuery('#one{{$step1->id}}');
            var select = this.value;
            Privileges.change(function () {
                if ($(this).val() == 'W6' ) {
                    $('.resources1').show();
                }
                else $('.resources1').hide();
            });

            

        


            //form step 

        // script for tab steps
    $('a[data-toggle="tab{{$step1->id}}"]').on('shown.bs.tab', function (e) {

        var href = $(e.target).attr('href');
        var $curr = $(".process-model{{$step1->id}}  a[href='" + href + "']").parent();

        $('.process-model{{$step1->id}} li').removeClass();

        $curr.addClass("active");
        $curr.prevAll().addClass("visited");
    });
// end  script for tab steps

            //end form step 



        </script>
        @endforeach
<script type="text/javascript">
    
$(document).ready(function() {

var found = [];
    $("select[name='remarkp6'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

});

</script>

    @endpush