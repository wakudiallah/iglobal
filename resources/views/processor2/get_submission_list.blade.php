 @extends('vadmin.tampilan')


@section('content')
	<section class="content">
        <div class="container-fluid">

            <div class="row clearfix"> <!-- Breadcrumber -->

                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">print</i>Submission List</a></li>

                    </ol>
                </div>

            </div> <!-- End of breadcrumber -->

            @include('shared.notif')

			<div class="row clearfix">
		        <div class="card">
		            <div class="header bg-red">
		                <h2>Submission List</h2>
		            </div>
		            <div class="body">
		                <div class="table-responsive">

		                    <table class="table table-hover dashboard-task-infos" id="example">

		                        <thead>
		                            <tr>
		                                <th>#</th>
		                                <th>Batch</th>
		                                <th>Date</th>
		                                <th>Detail</th>
		                                
		                            </tr>
		                        </thead>
		                        <tbody>
		                            <?php $i = 1; ?>

		                            @foreach($batch as $data)
		                            <tr>
		                                <td>{{$i++}}</td>
		                                <td>{{$data->uniq_id}}</td>
		                                <td>{{$data->created_at->toFormattedDateString()}}</td>
		                                <td>
		                                	
		                                	<button type="button" class="btn btn-success waves-effect m-r-20" data-toggle="modal" data-target="#largeModal{{$data->id}}">Detail</button>
		                                	
		                                </td>
		                                
		                            </tr>

		                            @endforeach
		                            
		                        </tbody>
		                    

		                    </table>

		                
		                </div>
		            </div>
		        </div>
			</div>

			
			@foreach($batch as $datas)

            <!-- Large Size -->
            <div class="modal fade" id="largeModal{{$datas->id}}" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-red">
                            <h4 class="modal-title" id="largeModalLabel">Batch {{$datas->uniq_id}}</h4>
                        </div>
                        <div class="modal-body">

                        	<?php
                                   $app = App\praapplication::where('uniq_no', $datas->uniq_id)->get();
                            ?>
                        	
                        	<form method="POST" class="form-horizontal" id="popup-validation{{$datas->id}}" action="{{ url('/post-reject')}}" >
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        	<table class="table table-hover dashboard-task-infos" id="example2{{$datas->uniq_id}}">
		                        <thead>
		                            <tr>
		                                <th>#</th>
		                                <th>Nama</th>
		                                <th>IC</th>
		                                <th>Email</th>
		                                <th>Phone</th>
		                                @can('edit_list_submissions')
		                                <th>Status</th>
		                                <th>Action</th>
		                                @endcan
		                            </tr>
		                        </thead>
		                        <tbody>
		                            <?php $i = 1; ?>
		                            @foreach($app as $data)
		                            <tr>
		                                <td>{{$i++}}</td>
		                                <td>{{$data->name}}</td>
		                                <td>{{$data->ic}}</td>
		                                <td>{{$data->email}}</td>
		                                <td>{{$data->notelp}}</td>
		                                 @can('edit_list_submissions')
		                                <td>@include('shared.stage_new')</td>
		                                <td>
		                                	@if(($data->stage == 'W11') || ($data->stage == 'W12'))
		                                	
		                                	@else
		                                	<div class="demo-checkbox">
                                                <input type="checkbox" id="basic_checkbox_{{$i}}" name="id[]" value="{{$data->id_cus}}" class="invitation-friends{{$datas->id}}{{$data->id}}" />

                                                <label for="basic_checkbox_{{$i}}"></label>
                                                 <input type="hidden" id="id_cus" name="id_cus[]" value="{{$data->id_cus}}" />

                                                 <input type="hidden"  name="ids[]" value="">
                                                 <input type="hidden"  name="sta[]" value="">
                                            </div>
                                            @endif

		                                </td>
		                                @endcan
		                            </tr>
		                            @endforeach 


		                            @can('edit_list_submissions')
		                            <button class="btn btn-sm bg-red" type="submit"><i class="material-icons">cancel</i> Rejected </button>
		                            @endcan
		                        </tbody>
		                    </table>

		                    </form>

                        </div>
                        <div class="modal-footer">
                            
                        </div>
                    </div>
                </div>
            </div>
            @endforeach



		</div>
	</section>


	<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>


	<script type="text/javascript">
		$(document).ready(function() {
	        $('#example').DataTable();
	        });
	</script>

	@foreach($batch as $datas)
	<script type="text/javascript">
		$(document).ready(function() {
	        $('#example2{{$datas->uniq_id}}').DataTable();
	        });
	</script>
	@endforeach

	@foreach($batch as $datas)
	@foreach($app as $data)
	<!-- Checkbox detect -->
    <script type="text/javascript">
        $( '#popup-validation{{$datas->id}}' ).on('submit', function(e) {
           if($( 'input[class^="invitation-friends{{$datas->id}}{{$data->id}}"]:checked' ).length === 0) {
              alert( 'Please! Select the application' );
              e.preventDefault();
           }
        });
    </script>
    @endforeach
    @endforeach
	
@endsection

    