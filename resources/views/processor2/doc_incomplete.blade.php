 @extends('vadmin.tampilan')


@section('content')
    <section class="content">
        <div class="container-fluid">

            
            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">cancel</i> Document Incomplete</a></li>
                        
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->


            <div class="row clearfix">
        <!-- Task Info -->
            <div class="card">
                <div class="header bg-red">
                    <h2>Document Incomplete</h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-hover dashboard-task-infos" id="example">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="30%">Name</th>
                                    <th width="20%">IC</th>
                                    
                                    <th>Status</th> <!-- ketika dhantar submit kira berubah loan pending documentation -->
                                    <th>Detail</th>
                                    <th>Confirm</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>

                                @foreach($docin as $data)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$data->name}}</td>
                                    <td>{{$data->ic}}</td>
                                    
                                    
                                    <td>
                                        @include('shared.stage_new')
                                    </td> 
                                    
                                    <td>
                                        @if($data->stage == 'W7')
                                         <button class="btn btn-success" aria-controls="collapse-{{$data->id}}" data-target="#collapseDetailOne{{$data->id}}" data-toggle="collapse" style="cursor:pointer;">Detail</button>
                                        @else($data->stage == 'W13')
                                        <!-- <a href="{{asset('/tenos/custtenos/'.$data->id_cus)}}" class="btn btn-success" target='_blank'> Detail</a> -->

                                        <button type="button" class="btn btn-success waves-effect m-r-20" data-toggle="modal" data-target="#largeModal{{$data->id}}">Detail</button>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{url('loan-eli/'.$data->id_cus.'/step2')}}" class="btn btn-danger" > Confirm</a>
                                    </td>
                                    
                                </tr>

                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        
        <!-- #END# Task Info -->
            </div>

            

            @foreach($docin as $datas)

            <?php $note = DB::table('history')->where('cus_id', $datas->id_cus)->where('remark_id', 'W7')->latest('id')->limit('1')->first(); ?>
            <!-- Large Size -->
            <div class="modal fade" id="largeModal{{$datas->id}}" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-red">
                            <h4 class="modal-title" id="largeModalLabel">Detail</h4>
                        </div>
                        <div class="modal-body">
                            <p><b>Note :</b></p>
                            <textarea id="txtArea" name="notep6" class="form-control" rows="6" cols="5" readonly>{{$note->note}}</textarea>
                        </div>
                        <div class="modal-footer">
                            
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

            <!-- ///////////////////  Data Target  //////////////// -->
            @foreach($docin as $datas)

            <div id="collapseDetailOne{{$datas->id}}" class="collapse" aria-expanded="false" data-collapse-group="collapse-group" aria-labelledby="headingFive" data-parent="#accordionExample">
                <div class="row clearfix">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Detail {{$datas->name}}</h2>
                        </div>
                        <div class="body">

                            <?php $note = DB::table('history')->where('cus_id', $datas->id_cus)->latest('created_at')->where('remark_id', 'W7')->limit('1')->first(); ?>

                           <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="form-line">
                                        <div class="col-md-2">
                                            <h2 class="card-inside-title">Note :</h2>
                                        </div>
                                        <div class="col-md-10">
                                            <textarea id="txtArea" name="notep6" class="form-control" rows="3" cols="3" readonly>{{$note->note}}</textarea>
                                        </div>
                                    </div>    
                                </div>  
                                <div class="col-md-2"></div>
                           </div>
                           
                           
                            <table class="table table-responsive" style="margin-top: 50px !important">
                                <thead>
                                    <tr>
                                        <th width="10%">No</th>
                                        <th width="30%">Document</th>
                                        <th width="10%">Status</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php $y = 1; ?>

                                    <?php
                                           $datax1 = DB::table('doc_cust')->join('praapplication', 'doc_cust.cus_id', '=', 'praapplication.id_cus')->where('cus_id', $datas->id_cus)->where('doc_cust.type','1')->latest('doc_cust.created_at')->select('doc_cust.id as iddoc', 'doc_cust.doc_pdf as doc_pdf', 'doc_cust.verification as verification')->limit('1')->first();

                                           $datax2 = DB::table('doc_cust')->join('praapplication', 'doc_cust.cus_id', '=', 'praapplication.id_cus')->where('cus_id', $datas->id_cus)->where('doc_cust.type','2')->latest('doc_cust.created_at')->select('doc_cust.id as iddoc', 'doc_cust.doc_pdf as doc_pdf','doc_cust.verification as verification')->limit('1')->first();

                                           $datax3 = DB::table('doc_cust')->join('praapplication', 'doc_cust.cus_id', '=', 'praapplication.id_cus')->where('cus_id', $datas->id_cus)->where('doc_cust.type','4')->latest('doc_cust.created_at')->select('doc_cust.id as iddoc', 'doc_cust.doc_pdf as doc_pdf', 'doc_cust.verification as verification')->limit('1')->first();
                                    ?>

                                    <tr>  
                                        <td>{{$y++}}</td>
                                        <td>
                                            <a href="{{asset('/documents/user_doc/'.$datas->ic.'/'.$datax1->doc_pdf)}}" target='_blank'>{{$datax1->doc_pdf}}</a>
                                        </td>
                                        <td>
                                            <input type="hidden" name="iddatadoc1" value="{{$datax1->iddoc}}">
                                            
                                            <!-- <input type="hidden" id="basic_checkbox_{{$datas->id}}{{$datax1->doc_pdf}}" name="checkbox1" value="0"  />
                                            <label for="basic_checkbox_{{$datas->id}}{{$datax1->doc_pdf}}"></label> -->

                                            @if($datax1->verification == 0)
                                                <button class="btn bg-red btn-circle waves-effect waves-circle waves-float"> <i class="material-icons">close</i></button>
                                            @else
                                                <button class="btn bg-blue btn-circle waves-effect waves-circle waves-float"> <i class="material-icons">check</i></button>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>  
                                        <td>{{$y++}}</td>
                                        <td>
                                            <a href="{{asset('/documents/user_doc/'.$datas->ic.'/'.$datax2->doc_pdf)}}" target='_blank'>{{$datax2->doc_pdf}}</a>
                                        </td>
                                        <td>
                                            <input type="hidden" name="iddatadoc2" value="{{$datax2->iddoc}}">


                                            <!-- <input type="hidden" id="basic_checkbox_{{$datas->id}}{{$datax2->doc_pdf}}" name="checkbox2" value="0"  />
                                            <label for="basic_checkbox_{{$datas->id}}{{$datax2->doc_pdf}}"></label> -->

                                           @if($datax2->verification == 0)
                                                <button class="btn bg-red btn-circle waves-effect waves-circle waves-float"> <i class="material-icons">close</i></button>
                                            @else
                                                <button class="btn bg-blue btn-circle waves-effect waves-circle waves-float"> <i class="material-icons">check</i></button>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>  
                                        <td>{{$y++}}</td>
                                        @if(empty($datax3->doc_pdf))
                                            <td></td>
                                        @else
                                            <td>
                                                <a href="{{asset('/documents/user_doc/'.$datas->ic.'/'.$datax3->doc_pdf)}}" target='_blank'>{{$datas->ic}}-Spekar.pdf</a>
                                            </td>
                                            <td>
                                                <input type="hidden" name="iddatadoc3" value="{{$datax3->iddoc}}">

                                               <!-- <input type="hidden" id="basic_checkbox_{{$datas->id}}{{$datax3->doc_pdf}}" name="checkbox3" value="0" />
                                                <label for="basic_checkbox_{{$datas->id}}{{$datax3->doc_pdf}}"></label> -->

                                                @if($datax3->verification == 0)
                                                    <button class="btn bg-red btn-circle waves-effect waves-circle waves-float"> <i class="material-icons">close</i></button>
                                                @else
                                                    <button class="btn bg-blue btn-circle waves-effect waves-circle waves-float"> <i class="material-icons">check</i></button>
                                                @endif
                                            </td>
                                        @endif
                                    </tr>
                                   

                                </tbody>

                            </table>  

                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            <!-- ///////////////////  End Data Target  //////////////// -->

        </div>
    </section>


    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>


    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable();
            });
    </script>
    
@endsection

    