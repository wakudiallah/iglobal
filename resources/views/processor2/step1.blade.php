@extends('vadmin.tampilan')

@section('content')

    <!-- CSS Step =-->
    <link href="{{ asset('admin/css/tabsteptype.css') }}" rel="stylesheet" />


    <section class="content">
        <div class="container-fluid">
            
            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">layers</i> Loan Check & Calculation</a></li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

            @include('shared.notif')


   
        <div class="row clearfix">
            <div class="card">
                <div class="header bg-red">
                    <h2>Loan Check & Calculation</h2>
                </div>
                <div class="body">
                    
                    <!-- start step -->
                   <section class="design-process-section" id="process-tab">
                        <div class="row">
                          <div class="col-xs-12"> 
                            <!-- design process steps--> 
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs process-model more-icon-preocess" role="tablist">
                              <li role="presentation" class="active"><a href="#discover{{$step1->id}}" aria-controls="discover{{$step1->id}}" role="tab" data-toggle="tab"> <i class="material-icons" aria-hidden="true">credit_card</i>
                                <p>Loan Calculation</p>
                                </a></li>
                              <li role="presentation"><a href="#strategy{{$step1->id}}" aria-controls="strategy{{$step1->id}}" role="tab" data-toggle="tab"><i class="material-icons" aria-hidden="true">note</i>
                                <p>Document</p>
                                </a></li>
                              <li role="presentation"><a href="#optimization{{$step1->id}}" aria-controls="optimization{{$step1->id}}" role="tab" data-toggle="tab"><i class="material-icons" aria-hidden="true">call</i>
                                <p>Call 103</p>
                                </a></li>
                             
                            </ul>
                            <!-- end design process steps--> 
                            <!-- Tab panes -->
                            <div class="tab-content">
                              <div role="tabpanel" class="tab-pane active" id="discover{{$step1->id}}">
                                <div class="design-process-content">
                                
                                    <!--   
                                    {!! Form::open(['url' => ['/recount/loan/'.$step1->id_cus], 'class' => "probootstrap-form border border-danger", 'method' => 'post', 'id' => 'form-validate-pra']) !!}
                                -->

                                    
                                    <h4 style="margin-bottom: 30px !important; color: red">Eligibility Calculation By {{$step1->p2->name}}</h4>
                                   

                                    <input type="hidden" name="process5" value="{{$step1->process5}}">
                                    
                                    <input type="hidden" name="id_cus" value="{{$pra->id_cus}}">

                                    <input type="hidden" name="process5" value="{{$step1->process5}}">
                                    
                                    <div class="row">   
                                        <div class="col-md-6"> <!-- kolom 1 -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Name : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                   <input  class="form-control" type="text" name="" value="{{$step1->name}} " readonly> 
                                                    <input  class="form-control" type="hidden" name="id_praapplication" value="{{$step1->id_cus}} " readonly> 
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>IC :  </b>    
                                                </div>
                                                <div class="col-md-6">
                                                   <input  class="form-control" type="text" name="" value="{{$step1->ic}} " readonly> 
                                                    <input  class="form-control" type="hidden" name="id_praapplication" value="{{$step1->id_cus}} " readonly> 
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Email :  </b>    
                                                </div>
                                                <div class="col-md-6">
                                                   <input  class="form-control" type="text" name="" value="{{$step1->email}} " readonly> 
                                                    <input  class="form-control" type="hidden" name="id_praapplication" value="{{$step1->id_cus}} " readonly> 
                                                </div>
                                            </div>
                                            
                                        </div> <!-- End of kolom 1 -->
                                        


                                        <!--  =================kolom 2============== -->
                                        <div class="col-md-6">
                                            
                                            <?php $lm = DB::table('loanammount')->where('id_praapplication', $step1->id_cus)->latest('created_at')->limit('1')->first(); ?>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Phone : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                    <input  class="form-control" type="text" name="" value="{{$step1->notelp}} " readonly>
                                                   
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Employer : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                    <input  class="form-control" type="text" name="" value="{{$step1->majikan->Emp_Desc}} " readonly>
                                                   
                                                </div>
                                            </div>



                                            <div class="row">
                                                <div class="col-md-6">
                                                </div>
                                                @if($step1->emp_code == 1)
                                                    @if($step1->clerical_type == 1)
                                                        <div class="col-md-6" >
                                                            <input name="group1" type="radio" id="radio_1{{$step1->id}}" value="1" checked disabled/>
                                                            <label for="radio_1{{$step1->id}}">Clerical</label>
                                                            <input name="group1" type="radio" id="radio_2{{$step1->id}}" value="0" disabled/>
                                                            <label for="radio_2{{$step1->id}}">Non Clerical</label>
                                                        </div>
                                                    @else
                                                        <div class="col-md-6" >
                                                            <input name="group1" type="radio" id="radio_1{{$step1->id}}" value="1" disabled/>
                                                            <label for="radio_1{{$step1->id}}">Clerical</label>
                                                            <input name="group1" type="radio" id="radio_2{{$step1->id}}" value="0" checked disabled/>
                                                            <label for="radio_2{{$step1->id}}">Non Clerical</label>
                                                        </div>
                                                    @endif
                                                @endif
                                            </div> 

                                            <?php $existing = DB::table('meet_cust')->where('cus_id', $step1->id_cus)->latest('created_at')->limit('1')->first(); ?>

                                            
                                            @if($existing->existing == 1)
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Existing Cust : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                    <input name="group2" type="radio" id="radio_11{{$step1->id}}" onclick="javascript:yesnoCheck2{{$step1->id}}();" value="1" disabled />
                                                    <label for="radio_11{{$step1->id}}" checked>Yes</label>

                                                    <input name="group2" type="radio" id="radio_21{{$step1->id}}" onclick="javascript:yesnoCheck2{{$step1->id}}();" value="0" disabled />
                                                    <label for="radio_21{{$step1->id}}">No</label>  
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6" >
                                                    <b>Date Disbursement : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                   <input type="text" id="datepicker{{$step1->id}}" name="disb" class="form-control" value="{{$existing->date_disbustment}}" readonly> 
                                                </div>
                                            </div>
                                            @else
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Existing Cust : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                    <input name="group2" type="radio" id="radio_11{{$step1->id}}" onclick="javascript:yesnoCheck2{{$step1->id}}();" value="1" disabled />
                                                    <label for="radio_11{{$step1->id}}" >Yes</label>

                                                    <input name="group2" type="radio" id="radio_21{{$step1->id}}" onclick="javascript:yesnoCheck2{{$step1->id}}();" value="0" disabled checked />
                                                    <label for="radio_21{{$step1->id}}">No</label>  
                                                </div>
                                            </div>

                                            @endif
                                                                                  

                                        </div> <!-- End of kolom 2 -->

                                    </div>


                                    <div class="row" style="margin-top: 20px !important "> <!-- Row 2 -->
                                        <div class="col-md-6"><!-- ======== Kolom 1 Row 2 ====== -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Job Type: </b>    
                                                </div>
                                                <div class="col-md-6">
                                                    <select class="form-control show-tick js-example-basic-single js-states" data-live-search="true" id="Employment" name="Employment" required readonly disabled>
                                                      @foreach ($employment as $data)
                                                      <option value="{{$data->id}} " {{ $step1->employment_code == $data->id ? 'selected' : '' }}>{{$data->name}}</option>
                                                      @endforeach 
                                                    </select>  
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Basic Salary (RM) : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                   <input name="gaji_asas"  class="form-control" type="text" value="{{$step1->gaji_asas}}" required readonly> 
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Total Deduction (RM) : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                    <input  class="form-control" type="text" name="pot_bul"" value="{{$step1->pot_bul}}" required readonly>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Years : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                   <input  class="form-control" type="text" name="" value="{{$loanammount->Tenure->years}}" readonly >
                                                </div>
                                            </div>


                                        </div> <!-- ========End Kolom 1 Row 2 ====== -->
                                        
                                        <div class="col-md-6"><!-- ======== Kolom 1 Row 2 ====== -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Package : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                <select class="form-control show-tick js-example-basic-single js-states" data-live-search="true" name="loanpkg" required disabled="">
                                                    @foreach($loanpkg as $data) 
                                                        <option value="{{ $data->id }}" {{ $step1->loanpkg_code == $data->id ? 'selected' : '' }}>{{ $data->Ln_Desc }}</option>
                                                    @endforeach 
                                                   <!-- <input  class="form-control" type="text" name="" value="{{$step1->loanpkg->Ln_Desc}} " >--> 
                                                </select>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Allowance (RM) : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                    <input  class="form-control" name="elaun" type="text"  value="{{$step1->elaun}}" required readonly>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Loan Amount (RM) : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                   <input  class="form-control" type="text" name="jml_pem"  value="{{$lm->loanammount}}" required readonly>
                                                </div>
                                            </div>

                                            <?php 
                                               $bunga2 = $loanammount->loanammount * $loanammount->Tenure->rate /100   ;
                                               $bunga = $bunga2 * $loanammount->Tenure->years;
                                               $total = $loanammount->loanammount + $bunga ;
                                               $bulan = $loanammount->Tenure->years * 12 ;
                                               $installment =  $total / $bulan ;
                                            ?>
                                               
                                            <div class="row" >
                                                <div class="col-md-6">
                                                    <b>Monthly Installement (RM) : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                   <input name="installment"  class="form-control" type="text" value="{{floor($installment) }} " readonly > 
                                                </div>
                                            </div>

                                           
                                            <div class="row">
                                                <div class="col-md-8"></div>
                                                <div class="col-md-4">
                                                    <a href="{{ url('/cektenus_p2/'.$step1->id_cus) }}" class="btn bg-red btn-block btn-lg waves-effect"> Recalculate </a>
                                                </div>
                                            </div> 
                                            
                                            <!-- 
                                            <div class="row">
                                                <div class="col-md-8"></div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn bg-red btn-block btn-lg waves-effect">
                                                        Recount
                                                    </button> 
                                                </div> -->
                                            </div> 
                                        </div><!-- ======== End Kolom 1 Row 2 ====== -->
                                    </div> <!-- End of Row 2 -->


                                        {{ Form::close() }}

                                 </div>
                              </div><!-- end loan calculation -->


 
  
        {!! Form::open(array('url'=>'/save/process6/'.$step1->id_cus, 'method'=>'post', 'files'=>'true')) !!}

        {{ csrf_field() }}

        <input name="id_praapplication" id="id_praapplication" type="hidden"  value="{{$pra->id_cus}}" >

        <div class="row">
            <div class="col-md-6"></div>
            <div class="col-md-6">
                <div class="row" style="margin-top: 30px !important">
                    <div class="col-md-2">
                        <b>Remark:</b>
                    </div>
                    <div class="col-md-10">
                        <select name="remarkp6" class="form-control" id="one" required>
                           @if(!empty($data->remark6))
                                @if($data->remark6 =='W6')
                                <option value="W6" selected>Reject</option>
                                <option value="W9">Pass </option>
                                @else
                                 <option value="W9" selected>Pass </option>
                                 <option value="W6">Reject</option>
                                @endif
                            @else
                            <option value="" selected disabled hidden>Choose Remark</option>
                            <option value="W9">Pass</option>
                            <option value="W6">Reject <!-- Calculation & Changes --></option>
                            @endif
                            
                        </select>
                    </div>
                </div>


                <div class="row">
                    <div class="resources1" style="display: none" id="two{{$step1->id}}">
                        <div class="col-md-2"><b>Note :</b></div>
                        <div class="col-md-10">
                            ​<textarea id="txtArea" name="notep6" class="form-control" rows="6" cols="3"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                                                    
         <div class="row">
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-md-offset-10">
                <button type="submit" class="btn bg-green btn-block btn-lg waves-effect">
                    Save
                </button>
                {{ csrf_field() }}
            </div>
        </div>

    {{ Form::close() }}
    </fieldset>
                           
                            </div>
                          </div>
                        </div>
                    </section><!-- end of step -->

                </div>

            </div>
        </div>
    </div>


    <!-- ///////////////////  End Data Target  //////////////// -->

    

    @push('js')

        <script>
            var Privileges = jQuery('#one');
            var select = this.value;
            Privileges.change(function () {
                if ($(this).val() == 'W6' ) {
                    $('.resources1').show();
                }
                else $('.resources1').hide();
            });
        </script>

        @foreach($process6 as $step1)
        <script type="text/javascript">

            var Privileges = jQuery('#three{{$step1->id}}');
            var select = this.value;
            Privileges.change(function () {
                if ($(this).val() == 'W7' ) {
                    $('.resources2').show();
                }
                else $('.resources2').hide();
            });

        


            //form step 

        // script for tab steps
    $('a[data-toggle="tab{{$step1->id}}"]').on('shown.bs.tab', function (e) {

        var href = $(e.target).attr('href');
        var $curr = $(".process-model{{$step1->id}}  a[href='" + href + "']").parent();

        $('.process-model{{$step1->id}} li').removeClass();

        $curr.addClass("active");
        $curr.prevAll().addClass("visited");
    });
// end  script for tab steps

            //end form step 



        </script>
        @endforeach
<script type="text/javascript">
        
$(document).ready(function() {

var found = [];
    $("select[name='remarkp6'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

});

</script>
<script type="text/javascript">

$( ".fn" ).keyup(function() {
  
// Income
var fn_monthly_basic = $('#fn_monthly_basic').val();
var fn_govt_service = $('#fn_govt_service').val();
var fn_financial_incentive = $('#fn_financial_incentive').val();
var fn_fixed_allowance = $('#fn_fixed_allowance').val();
var fn_housing_allowance = $('#fn_housing_allowance').val();
var fn_cost_of = $('#fn_cost_of').val();
var fn_special_allowance = $('#fn_special_allowance').val();

var fn_gross_income = parseFloat(fn_monthly_basic) + parseFloat(fn_govt_service)  + parseFloat(fn_financial_incentive)
      + parseFloat(fn_fixed_allowance) + parseFloat(fn_housing_allowance) + parseFloat(fn_cost_of) + parseFloat(fn_special_allowance)  ;
    
    
// Non fixed Variable
var fn_overtime_allowance = $('#fn_overtime_allowance').val();
var fn_travel = $('#fn_travel').val();
var fn_commission = $('#fn_commission').val();
var fn_business_income = $('#fn_business_income').val();
var fn_yearly_devidend = $('#fn_yearly_devidend').val();
var fn_food_allowance = $('#fn_food_allowance').val();
var fn_yearly_contractual = $('#fn_yearly_contractual').val();
var fn_housing_building = $('#fn_housing_building').val();
var fn_non_fixed = $('#fn_non_fixed').val();
var fn_share_income = $('#fn_share_income').val();

  
var fn_total_income = parseFloat(fn_overtime_allowance) + parseFloat(fn_travel)  + parseFloat(fn_commission)
      + parseFloat(fn_business_income) + parseFloat(fn_yearly_devidend) + parseFloat(fn_food_allowance) + parseFloat(fn_yearly_contractual)
      + parseFloat(fn_housing_building) + parseFloat(fn_non_fixed) + parseFloat(fn_share_income) + parseFloat(fn_gross_income) ;
    $("#fn_total_income").val(parseFloat(fn_total_income).toFixed(2));
  

// Deductions Variable
var fn_knwsp = $('#fn_knwsp').val();
var fn_income_tax = $('#fn_income_tax').val();
var fn_house_financing = $('#fn_house_financing').val();
var fn_others = $('#fn_others').val();
var fn_lembaga_tabung = $('#fn_lembaga_tabung').val();
var fn_insurance = $('#fn_insurance').val();
var fn_perkeso = $('#fn_perkeso').val();
var fn_zakat = $('#fn_zakat').val();
var fn_bpa = $('#fn_bpa').val();
var fn_asb = $('#fn_asb').val();

// Total Deductions
var fn_total_deductions = parseFloat(fn_knwsp) + parseFloat(fn_income_tax) + parseFloat(fn_house_financing)
      + parseFloat(fn_others) + parseFloat(fn_lembaga_tabung) + parseFloat(fn_insurance) + parseFloat(fn_perkeso)
      + parseFloat(fn_zakat) + parseFloat(fn_bpa) + parseFloat(fn_asb);

    $("#fn_total_deductions").val(parseFloat(fn_total_deductions).toFixed(2));
    
// Total net income
var fn_total_income = $('#fn_total_income').val();
var fn_total_deductions = $('#fn_total_deductions').val();

var fn_total_net = parseFloat(fn_total_income) - parseFloat(fn_total_deductions);
    $("#fn_total_net").val(parseFloat(fn_total_net).toFixed(2));
  
// Bayaran Balik
var pemrumah1 = $('#pemrumah1').val();
var pemrumah2 = $('#pemrumah2').val();
var pemrumah3 = $('#pemrumah3').val();
var kadkredit1 = $('#kadkredit1').val();
var kadkredit2 = $('#kadkredit2').val();
var kadkredit3 = $('#kadkredit3').val();
var pempribadi = $('#pempribadi').val();
var pemperabot = $('#pemperabot').val();
var pemkenderaan = $('#pemkenderaan').val();
var pemlain = $('#pemlain').val();
var jumlahpembiayaan = parseFloat(pemrumah1) + parseFloat(pemrumah2)
          + parseFloat(pemrumah3) + parseFloat(kadkredit1)
          + parseFloat(kadkredit2) + parseFloat(kadkredit3)
          + parseFloat(pempribadi) + parseFloat(pemperabot)
          + parseFloat(pemkenderaan) + parseFloat(pemlain);
          
    $("#jumlahpembiayaan").val(parseFloat(jumlahpembiayaan).toFixed(2));

// Langkah 1
$("#pendapatan").val(parseFloat(fn_total_income).toFixed(2));
$("#kwsp").val(parseFloat(fn_knwsp).toFixed(2));
$("#perkeso").val(parseFloat(fn_perkeso).toFixed(2));
$("#cukai_pendapatan").val(parseFloat(fn_income_tax).toFixed(2));
$("#zakat_pendapatan").val(parseFloat(fn_zakat).toFixed(2));
$("#lainlain1").val(parseFloat(fn_others).toFixed(2));

var lainlain2= parseFloat(fn_knwsp) + parseFloat(fn_perkeso) + parseFloat(fn_income_tax)
      + parseFloat(fn_zakat) + parseFloat(fn_others);
    
$("#lainlain2").val(parseFloat(lainlain2).toFixed(2));

var jumlah_pendapatan= parseFloat(fn_total_income) - parseFloat(lainlain2);
    $("#jumlah_pendapatan").val(parseFloat(jumlah_pendapatan).toFixed(2));
    
// Langkah 2

$("#l2_pemrumah").val(parseFloat(fn_house_financing).toFixed(2));
$("#l2_bps").val(parseFloat(fn_bpa).toFixed(2));
$("#l2_pemdarib").val(parseFloat(jumlahpembiayaan).toFixed(2));

var l2_jumlahbayaran= parseFloat(fn_house_financing) + parseFloat(fn_bpa) + parseFloat(jumlahpembiayaan);
$("#l2_jumlahbayaran").val(parseFloat(l2_jumlahbayaran).toFixed(2));

// Langkah 3
var l3_bkr = $('#l3_bkr').val();
var l3_koperasi = $('#l3_koperasi').val();
var l3_bank = $('#l3_bank').val();
var l3_lainlain = $('#l3_lainlain').val();

var l3_jumlah = parseFloat(l3_bkr) + parseFloat(l3_koperasi) + parseFloat(l3_bank)  + parseFloat(l3_lainlain);
$("#l3_jumlah").val(parseFloat(l3_jumlah).toFixed(2));


// Langkah 4


$("#l4_jumlahbayaran").val(parseFloat(l2_jumlahbayaran).toFixed(2));
$("#l4_jumlah_pendapatan").val(parseFloat(jumlah_pendapatan).toFixed(2));

var l4_dsr  = (parseFloat(l2_jumlahbayaran) / parseFloat(jumlah_pendapatan)) * 100;
$("#l4_dsr").val(parseFloat(l4_dsr).toFixed(2));

var l4_jumlahpinjam = $('#l4_jumlahpinjam').val();
var l4_jumlahpinjam2 = $('#l4_jumlahpinjam2').val();
var l4_tempoh = $('#l4_tempoh').val();
var l4_kadar = $('#l4_kadar').val();


var l4_tempohbulan = parseFloat(l4_tempoh) * 12;
$("#l4_tempohbulan").val(parseInt(l4_tempohbulan));


var l4_ansuran_total = parseFloat(l4_jumlahpinjam) * parseFloat(l4_tempoh)* (parseFloat(l4_kadar) / 100) + parseFloat(l4_jumlahpinjam);
var l4_ansuran = parseFloat(l4_ansuran_total) / parseFloat(l4_tempoh);


$("#l4_ansuran").val(parseFloat(l4_ansuran).toFixed(2));


var l4_bayaranbalik = parseFloat(l2_jumlahbayaran) + parseFloat(l4_ansuran)- parseFloat(l3_jumlah);
$("#l4_bayaranbalik").val(parseFloat(l4_bayaranbalik).toFixed(2));

var l4_jumlah_pendapatan2 = parseFloat(jumlah_pendapatan);
$("#l4_jumlah_pendapatan2").val(parseFloat(l4_jumlah_pendapatan2).toFixed(2));


var l4_dsr2 = (parseFloat(l4_bayaranbalik) / parseFloat(l4_jumlah_pendapatan2)) * 100;
$("#l4_dsr2").val(parseFloat(l4_dsr2).toFixed(2));

});
</script>
<script type="text/javascript">

$( ".fn" ).keyup(function() {

var insurance_premium = $('#insurance_premium').val();
var roadtax = $('#roadtax').val();

//Admin fee
var admin_fee =(parseFloat(insurance_premium)) * 0.1;
$("#admin_fee").val(parseFloat(admin_fee).toFixed(2));


var gst= (parseFloat(admin_fee) * 6) /100;
$("#gst").val(parseFloat(gst).toFixed(2));

var loan_amaun= parseFloat(insurance_premium)  + parseFloat(admin_fee) + parseFloat(gst) ;
$("#loan_amaun").val(parseFloat(loan_amaun).toFixed(2));

var tenure = $('#tenure').val();


var total_repayment= parseFloat(loan_amaun) * 0.08/12* parseFloat(tenure) +parseFloat(loan_amaun); 
$("#total_repayment").val(parseFloat(total_repayment).toFixed(2));


var installment= parseFloat(total_repayment) / parseFloat(tenure) ; 
$("#installment").val(parseFloat(installment).toFixed(2));

var deduction_fee= parseFloat(installment) * 0.020412; 
$("#deduction_fee").val(parseFloat(deduction_fee).toFixed(2));

var gst_fee= (parseFloat(deduction_fee) * 6) /100;
$("#gst_fee").val(parseFloat(gst_fee).toFixed(2));

var salary_deduction= parseFloat(gst_fee) + parseFloat(installment) + parseFloat(deduction_fee) ; 
$("#salary_deduction").val(parseFloat(salary_deduction).toFixed(2));

var total_repayment= parseFloat(loan_amaun) * 0.08/12* parseFloat(tenure) +parseFloat(loan_amaun); 
$("#total_repayment").val(parseFloat(total_repayment).toFixed(2));
});
</script>



<script type="text/javascript">


    $( "#Employment" ).change(function() {
        var Employment = $('#Employment').val();

        if( Employment == '1') {

          $("#Employer").html(" ");
          $("#Employer2").val(" ");
            $("#majikan2").show();
            $("#majikantext").focus();
             $("#majikan").hide();
           $("#Package").val("Mumtaz-i");
        }
          else if( Employment == '2') {
       
         $("#Employer").html(" ");
         $("#Employer2").val(" ");
            $("#majikan2").show();
            $("#majikantext").focus();
             $("#majikan").hide();
             $("#Package").val("Afdhal-i");
        }

        else if( Employment == '3') {
       
         $("#Employer").html(" ");
         $("#Employer2").val(" ");
            $("#majikan2").show();
            $("#majikantext").focus();
             $("#majikan").hide();
             $("#Package").val("Afdhal-i");
        }
          else if( Employment == '4') {
       
         $("#Employer").html(" ");
         $("#Employer2").val(" ");
            $("#majikan2").show();
            $("#majikantext").focus();
             $("#majikan").hide();
             $("#Package").val("Private Sector PF-i");
        }
        else if( Employment == '5') {
             
             $("#Employer").html(" ");
         $("#Employer2").val(" ");
            $("#majikan2").show();
            $("#majikantext").focus();
             $("#majikan").hide();

              $("#Package").val("Afdhal-i");
                
            //  $("#majikan").simulate('click');

        }
      $.ajax({
                    url: "<?php  print url('/'); ?>/employer/"+Employment,
                    dataType: 'json',
                    data: {
                       
                    },
                    success: function (data, status) {

                        jQuery.each(data, function (k) {

                            $("#Employer").append("<option value='" + data[k].id + "'>" + data[k].name + "</option>");
                        });
                    }
                });
    });
</script>
    @endpush




        </div>
    </section>

@endsection

