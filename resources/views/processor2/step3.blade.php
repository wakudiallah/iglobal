<!-- CSS Step =-->
<link href="{{ asset('admin/css/tabsteptype.css') }}" rel="stylesheet" />


<div class="row clearfix">
    <!-- Task Info -->
        <div class="row clearfix"> <!-- Breadcrumber -->
            <div class="col-md-6">
                <ol class="breadcrumb breadcrumb-col-pink">
                    <li><a href="javascript:void(0);"><i class="material-icons">layers</i> Loan Check & Calculation</a></li>
                </ol>
            </div>
        </div> <!-- End of breadcrumber -->

        @include('shared.notif')
    

    <!-- #END# Task Info -->
</div>

    <!-- ///////////////////  Data Target  ///////////////// -->
   
   
        <div class="row clearfix">
            <div class="card">
                <div class="header bg-red">
                    <h2>Detail {{$step1->name}} </h2>
                </div>
                <div class="body">
                    
                    <!-- start step -->
                   <section class="design-process-section" id="process-tab">
                        <div class="row">
                          <div class="col-xs-12"> 
                            <!-- design process steps--> 
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs process-model more-icon-preocess" role="tablist">
                              <li role="presentation" ><a href="#discover{{$step1->id}}" aria-controls="discover{{$step1->id}}" role="tab" data-toggle="tab"> <i class="material-icons" aria-hidden="true">credit_card</i>
                                <p>Loan Calculation</p>
                                </a></li>
                              <li role="presentation" ><a href="#strategy{{$step1->id}}" aria-controls="strategy{{$step1->id}}" role="tab" data-toggle="tab"><i class="material-icons" aria-hidden="true">note</i>
                                <p>Document</p>
                                </a></li>
                              <li role="presentation" class="active"><a href="#optimization{{$step1->id}}" aria-controls="optimization{{$step1->id}}" role="tab" data-toggle="tab"><i class="material-icons" aria-hidden="true">call</i>
                                <p>Call 103</p>
                                </a></li>
                             
                            </ul>
                            <!-- end design process steps--> 
                            <!-- Tab panes -->
                            <div class="tab-content">
                              <div role="tabpanel" class="tab-pane active" id="discover{{$step1->id}}">
                                <div class="design-process-content">
                                    
                                    {!! Form::open(array('url'=>'save/process8/'.$step1->id_cus, 'method'=>'post', 'files'=>'true')) !!}

                                    {{ csrf_field() }}

                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <b>Customer Office Telp : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                   <input class="form-control" type="text" name="telpoffice" value="" min="1" maxlength="15" oninput="this.value=this.value.replace(/[^0-9]/g,'');" required>
                                                </div>
                                                <div class="col-md-4"></div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-2">
                                                    <b>Note : </b>    
                                                </div>
                                                <div class="col-md-10">
                                                   <textarea id="txtArea" name="note" class="form-control" rows="5" cols="3" maxlength="255" required></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2"></div>

                                        <!-- <div class="col-md-6">
                                            <div class="col-md-6">
                                                <b>Remark:</b>
                                            </div>
                                            <div class="col-md-6">
                                                <select name="pro2remark" class="form-control" id="one{{$step1->id}}">
                                                    <option value="" selected disabled hidden>Choose Remark</option>
                                                    <option value="W8">MBSB Processing</option>
                                                    <option value="W8">Pass</option>
                                                    <option value="W7">Document Incomplete</option>
                                                </select>
                                            </div>

                                            <div class="resources" style="display: none" id="two{{$step1->id}}">
                                                <div class="col-md-6"><b>Note :</b></div>
                                                <div class="col-md-6">
                                                    ​<textarea id="txtArea" name="note" class="form-control" rows="3" cols="3"></textarea>
                                                </div>
                                            </div>
                                        </div> -->
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6 col-md-offset-6">
                                            <input  class="hidden" type="text" name="name" value="{{$step1->name}} " >

                                            <div class="col-md-3">
                                                <b>Remark:</b>
                                            </div>
                                            <div class="col-md-9">
                                                <select name="stage" class="form-control" id="three" required>
                                                    <option value="" selected disabled hidden>Choose Remark</option>
                                                    <option value="W8">Pass</option>
                                                    <option value="W19">Reject <!-- Document Check --></option>
                                                </select>
                                            </div>

                                            <div class="resources2" style="display: none" id="two{{$step1->id}}">
                                                <div class="col-md-3"><b>Note :</b></div>
                                                <div class="col-md-9">
                                                    ​<textarea id="txtArea" name="notep7" class="form-control" rows="6" cols="3"></textarea>
                                                </div>
                                            </div>

                                        </div>
                                        
                                    </div> 



                                    <div class="row">
                                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-md-offset-10">
                                            <button type="submit" class="btn bg-green btn-block btn-lg waves-effect">
                                                Save
                                            </button>
                                            {{ csrf_field() }}
                                        </div>
                                    </div>
                                    
                                    {{ Form::close() }}
                                        </fieldset>
                           
                            </div>
                          </div>
                        </div>
                    </section><!-- end of step -->

                </div>

            </div>
        </div>
    </div>


    <!-- ///////////////////  End Data Target  //////////////// -->


    @push('js')

        @foreach($process6 as $step1)
        <script type="text/javascript">
            var Privileges = jQuery('#one{{$step1->id}}');
            var select = this.value;
            Privileges.change(function () {
                if ($(this).val() == 'W6' ) {
                    $('.resources1').show();
                }
                else $('.resources1').hide();
            });

            var Privileges = jQuery('#three{{$step1->id}}');
            var select = this.value;
            Privileges.change(function () {
                if ($(this).val() == 'W7' ) {
                    $('.resources2').show();
                }
                else $('.resources2').hide();
            });

        


            //form step 

        // script for tab steps
    $('a[data-toggle="tab{{$step1->id}}"]').on('shown.bs.tab', function (e) {

        var href = $(e.target).attr('href');
        var $curr = $(".process-model{{$step1->id}}  a[href='" + href + "']").parent();

        $('.process-model{{$step1->id}} li').removeClass();

        $curr.addClass("active");
        $curr.prevAll().addClass("visited");
    });
// end  script for tab steps

            //end form step 



        </script>
        @endforeach
<script type="text/javascript">
    
$(document).ready(function() {

var found = [];
    $("select[name='remarkp6'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

});

</script>

    @endpush