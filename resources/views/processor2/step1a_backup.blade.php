@extends('vadmin.tampilan')

@section('content')

    <!-- CSS Step =-->
    <link href="{{ asset('admin/css/tabsteptype.css') }}" rel="stylesheet" />

    <section class="content">
        <div class="container-fluid">
            
            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">layers</i> Loan Check & Calculation</a></li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

            @include('shared.notif')

   
            <div class="row clearfix">
                <div class="card">
                    <div class="header bg-red">
                        <h2>Loan Check & Calculation </h2>
                    </div> <!-- end of header -->
                    <div class="body">
                        <!-- start step -->
                       <section class="design-process-section" id="process-tab">
                            <div class="row">
                              <div class="col-xs-12"> 
                                <!-- design process steps--> 
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs process-model more-icon-preocess" role="tablist">
                                  <li role="presentation" class="active"><a href="#discover{{$step1->id}}" aria-controls="discover{{$step1->id}}" role="tab" data-toggle="tab"> <i class="material-icons" aria-hidden="true">credit_card</i>
                                    <p>Loan Calculation</p>
                                    </a></li>
                                  <li role="presentation"><a href="#strategy{{$step1->id}}" aria-controls="strategy{{$step1->id}}" role="tab" data-toggle="tab"><i class="material-icons" aria-hidden="true">note</i>
                                    <p>Document</p>
                                    </a></li>
                                  <li role="presentation"><a href="#optimization{{$step1->id}}" aria-controls="optimization{{$step1->id}}" role="tab" data-toggle="tab"><i class="material-icons" aria-hidden="true">call</i>
                                    <p>Call 103</p>
                                    </a></li>
                                 
                                </ul>
                                <!-- end design process steps--> 
                                <!-- Tab panes -->


                                <div class="tab-content">
                                  <div role="tabpanel" class="tab-pane active" id="discover{{$step1->id}}">
                                    <div class="design-process-content">
                                    


                                        {{ csrf_field() }}

                                        
                                        <h4 style="margin-bottom: 30px !important; color: red">Eligibility Calculation By {{$step1->p2->name}} </h4>
                                       

                                        <input type="hidden" name="process5" value="{{$step1->process5}}">
                                        
                                        <input type="hidden" name="id_cus" value="{{$pra->id_cus}}">


                                        <input type="hidden" name="process5" value="{{$step1->process5}}">
                                        
                                        <div class="row">   
                                        <div class="col-md-6"> <!-- kolom 1 -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Name  </b>    
                                                </div>
                                                <div class="col-md-6">
                                                   <input  class="form-control" type="text" name="" value="{{$step1->name}} " readonly> 
                                                    <input  class="form-control" type="hidden" name="id_praapplication" value="{{$step1->id_cus}} " readonly> 
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>IC :  </b>    
                                                </div>
                                                <div class="col-md-6">
                                                   <input  class="form-control" type="text" name="" value="{{$step1->ic}} " readonly> 
                                                    <input  class="form-control" type="hidden" name="id_praapplication" value="{{$step1->id_cus}} " readonly> 
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Email :  </b>    
                                                </div>
                                                <div class="col-md-6">
                                                   <input  class="form-control" type="text" name="" value="{{$step1->email}} " readonly> 
                                                    <input  class="form-control" type="hidden" name="id_praapplication" value="{{$step1->id_cus}} " readonly> 
                                                </div>
                                            </div>
                                            
                                            
                                            
                                            
                                        </div> <!-- End of kolom 1 -->
                                        


                                        <!--  =================kolom 2============== -->
                                        <div class="col-md-6">
                                            
                                            <?php $lm = DB::table('loanammount')->where('id_praapplication', $step1->id_cus)->latest('created_at')->limit('1')->first(); ?>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>No Telf : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                    <input  class="form-control" type="text" name="" value="{{$step1->notelp}} " readonly>
                                                   
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Employer : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                    <input  class="form-control" type="text" name="" value="{{$step1->majikan->Emp_Desc}} " readonly>
                                                   
                                                </div>
                                            </div>
                                                                                  

                                        </div> <!-- End of kolom 2 -->

                                    </div>


                                    <?php $year = DB::table('loanammount')->join('tenure_detail', 'loanammount.id_tenure', '=', 'tenure_detail.id' )->where('id_praapplication', $step1->id_cus)->latest('tenure_detail.created_at')->limit('1')->first(); ?>

                                    <div class="row" style="margin-top: 20px !important "> <!-- Row 2 -->
                                        <div class="col-md-6"><!-- ======== Kolom 1 Row 2 ====== -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Job Type: </b>    
                                                </div>
                                                <div class="col-md-6">
                                                    <select class="form-control" id="Employment" name="Employment">
                                                      @foreach ($employment as $data)
                                                      <option value="{{$data->id}}">{{$data->name}}</option>
                                                      @endforeach                          
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Basic Salary (RM) : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                   <input name="gaji_asas"  class="form-control" type="text" value="{{$step1->gaji_asas}} " readonly> 
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Total Deduction (RM) : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                    <input  class="form-control" type="text" name="pot_bul"" value="{{$step1->pot_bul}}" readonly>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Years : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                   <input  class="form-control" type="text" name="" value="{{$year->years}}" readonly>
                                                </div>
                                            </div>


                                        </div> <!-- ========End Kolom 1 Row 2 ====== -->
                                        
                                        <div class="col-md-6"><!-- ======== Kolom 1 Row 2 ====== -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Package : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                   <input  class="form-control" type="text" name="" value="{{$step1->loanpkg->Ln_Desc}} " readonly> 
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Allowance (RM) : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                    <input  class="form-control" name="elaun" type="text"  value="{{$step1->elaun}}" readonly>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <b>Loan Amount (RM) : </b>    
                                                </div>
                                                <div class="col-md-6">
                                                   <input  class="form-control" type="text" name=""  value="{{$lm->loanammount}}" readonly>
                                                </div>
                                            </div>



                                        </div><!-- ======== End Kolom 1 Row 2 ====== -->
                                    </div> <!-- End of Row 2 -->




                                     </div>
                                  </div><!-- end loan calculation -->


     
                                            </fieldset>
                               
                                </div>




                              </div>
                            </div>
                        </section><!-- end of step -->
                        

                    </div> 
                </div>    
            </div> <!-- end of card-->

            

            <!-- start tenure -->
            <div class="row clearfix">
                <div class="card">
                    <div class="header bg-red">
                        <h2>Tenure </h2>
                    </div> <!-- end of header -->
                    <div class="body">

                        <?php
                $icnumber = $pra->icnumber;
                $tanggal  = substr($icnumber,4, 2);
                $bulan    = substr($icnumber,2, 2);
                $tahun    = substr($icnumber,0, 2); 

                if($tahun > 30) {
                    $tahun2 = "19".$tahun;
                }
                else {
                     $tahun2 = "20".$tahun;
                }
               
                $lahir = $tahun2.'-'.$bulan.'-'.$tanggal;
                $lahir =  date('Y-m-d', strtotime($lahir. ' - 16 month'));
                $oDateNow = new DateTime();
                $oDateBirth = new DateTime($lahir);
                $oDateIntervall = $oDateNow->diff($oDateBirth);

                $umur =  $oDateIntervall->y;
                $durasix = 60 - $oDateIntervall->y;
                if( $durasix  > 10){ $durasi = 10 ;} 
                else { $durasi = $durasix ;}
            ?>

            <!--- ============== Content ============ --> 
        

                        <div class="row">

                           
                            <form action='{{url('/')}}/p2/custtenus/update/{{$step1->id_cus}}' method='post' enctype="multipart/form-data">


                            {{ csrf_field() }}

                            <div class="col-md-3"></div>

                            <div class="col-md-6">
                                <div class="col-md-6">
                                    <label><b>Package</b></label>   
                                </div>
                                <div class="col-md-6">
                                    <b><input type="text" id="Package2" value="{{$pra->loanpkg->Ln_Desc}}"  name="Package2" placeholder="Package" disabled="disabled" class="form-control"></b>

                                    <input name="id_praapplication" id="id_praapplication" type="hidden"  value="{{$pra->id}}" >
                                      <input name="id_cus" id="id_cus" type="text"  value="{{$pra->id_cus}}" >
                                     <input name="employment_code" id="employment_code" type="hidden"  value="{{$pra->employment_code}}" >
                                       <input name="Employment" id="Employment" type="hidden"  value="{{$pra->employment_code}}" >
                                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                    
                                </div>
                                
                                <div class="col-md-6">                    
                                    <label> <b> <font color="red" size="3.0" >FUNDAMENTAL FINANCING QUALIFICATIONS <!--KELAYAKAN  PEMBIAYAAN  MAKSIMA -->  </font>  </b> </label>
                                </div>

                                <div class="col-md-6">
                                            
                                            @foreach($loan as $loan)
                                                <?php
                                                $salary_dsr = ($zbasicsalary * ($loan->dsr / 100)) - $zdeduction;
                                                $ndi = ($zbasicsalary - $zdeduction) -  1300;
                                                $max  =  $salary_dsr * 12 * 10 ;
                                                                               
                                                function pembulatan($uang) {
                                                    $puluhan = substr($uang, - 3);
                                                        if($puluhan<500) {
                                                                $akhir = $uang - $puluhan; 
                                                            }

                                                        else{
                                                                $akhir = $uang - $puluhan;
                                                            }

                                                        return $akhir;
                                                            }

                                                if(!empty($loan->max_byammount))  {
                                                  
                                                    $ansuran = intval($salary_dsr)-1;
                                                      if($pra->loanpkg_code =="1") {  //aku edit
                                                          $bunga = 3.8/100;
                                                      }
                                                      elseif($pra->loanpkg_code == "2") {
                                                          $bunga = 4.9/100;
                                                      }

                                                      else {
                                                          $bunga = 5.92/100;
                                                      }
                                                   
                                                      $pinjaman = 0;

                                                      for ($i = 0; $i <= $loan->max_byammount; $i++) {
                                                          $bungapinjaman = $i  * $bunga * $durasi;   //
                                                          $totalpinjaman = $i + $bungapinjaman ;
                                                          $durasitahun = $durasi * 12;
                                                          $ansuran2 = intval($totalpinjaman / ($durasi * 12))  ;
                                                          //echo $ansuran2."<br>";
                                                          if ($ansuran2 < $ndi)
                                                          {
                                                              $pinjaman = $i;
                                                          }
                                                      }   

                                                      if($pinjaman > 1) {

                                                          $bulat = pembulatan($pinjaman);
                                                          $loanx =  number_format($bulat, 0 , ',' , ',' ) ; 
                                                          $loanz = $bulat;
                                                      }
                                                      else {
                                                          $loanx =  number_format($loan->max_byammount, 0 , ',' , ',' ) ; 
                                                          $loanz = $loan->max_byammount;
                                                      }
                                                }
                                                else { 

                                                    $bulat = pembulatan($loan->max_bysalary * $total_salary);
                                                    $loanx =  number_format($bulat, 0 , ',' , ',' ) ; 
                                                    $loanz = $bulat;
                                                    if ($loanz > 199000) {

                                                          $loanz  = 250000;
                                                          $loanx =  number_format($loanz, 0 , ',' , ',' ) ; 
                                                    }
                                                }

                                                ?>
                                            @endforeach

                                        <b><input readonly type="text" id="MaxLoan" class="form-control" value=" RM {{$loanx}}" name="MaxLoan" placeholder="Max Loan Eligibility (RM)" class="merah" requierd> </b>
                                        <input readonly type="hidden" id="maxloanz" value="{{$loanz}}" name="maxloanz" placeholder="Max Loan Eligibility (RM)" requierd>            
                                </div> 
                            </div>
                            <div class="col-md-3"></div> <!-- margin -->               
                        </div> <!-- end row -->
                        
                        <div class="row" style="margin-bottom: 40px !important">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <div class="col-md-6">
                                    <label><b>  <!-- Jumlah Pembiayaan --> Loan Ammount (RM) </b> </label>   
                                </div>
                                <div class="col-md-6">
                                    <label class="input state-<?php if( $pra->jml_pem <= $loanz ) { print "success"; } else { print "error"; }?>">
                                    <i class="icon-append fa fa-credit-card"></i>
                                    <input type="text" name="jml_pem" class="form-control"  id="jml_pem" onkeypress="return isNumberKey(event)" value="{{ $pra->jml_pem }}"  placeholder="RM " onkeyup="this.value = minmax(this.value, 0, {{$loanz}})">
                                    <b class="tooltip tooltip-bottom-right">Jumlah Pembiayaan</b>
                                    </label>    
                                </div>
                                                    

                                <div class="col-md-6">
                                    <label> <b>  <!-- Jumlah Pendapatan --> Total income </b> </label>  
                                </div>
                                
                                <div class="col-md-6">
                                    <input type="text" id="pendapatan" class="form-control" value="RM {{ number_format($total_salary, 0 , ',' , ',' )}}" name="pendapatan" placeholder="Loan Amount" disabled="disabled">
                                    <input type="hidden" id="gaji_asas" class="form-control" value="{{$pra->gaji_asas}}" name="gaji_asas">
                                    <input type="hidden" id="elaun" class="form-control" value="{{$pra->gaji_asas}}" name="elaun">
                                    <input type="hidden" id="pot_bul" class="form-control" value="{{$pra->pot_bul}}"" name="pot_bul">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">  
                                </div>
                                
                                <div class="col-md-6">
                                    <label> <b>  <!-- Ansuran Maksima --> Max Installment </b> </label> 
                                </div>
                                <div class="col-md-6">
                                    <input type="text" id="ansuran_maksima" class="form-control" value="RM {{ number_format($ndi, 0 , ',' , ',' )  }}   / month" name="ansuran_maksima" placeholder="Ansuran Maksima" readonly>
                                    <b class="tooltip tooltip-bottom-right">Loan Amount</b> 
                                </div>

                                <!-- button -->
                                <div class="col-md-8"></div>
                                <div class="col-md-4">
                                    <button class="btn btn-success" type="submit"> Recount </button>
                                </div>
                                <!-- end button -->

                            </div>
                            <div class="col-md-3"></div> <!-- margin -->
                                                      
                                   
                        </div>  
                        
                           {!! Form::close() !!}    

                            

                                    <input type="hidden" name="LoanAmount2" class="form-control"  id="LoanAmount2" onkeypress="return isNumberKey(event)" value="{{$pra->jml_pem }}"  >
                                    <input type="hidden" name="maxloanz" class="form-control"  id="maxloanz" onkeypress="return isNumberKey(event)" value="{{$loanz }}"  >
                        <table class="table table-hover" style="margin-top: 30px !important">
                            <thead>
                                <tr>
                                    <th width="20" valign="middle"> <b>  Duration   </b> </th>
                                    <th width="40"> <b> Financing Amount  </b> </th>
                                    <th width="20"><b>  Monthly installment </b> </th>
                                    <th width="20"> <b>  Profit Rate </b> </th>
                                    <th width="10" align="center"> <b>  Select </b> </th>
                                </tr>
                            </thead>
                            <tbody>
                                 <?php if( $pra->jml_pem <= $loanz ) { $ndi_limit=$loan->ndi_limit;?>
                                        @foreach($tenure as $tenure)
                                            <?php 
                                                   $bunga2 =  $pra->jml_pem * $tenure->rate /100   ;
                                                   $bunga = $bunga2 * $tenure->years;
                                                   $total = $pra->jml_pem + $bunga ;
                                                   $bulan = $tenure->years * 12 ;
                                                   $installment =  $total / $bulan ;
                                                   $ndi_state = ($total_salary - $zdeduction) - $installment; 
                                                   
                                                   if($installment  <= $salary_dsr && $ndi_state>=$ndi_limit) {     
                                                ?>
                                            <tr>
                                                <td>{{$tenure->years}} years</td>
                                                <td> RM {{ number_format( $pra->jml_pem, 0 , ',' , ',' )  }}  </td>
                                                
                                                <td> RM {{ number_format($installment, 0 , ',' , ',' )  }} /month</td>
                                              
                                                <td>{{$tenure->rate}} %</td>
                                                <td>  
                                                    <input type="hidden" name="cus_id" value="{{$pra->id_cus}}">
                                                    <input name="tenure" type="radio" id="radio_1_{{$tenure->id}}" value="{{$tenure->id}}"  required />
                                                    <label for="radio_1_{{$tenure->id}}"></label>
                                                </td>
                                            </tr>
                                                            
                                             <?php }  ?>
                                        @endforeach
                                    <?php } ?>
                            
                            </tbody>
                            

                        </table>





                        {!! Form::open(['url' => ['/save/process6/'.$step1->id_cus], 'class' => "probootstrap-form border border-danger", 'method' => 'post', 'id' => 'form-validate-pra']) !!}

                            {{ csrf_field() }}

                                <input name="id_praapplication" id="id_praapplication" type="hidden"  value="{{$pra->id_cus}}" >
                        
                                <div class="row">
                                    <div class="col-md-6"></div>
                                    <div class="col-md-6">
                                        <div class="row" style="margin-top: 30px !important">
                                            <div class="col-md-2">
                                                <b>Remark:</b>
                                            </div>
                                            <div class="col-md-10">
                                                <select name="remarkp6" class="form-control" id="one{{$step1->id}}">
                                                   @if(!empty($data->remark6))
                                                        @if($data->remark6 =='W6')
                                                        <option value="W6"  selected>Reject</option>
                                                        <option value="W9">Pass </option>
                                                        @else
                                                         <option value="W9" selected>Pass </option>
                                                         <option value="W6">Reject</option>
                                                        @endif
                                                    @else
                                                    <option value="" selected disabled hidden>Choose Remark</option>
                                                    <option value="W9">Pass </option>
                                                    <option value="W6">Reject <!-- Calculation & Changes --></option>
                                                    @endif
                                                    
                                                </select>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="resources1" style="display: none" id="two{{$step1->id}}">
                                                <div class="col-md-2"><b>Note :</b></div>
                                                <div class="col-md-10">
                                                    ​<textarea id="txtArea" name="notep6" class="form-control" rows="6" cols="3"></textarea>
                                                </div>
                                            </div>
                                        </div>

                                   
                                        <div class="row">
                                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-md-offset-10">
                                                <button type="submit" class="btn bg-green btn-block btn-lg waves-effect">
                                                    Save 
                                                </button>
                                                {{ csrf_field() }}
                                            </div>
                                        </div>
                                            
                                {{ Form::close() }}
               

                    </div>
                </div>
            </div>



        </div>
    </section>

   

@endsection


@push('js')

        <script type="text/javascript">
            var Privileges = jQuery('#one{{$step1->id}}');
            var select = this.value;
            Privileges.change(function () {
                if ($(this).val() == 'W6' ) {
                    $('.resources1').show();
                }
                else $('.resources1').hide();
            });
        </script>

           
        
    @endpush

