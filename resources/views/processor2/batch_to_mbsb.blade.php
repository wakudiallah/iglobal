 @extends('vadmin.tampilan')


@section('content')
	<section class="content">
        <div class="container-fluid">

            <div class="row clearfix"> <!-- Breadcrumber -->

                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">donut_small</i> Ready to MBSB</a></li>

                    </ol>
                </div>

            </div> <!-- End of breadcrumber -->

            @include('shared.notif')

			<div class="row clearfix">
		        <div class="card">
		            <div class="header bg-red">
		                <h2>Ready to MBSB</h2>
		            </div>
		            <div class="body">
		                <div class="table-responsive">

		                <form method="POST" class="form-horizontal" id="popup-validation" action="{{ url('/submission_list')}}" >
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

		                    <table class="table table-hover dashboard-task-infos" id="example">
		                    
		                        <thead>
		                            <tr>
		                                <th>#</th>
		                                <th>Name</th>
		                                <th>IC</th>
		                                <th>Phone</th>
		                                <th>Status</th> 
		                                <th width="5%">Action</th>
		                                <!-- <th>Batch Header</th> -->
		                            </tr>
		                        </thead>
		                        <tbody>
		                            <?php $i = 1; ?>

		                            @foreach($mbsb as $data)
		                            <tr>
		                                <td>{{$i++}}</td>
		                                <td>{{$data->name}}</td>
		                                <td>{{$data->ic}}</td>
		                                <td>{{$data->notelp}}</td>
		                                <!-- <td>
		                                     <button class="btn btn-success" aria-controls="collapse-{{$data->id}}" data-target="#collapseDetailOne{{$data->id}}" data-toggle="collapse" style="cursor:pointer;">Detail</button>
		                                </td>-->
		                                <td>
		                                     @include('shared.stage_new')
		                                </td> 
		                                <!-- <td>
		                                	<button type="button" class="btn btn-success waves-effect m-r-20" data-toggle="modal" data-target="#largeModal{{$data->id}}">Detail</button>
		                                </td> -->
		                                <td>
		                                	<div class="demo-checkbox">
                                                <input type="checkbox" class="invitation-friends" id="basic_checkbox_{{$i}}" name="id[]" value="{{$data->id}}" />
                                                <label for="basic_checkbox_{{$i}}"></label>
                                                
                                                
                                                <input type="hidden"  name="cus_id[]" value="{{$data->id_cus}}">

                                                <input type="hidden"  name="ids[]" value="">
                                                <input type="hidden"  name="sta[]" value="">
                                            </div>

		                                </td>
		                                <!-- <td>
		                                	@if($data->doc == 1)
                                            <a href="{{url('/pdfbatchheader/'.$data->id_cus)}}" class="btn bg-pink btn-circle waves-effect" target="_blank"> <i class="material-icons">dns</i> </a>
                                            @else
                                            -
                                            @endif
		                                </td> -->
		                            </tr>

		                            @endforeach
		                            
		                        </tbody>
		                    
		                       <button class="btn btn-sm btn-success" type="submit"><i class="material-icons">file_download</i> Create Submission List</button>

		                       </form>
		                    </table>

		                
		                </div>
		            </div>
		        </div>
			</div>

			
			@foreach($mbsb as $datas)
            <!-- Large Size -->
            <div class="modal fade" id="largeModal{{$datas->id}}" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-red">
                            <h4 class="modal-title" id="largeModalLabel">Detail</h4>
                        </div>
                        <div class="modal-body">
                        	<p><b>Note :</b></p>
                            <textarea id="txtArea" name="notep6" class="form-control" rows="6" cols="5" readonly></textarea>
                        </div>
                        <div class="modal-footer">
                            
                        </div>
                    </div>
                </div>
            </div>
            @endforeach


			<!-- ///////////////////  Data Target  //////////////// -->
		    @foreach($mbsb as $datas)
		    <div id="collapseDetailOne{{$datas->id}}" class="collapse" aria-expanded="false" data-collapse-group="collapse-group" aria-labelledby="headingFive" data-parent="#accordionExample">
		        <div class="row clearfix">
		            <div class="card">
		                <div class="header bg-red">
		                    <h2>Detail {{$datas->name}}</h2>
		                </div>
		                <div class="body">
		                   
		                   	 

		                </div>
		            </div>
		        </div>
		    </div>
		    @endforeach
		    <!-- ///////////////////  End Data Target  //////////////// -->

		</div>
	</section>


	<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>


	<script type="text/javascript">
		$(document).ready(function() {
	        $('#example').DataTable();
	        });
	</script>

	<!-- Checkbox detect -->
    <script type="text/javascript">
        $( '#popup-validation' ).on('submit', function(e) {
           if($( 'input[class^="invitation-friends"]:checked' ).length === 0) {
              alert( 'Please! Select the application' );
              e.preventDefault();
           }
        });
    </script>
	
@endsection

    