@extends('vadmin.tampilan')


@section('content')


<!-- CSS Step =-->
<link href="{{ asset('admin/css/tabsteptype.css') }}" rel="stylesheet" />


    <section class="content">
        <div class="container-fluid">    

            <div class="row clearfix">
                <!-- Task Info -->
                    <div class="row clearfix"> <!-- Breadcrumber -->
                        <div class="col-md-6">
                            <ol class="breadcrumb breadcrumb-col-pink">
                                <li><a href="javascript:void(0);"><i class="material-icons">layers</i> Loan Check & Calculation</a></li>
                            </ol>
                        </div>
                    </div> <!-- End of breadcrumber -->

                    @include('shared.notif')
                
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Loan Eligibility Checking and Calculation</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos" id="example">
                                    <thead>
                                        <tr>
                                            <th width="2%">#</th>
                                            <th width="30%">Name</th>
                                            <th width="20%">IC</th>
                                            <th>Location</th>
                                            <th>Status</th>
                                            <th>Detail</th>
                                            <th>Office Telp No</th>
                                            <th>Batch Header</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1; ?>

                                        @foreach($internal_cal as $data)
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{$data->name}}</td>
                                            <td>{{$data->ic}}</td>
                                            
                                            <td>
                                                <a href="{{url('/location/show/'.$data->id)}}" class="btn bg-light-blue btn-circle waves-effect waves-circle waves-float" onclick="window.open('{{url('/location/show/'.$data->id)}}', 'newwindow', 'width=600,height=400'); return false;"> <i class="material-icons">place</i> </a>
                                            </td>
                                            <td>@include('shared.stage_new')</td>
                                            <td>
                                                <button class="btn btn-success" aria-controls="collapse-{{$data->id}}" data-target="#collapseDetailOne{{$data->id}}" data-toggle="collapse" style="cursor:pointer;">Detail</button>
                                            </td>

                                            <?php $telp = DB::table('add_info')->where('cus_id', $data->id_cus)->latest('created_at')->limit('1')->first(); ?>

                                            <td>@if(empty($telp->office_telp))- @else {{$telp->office_telp}} @endif</td>
                                            <td>
                                                @if($data->doc == 1)
                                                <a href="{{url('/pdfbatchheader/'.$data->id_cus)}}" class="btn bg-pink btn-circle waves-effect" target="_blank"> <i class="material-icons">dns</i> </a>
                                                @else
                                                -
                                                @endif
                                            </td>   
                                        </tr>
                                        @endforeach
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                
                <!-- #END# Task Info -->
            </div>

            <!-- ///////////////////  Data Target  ///////////////// -->
            @foreach($internal_cal as $datas)
            <div id="collapseDetailOne{{$datas->id}}" class="collapse" aria-expanded="false" data-collapse-group="collapse-group" aria-labelledby="headingFive" data-parent="#accordionExample">
                <div class="row clearfix">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Detail {{$datas->name}} </h2>
                        </div>
                        <div class="body">
                            
                            <!-- start step -->
                           <section class="design-process-section" id="process-tab">
                                <div class="row">
                                  <div class="col-xs-12"> 
                                    <!-- design process steps--> 
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs process-model more-icon-preocess" role="tablist">
                                      <li role="presentation" class="active"><a href="#discover{{$datas->id}}" aria-controls="discover{{$datas->id}}" role="tab" data-toggle="tab"> <i class="material-icons" aria-hidden="true">credit_card</i>
                                        <p>Loan Calculation</p>
                                        </a></li>
                                      <li role="presentation"><a href="#strategy{{$datas->id}}" aria-controls="strategy{{$datas->id}}" role="tab" data-toggle="tab"><i class="material-icons" aria-hidden="true">note</i>
                                        <p>Document</p>
                                        </a></li>
                                      <li role="presentation"><a href="#optimization{{$datas->id}}" aria-controls="optimization{{$datas->id}}" role="tab" data-toggle="tab"><i class="material-icons" aria-hidden="true">call</i>
                                        <p>Call 103</p>
                                        </a></li>
                                     
                                    </ul>
                                    <!-- end design process steps--> 
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                      <div role="tabpanel" class="tab-pane active" id="discover{{$datas->id}}">
                                        <div class="design-process-content">
                                            
                                            {!! Form::open(array('url'=>'save/process6/'.$datas->id_cus, 'method'=>'post', 'files'=>'true')) !!}

                                            {{ csrf_field() }}
                                            
                                            <input type="hidden" name="process5" value="{{$datas->process5}}">
                                            <div class="row">
                                                
                                                <div class="col-md-6"> <!-- kolom 1 -->
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <b>Name  </b>    
                                                        </div>
                                                        <div class="col-md-6">
                                                           <input  class="form-control" type="text" name="" value="{{$datas->name}} " readonly> 
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <b>No Telf : </b>    
                                                        </div>
                                                        <div class="col-md-6">
                                                            <input  class="form-control" type="text" name="" value="{{$datas->notelp}} " readonly>
                                                           
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <b>Employer : </b>    
                                                        </div>
                                                        <div class="col-md-6">
                                                            <input  class="form-control" type="text" name="" value="{{$datas->majikan->Emp_Desc}} " readonly>
                                                           
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <b>By : </b>    
                                                        </div>
                                                        <div class="col-md-6">
                                                           <p style="color: red">{{$datas->user->name}} </p>  
                                                        </div>
                                                    </div>
                                                    
                                                </div> <!-- End of kolom 1 -->
                                                <!-- kolom 2 -->
                                                <div class="col-md-6">
                                                    
                                                    <?php $lm = DB::table('loanammount')->where('id_praapplication', $datas->id_cus)->latest('created_at')->limit('1')->first(); ?>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <b>Basic Salary (RM) : </b>    
                                                        </div>
                                                        <div class="col-md-6">
                                                           <input  class="form-control" type="text" name="" value="{{$datas->gaji_asas}} " readonly> 
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <b>Allowance (RM) : </b>    
                                                        </div>
                                                        <div class="col-md-6">
                                                            <input  class="form-control" type="text" name="" value="{{$datas->elaun}}" readonly>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <b>Total Deduction (RM) : </b>    
                                                        </div>
                                                        <div class="col-md-6">
                                                            <input  class="form-control" type="text" name="" value="{{$datas->pot_bul}}" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <b>Loan Amount (RM) : </b>    
                                                        </div>
                                                        <div class="col-md-6">
                                                           <input  class="form-control" type="text" name="" value="{{$lm->loanammount}}" readonly>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <b>Remark:</b>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <select name="remarkp6" class="form-control" id="one{{$datas->id}}">
                                                               @if(!empty($data->remark6))
                                                                    @if($data->remark6=='W6')
                                                                    <option value="W6"  selected>Reject</option>
                                                                    <option value="W9">Pass </option>
                                                                    @else
                                                                     <option value="W9" selected>Pass </option>
                                                                     <option value="W6">Reject</option>
                                                                    @endif
                                                                @else
                                                                <option value="" selected disabled hidden>Choose Remark</option>
                                                                <option value="W9">Pass </option>
                                                                <option value="W6">Reject <!-- Calculation & Changes --></option>
                                                                @endif
                                                                
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="resources1" style="display: none" id="two{{$datas->id}}">
                                                        <div class="col-md-6"><b>Note :</b></div>
                                                        <div class="col-md-6">
                                                            ​<textarea id="txtArea" name="notep6" class="form-control" rows="3" cols="3"></textarea>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-md-offset-10">
                                                            <button type="submit" class="btn bg-green btn-block btn-lg waves-effect">
                                                                Save
                                                            </button>
                                                            {{ csrf_field() }}
                                                        </div>
                                                    </div>
                                                
                                                {{ Form::close() }}

                                                </div> <!-- End of kolom 2 -->
                                            </div>
                                         </div>
                                      </div><!-- end loan calculation -->



                                      <div role="tabpanel" class="tab-pane" id="strategy{{$datas->id}}">
                                        <div class="design-process-content">
                                          <div class="row">
                                            {!! Form::open(array('url'=>'save/process7/'.$datas->id_cus, 'method'=>'post', 'files'=>'true')) !!}

                                            {{ csrf_field() }}

                                            <input type="hidden" name="process5_7" value="{{$datas->process5}}">

                                                <table class="table table-responsive">
                                                    <thead>
                                                        <tr>
                                                            <th width="10%">No</th>
                                                            <th width="30%">Document</th>
                                                            <th width="10%">Checklist</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        <?php $y = 1; ?>

                                                        <?php
                                                               $datax1 = DB::table('doc_cust')->join('praapplication', 'doc_cust.cus_id', '=', 'praapplication.id_cus')->where('cus_id', $datas->id_cus)->where('doc_cust.type','1')->latest('doc_cust.created_at')->select('doc_cust.id as iddoc', 'doc_cust.doc_pdf as doc_pdf')->limit('1')->first();

                                                               $datax2 = DB::table('doc_cust')->join('praapplication', 'doc_cust.cus_id', '=', 'praapplication.id_cus')->where('cus_id', $datas->id_cus)->where('doc_cust.type','2')->latest('doc_cust.created_at')->select('doc_cust.id as iddoc', 'doc_cust.doc_pdf as doc_pdf')->limit('1')->first();

                                                               $datax3 = DB::table('doc_cust')->join('praapplication', 'doc_cust.cus_id', '=', 'praapplication.id_cus')->where('cus_id', $datas->id_cus)->where('doc_cust.type','4')->latest('doc_cust.created_at')->select('doc_cust.id as iddoc', 'doc_cust.doc_pdf as doc_pdf')->limit('1')->first();
                                                        ?>

                                                        <tr>  
                                                            <td>{{$y++}}</td>
                                                            <td>
                                                                <a href="{{asset('/documents/user_doc/'.$datas->ic.'/'.$datax1->doc_pdf)}}" target='_blank'>{{$datax1->doc_pdf}}</a>
                                                            </td>
                                                            <td>
                                                                <input type="text" name="iddatadoc1" value="{{$datax1->iddoc}}">
                                                                
                                                                <!-- <input type="hidden" id="basic_checkbox_{{$datas->id}}{{$datax1->doc_pdf}}" name="checkbox1" value="0"  />
                                                                <label for="basic_checkbox_{{$datas->id}}{{$datax1->doc_pdf}}"></label> -->

                                                                <input type="checkbox" id="basic_checkbox_{{$datas->id}}{{$datax1->doc_pdf}}" name="checkbox1"  />
                                                                
                                                                <label for="basic_checkbox_{{$datas->id}}{{$datax1->doc_pdf}}"></label>
                                                            </td>
                                                        </tr>
                                                        <tr>  
                                                            <td>{{$y++}}</td>
                                                            <td>
                                                                <a href="{{asset('/documents/user_doc/'.$datas->ic.'/'.$datax2->doc_pdf)}}" target='_blank'>{{$datax2->doc_pdf}}</a>
                                                            </td>
                                                            <td>
                                                                <input type="text" name="iddatadoc2" value="{{$datax2->iddoc}}">


                                                                <!-- <input type="hidden" id="basic_checkbox_{{$datas->id}}{{$datax2->doc_pdf}}" name="checkbox2" value="0"  />
                                                                <label for="basic_checkbox_{{$datas->id}}{{$datax2->doc_pdf}}"></label> -->

                                                                <input type="checkbox" id="basic_checkbox_{{$datas->id}}{{$datax2->doc_pdf}}" name="checkbox2"  value="1" />
                                                                <label for="basic_checkbox_{{$datas->id}}{{$datax2->doc_pdf}}"></label>
                                                            </td>
                                                        </tr>
                                                        <tr>  
                                                            <td>{{$y++}}</td>
                                                            @if(empty($datax3->doc_pdf))
                                                                <td></td>
                                                            @else
                                                                <td>
                                                                    <a href="{{asset('/documents/user_doc/'.$datas->ic.'/'.$datax3->doc_pdf)}}" target='_blank'>{{$datas->ic}}-Spekar.pdf</a>
                                                                </td>
                                                                <td>
                                                                    <input type="text" name="iddatadoc3" value="{{$datax3->iddoc}}">

                                                                   <!-- <input type="hidden" id="basic_checkbox_{{$datas->id}}{{$datax3->doc_pdf}}" name="checkbox3" value="0" />
                                                                    <label for="basic_checkbox_{{$datas->id}}{{$datax3->doc_pdf}}"></label> -->

                                                                    <input type="checkbox" id="basic_checkbox_{{$datas->id}}{{$datax3->doc_pdf}}" name="checkbox3" value="1" />
                                                                    <label for="basic_checkbox_{{$datas->id}}{{$datax3->doc_pdf}}"></label>
                                                                </td>
                                                            @endif
                                                        </tr>
                                                       

                                                    </tbody>

                                                </table>  

                                                <div class="row">
                                                    <div class="col-md-6 col-md-offset-6">
                                                        <input  class="hidden" type="text" name="name" value="{{$datas->name}} " >

                                                        <div class="col-md-3">
                                                            <b>Remark:</b>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <select name="stagep7" class="form-control" id="three{{$datas->id}}">
                                                                <option value="" selected disabled hidden>Choose Remark</option>
                                                                <option value="W10">Pass</option>
                                                                <option value="W7">Reject <!-- Document Check --></option>
                                                            </select>
                                                        </div>

                                                        <div class="resources2" style="display: none" id="two{{$datas->id}}">
                                                            <div class="col-md-3"><b>Note :</b></div>
                                                            <div class="col-md-9">
                                                                ​<textarea id="txtArea" name="notep7" class="form-control" rows="3" cols="3"></textarea>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    
                                                </div> 
                                                <div class="row">
                                                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-md-offset-10">
                                                        <button type="submit" class="btn bg-green btn-block btn-lg waves-effect">
                                                            Save
                                                        </button>
                                                        {{ csrf_field() }}
                                                    </div>
                                                </div>

                                            {{ Form::close() }}

                                            </div>
                                          </div>
                                      </div><!-- end document -->

                                      <div role="tabpanel" class="tab-pane" id="optimization{{$datas->id}}">
                                        <div class="design-process-content">
                                            {!! Form::open(array('url'=>'save/process8/'.$datas->id_cus, 'method'=>'post', 'files'=>'true')) !!}

                                            {{ csrf_field() }}

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="col-md-6">
                                                        <b>Customer Office Telp : </b>    
                                                    </div>
                                                    <div class="col-md-6">
                                                       <input  class="form-control" type="text" name="telpoffice" value="" >
                                                    </div>

                                                </div>

                                                <!-- <div class="col-md-6">
                                                    <div class="col-md-6">
                                                        <b>Remark:</b>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <select name="pro2remark" class="form-control" id="one{{$datas->id}}">
                                                            <option value="" selected disabled hidden>Choose Remark</option>
                                                            <option value="W8">MBSB Processing</option>
                                                            <option value="W8">Pass</option>
                                                            <option value="W7">Document Incomplete</option>
                                                        </select>
                                                    </div>

                                                    <div class="resources" style="display: none" id="two{{$datas->id}}">
                                                        <div class="col-md-6"><b>Note :</b></div>
                                                        <div class="col-md-6">
                                                            ​<textarea id="txtArea" name="note" class="form-control" rows="3" cols="3"></textarea>
                                                        </div>
                                                    </div>
                                                </div> -->
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-md-offset-10">
                                                    <button type="submit" class="btn bg-green btn-block btn-lg waves-effect">
                                                        Save
                                                    </button>
                                                    {{ csrf_field() }}
                                                </div>
                                            </div>
                                            
                                            {{ Form::close() }}

                                        </div>
                                      </div> <!-- end call 103 -->
                                    </div>
                                  </div>
                                </div>
                            </section><!-- end of step -->

                        </div>

                    </div>
                </div>
            </div>

            @endforeach
            <!-- ///////////////////  End Data Target  //////////////// -->
        </div>
    </section>

@endsection


    @push('js')

        @foreach($internal_cal as $datas)
        <script type="text/javascript">
            var Privileges = jQuery('#one{{$datas->id}}');
            var select = this.value;
            Privileges.change(function () {
                if ($(this).val() == 'W6' ) {
                    $('.resources1').show();
                }
                else $('.resources1').hide();
            });

            var Privileges = jQuery('#three{{$datas->id}}');
            var select = this.value;
            Privileges.change(function () {
                if ($(this).val() == 'W7' ) {
                    $('.resources2').show();
                }
                else $('.resources2').hide();
            });

        


            //form step 

        // script for tab steps
    $('a[data-toggle="tab{{$datas->id}}"]').on('shown.bs.tab', function (e) {

        var href = $(e.target).attr('href');
        var $curr = $(".process-model{{$datas->id}}  a[href='" + href + "']").parent();

        $('.process-model{{$datas->id}} li').removeClass();

        $curr.addClass("active");
        $curr.prevAll().addClass("visited");
    });
// end  script for tab steps

            //end form step 



        </script>
        @endforeach
<script type="text/javascript">
    
$(document).ready(function() {

var found = [];
    $("select[name='remarkp6'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

});

</script>
    @endpush