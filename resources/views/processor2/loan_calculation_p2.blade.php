@extends('vadmin.tampilan')

@section('content')
	<section class="content">
        <div class="container-fluid">
            
            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">person_pin</i> Customer</a></li>
                        <li class="active"><i class="material-icons">create</i> Loan Eligibility</li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->
            
            

            <!-- Advanced Form Example With Validation -->
            <div class="row clearfix">
            	<div class="col-md-2"></div>
            	<div class="col-md-8">
	                <div class="card">
	                    <div class="header bg-red">
	                        <h2>Loan Eligibility</h2>
	                    </div>
	                    <div class="body">

	                		<div class="row clearfix">
				                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
				                    <button class="btn btn-success btn-lg btn-block waves-effect" type="button" style="line-height: 40px !important"><b>Name</b>: {{$pra->name}} </button>
				                </div>
				                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
				                    <button class="btn btn-primary btn-lg btn-block waves-effect" type="button" style="line-height: 40px !important"><b>IC Number</b>: {{$pra->ic}} </button>
				                </div>
				                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
				                    <button class="btn btn-danger btn-lg btn-block waves-effect" type="button" style="line-height: 40px !important"><b>Phone Number</b>: {{$pra->notelp}} </button>
				                </div>
				                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
				                    <button class="btn btn-warning btn-lg btn-block waves-effect" type="button" style="line-height: 40px !important"><b>Email</b>: {{$pra->email}} </button>
				                </div>
				            </div>

	                    	{!! Form::open(['url' => ['/p2/custtenus/result'], 'class' => "probootstrap-form border border-danger", 'method' => 'post', 'id' => 'form-validate-pra']) !!}

	                        {{ csrf_field() }}

	                        <input type="hidden" name="id_cus" value="{{$pra->id_cus}}">

	                    	<label for="">Job Type:</label>
	                        <div class="form-group">
	                            <div class="form-line">
	                                <select class="form-control" id="Employment" name="Employment">
	                                  @foreach ($employment as $data)
	                                  <option value="{{$data->id}}">{{$data->name}}</option>
	                                  @endforeach                          
	                                </select>
	                            </div>
	                        </div>

	                        <div class="form-group form-float">
                                <label class="form-label">Package:</label>
                                <div class="form-line">
                                    
                                    <!-- <select class="js-example-basic-single js-states form-control" id="loanpkg_code" name="loanpkg_code">
                                      @foreach ($loanpkg as $data)
                                      <option value="{{$data->LnPkg_Code}}">{{$data->Ln_Desc}}</option>
                                      @endforeach
                                    </select> -->
                                    
                                    <input type="text" name="Package" id="Package" @if (Session::has('package_name'))  value="{{ Session::get('package_name') }}" @else value="Mumtaz-i" @endif class="form-control" disabled>

                                </div>    
                            </div>
                           

	                    	<label for="">Basic Salary (RM):</label>
	                        <div class="form-group">
	                            <div class="form-line">
	                                <input type="text" id="gaji_asas" name="gaji_asas" class="form-control"  min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');"  value="{{$pra->gaji_asas}}"required>
	                            </div>
	                        </div>
	                        <label for="">Allowance (RM):</label>
	                        <div class="form-group">
	                            <div class="form-line">
	                                <input type="text" class="form-control"  id="elaun" name="elaun" min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');"  value="{{$pra->elaun}}" required>
	                            </div>
	                        </div>
	                        <label for="">Total Deduction (RM):</label>
	                        <div class="form-group">
	                            <div class="form-line">
	                                <input type="text" class="form-control"  id="pot_bul" name="pot_bul" min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');"  value="{{$pra->pot_bul}}" required>
	                            </div>
	                        </div>
	                        <label for="">Loan Amount (RM):</label>
	                        <div class="form-group">
	                            <div class="form-line">
	                                <input type="text" class="form-control" id="jml_pem" name="jml_pem" min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');"  value="{{$pra->jml_pem}}" required>
	                            </div>
	                        </div>

	                         <div class="row">
	                            <div class="col-md-10"></div>
	                            <div class="col-md-2">
	                                <input type="submit" value="Submit" class="btn btn-lg btn-success btn-block" style="cursor:pointer;">
	                                    {{ csrf_field() }}
	                            </div>
	                        </div>
	                    
	                     {!! Form::close() !!}
	                    </div>
	                </div>
	            </div>
	            <div class="col-md-2"></div>
            </div>

        </div>
    </section>



@endsection

@push('js')


<script type="text/javascript">


	$( "#Employment" ).change(function() {
	    var Employment = $('#Employment').val();

	    if( Employment == '1') {

	      $("#Employer").html(" ");
	      $("#Employer2").val(" ");
	        $("#majikan2").show();
	        $("#majikantext").focus();
	         $("#majikan").hide();
	       $("#Package").val("Mumtaz-i");
	    }
	      else if( Employment == '2') {
	   
	     $("#Employer").html(" ");
	     $("#Employer2").val(" ");
	        $("#majikan2").show();
	        $("#majikantext").focus();
	         $("#majikan").hide();
	         $("#Package").val("Afdhal-i");
	    }

	    else if( Employment == '3') {
	   
	     $("#Employer").html(" ");
	     $("#Employer2").val(" ");
	        $("#majikan2").show();
	        $("#majikantext").focus();
	         $("#majikan").hide();
	         $("#Package").val("Afdhal-i");
	    }
	      else if( Employment == '4') {
	   
	     $("#Employer").html(" ");
	     $("#Employer2").val(" ");
	        $("#majikan2").show();
	        $("#majikantext").focus();
	         $("#majikan").hide();
	         $("#Package").val("Private Sector PF-i");
	    }
	    else if( Employment == '5') {
	         
	         $("#Employer").html(" ");
	     $("#Employer2").val(" ");
	        $("#majikan2").show();
	        $("#majikantext").focus();
	         $("#majikan").hide();

	          $("#Package").val("Afdhal-i");
	            
	        //  $("#majikan").simulate('click');

	    }
	  $.ajax({
	                url: "<?php  print url('/'); ?>/employer/"+Employment,
	                dataType: 'json',
	                data: {
	                   
	                },
	                success: function (data, status) {

	                    jQuery.each(data, function (k) {

	                        $("#Employer").append("<option value='" + data[k].id + "'>" + data[k].name + "</option>");
	                    });
	                }
	            });
	});
</script>
@endpush