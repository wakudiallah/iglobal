@extends('vadmin.tampilan')       


@section('content')

    <!-- Bootstrap Select Css -->
    <link href="{{ asset('admin/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
    
    <section class="content">
        <div class="container-fluid">

            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">border_color</i> Loan Calculation</a></li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

            <!-- Notification -->
            <div class="row clearfix">
                @if ($message = Session::get('success')) 
                <div class="alert bg-green alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @elseif($message = Session::get('delete'))
                <div class="alert bg-pink alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @elseif($message = Session::get('update'))
                <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @endif
            </div>
            <!-- End of Notif -->
    

            <div class="row clearfix"> <!-- Task Info -->
                <div class="card">
                    <div class="header bg-red">
                        <h2>Loan Calculation</h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-hover dashboard-task-infos" id="example">
                                <thead>
                                    <tr>
                                        <th width="5%">#</th>
                                        <th width="30%">Name</th>
                                        <th width="20%">IC</th>
                                        <th>Spekar</th>
                                        <th>Status</th> 
                                        <th>Process</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>

                                    @foreach($pending as $data)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$data->name}}</td>
                                        <td>{{$data->ic}}</td>
                                        <td>
                                            @if((empty($data->spekar))) 
                                                <a href="#" class="btn bg-red"><i class="material-icons disabled">do_not_disturb_alt</i> </a>
                                            @else

                                                {!! Form::open(array('url'=>'download/spekar/'.$data->id_cus, 'method'=>'post', 'files'=>'true', 'target'=>'_blank')) !!}

                                                {{ csrf_field() }}

                                                <button type="submit" name="submit" class="btn bg-light-blue" target='_blank'><i class="material-icons">library_books</i></button>
                                                <!-- <a href="{{asset('/documents/user_doc/'.$data->ic.'/'.$data->spekar)}}" class="btn bg-light-blue" target='_blank'> <i class="material-icons">library_books</i></a>-->

                                                 {{ Form::close() }}  

                                            @endif
                                        </td>
                                        <td>
                                            @include('shared.stage_new')
                                        </td>
                                        <td>
                                             <a href="{{ url('processor2/'.$data->id_cus.'/step1') }}" class="btn btn-sm bg-pink"><i class="material-icons">border_color</i> Loan Calculation </a>
                                        </td>
                                           
                                        
                                    </tr>

                                    @endforeach
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            
            <!-- #END# Task Info -->
            </div>

            <!-- ////////////////  Modal Route ///////////////-->
            @foreach($pending as $dataz)
                <div class="modal fade" id="defaultModal{{$dataz->id}}" tabindex="-1" role="dialog">
                        <div class="modal-dialog " role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="defaultModalLabel" style="color: red">{{$dataz->name}} -- ({{$dataz->ic}}) </h4>
                                </div>
                                <div class="modal-body">
                                   {!! Form::open(array('url'=>'passreject/mo/'.$dataz->id_cus, 'method'=>'post', 'files'=>'true')) !!}

                                    {{ csrf_field() }} 

                                    <input  class="hidden" type="text" name="mng" value="{{Auth::user()->manager }} " >                                    
                                    <div class="row ">
                                        <h5><b>Remark :</b></h5>
                                        <select name="moremark" class="chosen-select form-control" id="one{{$dataz->id}}" required>
                                            <option value="" selected disabled hidden>Choose Remark</option>
                                            <option value="W4">Pass</option>
                                            <option value="W5">Reject</option>
                                        </select>
                                    </div>


                                    <div class="row resources" style="display: none; margin-top: 20px !important" id="two">
                                        <h5><b>Note :</b></h5>
                                        <textarea id="txtArea" name="notep6" class="form-control" rows="5" cols="3"></textarea>
                                    </div>
                                    

                                </div>
                                <div class="modal-footer">
                                    
                                    <input type="submit" value="Submit" class="btn btn btn-success">
                                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                </div>

                                {{ Form::close() }}   
                            </div>
                        </div>
                </div> 
            @endforeach
            <!-- ////////////////  Modal Route ///////////////-->


            <!-- ///////////////////  Data Target  //////////////// -->
            @foreach($pending as $datas)
            <div id="collapseDetailOne{{$datas->id}}" class="collapse" aria-expanded="false" data-collapse-group="collapse-group" aria-labelledby="headingFive" data-parent="#accordionExample">
                <div class="row clearfix">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Detail {{$datas->name}}</h2>
                        </div>
                        <div class="body">
                            {!! Form::open(array('url'=>'save/meetcus/'.$datas->id_cus, 'method'=>'post', 'files'=>'true')) !!}

                            {{ csrf_field() }}
                            <span id="latitude"></span>
                            <span id="longitude"></span>
                            <span id="location"></span>

                            <div class="row">
                                <!-- kolom 1 -->
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>Name : </b>    
                                        </div>
                                        <div class="col-md-6">
                                           <p>{{$datas->name}} </p>  
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>Phone Number : </b>    
                                        </div>
                                        <div class="col-md-6">
                                           <p>{{$datas->notelp}} </p>  
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>By : </b>    
                                        </div>
                                        <div class="col-md-6">
                                           <p style="color: red">{{$datas->user->name}} </p>  
                                        </div>
                                    </div>

                                    <!-- 
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="probootstrap-date-arrival">Remark</label>
                                        </div>
                                        <div class="col-md-6">
                                           
                                            
                                                <input  class="hidden" type="text" name="name" value="{{$datas->name}} " >
                                                <input  class="hidden" type="text" name="ic" value="{{$datas->ic}} " >
                                                <input  class="hidden" type="text" name="id_cus" value="{{$datas->id_cus}} " >

                                                
                                                <select name="moremark" class="form-control" id="one{{$datas->id}}">
                                                    <option value="" selected disabled hidden>Choose Remark</option>
                                                    <option value="W3">Pass</option>
                                                    <option value="W4">Reject</option>
                                                </select>
                                        </div>                                         
                                    </div>
                                -->
                                    

                                   
                                </div> <!-- End of kolom 1 -->
                                <!-- kolom 2 -->
                                <div class="col-md-6">
                                     
                                    

                                   

                                    <div class="row">
                                        <div class="form-group">
                                        <div class="col-xs-6 col-sm-3 col-md-4 col-lg-4 col-md-offset-8">
                                            <button type="submit" class="btn bg-green btn-block btn-lg waves-effect">
                                                Save
                                            </button>
                                            {{ csrf_field() }}
                                        </div>
                                        </div>

                                        
                                    </div>
                                </div> <!-- End of kolom 2 -->
                            </div>
                            {{ Form::close() }}   
                            
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            <!-- ///////////////////  End Data Target  //////////////// -->
        </div>
    </section>

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>


    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable();
            });
    </script>
@endsection


