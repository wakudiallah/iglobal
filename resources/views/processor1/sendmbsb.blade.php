@extends('vadmin.tampilan')

@section('content')
<style type="text/css">
    input[type="checkbox"] {
    -webkit-appearance: checkbox;
    border-radius: 0;
}
input[type="radio"] {
    -webkit-appearance: radio;
    border-radius: 0;
}
</style>
    <section class="content">
        <div class="container-fluid">

            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">assignment</i> Generate & Send to MBSB</a></li>
                        
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

            <!-- Notification -->
            <div class="row clearfix">
                @if ($message = Session::get('success')) 
                <div class="alert bg-green alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @elseif($message = Session::get('delete'))
                <div class="alert bg-pink alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @elseif($message = Session::get('update'))
                <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @endif
            </div>
            <!-- End of Notif -->

            <div class="row clearfix">
                
                <div class="card">
                    <div class="header bg-red">
                        <h2>Generate & Send to MBSB</h2>
                    </div>
                    
                   <div class="body">

                               <!--  <a href="{{ url('send/excel') }}" class="btn btn-success btn-sm" data-target="#collapseDetailOne" data-toggle="collapse" target="_blank" style="margin-bottom: 40px;"> <i class="material-icons">file_download</i> Sent to MBSB</a> -->
                        
                         <form method="POST" class="form-horizontal" id="popup-validation" action="{{ url('/send_aset')}}" >
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <!-- <input  value="Generate" class="btn btn-success" > -->
                                <table class="table table-responsive" id="example">
                                    <thead>
                                        <tr>
                                            
                                            <th>#</th>
                                            <th>Date Submited</th>
                                            <th>Customer Name</th>
                                            <th>New IC</th>
                                            <!-- <th>Old IC (if Any)</th> -->
                                            <th>Company</th>
                                            <th>Doc</th>
                                            <th>Status</th>
                                            <th>Route Back</th> 
                                            <th width="3%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1; ?>

                                        @foreach($assessment as $data)

                                            <?php
                                               $datax1 = DB::table('doc_cust')->join('praapplication', 'doc_cust.cus_id', '=', 'praapplication.id_cus')->where('cus_id', $data->id_cus)->where('doc_cust.type','1')->latest('doc_cust.created_at')->limit('1')->first();

                                               $datax2 = DB::table('doc_cust')->join('praapplication', 'doc_cust.cus_id', '=', 'praapplication.id_cus')->where('cus_id', $data->id_cus)->where('doc_cust.type','2')->latest('doc_cust.created_at')->limit('1')->first();
                                            ?>
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{ $data->created_at->toFormattedDateString() }}</td>
                                            <td>{{ $data->name }}</td>
                                            <td>{{ $data->ic }}</td>
                                            <td>{{ $data->majikan->Emp_Desc }}</td>
                                            <td>
                                                @if(empty($datax1->doc_pdf))
                                                    <span class="btn bg-red"><i class="material-icons disabled">do_not_disturb_alt</i> IC</span>
                                                @else
                                                    <a href="{{asset('/documents/user_doc/'.$data->ic.'/'.$datax1->doc_pdf)}}" target='_blank' class="btn bg-blue"><i class="material-icons">library_books</i> IC</a>
                                                @endif

                                                @if(empty($datax2->doc_pdf))
                                                    <span class="btn bg-red"><i class="material-icons disabled">do_not_disturb_alt</i>Consent Letter</span>
                                                @else
                                                    <a href="{{asset('/documents/user_doc/'.$data->ic.'/'.$datax2->doc_pdf)}}" target='_blank' class="btn bg-blue"><i class="material-icons">library_books</i> Consent Letter</a>
                                                @endif
                                            </td>
                                            <td>
                                                @include('shared.stage')
                                            </td>
                                            <td>
                                                @if(empty($datax1->doc_pdf) || empty($datax2->doc_pdf))
                                                    <button class="btn btn-sm bg-red" type="button" data-toggle="modal" data-target="#defaultModal{{$data->id}}" style="margin-left: 10px"><i class="material-icons">close</i> Reject</button>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>
                                                @can('add_send_mbsb_ics')

                                                @if(empty($datax1->doc_pdf) || empty($datax2->doc_pdf))

                                                @else
                                                <div class="demo-checkbox">
                                                    <input type="checkbox" id="basic_checkbox_{{$i}}" name="id[]" value="{{$data->id_cus}}" class="invitation-friends Bike" />

                                                    <label for="basic_checkbox_{{$i}}"></label>
                                                     <input type="hidden" id="id_cus" name="id_cus[]" value="{{$data->id_cus}}" />
                                                     <input type="hidden" id="ic" name="ic[]" value="{{$data->id_cus}}" />
                                                    <input type="hidden"  name="ci[]" value="{{$data->id_cus}}">
                                                    <input type="hidden"  name="ktp[]" value="{{$data->ic}}">

                                                     <input type="hidden"  name="ids[]" value="">
                                                     <input type="hidden"  name="sta[]" value="">
                                                </div>
                                                @endif
                                                
                                                @endcan
                                            </td>

                                        </tr>
                                    @endforeach
                                    
                                    @can('add_send_mbsb_ics')
                                    <div class="row">
                                        <div class="col-md-2 col-md-offset-10">
                                            <button class="btn btn-sm btn-success" type="submit" ><i class="material-icons">file_download</i> Generate & Send to MBSB</button>
                                        </div>
                                    </div>
                                    
                                    @endcan 

                                </form>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>

            <div id="collapseDetailOne" class="collapse"  data-parent="#accordionExample">
                <div class="row clearfix">
                    <div class="card" style="height: 650px;">
                        <div class="header bg-red">
                            Email to MBSB
                        </div>
                        <div class="body">

                            {!! Form::open(['url' => ['send/mbsbdata'], 'method'=>"POST", 'enctype' => "multipart/form-data" ]) !!}
                                {{ csrf_field() }}

                                    @include('adminpage.reporting.assessment.form')
                                     
                                    <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2 col-md-offset-10">
                                        <button type="submit" class="btn bg-green btn-block btn-lg waves-effect">
                                            Send
                                        </button>
                                        {{ csrf_field() }}
                                    </div>
                                    
                            {!! Form::close() !!}
        
                        </div>
                    </div>
                </div>
            </div>



            <!-- Modal  -->
    @foreach($assessment as $dataz)
        <div class="modal fade" id="defaultModal{{$dataz->id}}" tabindex="-1" role="dialog">
                <div class="modal-dialog " role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel" style="color: red">{{$dataz->name}} -- ({{$dataz->ic}})</h4>
                        </div>
                        <div class="modal-body">
                           {!! Form::open(array('url'=>'iclocreject/'.$dataz->id_cus, 'method'=>'post', 'files'=>'true')) !!}

                            {{ csrf_field() }} 

                            <input type="hidden" name="process2" value="{{$dataz->process2}}">

                            <div class="row">
                                

                                <h5><b>Note :</b></h5>
                                <textarea id="txtArea" name="note" class="form-control" rows="5" cols="3"></textarea>
                            </div>

                            

                        </div>
                        <div class="modal-footer">
                            
                            <input type="submit" value="Submit" class="btn btn-lg btn-success">
                            <button type="button" class="btn btn-danger btn-lg waves-effect" data-dismiss="modal">Close</button>
                        </div>

                        {{ Form::close() }}   
                    </div>
                </div>
            </div> 
        @endforeach
            <!-- End Modal  -->

            <!-- 
            <div class="row clearfix">
                
                <div class="card">
                    <div class="header bg-red">
                        <h2>Assessment List</h2>
                    </div>
                <div class="body">
                    <table class="table" id="example2">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>File</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                                @foreach($mbsb as $data)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>
                                    <a href="{{url('/mbsb/doc/'.$data->file)}}" target='_blank' class="btn btn-sm bg-blue"><i class="material-icons">library_books</i>{{$data->file}} </a>
                                </td>
                                <td>{{$data->created_at->toDayDateTimeString()}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        -->
            
        </div>
    </section>


   

    <!-- Slimscroll Plugin Js -->
    <!-- <script src="{{ asset('admin/js/pages/forms/basic-form-elements.js') }}"></script> -->

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>


    <!-- 
    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        title: 'LIST OF ASSESSMENT',
                        title : 'MASTER AGENT: GLOBAL I EXCEED MANAGEMENT SDN BHD'
                    },
                    {
                        extend: 'pdfHtml5',
                        title: 'LIST OF ASSESSMENT',
                        title : 'MASTER AGENT: GLOBAL I EXCEED MANAGEMENT SDN BHD'
                    }
                ]
            } );
        } );
    </script> -->

    <script>
        $(document).ready(function() {
        $('#example').DataTable();
        });

        $(document).ready(function() {
        $('#example2').DataTable();
        });

        $(document).on('change','.Bike:gt(9)',function(){
            if(this.checked)
                  alert('Cannot more than 10 checkbox');
                  this.checked = false;
        }); 
    </script>

    <!-- Checkbox detect -->
    <script type="text/javascript">
        $( '#popup-validation' ).on('submit', function(e) {
           if($( 'input[class^="invitation-friends"]:checked' ).length === 0) {
              alert( 'Please! Select the application' );
              e.preventDefault();
           }
        });
    </script>  


<@endsection